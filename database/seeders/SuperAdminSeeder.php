<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');


        Permission::create(['name' => 'permission_view']);
        Permission::create(['name' => 'permission_create']);
        Permission::create(['name' => 'permission_delete']);
        Permission::create(['name' => 'permission_edit']);
        Permission::create(['name' => 'role_view']);
        Permission::create(['name' => 'role_create']);
        Permission::create(['name' => 'role_delete']);
        Permission::create(['name' => 'role_edit']);
        Permission::create(['name' => 'user_create']);
        Permission::create(['name' => 'user_delete']);
        Permission::create(['name' => 'user_edit']);
        Permission::create(['name' => 'user_view']);

        Permission::create(['name' => 'thd_summary_edit']);
        Permission::create(['name' => 'thd_summary_delete']);
        Permission::create(['name' => 'thd_summary_create']);
        Permission::create(['name' => 'thd_summary_view']);
        Permission::create(['name' => 'summary_admin_view_all_company']);
        Permission::create(['name' => 'summary_view']);
        Permission::create(['name' => 'summary_edit']);
        Permission::create(['name' => 'summary_delete']);
        Permission::create(['name' => 'summary_create']);
        Permission::create(['name' => 'budget_details_create']);
        Permission::create(['name' => 'budget_details_delete']);
        Permission::create(['name' => 'budget_details_edit']);
        Permission::create(['name' => 'budget_details_view']);
        Permission::create(['name' => 'static_budget_edit']);
        Permission::create(['name' => 'static_budget_view']);
        Permission::create(['name' => 'static_budget_delete']);
        Permission::create(['name' => 'static_budget_create']);


        Permission::create(['name' => 'corporate_services_company_budget_details']);
        Permission::create(['name' => 'kabankalan_company_budget_details']);
        Permission::create(['name' => 'jaro_company_budget_details']);
        Permission::create(['name' => 'sumag_company_budget_details']);
        Permission::create(['name' => 'diversion_company_budget_details']);
        Permission::create(['name' => 'roxas_company_budget_details']);
        Permission::create(['name' => 'sibulan_company_budget_details']);
        Permission::create(['name' => 'hilado_company_budget_details']);
        Permission::create(['name' => 'kabankalan_company_thd_summary']);
        Permission::create(['name' => 'jaro_company_thd_summary']);
        Permission::create(['name' => 'diversion_company_thd_summary']);
        Permission::create(['name' => 'sumag_company_thd_summary']);
        Permission::create(['name' => 'roxas_company_thd_summary']);
        Permission::create(['name' => 'sibulan_company_thd_summary']);
        Permission::create(['name' => 'hilado_company_thd_summary']);
        Permission::create(['name' => 'corporate_services_company_thd_summary']);
        Permission::create(['name' => 'corporate_services_company_dashboard']);
        Permission::create(['name' => 'kabankalan_company_dashboard']);
        Permission::create(['name' => 'jaro_company_dashboard']);
        Permission::create(['name' => 'diversion_company_dashboard']);
        Permission::create(['name' => 'sumag_company_dashboard']);
        Permission::create(['name' => 'roxas_company_dashboard']);
        Permission::create(['name' => 'sibulan_company_dashboard']);
        Permission::create(['name' => 'hilado_company_dashboard']);
        Permission::create(['name' => 'filter_company']);
        Permission::create(['name' => 'allowed_salaries_breakdown']);

        $role_operations_thd_II = Role::create(['name' => 'operations_thd_II']);
        $role_operations_thd_II->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_admin_view_all_company',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'filter_company',
            'diversion_company_dashboard',
            'jaro_company_dashboard',
            'diversion_company_thd_summary',
            'jaro_company_thd_summary',
            'diversion_company_budget_details',
            'jaro_company_budget_details',
        ]);


        $role_operations_thd_V = Role::create(['name' => 'operations_thd_V']);
        $role_operations_thd_V->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_admin_view_all_company',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'filter_company',
            'hilado_company_dashboard',
            'sibulan_company_dashboard',
            'roxas_company_dashboard',
            'sumag_company_dashboard',
            'kabankalan_company_dashboard',
            'hilado_company_thd_summary',
            'sibulan_company_thd_summary',
            'roxas_company_thd_summary',
            'sumag_company_thd_summary',
            'kabankalan_company_thd_summary',
            'hilado_company_budget_details',
            'sibulan_company_budget_details',
            'roxas_company_budget_details',
            'sumag_company_budget_details',
            'kabankalan_company_budget_details',
        ]);

        $role_corporate_service_manager = Role::create(['name' => 'corporate_service_manager']);
        $role_corporate_service_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'corporate_services_company_dashboard',
            'corporate_services_company_thd_summary',
        ]);

        $role_kabankalan_manager = Role::create(['name' => 'kabankalan_manager']);
        $role_kabankalan_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'kabankalan_company_dashboard',
            'kabankalan_company_thd_summary',
        ]);

        $role_jaro_manager = Role::create(['name' => 'jaro_manager']);
        $role_jaro_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'jaro_company_dashboard',
            'jaro_company_thd_summary',
        ]);

        $role_diversion_manager = Role::create(['name' => 'diversion_manager']);
        $role_diversion_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'diversion_company_dashboard',
            'diversion_company_thd_summary',
        ]);

        $role_sumag_manager = Role::create(['name' => 'sumag_manager']);
        $role_sumag_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'sumag_company_dashboard',
            'sumag_company_thd_summary',
        ]);

        $role_roxas_manager = Role::create(['name' => 'roxas_manager']);
        $role_roxas_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'roxas_company_dashboard',
            'roxas_company_thd_summary',
        ]);

        $role_sibulan_manager = Role::create(['name' => 'sibulan_manager']);
        $role_sibulan_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'roxas_company_dashboard',
            'roxas_company_thd_summary',
        ]);

        $role_hilado_manager = Role::create(['name' => 'hilado_manager']);
        $role_hilado_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'hilado_company_dashboard',
            'hilado_company_thd_summary',
        ]);

        $role_super_manager = Role::create(['name' => 'super_manager']);
        $role_super_manager->givePermissionTo([
            'role_view',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'hilado_company_dashboard',
            'hilado_company_thd_summary',
            'filter_company',
            'corporate_services_company_thd_summary',
            'sibulan_company_thd_summary',
            'roxas_company_thd_summary',
            'sumag_company_thd_summary',
            'diversion_company_thd_summary',
            'jaro_company_thd_summary',
            'kabankalan_company_thd_summary',
            'hilado_company_budget_details',
            'sibulan_company_budget_details',
            'roxas_company_budget_details',
            'sumag_company_budget_details',
            'diversion_company_budget_details',
            'jaro_company_budget_details',
            'kabankalan_company_budget_details',
            'corporate_services_company_budget_details',
            'allowed_salaries_breakdown',
        ]);

        $role_super_admin = Role::create(['name' => 'super_administrator']);
        $role_super_admin->givePermissionTo([
            'permission_view',
            'permission_create',
            'permission_delete',
            'permission_edit',
            'role_view',
            'role_create',
            'role_delete',
            'role_edit',
            'user_create',
            'user_delete',
            'user_edit',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'static_budget_edit',
            'static_budget_view',
            'static_budget_delete',
            'static_budget_create',
            'summary_admin_view_all_company',
            'filter_company',
            'corporate_services_company_thd_summary',
            'hilado_company_thd_summary',
            'sibulan_company_thd_summary',
            'roxas_company_thd_summary',
            'sumag_company_thd_summary',
            'diversion_company_thd_summary',
            'jaro_company_thd_summary',
            'kabankalan_company_thd_summary',
            'hilado_company_budget_details',
            'sibulan_company_budget_details',
            'roxas_company_budget_details',
            'sumag_company_budget_details',
            'diversion_company_budget_details',
            'jaro_company_budget_details',
            'kabankalan_company_budget_details',
            'corporate_services_company_budget_details',
            'allowed_salaries_breakdown',
        ]);

        $role = Role::create(['name' => 'administrator']);
        $role->givePermissionTo([
            'permission_view',
            'permission_create',
            'permission_delete',
            'permission_edit',
            'role_view',
            'role_create',
            'role_delete',
            'role_edit',
            'user_create',
            'user_delete',
            'user_edit',
            'user_view',
            'thd_summary_edit',
            'thd_summary_delete',
            'thd_summary_create',
            'thd_summary_view',
            'summary_view',
            'summary_edit',
            'summary_delete',
            'summary_create',
            'budget_details_create',
            'budget_details_delete',
            'budget_details_edit',
            'budget_details_view',
            'static_budget_edit',
            'static_budget_view',
            'static_budget_delete',
            'static_budget_create',
            'summary_admin_view_all_company',
            'filter_company',
            'corporate_services_company_thd_summary',
            'hilado_company_thd_summary',
            'sibulan_company_thd_summary',
            'roxas_company_thd_summary',
            'sumag_company_thd_summary',
            'diversion_company_thd_summary',
            'jaro_company_thd_summary',
            'kabankalan_company_thd_summary',
            'hilado_company_budget_details',
            'sibulan_company_budget_details',
            'roxas_company_budget_details',
            'sumag_company_budget_details',
            'diversion_company_budget_details',
            'jaro_company_budget_details',
            'kabankalan_company_budget_details',
            'corporate_services_company_budget_details',
            'allowed_salaries_breakdown',
        ]);
        //Create Super admin
        $user_super = User::create([
            'name' => 'Super admin',
            'username' => 'vcyadmin',
            'email' => 'itsupport@vcygroup.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        //Create Admin
        $user = User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@test.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        //users below
        $thomas = User::create([
            'name' => 'Thomas Carl Bomban',
            'username' => 'vcythomas',
            'email' => 'thomascarl.bomban@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1237',
        ]);

        $owen = User::create([
            'name' => 'Owen Aregadas',
            'username' => 'vcyowen',
            'email' => 'owen.aregadas@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        $lea = User::create([
            'name' => 'Lea Moreno',
            'username' => 'vcylea',
            'email' => 'lea.moreno@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        $eli = User::create([
            'name' => 'Eli Dondoy',
            'username' => 'vcyeli',
            'email' => 'eli.dondoy@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1232',
        ]);

        $anna = User::create([
            'name' => 'Anna Karina Tuto',
            'username' => 'vcyanna',
            'email' => 'annakarina.tuto@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1233',
        ]);

        $roderick = User::create([
            'name' => 'Roderick Francisco',
            'username' => 'vcyroderick',
            'email' => 'roderick.francisco@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1234',
        ]);

        $berly = User::create([
            'name' => 'Berly Jabatan',
            'username' => 'vcyberly',
            'email' => 'berly.jabatan@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1235',
        ]);

        $lorieann = User::create([
            'name' => 'Lorieann Dayon',
            'username' => 'vcylorieann',
            'email' => 'lorieann.dayon@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1237',
        ]);

        $junie = User::create([
            'name' => 'Junie Goloarca',
            'username' => 'vcyjunie',
            'email' => 'junie.goloarca@vcygroup.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        $phoebelou = User::create([
            'name' => 'Phoebelou Pastera',
            'username' => 'vcyphoebelou',
            'email' => 'phoebelou.pastera@vcygroup.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        $maricon = User::create([
            'name' => 'Maricon Montebon',
            'username' => 'vcymaricon',
            'email' => 'maricon.montebon@triumphhomedepot.com',
            'password' => 'password',
            'company' => '1231',
        ]);

        $maricon = User::where('username', 'vcymaricon')->first();

        $maricon->assignRole('super_manager');

        $phoebelou = User::where('username', 'vcyphoebelou')->first();

        $phoebelou->assignRole('administrator');

        $junie = User::where('username', 'vcyjunie')->first();

        $junie->assignRole('super_manager');

        $lorieann = User::where('username', 'vcylorieann')->first();

        $lorieann->assignRole('kabankalan_manager');

        $berly = User::where('username', 'vcyberly')->first();

        $berly->assignRole('diversion_manager');

        $roderick = User::where('username', 'vcyroderick')->first();

        $roderick->assignRole('sumag_manager');

        $anna = User::where('username', 'vcyanna')->first();

        $anna->assignRole('roxas_manager');

        $eli = User::where('username', 'vcyeli')->first();

        $eli->assignRole('sibulan_manager');

        $lea = User::where('username', 'vcylea')->first();

        $lea->assignRole('hilado_manager');

        $owen = User::where('username', 'vcyowen')->first();

        $owen->assignRole('operations_thd_II');

        $thomas = User::where('username', 'vcythomas')->first();

        $thomas->assignRole('operations_thd_V');

        $user_super = User::where('username', 'vcyadmin')->first();

        $user_super->assignRole('super_administrator');

        $user = User::where('username', 'admin')->first();

        $user->assignRole('administrator');

    }
}
