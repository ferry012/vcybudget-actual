<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalSalariesToStaticBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('static_budgets', function (Blueprint $table) {
            $table->decimal('total_salaries',15,2)->nullable();
            $table->decimal('total_fuel_and_selling',15,2)->nullable();
            $table->decimal('total_operating',15,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('static_budgets', function (Blueprint $table) {
            //
        });
    }
}
