import store from '../store';


let App = () => import(/* webpackChunkName: "home" */'./../components/App');
let Home = () => import(/* webpackChunkName: "home" */'../views/Home');
let Login = () => import(/* webpackChunkName: "home" */'../views/Login');

let Profile = () => import(/* webpackChunkName: "profile" */'../views/profile/Profile');


/*permissions*/
let PermissionIndex = () => import(/* webpackChunkName: "permission" */'../views/permissions/PermissionIndex');
let PermissionTable = () => import(/* webpackChunkName: "permission" */'../views/permissions/PermissionTable');
let PermissionCreate = () => import(/* webpackChunkName: "permission" */'../views/permissions/PermissionCreate');

/*roles*/
let RoleIndex = () => import(/* webpackChunkName: "role" */'../views/roles/RoleIndex');
let RoleTable = () => import(/* webpackChunkName: "role" */'../views/roles/RoleTable');

/*users*/
let UserIndex = () => import(/* webpackChunkName: "user" */'../views/users/UserIndex');
let UserTable = () => import(/* webpackChunkName: "user" */'../views/users/UserTable');

/*static-budget*/
let StaticBudgetIndex = () => import(/* webpackChunkName: "static-budget" */'../views/static_budget/StaticIndex');
let StaticBudgetTable = () => import(/* webpackChunkName: "static-budget" */'../views/static_budget/StaticTable');

/*budget-details*/
let BudgetDetailsIndex = () => import(/* webpackChunkName: "budget-details" */'../views/budget_details/BudgetDetailsIndex');
let BudgetDetailsTable = () => import(/* webpackChunkName: "budget-details" */'../views/budget_details/BudgetDetailsTable');

/*summary*/
let SummaryIndex = () => import(/* webpackChunkName: "summary" */'../views/summary/SummaryIndex');
let SummaryTable = () => import(/* webpackChunkName: "summary" */'../views/summary/SummaryTable');

/*thd_summary*/
let THDSummaryIndex = () => import(/* webpackChunkName: "thd-summary" */'../views/thd_summary/THDSummaryIndex');
let THDSummaryTable = () => import(/* webpackChunkName: "thd-summary" */'../views/thd_summary/THDSummaryTable');

/*budget-maintanence*/
let BudgetMaintenanceCreate = () => import(/* webpackChunkName: "budget-maintenance" */'../views/budget_maintenance/BudgetMaintenanceCreate');
let BudgetMaintenanceTable = () => import(/* webpackChunkName: "budget-maintenance" */'../views/budget_maintenance/BudgetMaintenanceTable');
let BudgetMaintenanceIndex = () => import(/* webpackChunkName: "budget-maintenance" */'../views/budget_maintenance/BudgetMaintenanceIndex');

/*gl-accounts*/
let GLAccountIndex = () => import(/* webpackChunkName: "gl-account" */'../views/gl_accounts/GLAccountIndex');
let GLAccountTable = () => import(/* webpackChunkName: "gl-account" */'../views/gl_accounts/GLAccountTable');


/*error views*/
let Error_404 = () => import(/* webpackChunkName: "error_view" */'./../views/error_view/Error_404');

const routes = [
    {
        path: "",
        redirect: {name: 'summaryTable'},
        component: App,
        children: [
            // {
            //     path: "/home",
            //     name: "home",
            //     component: Home,
            //     meta: {
            //         title: 'Home',
            //         requiresAuth: true,
            //     },
            //
            // },

            {
                path: "/profile",
                name: "profile",
                component: Profile,
                meta: {
                    title: 'Profile',
                    requiresAuth: true,
                    breadCrumbs: [
                        {
                            to: {name: 'summaryTable'},           // hyperlink
                            text: 'Dashboard', // crumb text
                        },
                        {
                        to: {name: 'profile'},           // hyperlink
                        text: 'Profile', // crumb text
                        active:true
                    }]
                },

            },

            {
                path: "/permissions",
                name: "permissionIndex",
                redirect: {name: 'permissionTable'},
                component: PermissionIndex,
                children: [
                    {
                        path: "/",
                        name: "permissionTable",
                        component: PermissionTable,
                        meta: {
                            title: 'Permissions',
                            requiresAuth: true,
                            permissions: ['permission_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text,
                                    icon:'house-fill'
                                },
                                {
                                to: {name: 'permissionIndex'},           // hyperlink
                                text: 'Permissions', // crumb text
                                active:true
                            }]
                        },
                    },
                    {
                        path: "create",
                        name: "permissionCreate",
                        component: PermissionCreate,
                        meta: {
                            title: 'Permissions',
                            requiresAuth: true,
                            permissions: ['permission_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                to: {name: 'permissionIndex'},           // hyperlink
                                text: 'Permissions' // crumb text
                            },
                                {
                                    to: {name: 'permissionCreate'},           // hyperlink
                                    text: 'Create Permission', // crumb text,
                                    active: true
                                },

                            ]
                        },
                    },
                ]

            },
            {
                path: "/roles",
                name: "roleIndex",
                redirect: {name: 'roleTable'},
                component: RoleIndex,
                children: [
                    {
                        path: "/",
                        name: "roleTable",
                        component: RoleTable,
                        meta: {
                            title: 'Roles',
                            requiresAuth: true,
                            permissions: ['role_view'],
                            breadCrumbs: [ {
                                to: {name: 'summaryTable'},           // hyperlink
                                text: 'Dashboard', // crumb text
                            },
                                {
                                to: {name: 'roleIndex'},          // hyperlink
                                text: 'Roles', // crumb text
                                active:true
                            }]
                        },
                    },
                ]

            },
            {
                path: "/users",
                name: "userIndex",
                redirect: {name: 'userTable'},
                component: UserIndex,
                children: [
                    {
                        path: "/",
                        name: "userTable",
                        component: UserTable,
                        meta: {
                            title: 'Users',
                            requiresAuth: true,
                            permissions: ['user_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                to: {name: 'userIndex'},      // hyperlink
                                text: 'Users', // crumb text
                                active:true
                            }]
                        },
                    },
                ]

            },
            //Static Budget
            {
                path: "/static-budget/accounts/table",
                name: "staticIndex",
                redirect: {name: 'staticTable'},
                component: StaticBudgetIndex,
                children: [
                    {
                        path: "/",
                        name: "staticTable",
                        component: StaticBudgetTable,
                        meta: {
                            title: 'Static Budget',
                            requiresAuth: true,
                            permissions: ['static_budget_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                    to: {name: 'staticIndex'},  // hyperlink
                                    text: 'Static Budget', // crumb text
                                    active:true
                                }]
                        },
                    },
                ]
            },
            //Budget Details
            {
                path: "/budget-details/per-company/table",
                name: "budgetIndex",
                redirect: {name: 'budgetTable'},
                component: BudgetDetailsIndex,
                children: [
                    {
                        path: "/",
                        name: "budgetTable",
                        component: BudgetDetailsTable,
                        meta: {
                            title: 'Budget Details',
                            requiresAuth: true,
                            permissions: ['budget_details_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                    to: {name: 'budgetIndex'},  // hyperlink
                                    text: 'Budget Details', // crumb text
                                    active:true
                                }]
                        },
                    },
                ]

            },
            //Summary before dashboard now
            {
                path: "/dashboard",
                name: "summaryIndex",
                redirect: {name: 'summaryTable'},
                component: SummaryIndex,
                children: [
                    {
                        path: "/",
                        name: "summaryTable",
                        component: SummaryTable,
                        meta: {
                            requiresAuth: true,
                            title: 'Dashboard',
                            permissions: ['summary_view'],
                        },
                    },
                ]

            },
            //THDSummary
            {
                path: "/thd-summary/get-all-companies/table",
                name: "thdSummaryIndex",
                redirect: {name: 'thdSummaryTable'},
                component: THDSummaryIndex,
                children: [
                    {
                        path: "/",
                        name: "thdSummaryTable",
                        component: THDSummaryTable,
                        meta: {
                            requiresAuth: true,
                            title: 'THD Summary',
                            permissions: ['summary_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'thdSummaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                    to: {name: 'thdSummaryIndex'},  // hyperlink
                                    text: 'THD Summary', // crumb text
                                    active:true
                                }]
                        },
                    },
                ]

            },
            //GL Accounts
            {
                path: "/gl-accounts",
                name: "GLAccountIndex",
                redirect: {name: 'GLAccountTable'},
                component: GLAccountIndex,
                children: [
                    {
                        path: "/",
                        name: "GLAccountTable",
                        component: GLAccountTable,
                        meta: {
                            requiresAuth: true,
                            title: 'GL Accounts',
                            permissions: ['gl_accounts_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                    to: {name: 'GLAccountIndex'},  // hyperlink
                                    text: 'GL Accounts', // crumb text
                                    active:true
                                }]
                        },
                    },
                ]
            },
            //BudgetMaintenance
            {
                path: "/budget-maintenance",
                name: "BudgetMaintenanceIndex",
                redirect: {name: 'BudgetMaintenanceTable'},
                component: BudgetMaintenanceIndex,
                children: [
                    {
                        path: "/",
                        name: "BudgetMaintenanceTable",
                        component: BudgetMaintenanceTable,
                        meta: {
                            requiresAuth: true,
                            title: 'Budget Maintenance',
                            permissions: ['budget_maintenance_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'summaryTable'},           // hyperlink
                                    text: 'Dashboard', // crumb text
                                },
                                {
                                    to: {name: 'BudgetMaintenanceIndex'},  // hyperlink
                                    text: 'Budget Maintenance', // crumb text
                                    active:true
                                }]
                        },
                    },
                ]
            },
        ]
    },
    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            title: 'Login',
            requiresAuth: false,
        },
    },
    {
        path: "*",
        name: "error_404",
        component: Error_404,
        meta: {
            requiresAuth: false,
        }
    }
];

export default routes;
