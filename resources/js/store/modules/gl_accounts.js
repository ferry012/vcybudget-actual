import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";
import {
    ALL_GL_ACCOUNT, GL_ACCOUNT,
    GL_ACCOUNT_CREATE,
    GL_ACCOUNT_CREATE_BTN, GL_ACCOUNT_CREATE_CLEAR, GL_ACCOUNT_ID,
    GL_ACCOUNT_PAGE, GL_ACCOUNT_PERPAGE,
    GL_ACCOUNT_SEARCH,
    GL_ACCOUNT_SORTBY, GL_ACCOUNT_SORTDIR, GL_ACCOUNT_UPDATE
} from "../mutation-types";


const state = {
    gl_accounts: {},
    all_gl_accounts: [],
    page: {},
    search: '',
    sortBy: 'created_at',
    sortDir: 'desc',
    perPage: 5,
    gl: new Form({
        name: null,
        gl_accounts: [],
    }),
    gl_id: null,
    submitBtn:true,
    clear_create:false,
};
const getters = {
    gl_accounts: state => state.gl_accounts,
    all_gl_accounts: state => state.all_gl_accounts,
    page: state => state.page,
    search: state => state.search,
    sortBy: state => state.sortBy,
    sortDir: state => state.sortDir,
    perPage: state => state.perPage,
    gl: state => state.gl,
    gl_id: state => state.gl_id,
    submitBtn: state => state.submitBtn,
    clear_create: state => state.clear_create,
};

const actions = {
    async getUser({commit, state}) {
        try {
            const uri = `/api/gl_accounts/${state.gl_id}/edit`;
            const {data} = await axios.get(uri);
            commit(types.USER, data.data)
        } catch (e) {
            console.log(e);
        }
    },
    // async fetchGLForDatatable({commit, state}) {
    //     try {
    //         const uri = '/api/gl_accounts?page=' + state.page + '&search='
    //             + state.search + '&sortby=' + state.sortBy
    //             + '&sortdir=' + state.sortDir + '&currentpage=' + state.perPage;
    //         const {data} = await axios.get(uri);
    //         commit(types.GL_ACCOUNTS, data)
    //
    //     } catch (e) {
    //         console.log(e);
    //     }
    // },

    async fetchGLForBudgetMaintenanceDatatable({commit, state}) {
        try {
            const uri = '/api/gl_accounts?budgetMaintenance=true';
            const {data} = await axios.get(uri);
            commit(types.GL_ACCOUNTS, data)

        } catch (e) {
            console.log(e);
        }
    },

    async getUsers({commit, state}) {
        try {
            const uri = `/api/gl_accounts?getAll=true`;
            const {data} = await axios.get(uri);
            commit(types.ALL_USER, data.data)
        } catch (e) {
            console.log(e);
        }
    },

    createGL({commit, state}) {
        return new Promise((res, rej) => {
            state.gl.post('/api/gl_accounts')
                .then((response) => {
                    commit(types.GL_ACCOUNT_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
    updateUser({commit, state}) {
        return new Promise((res, rej) => {
            state.gl.patch(`/api/gl_accounts/${state.gl_id}`)
                .then((response) => {
                    commit(types.GL_ACCOUNT_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
};

const mutations = {
    [types.GL_ACCOUNT_CLEAR](state) {
        state.gl.clear();
        state.gl.reset();
    },
    [types.GL_ACCOUNTS](state, gl_accounts) {
        state.gl_accounts = gl_accounts
    },
    [types.GL_ACCOUNT](state, gl) {
        state.gl.fill(gl)
    },
    [types.GL_ACCOUNT_PAGE](state, page) {
        state.page = page
    },
    [types.GL_ACCOUNT_SEARCH](state, search) {
        state.search = search
    },
    [types.GL_ACCOUNT_SORTBY](state, sortBy) {
        state.sortBy = sortBy
    },
    [types.GL_ACCOUNT_SORTDIR](state, sortDir) {
        state.sortDir = sortDir
    },
    [types.GL_ACCOUNT_PERPAGE](state, perPage) {
        state.perPage = perPage
    },
    [types.GL_ACCOUNT_CREATE](state, gl) {
        state.gl = gl
    },
    [types.GL_ACCOUNT_UPDATE](state, gl) {
        state.gl  = gl
    },
    [types.GL_ACCOUNT_ID](state, id) {
        state.gl_id = id
    },
    [types.ALL_GL_ACCOUNT](state, gl_accounts) {
        state.all_gl_accounts = gl_accounts
    },
    [types.GL_ACCOUNT_CREATE_BTN](state, submitBtn) { // disabled if no data within an array
        state.submitBtn = submitBtn
    },
    [types.GL_ACCOUNT_CREATE_CLEAR](state, clear_create) { // disabled if no data within an array
        state.clear_create = clear_create
    },

};

export {
    state,
    getters,
    actions,
    mutations
};
