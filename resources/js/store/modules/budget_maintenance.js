import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";


const state = {
    budgetMaintenance: [],
    company:null,
    from:null,
    to:null,
};
const getters = {
    budgetMaintenance: state => state.budgetMaintenance,
    company: state => state.company,
};

const actions = {
    async getTHDSummary({commit, state}) {
        try {
            const uri = `/api/thd-summary?from=` + state.from
                + '&to=' + state.to;
            const {data} = await axios.get(uri);
            console.log(data,'data');
            commit(types.THD_SUMMARY, data)
        } catch (e) {
            console.log(e);
        }
    },
};

const mutations = {
    [types.THD_SUMMARY](state,thdSummaryOperating) {
        state.thdSummaryOperating = thdSummaryOperating;
    },
    [types.THD_SUMMARY_COMPANY](state,company) {
        state.company = company;
    },
    [types.THD_DATE_RANGE](state,dateRange) {
        console.log(state,dateRange,'state,dateRange');
        state.from = dateRange[0];
        state.to = dateRange[1];
    },
};

export {
    state,
    getters,
    actions,
    mutations
};
