import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";


const state = {
    budget_details: {},
    all_budget_detail: [],
    page: {},
    search: '',
    sortBy: 'created_at',
    sortDir: 'desc',
    perPage: 5,
    budget_detail: new Form({
        name: null
    }),
    budget_detail_id: null,

    budget_total:[],

    company_user:null,
    filter_company:null,

};
const getters = {
    budget_details: state => state.budget_details,
    all_budget_detail: state => state.all_budget_detail,
    page: state => state.page,
    search: state => state.search,
    sortBy: state => state.sortBy,
    sortDir: state => state.sortDir,
    perPage: state => state.perPage,
    budget_detail: state => state.budget_detail,
    budget_detail_id: state => state.budget_detail_id,

    budget_total:state => state.budget_total,
    company_user: state => state.company_user,
    filter_company: state => state.filter_company,
};

const actions = {
    async budgetTotal({commit, state}) {
        try {
            const uri = `/api/budget-total?company=` + await state.company_user + '&filter=' + state.filter_company;
            const {data} = await axios.get(uri);
            // console.log(data);
            commit(types.GET_BUDGET_TOTAL, data)
        } catch (e) {
            console.log(e);
        }
    },
    async getPermission({commit, state}) {
        try {
            const uri = `/api/permissions/${state.permission_id}/edit`;
            const {data} = await axios.get(uri);
            commit(types.PERMISSION, data.data)
        } catch (e) {
            console.log(e);
        }
    },
    async fetchPermissionsForDatatable({commit, state}) {
        try {
            const uri = '/api/permissions?page=' + state.page
                + '&search=' + state.search
                + '&sortby=' + state.sortBy
                + '&sortdir=' + state.sortDir
                + '&currentpage=' + state.perPage;
            const {data} = await axios.get(uri);
            commit(types.PERMISSIONS, data)

        } catch (e) {
            console.log(e);
        }
    },

    async getPermissions({commit, state}) {
        try {
            const uri = `/api/permissions?getAll=true`;
            const {data} = await axios.get(uri);
            commit(types.ALL_PERMISSION, data.data)
        } catch (e) {
            console.log(e);
        }
    },

    createPermission({commit, state}) {
        return new Promise((res, rej) => {
            state.permission.post('/api/permissions')
                .then((response) => {
                    commit(types.PERMISSION_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
    updatePermission({commit, state}) {
        return new Promise((res, rej) => {
            state.permission.patch(`/api/permissions/${state.permission_id}`)
                .then((response) => {
                    commit(types.PERMISSION_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },

};

const mutations = {
    [types.BUDGET_DETAILS_CLEAR](state) {
        state.budget_detail.clear();
        state.budget_detail.reset();
    },
    [types.BUDGET_DETAILS](state, budget_details) {
        state.budget_details = budget_details
    },
    [types.BUDGET_DETAIL](state, permission) {
        state.budget_detail.fill(permission)
    },
    [types.BUDGET_DETAILS_PAGE](state, page) {
        state.page = page
    },
    [types.BUDGET_DETAILS_SEARCH](state, search) {
        state.search = search
    },
    [types.BUDGET_DETAILS_SORTBY](state, sortBy) {
        state.sortBy = sortBy
    },
    [types.BUDGET_DETAILS_SORTDIR](state, sortDir) {
        state.sortDir = sortDir
    },
    [types.BUDGET_DETAILS_PERPAGE](state, perPage) {
        state.perPage = perPage
    },
    [types.BUDGET_DETAILS_CREATE](state, budget_detail) {
        state.budget_detail = budget_detail
    },
    [types.BUDGET_DETAIL_UPDATE](state, budget_detail) {
        state.budget_detail = budget_detail
    },
    [types.BUDGET_DETAILS_ID](state, id) {
        state.budget_detail_id = id
    },
    [types.ALL_BUDGET_DETAIL](state, budget_details) {
        state.all_budget_detail = budget_details
    },
    //Budget Total
    [types.GET_BUDGET_TOTAL](state, budget_total) { // get plant
        state.budget_total = budget_total
    },
    //Company User
    [types.COMPANY_USER](state, company_user) { // get company_user
        state.company_user = company_user
    },

    [types.FILTER_COMPANY](state, filter_company) {
        state.filter_company = filter_company
    },
};

export {
    state,
    getters,
    actions,
    mutations
};
