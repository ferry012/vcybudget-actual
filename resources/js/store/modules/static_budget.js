import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";


const state = {
    staticBuds: {},
    all_staticBuds: [],
    page: {},
    search: '',
    sortBy: 'created_at',
    sortDir: 'desc',
    perPage: 5,
    staticBudget: new Form({
        company: null,
        salaries:[],
        fuels:[],
        maintenance:[],
        repairs:[],
        parking:[],
        tolling:[],
        utilities:[],
        op_expenses:[],
        under_total_fuel_and_exp:[],
        under_total_admin: [],
        totalExpenses:0, //<- OPERATING EXPENSES
        totalSalaries:0,
        totalAdminExpenses:0,
        totalFuelAndSellingExp:0,
        totalNotNaN:true,
    }),
    // staticBudget: {},
    staticBudgetArr:[],
    salaries:[],
    fuels:[],
    maintenance:[],
    repairs:[],
    parking:[],
    tolling:[],
    utilities:[],
    op_expenses:[],

    staticBud_id: null,
    static_id:null, // <- id for static budgets table
    plants:[],

    submitBtn:true,
    clear_data_after_submit:false,

};
const getters = {
    staticBuds: state => state.staticBuds,
    all_staticBuds: state => state.all_staticBuds,
    page: state => state.page,
    search: state => state.search,
    sortBy: state => state.sortBy,
    sortDir: state => state.sortDir,
    perPage: state => state.perPage,

    staticBudgetArr:state => state.staticBudgetArr,
    staticBudget: state => state.staticBudget,
    salaries: state => state.salaries,
    submitBtn: state => state.submitBtn,

    staticBud_id: state => state.staticBud_id,
    plants: state => state.plants,
    static_id: state => state.static_id,
    clear_data_after_submit: state => state.clear_data_after_submit,
};

const actions = {
    async getStaticBudget({commit, state}) {
        try {
            const uri = `/api/static-budget/${state.staticBud_id}/edit`;
            const {data} = await axios.get(uri);
            console.log(data.data,'data.data');
            // commit(types.STATIC_BUDGET_ARR, data.data)
            commit(types.STATIC_BUDGET, data.data)
        } catch (e) {
            console.log(e);
        }
    },
    async fetchStaticBudgetsForDatatable({commit, state}) {
        try {
            const uri = '/api/static-budget?page=' + state.page + '&search='
                + state.search + '&sortby=' + state.sortBy
                + '&sortdir=' + state.sortDir
                + '&currentpage=' + state.perPage;
            const {data} = await axios.get(uri);
            commit(types.STATIC_BUDGETS, data)

        } catch (e) {
            console.log(e);
        }
    },

    async getPlant({commit, state}) {
        try {
            const uri = `/api/all-company-codes`;
            const {data} = await axios.get(uri);
            // console.log(data);
            commit(types.GET_PLANT_STATIC_BUDGET, data)
        } catch (e) {
            console.log(e);
        }
    },

    createStaticBudget({commit, state}) {
        return new Promise((res, rej) => {
            console.log(state,'state');
            state.staticBudget.post('/api/static-budget')
                .then((response) => {
                    commit(types.STATIC_BUDGET_CLEAR)
                    console.log(response.data,'createStaticBudget');
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },

    updateStaticBudget({commit, state}) {
        return new Promise((res, rej) => {
            state.staticBudget.patch(`/api/static-budget/${state.static_id}`)
                .then((response) => {
                    commit(types.STATIC_BUDGET_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },

};

const mutations = {
    [types.STATIC_BUDGET_CLEAR ](state) {
        state.staticBudget.clear();
        state.staticBudget.reset();
    },
    [types.STATIC_BUDGETS](state, staticBuds) {
        state.staticBuds = staticBuds
    },
    [types.STATIC_BUDGET](state, staticBudget) {
        state.staticBudget.fill(staticBudget)
    },
    [types.STATIC_BUDGET_PAGE](state, page) {
        state.page = page
    },
    [types.STATIC_BUDGET_SEARCH](state, search) {
        state.search = search
    },
    [types.STATIC_BUDGET_SORTBY](state, sortBy) {
        state.sortBy = sortBy
    },
    [types.STATIC_BUDGET_SORTDIR](state, sortDir) {
        state.sortDir = sortDir
    },
    [types.STATIC_BUDGET_PERPAGE](state, perPage) {
        state.perPage = perPage
    },
    [types.STATIC_BUDGET_CREATE](state, staticBudget) {
        console.log(state,staticBudget,'STATIC_BUDGET_CREATE');
        state.staticBudget = staticBudget
        // state.staticBudgetArr = staticBudget
    },
    [types.STATIC_BUDGET_UPDATE](state, staticBudget) {
        state.staticBudget  = staticBudget
    },
    [types.STATIC_BUDGET_ID](state, id) {
        console.log(state,id,'types.STATIC_BUDGET_ID');
        state.staticBud_id = id
    },
    [types.STATIC_ID](state, id) {
        console.log(state,id,'types.STATIC_ID');
        state.static_id = id
    },
    [types.ALL_STATIC_BUDGET](state, staticBuds) {
        state.all_staticBuds = staticBuds
    },
    //Plant
    [types.GET_PLANT_STATIC_BUDGET](state, plantsBranch) { // get plant
        state.plants = plantsBranch
    },
    [types.STATIC_BUDGET_CREATE_BTN](state, submitBtn) { // disabled if no data within an array
        state.submitBtn = submitBtn
    },
    [types.STATIC_BUDGET_CLEAR_AFTER_SUBMIT](state, clearSubmitBtn) { // clear data after submitting
        state.clear_data_after_submit = clearSubmitBtn
    },
    [types.STATIC_BUDGET_EDIT_BTN](state, submitBtn) { // disabled if no data within an array
        state.submitBtn = submitBtn
    },
    [types.STATIC_BUDGET_ARR](state, staticBudget) {
        state.staticBudget.fill(staticBudget)
    },
};

export {
    state,
    getters,
    actions,
    mutations
};
