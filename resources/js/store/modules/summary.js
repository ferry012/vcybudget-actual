import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";
import {GET_BUDGET_DETAILS_OPERATING_EXP, SUMMARY, SUMMARY_COMPANY, YEAR_BREAKDOWN} from "../mutation-types";


const state = {
    summaryOperating: [],
    budget_details_operating_exp:[],
    company:null,
    yearBreakdown:[],
    filter_company:null,
};
const getters = {
    summaryOperating: state => state.summaryOperating,
    budget_details_operating_exp: state => state.budget_details_operating_exp,
    company: state => state.company,
    yearBreakdown: state => state.yearBreakdown,
    filter_company: state => state.filter_company,
};

const actions = {
    async getSummaryOperating({commit, state}) {
        try {
            const uri = `/api/summary-operating-exp?company=` + state.company;
            const {data} = await axios.get(uri);
            commit(types.SUMMARY, data)
        } catch (e) {
            console.log(e);
        }
    },
    async getYearBreakdown({commit, state}) {
        try {
            const uri = `/api/year-breakdown?company=` + state.company + '&filter=' + state.filter_company;
            const {data} = await axios.get(uri);
            commit(types.YEAR_BREAKDOWN, data)
        } catch (e) {
            console.log(e);
        }
    },
};

const mutations = {
    [types.YEAR_BREAKDOWN](state,yearBreakdown) {
        state.yearBreakdown = yearBreakdown;
    },
    [types.SUMMARY](state,summaryOperating) {
        state.summaryOperating = summaryOperating;
    },
    [types.GET_BUDGET_DETAILS_OPERATING_EXP](state,budget_details_operating_exp) {
        state.budget_details_operating_exp = budget_details_operating_exp;
    },
    [types.SUMMARY_COMPANY](state,company) {
        state.company = company;
    },
    [types.FILTER_COMPANY](state,filter_company) {
        state.filter_company = filter_company;
    },
};

export {
    state,
    getters,
    actions,
    mutations
};
