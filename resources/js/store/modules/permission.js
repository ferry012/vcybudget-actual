import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";


const state = {

    permissions: {},
    all_permission: [],
    page: {},
    search: '',
    sortBy: 'created_at',
    sortDir: 'desc',
    perPage: 5,
    permission: new Form({
        name: null
    }),
    permission_id: null,

};
const getters = {
    permissions: state => state.permissions,
    all_permission: state => state.all_permission,
    page: state => state.page,
    search: state => state.search,
    sortBy: state => state.sortBy,
    sortDir: state => state.sortDir,
    perPage: state => state.perPage,
    permission: state => state.permission,
    permission_id: state => state.permission_id,

};

const actions = {
    async getPermission({commit, state}) {
        try {
            const uri = `/api/permissions/${state.permission_id}/edit`;
            const {data} = await axios.get(uri);
            commit(types.PERMISSION, data.data)
        } catch (e) {
            console.log(e);
        }
    },
    async fetchPermissionsForDatatable({commit, state}) {
        try {
            const uri = '/api/permissions?page=' + state.page + '&search=' + state.search + '&sortby=' + state.sortBy + '&sortdir=' + state.sortDir + '&currentpage=' + state.perPage;
            const {data} = await axios.get(uri);
            commit(types.PERMISSIONS, data)

        } catch (e) {
            console.log(e);
        }
    },

    async getPermissions({commit, state}) {
        try {
            const uri = `/api/permissions?getAll=true`;
            const {data} = await axios.get(uri);
            commit(types.ALL_PERMISSION, data.data)
        } catch (e) {
            console.log(e);
        }
    },

    createPermission({commit, state}) {
        return new Promise((res, rej) => {
            state.permission.post('/api/permissions')
                .then((response) => {
                    commit(types.PERMISSION_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
    updatePermission({commit, state}) {
        return new Promise((res, rej) => {
            state.permission.patch(`/api/permissions/${state.permission_id}`)
                .then((response) => {
                    commit(types.PERMISSION_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },

};

const mutations = {
    [types.PERMISSION_CLEAR](state) {
        state.permission.clear();
        state.permission.reset();
    },
    [types.PERMISSIONS](state, permissions) {
        state.permissions = permissions
    },
    [types.PERMISSION](state, permission) {
        state.permission.fill(permission)
    },
    [types.PERMISSION_PAGE](state, page) {
        state.page = page
    },
    [types.PERMISSION_SEARCH](state, search) {
        state.search = search
    },
    [types.PERMISSION_SORTBY](state, sortBy) {
        state.sortBy = sortBy
    },
    [types.PERMISSION_SORTDIR](state, sortDir) {
        state.sortDir = sortDir
    },
    [types.PERMISSION_PERPAGE](state, perPage) {
        state.perPage = perPage
    },
    [types.PERMISSION_CREATE](state, permission) {
        state.permission = permission
    },
    [types.PERMISSION_UPDATE](state, permission) {
        state.permission = permission
    },
    [types.PERMISSION_ID](state, id) {
        state.permission_id = id
    },
    [types.ALL_PERMISSION](state, permissions) {
        state.all_permission = permissions
    },


};

export {
    state,
    getters,
    actions,
    mutations
};
