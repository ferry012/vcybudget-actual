<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {

    Route::get('abilities', [\App\Http\Controllers\Api\AbilitiesController::class, 'index']);
    Route::patch('profile-update', [\App\Http\Controllers\Api\AbilitiesController::class, 'updateProfile']);
    Route::resource('permissions', \App\Http\Controllers\Api\PermissionController::class);
    Route::resource('roles', \App\Http\Controllers\Api\RoleController::class);
    Route::resource('users', \App\Http\Controllers\Api\UserController::class);

    Route::resource('static-budget',\App\Http\Controllers\StaticBudgetController::class);

    Route::get('budget-total',[\App\Http\Controllers\BudgetDetailsController::class,'budgetTotal']);
    Route::get('summary-operating-exp',[\App\Http\Controllers\SummaryController::class,'summaryOperatingExp']);
    Route::get('thd-summary',[\App\Http\Controllers\THDSummaryController::class,'thdSummary']);
    Route::get('year-breakdown',[\App\Http\Controllers\YearBreakdownController::class,'yearBreakDown']);
});
Route::get('all-company-codes',[\App\Http\Controllers\CompanyController::class,'allCompany']);

Route::resource('gl_accounts',\App\Http\Controllers\GLAccountController::class);
