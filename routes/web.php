<?php

use App\Models\AdminExpense;
use App\Models\FuelAndSellingExp;
use App\Models\Fuels;
use App\Models\Maintenance;
use App\Models\Parking;
use App\Models\Repairs;
use App\Models\Salary;
use App\Models\StaticBudget;
use App\Models\Tolling;
use App\Models\User;
use App\Models\Utilities;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/check_it', function () {
//    //$hashedPassword = '$2y$10$acLfURZomNLwmqZcWhXzOu0e91UEErCx7YUdWPrK535wsFDiiwU0y';
//    //id 1
//    $user = User::where('id',1)->first();
//
//    $hashedPassword = '$2y$10$H0uHhXXpvIeF/RBh7g5uauVDSOQJ42Om2qPcm2r.YkspqVrckell.';
//    if($user->password === $hashedPassword){
//        dd('true');
//    }
//    else{
//        dd('false');
//    }
////    if (Hash::check('passwor123d', $hashedPassword)) {
////        dd('true');
////    }
////    else{
////        dd('false');
////    }
//});

Route::get('/removing/gl-account/{name}/{auth_access}', function ($name,$auth) {
    if (Hash::check($auth, '$2y$10$3yo5aSgVNCIfJ3R8JswM9eP3xv/SaYEQBJKmn5yy27t.2sZxGIuOO')) {
        $company = StaticBudget::orderBy('company','asc')->get();
        $com_length = count($company);
        $getAdminExp = [];
        $getFuelAndSellExp = [];
        $getMaintenance = [];
        $getRepairs = [];
        $getFuels = [];
        $getParking = [];
        $getSalaries = [];
        $getTolling = [];
        $getUtilities = [];

        for($i = 0; $i < $com_length;$i++){
            $admin_expenses = AdminExpense::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
            })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $fuels = Fuels::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
            })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $fuel_and_selling_exp = FuelAndSellingExp::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
            })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $maintenance = Maintenance::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
            })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();


            $parking = Parking::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
                })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $repairs = Repairs::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
                })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $salaries = Salary::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
                })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $tolling = Tolling::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
                })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();

            $utilities = Utilities::where(function ($query) use ($company,$i,$name) {
                $query->where('static_budget_id', '=',  $company[$i]['id'])
                    ->where('name','=',$name);
                })
                ->select('id','static_budget_id','name', 'budget')
                ->orderBy('id', 'asc')->first();



            if($fuels !== null)$getFuels[] = ['id' => $fuels->id];
            if($parking !== null)$getParking[] = ['id' => $parking->id];
            if($admin_expenses !== null)$getAdminExp[] = ['id' => $admin_expenses->id];
            if($fuel_and_selling_exp !== null)$getFuelAndSellExp[] = ['id' => $fuel_and_selling_exp->id];
            if($maintenance !== null)$getMaintenance[] = ['id' => $maintenance->id];
            if($repairs !== null)$getRepairs[] = ['id' => $repairs->id];
            if($salaries !== null)$getSalaries[] = ['id' => $salaries->id];
            if($tolling !== null)$getTolling[] = ['id' => $tolling->id];
            if($utilities !== null)$getUtilities[] = ['id' => $utilities->id];
        }
        if($getFuels != 0){
            for($i = 0;$i < count($getFuels);$i++){
                $query = Fuels::find($getFuels[$i]['id']);
                $query->delete();
            }
        }
        if($getParking != 0){
            for($i = 0;$i < count($getParking);$i++){
                $query = Parking::find($getParking[$i]['id']);
                $query->delete();
            }
        }
        if($getAdminExp != 0){
            for($i = 0;$i < count($getAdminExp);$i++){
                $query = AdminExpense::find($getAdminExp[$i]['id']);
                $query->delete();
            }
        }
        if($getFuelAndSellExp != 0){
            for($i = 0;$i < count($getFuelAndSellExp);$i++){
                $query = FuelAndSellingExp::find($getFuelAndSellExp[$i]['id']);
                $query->delete();
            }
        }
        if($getMaintenance != 0){
            for($i = 0;$i < count($getMaintenance);$i++){
                $query = Maintenance::find($getMaintenance[$i]['id']);
                $query->delete();
            }
        }
        if($getRepairs != 0){
            for($i = 0;$i < count($getRepairs);$i++){
                $query = Repairs::find($getRepairs[$i]['id']);
                $query->delete();
            }
        }
        if($getSalaries != 0){
            for($i = 0;$i < count($getSalaries);$i++){
                $query = Salary::find($getSalaries[$i]['id']);
                $query->delete();
            }
        }
        if($getTolling != 0){
            for($i = 0;$i < count($getTolling);$i++){
                $query = Tolling::find($getTolling[$i]['id']);
                $query->delete();
            }
        }
        if($getUtilities != 0){
            for($i = 0;$i < count($getUtilities);$i++){
                $query = Utilities::find($getUtilities[$i]['id']);
                $query->delete();
            }
        }
        dd("success in removing => $name");
    }
    else{
        dd("Your not authenticated!");
    }
});



//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/{any}',[\App\Http\Controllers\AppController::class,'index'])->where('any','.*');
Auth::routes();
