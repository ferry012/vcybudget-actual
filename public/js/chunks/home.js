(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _layout_Sidebar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layout/Sidebar */ "./resources/js/components/layout/Sidebar.vue");
/* harmony import */ var _layout_Navbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./layout/Navbar */ "./resources/js/components/layout/Navbar.vue");
/* harmony import */ var _layout_Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout/Footer */ "./resources/js/components/layout/Footer.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "App",
  components: {
    Footer: _layout_Footer__WEBPACK_IMPORTED_MODULE_2__["default"],
    Navbar: _layout_Navbar__WEBPACK_IMPORTED_MODULE_1__["default"],
    Sidebar: _layout_Sidebar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  computed: {
    crumbs: function crumbs() {
      return this.$route.meta.breadCrumbs;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Footer.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Footer.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Footer"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Navbar.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Navbar.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "navbar",
  data: function data() {
    return {};
  },
  computed: {
    currentUser: function currentUser() {
      return this.$store.getters['auth/user'];
    },
    pageName: function pageName() {
      return this.$store.getters['page/pageName'];
    }
  },
  methods: {
    logout: function logout() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _this.$store.dispatch('auth/logout');

              case 3:
                _context.next = 5;
                return _this.$router.push('/login');

              case 5:
                _context.next = 10;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 7]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Sidebar.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Sidebar.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "sidebar"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Home.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Home",
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])({
    isLogin: ["auth/check"],
    user: ["auth/user"]
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Login.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vform__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vform */ "./node_modules/vform/dist/vform.common.js");
/* harmony import */ var vform__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vform__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _vanta_vanta_net__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../vanta/vanta.net */ "./resources/js/vanta/vanta.net.js");
/* harmony import */ var vue_vanta__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-vanta */ "./node_modules/vue-vanta/src/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Login",
  components: {
    HasError: vform__WEBPACK_IMPORTED_MODULE_1__["HasError"],
    AlertError: vform__WEBPACK_IMPORTED_MODULE_1__["AlertError"],
    AlertSuccess: vform__WEBPACK_IMPORTED_MODULE_1__["AlertSuccess"],
    jay3DCustom: vue_vanta__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  data: function data() {
    return {
      form: new vform__WEBPACK_IMPORTED_MODULE_1__["Form"]({
        email: null,
        password: null,
        remember: false
      }),
      btnText: 'Login',
      disabled: false,
      loading: false,
      options: {
        mouseControls: true,
        touchControls: true,
        gyroControls: false,
        minHeight: 969.00,
        minWidth: 200.00,
        scale: 1.00,
        scaleMobile: 1.00
      }
    };
  },
  methods: {
    login: function login() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                // Submit the form.
                // First get csrf-token with airlock
                _this.btnText = 'Loading...';
                axios.get('/sanctum/csrf-cookie').then( /*#__PURE__*/function () {
                  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(response) {
                    var _yield$_this$form$pos, data;

                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _context.next = 2;
                            return _this.form.post('/login');

                          case 2:
                            _yield$_this$form$pos = _context.sent;
                            data = _yield$_this$form$pos.data;
                            _context.next = 6;
                            return _this.$store.dispatch('auth/setLogin');

                          case 6:
                            _context.next = 8;
                            return _this.$store.dispatch('auth/fetchUser');

                          case 8:
                            _context.next = 10;
                            return _this.$store.dispatch('auth/fetchAbilities');

                          case 10:
                            // Redirect home.
                            // this.$router.push(this.$route.query.redirect || '/');
                            _this.$router.push({
                              name: 'summaryTable'
                            });

                            _this.btnText = 'Login';

                          case 12:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }));

                  return function (_x) {
                    return _ref.apply(this, arguments);
                  };
                }())["catch"](function (err) {
                  _this.btnText = 'Login';
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vanta_top[data-v-a5f8f970]{\n    top: -0px;\n    position: absolute;\n    height: 100%;\n    width: 100%;\n    left: 0px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.fade-enter-active, .fade-leave-active {\r\n    transition: opacity .5s;\n}\n.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */\r\n{\r\n    opacity: 0;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/*@keyframes logo-rotate{to{transform:rotate(-360deg);}}*/\n/*.landing {*/\n/*    background: #111;*/\n/*}*/\n/*.landing__space {*/\n/*    width: 500vw;*/\n/*    height: 400vh;*/\n/*    top: 50%;*/\n/*    left: 50%;*/\n/*    margin-top: -200vh;*/\n/*    margin-left: -200vw;*/\n/*    animation: logo-rotate 200s linear infinite;*/\n/*    background-size: 240px;*/\n/*    -webkit-backface-visibility: visible;*/\n/*    backface-visibility: visible;*/\n/*    background-image: url('/images/download.svg');*/\n/*}*/\n.vanta-top[data-v-12f5395a]{\n        /*top: -50px !important;*/\n        /*background-size: cover;*/\n        /*!* width: 1903px; *!*/\n        /*background-position: center center;*/\n        /*background-repeat: no-repeat;*/\n        /*background-attachment: fixed;*/\n        /*!* height: 969px; *!*/\n        /*width: 100%;*/\n}\n/*add below in node modules if npm will be updated*/\n/*.vanta_top{*/\n/*    top: -0px;*/\n/*    position: absolute;*/\n/*    height: 100%;*/\n/*    width: 100%;*/\n/*    left: 0px;*/\n/*}*/\n.login-form[data-v-12f5395a] {\n        width: 340px;\n        margin: 50px auto;\n        font-size: 15px;\n}\n.login-form form[data-v-12f5395a] {\n        margin-bottom: 15px;\n        background: #f7f7f7;\n        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\n        padding: 30px;\n}\n.login-form h2[data-v-12f5395a] {\n        margin: 0 0 15px;\n}\n.form-control[data-v-12f5395a], .btn[data-v-12f5395a] {\n        min-height: 38px;\n        border-radius: 2px;\n}\n.btn[data-v-12f5395a] {\n        font-size: 15px;\n        font-weight: bold;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../css-loader??ref--6-1!../../vue-loader/lib/loaders/stylePostLoader.js!../../postcss-loader/src??ref--6-2!../../vue-loader/lib??vue-loader-options!./Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-vanta/src/Vanta.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'VueVanta',
  props: {
    effect: {
      type: String,
      default: () => 'birds'
    },
    options: {
      type: Object,
      default: () => ({})
    },
    src: {
      type: String,
      default: () => 'https://cdn.jsdelivr.net/gh/tengbao/vanta@master/dist'
    },
    vantaId: {
      type: String,
      default: () => 'vanta-1'
    }
  },
  beforeMount () {
    this.setup()
  },
  methods: {
    setup () {
      // Normalize the url
      const normalize = string =>
        string.substr(-1) === '/'
          ? string.substr(0, string.length - 1)
          : string
      const url = normalize(this.src)
      // Create the script for threejs
      const threejs = document.createElement('script')
      threejs.async = true
      threejs.defer = true
      threejs.id = 'three'
      threejs.src = `https://cdnjs.cloudflare.com/ajax/libs/three.js/101/three.min.js`
      document.head.appendChild(threejs)

      // Create the script for Vanta
      threejs.onload = () => {
        const vantaScript = document.createElement('script')
        vantaScript.async = true
        vantaScript.defer = true
        vantaScript.id = 'vanta'
        vantaScript.src = `${url}/vanta.${this.effect}.min.js`
        document.head.appendChild(vantaScript)
        vantaScript.onload = () => {
          window.VANTA[this.effect.toUpperCase()](
            Object.assign(this.options, {
              el: `#${this.vantaId}`
            })
          )
        }
      }
    }
  }
});


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=template&id=a5f8f970&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/vue-vanta/src/Vanta.vue?vue&type=template&id=a5f8f970&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", {
      staticClass: "vanta-container",
      class: { vanta_top: true },
      attrs: { id: _vm.vantaId }
    })
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=template&id=332fccf4&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=template&id=332fccf4& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "wrapper " },
      [
        _c("Sidebar"),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "main-panel" },
          [
            _c("Navbar"),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "content" },
              [
                _c("b-breadcrumb", { attrs: { items: _vm.crumbs } }),
                _vm._v(" "),
                _c(
                  "transition",
                  { attrs: { name: "fade" } },
                  [_c("router-view")],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("Footer")
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Footer.vue?vue&type=template&id=e4df7ff6&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Footer.vue?vue&type=template&id=e4df7ff6&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "footer" }, [
      _c("div", { staticClass: "container-fluid" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Navbar.vue?vue&type=template&id=2c4263fa&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Navbar.vue?vue&type=template&id=2c4263fa&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "nav",
    {
      staticClass:
        "navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top "
    },
    [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "navbar-wrapper" }, [
          _c(
            "a",
            { staticClass: "navbar-brand", attrs: { href: "javascript:;" } },
            [_vm._v(_vm._s(_vm.pageName))]
          )
        ]),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "collapse navbar-collapse justify-content-end" },
          [
            _c("ul", { staticClass: "navbar-nav" }, [
              _c("li", { staticClass: "nav-item dropdown" }, [
                _vm.currentUser
                  ? _c(
                      "a",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          href: "javascript:;",
                          id: "navbarDropdownProfile",
                          "data-toggle": "dropdown",
                          "aria-haspopup": "true",
                          "aria-expanded": "false"
                        }
                      },
                      [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("person")
                        ]),
                        _vm._v(
                          "\n                            " +
                            _vm._s(_vm.currentUser.username) +
                            "\n                        "
                        )
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "dropdown-menu dropdown-menu-right",
                    attrs: { "aria-labelledby": "navbarDropdownProfile" }
                  },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "dropdown-item",
                        attrs: { to: { name: "profile" }, exact: "" }
                      },
                      [
                        _vm._v(
                          "\n                             Profile\n                            "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-divider" }),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "dropdown-item",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.logout($event)
                          }
                        }
                      },
                      [_vm._v("Log out")]
                    )
                  ],
                  1
                )
              ])
            ])
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "aria-controls": "navigation-index",
          "aria-expanded": "false",
          "aria-label": "Toggle navigation"
        }
      },
      [
        _c("span", { staticClass: "sr-only" }, [_vm._v("Toggle navigation")]),
        _vm._v(" "),
        _c("span", { staticClass: "navbar-toggler-icon icon-bar" }),
        _vm._v(" "),
        _c("span", { staticClass: "navbar-toggler-icon icon-bar" }),
        _vm._v(" "),
        _c("span", { staticClass: "navbar-toggler-icon icon-bar" })
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Sidebar.vue?vue&type=template&id=65bb863c&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Sidebar.vue?vue&type=template&id=65bb863c&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "sidebar",
      attrs: { "data-color": "azure", "data-background-color": "black" }
    },
    [
      _c("div", {
        staticClass: "sidebar-background",
        staticStyle: { "background-image": "url('/images/budget1.jpg')" }
      }),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "sidebar-wrapper" }, [
        _c(
          "ul",
          { staticClass: "nav" },
          [
            _vm.$can("summary_view")
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-item",
                      attrs: {
                        tag: "li",
                        to: { name: "summaryTable" },
                        exact: ""
                      }
                    },
                    [
                      _c("a", { staticClass: "nav-link" }, [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("dashboard")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("Dashboard")])
                      ])
                    ]
                  )
                ]
              : _vm._e(),
            _vm._v(" "),
            _vm.$can("user_view") &&
            _vm.$can("user_create") &&
            _vm.$can("user_edit") &&
            _vm.$can("user_delete") &&
            _vm.$can("permission_view") &&
            _vm.$can("permission_create") &&
            _vm.$can("permission_edit") &&
            _vm.$can("permission_delete") &&
            _vm.$can("role_view") &&
            _vm.$can("role_create") &&
            _vm.$can("role_edit") &&
            _vm.$can("role_delete")
              ? [
                  _c("li", { staticClass: "nav-item" }, [
                    _vm._m(1),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "collapse",
                        attrs: { id: "userManagement" }
                      },
                      [
                        _c(
                          "ul",
                          { staticClass: "nav" },
                          [
                            _vm.$can("permission_view") &&
                            _vm.$can("permission_create") &&
                            _vm.$can("permission_edit") &&
                            _vm.$can("permission_delete")
                              ? _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-item",
                                    attrs: {
                                      tag: "li",
                                      to: { name: "permissionIndex" },
                                      exact: ""
                                    }
                                  },
                                  [
                                    _c("a", { staticClass: "nav-link" }, [
                                      _c(
                                        "span",
                                        { staticClass: "sidebar-mini" },
                                        [_vm._v(" P ")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "sidebar-normal" },
                                        [_vm._v(" Permissions ")]
                                      )
                                    ])
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.$can("role_view") &&
                            _vm.$can("role_create") &&
                            _vm.$can("role_edit") &&
                            _vm.$can("role_delete")
                              ? _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-item",
                                    attrs: {
                                      tag: "li",
                                      to: { name: "roleIndex" },
                                      exact: ""
                                    }
                                  },
                                  [
                                    _c("a", { staticClass: "nav-link" }, [
                                      _c(
                                        "span",
                                        { staticClass: "sidebar-mini" },
                                        [_vm._v(" R ")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "sidebar-normal" },
                                        [_vm._v(" Roles ")]
                                      )
                                    ])
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.$can("user_view") &&
                            _vm.$can("user_create") &&
                            _vm.$can("user_edit") &&
                            _vm.$can("user_delete")
                              ? _c(
                                  "router-link",
                                  {
                                    staticClass: "nav-item",
                                    attrs: {
                                      tag: "li",
                                      to: { name: "userIndex" },
                                      exact: ""
                                    }
                                  },
                                  [
                                    _c("a", { staticClass: "nav-link" }, [
                                      _c(
                                        "span",
                                        { staticClass: "sidebar-mini" },
                                        [_vm._v(" U ")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "sidebar-normal" },
                                        [_vm._v(" Users ")]
                                      )
                                    ])
                                  ]
                                )
                              : _vm._e()
                          ],
                          1
                        )
                      ]
                    )
                  ])
                ]
              : _vm._e(),
            _vm._v(" "),
            _vm.$can("static_budget_view")
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-item",
                      attrs: {
                        tag: "li",
                        to: { name: "staticIndex" },
                        exact: ""
                      }
                    },
                    [
                      _c("a", { staticClass: "nav-link" }, [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("analytics")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("Budget Data")])
                      ])
                    ]
                  )
                ]
              : _vm._e(),
            _vm._v(" "),
            _vm.$can("budget_details_view")
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-item",
                      attrs: {
                        tag: "li",
                        to: { name: "budgetIndex" },
                        exact: ""
                      }
                    },
                    [
                      _c("a", { staticClass: "nav-link" }, [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("insights")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("Budget Details")])
                      ])
                    ]
                  )
                ]
              : _vm._e(),
            _vm._v(" "),
            _vm.$can("thd_summary_view") && _vm.$can("thd_summary_create")
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-item",
                      attrs: {
                        tag: "li",
                        to: { name: "thdSummaryIndex" },
                        exact: ""
                      }
                    },
                    [
                      _c("a", { staticClass: "nav-link" }, [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("store")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("THD Summary")])
                      ])
                    ]
                  )
                ]
              : _vm._e(),
            _vm._v(" "),
            _vm.$can("gl_accounts_view")
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-item",
                      attrs: {
                        tag: "li",
                        to: { name: "GLAccountTable" },
                        exact: ""
                      }
                    },
                    [
                      _c("a", { staticClass: "nav-link" }, [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("pivot_table_chart")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("GL Accounts")])
                      ])
                    ]
                  )
                ]
              : _vm._e(),
            _vm._v(" "),
            _vm.$can("budget_maintenance_view")
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-item",
                      attrs: {
                        tag: "li",
                        to: { name: "BudgetMaintenanceTable" },
                        exact: ""
                      }
                    },
                    [
                      _c("a", { staticClass: "nav-link" }, [
                        _c("i", { staticClass: "material-icons" }, [
                          _vm._v("analytics")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v("Budget Maintenance")])
                      ])
                    ]
                  )
                ]
              : _vm._e()
          ],
          2
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo" }, [
      _c(
        "a",
        { staticClass: "simple-text logo-normal", attrs: { href: "/" } },
        [
          _c("img", { attrs: { src: "", width: "60" } }),
          _vm._v("\n            VCY Budget vs Actual\n        ")
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "nav-link collapsed",
        attrs: {
          "data-toggle": "collapse",
          href: "#userManagement",
          "aria-expanded": "false"
        }
      },
      [
        _c("i", { staticClass: "material-icons" }, [_vm._v("person")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(" Manage Users\n                            "),
          _c("b", { staticClass: "caret" })
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm.isLogin
      ? _c("div", { staticClass: "row" }, [
          _c("h1", [_vm._v("WELCOME, " + _vm._s(_vm.user.name))])
        ])
      : _c("div", { staticClass: "row" }, [_c("h1", [_vm._v("Welcome Guest")])])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=template&id=12f5395a&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Login.vue?vue&type=template&id=12f5395a&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("jay3DCustom", { attrs: { effect: "rings", options: _vm.options } }),
      _vm._v(" "),
      _c("div", { staticClass: "login-form" }, [
        _c(
          "form",
          {
            staticStyle: {
              width: "30%",
              position: "absolute",
              top: "35%",
              left: "35%"
            },
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.login($event)
              },
              keydown: function($event) {
                return _vm.form.onKeydown($event)
              }
            }
          },
          [
            _c("h2", { staticClass: "text-center" }, [
              _vm._v("VCY Budget vs Actual")
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.email,
                      expression: "form.email"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.form.errors.has("email") },
                  attrs: { type: "text", placeholder: "Email" },
                  domProps: { value: _vm.form.email },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "email", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _c("has-error", { attrs: { form: _vm.form, field: "email" } })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.password,
                      expression: "form.password"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.form.errors.has("password") },
                  attrs: { type: "password", placeholder: "Password" },
                  domProps: { value: _vm.form.password },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "password", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _c("has-error", {
                  attrs: { form: _vm.form, field: "password" }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-block",
                  attrs: { type: "submit", disabled: _vm.form.busy }
                },
                [_vm._v(_vm._s(_vm.btnText) + "\n                    ")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "clearfix" }, [
              _c("label", { staticClass: "float-left form-check-label" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.remember,
                      expression: "form.remember"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.form.remember)
                      ? _vm._i(_vm.form.remember, null) > -1
                      : _vm.form.remember
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.form.remember,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.form, "remember", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.form,
                              "remember",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.form, "remember", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" Remember\n                        me")
              ])
            ])
          ]
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-vanta/src/Vanta.vue":
/*!**********************************************!*\
  !*** ./node_modules/vue-vanta/src/Vanta.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Vanta_vue_vue_type_template_id_a5f8f970_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Vanta.vue?vue&type=template&id=a5f8f970&scoped=true& */ "./node_modules/vue-vanta/src/Vanta.vue?vue&type=template&id=a5f8f970&scoped=true&");
/* harmony import */ var _Vanta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Vanta.vue?vue&type=script&lang=js& */ "./node_modules/vue-vanta/src/Vanta.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Vanta_vue_vue_type_style_index_0_id_a5f8f970_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css& */ "./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css&");
/* harmony import */ var _vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Vanta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Vanta_vue_vue_type_template_id_a5f8f970_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Vanta_vue_vue_type_template_id_a5f8f970_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "a5f8f970",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "node_modules/vue-vanta/src/Vanta.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./node_modules/vue-vanta/src/Vanta.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./node_modules/vue-vanta/src/Vanta.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../vue-loader/lib??vue-loader-options!./Vanta.vue?vue&type=script&lang=js& */ "./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css&":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css& ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_loader_index_js_css_loader_index_js_ref_6_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_2_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_style_index_0_id_a5f8f970_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../style-loader!../../css-loader??ref--6-1!../../vue-loader/lib/loaders/stylePostLoader.js!../../postcss-loader/src??ref--6-2!../../vue-loader/lib??vue-loader-options!./Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=style&index=0&id=a5f8f970&scoped=true&lang=css&");
/* harmony import */ var _style_loader_index_js_css_loader_index_js_ref_6_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_2_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_style_index_0_id_a5f8f970_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_loader_index_js_css_loader_index_js_ref_6_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_2_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_style_index_0_id_a5f8f970_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _style_loader_index_js_css_loader_index_js_ref_6_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_2_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_style_index_0_id_a5f8f970_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _style_loader_index_js_css_loader_index_js_ref_6_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_6_2_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_style_index_0_id_a5f8f970_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./node_modules/vue-vanta/src/Vanta.vue?vue&type=template&id=a5f8f970&scoped=true&":
/*!*****************************************************************************************!*\
  !*** ./node_modules/vue-vanta/src/Vanta.vue?vue&type=template&id=a5f8f970&scoped=true& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vue_loader_lib_loaders_templateLoader_js_vue_loader_options_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_template_id_a5f8f970_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../vue-loader/lib??vue-loader-options!./Vanta.vue?vue&type=template&id=a5f8f970&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/vue-vanta/src/Vanta.vue?vue&type=template&id=a5f8f970&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _vue_loader_lib_loaders_templateLoader_js_vue_loader_options_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_template_id_a5f8f970_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _vue_loader_lib_loaders_templateLoader_js_vue_loader_options_vue_loader_lib_index_js_vue_loader_options_Vanta_vue_vue_type_template_id_a5f8f970_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/vue-vanta/src/index.js":
/*!*********************************************!*\
  !*** ./node_modules/vue-vanta/src/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Vanta_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Vanta.vue */ "./node_modules/vue-vanta/src/Vanta.vue");

/* harmony default export */ __webpack_exports__["default"] = (_Vanta_vue__WEBPACK_IMPORTED_MODULE_0__["default"]);
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.component('v-vanta', _Vanta_vue__WEBPACK_IMPORTED_MODULE_0__["default"]);
}

/***/ }),

/***/ "./resources/js/components/App.vue":
/*!*****************************************!*\
  !*** ./resources/js/components/App.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=332fccf4& */ "./resources/js/components/App.vue?vue&type=template&id=332fccf4&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./resources/js/components/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/App.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/App.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/components/App.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/App.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/App.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/App.vue?vue&type=template&id=332fccf4&":
/*!************************************************************************!*\
  !*** ./resources/js/components/App.vue?vue&type=template&id=332fccf4& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=332fccf4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=template&id=332fccf4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/layout/Footer.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/layout/Footer.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Footer_vue_vue_type_template_id_e4df7ff6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Footer.vue?vue&type=template&id=e4df7ff6&scoped=true& */ "./resources/js/components/layout/Footer.vue?vue&type=template&id=e4df7ff6&scoped=true&");
/* harmony import */ var _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Footer.vue?vue&type=script&lang=js& */ "./resources/js/components/layout/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Footer_vue_vue_type_template_id_e4df7ff6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Footer_vue_vue_type_template_id_e4df7ff6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "e4df7ff6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/layout/Footer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/layout/Footer.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/layout/Footer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/layout/Footer.vue?vue&type=template&id=e4df7ff6&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/layout/Footer.vue?vue&type=template&id=e4df7ff6&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_e4df7ff6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=template&id=e4df7ff6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Footer.vue?vue&type=template&id=e4df7ff6&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_e4df7ff6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_e4df7ff6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/layout/Navbar.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/layout/Navbar.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar_vue_vue_type_template_id_2c4263fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar.vue?vue&type=template&id=2c4263fa&scoped=true& */ "./resources/js/components/layout/Navbar.vue?vue&type=template&id=2c4263fa&scoped=true&");
/* harmony import */ var _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar.vue?vue&type=script&lang=js& */ "./resources/js/components/layout/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Navbar_vue_vue_type_template_id_2c4263fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar_vue_vue_type_template_id_2c4263fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2c4263fa",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/layout/Navbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/layout/Navbar.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/layout/Navbar.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Navbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/layout/Navbar.vue?vue&type=template&id=2c4263fa&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/layout/Navbar.vue?vue&type=template&id=2c4263fa&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_2c4263fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar.vue?vue&type=template&id=2c4263fa&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Navbar.vue?vue&type=template&id=2c4263fa&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_2c4263fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar_vue_vue_type_template_id_2c4263fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/layout/Sidebar.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/layout/Sidebar.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sidebar_vue_vue_type_template_id_65bb863c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=template&id=65bb863c&scoped=true& */ "./resources/js/components/layout/Sidebar.vue?vue&type=template&id=65bb863c&scoped=true&");
/* harmony import */ var _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=script&lang=js& */ "./resources/js/components/layout/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Sidebar_vue_vue_type_template_id_65bb863c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Sidebar_vue_vue_type_template_id_65bb863c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "65bb863c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/layout/Sidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/layout/Sidebar.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/layout/Sidebar.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Sidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/layout/Sidebar.vue?vue&type=template&id=65bb863c&scoped=true&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/layout/Sidebar.vue?vue&type=template&id=65bb863c&scoped=true& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_65bb863c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=template&id=65bb863c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Sidebar.vue?vue&type=template&id=65bb863c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_65bb863c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_65bb863c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/vanta/_base.js":
/*!*************************************!*\
  !*** ./resources/js/vanta/_base.js ***!
  \*************************************/
/*! exports provided: VANTA, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VANTA", function() { return VANTA; });
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers */ "./resources/js/vanta/helpers.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

 // const DEBUGMODE = window.location.toString().indexOf('VANTADEBUG') !== -1

var win = (typeof window === "undefined" ? "undefined" : _typeof(window)) == 'object';
var THREE = win && window.THREE || {};
if (win && !window.VANTA) window.VANTA = {};
var VANTA = win && window.VANTA || {};

VANTA.register = function (name, Effect) {
  return VANTA[name] = function (opts) {
    return new Effect(opts);
  };
};

VANTA.version = '0.5.21';
 // const ORBITCONTROLS = {
//   enableZoom: false,
//   userPanSpeed: 3,
//   userRotateSpeed: 2.0,
//   maxPolarAngle: Math.PI * 0.8, // (pi/2 is pure horizontal)
//   mouseButtons: {
//     ORBIT: THREE.MOUSE.LEFT,
//     ZOOM: null,
//     PAN: null
//   }
// }
// if (DEBUGMODE) {
//   Object.assign(ORBITCONTROLS, {
//     enableZoom: true,
//     zoomSpeed: 4,
//     minDistance: 100,
//     maxDistance: 4500
//   })
// }
// Namespace for errors

var error = function error() {
  Array.prototype.unshift.call(arguments, '[VANTA]');
  return console.error.apply(this, arguments);
};

VANTA.VantaBase = /*#__PURE__*/function () {
  function VantaBase() {
    var userOptions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, VantaBase);

    if (!win) return false;
    VANTA.current = this;
    this.windowMouseMoveWrapper = this.windowMouseMoveWrapper.bind(this);
    this.windowTouchWrapper = this.windowTouchWrapper.bind(this);
    this.windowGyroWrapper = this.windowGyroWrapper.bind(this);
    this.resize = this.resize.bind(this);
    this.animationLoop = this.animationLoop.bind(this);
    this.restart = this.restart.bind(this);
    var defaultOptions = typeof this.getDefaultOptions === 'function' ? this.getDefaultOptions() : this.defaultOptions;
    this.options = Object.assign({
      mouseControls: true,
      touchControls: true,
      gyroControls: false,
      minHeight: 200,
      minWidth: 200,
      scale: 1,
      scaleMobile: 1
    }, defaultOptions);

    if (userOptions instanceof HTMLElement || typeof userOptions === 'string') {
      userOptions = {
        el: userOptions
      };
    }

    Object.assign(this.options, userOptions);

    if (this.options.THREE) {
      THREE = this.options.THREE; // Optionally use a custom build of three.js
    } // Set element


    this.el = this.options.el;

    if (this.el == null) {
      error("Instance needs \"el\" param!");
    } else if (!(this.options.el instanceof HTMLElement)) {
      var selector = this.el;
      this.el = Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["q"])(selector);

      if (!this.el) {
        error("Cannot find element", selector);
        return;
      }
    }

    this.prepareEl();
    this.initThree();
    this.setSize(); // Init needs size

    try {
      this.init();
    } catch (e) {
      // FALLBACK - just use color
      error('Init error', e);

      if (this.renderer && this.renderer.domElement) {
        this.el.removeChild(this.renderer.domElement);
      }

      if (this.options.backgroundColor) {
        console.log('[VANTA] Falling back to backgroundColor');
        this.el.style.background = Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["color2Hex"])(this.options.backgroundColor);
      }

      return;
    } // After init


    this.initMouse(); // Triggers mouse, which needs to be called after init

    this.resize();
    this.animationLoop(); // Event listeners

    var ad = window.addEventListener;
    ad('resize', this.resize);
    window.requestAnimationFrame(this.resize); // Force a resize after the first frame
    // Add event listeners on window, because this element may be below other elements, which would block the element's own mousemove event

    if (this.options.mouseControls) {
      ad('scroll', this.windowMouseMoveWrapper);
      ad('mousemove', this.windowMouseMoveWrapper);
    }

    if (this.options.touchControls) {
      ad('touchstart', this.windowTouchWrapper);
      ad('touchmove', this.windowTouchWrapper);
    }

    if (this.options.gyroControls) {
      ad('deviceorientation', this.windowGyroWrapper);
    }
  }

  _createClass(VantaBase, [{
    key: "setOptions",
    value: function setOptions() {
      var userOptions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      Object.assign(this.options, userOptions);
      this.triggerMouseMove();
    }
  }, {
    key: "prepareEl",
    value: function prepareEl() {
      var i, child; // wrapInner for text nodes, so text nodes can be put into foreground

      if (typeof Node !== 'undefined' && Node.TEXT_NODE) {
        for (i = 0; i < this.el.childNodes.length; i++) {
          var n = this.el.childNodes[i];

          if (n.nodeType === Node.TEXT_NODE) {
            var s = document.createElement('span');
            s.textContent = n.textContent;
            n.parentElement.insertBefore(s, n);
            n.remove();
          }
        }
      } // Set foreground elements


      for (i = 0; i < this.el.children.length; i++) {
        child = this.el.children[i];

        if (getComputedStyle(child).position === 'static') {
          child.style.position = 'relative';
        }

        if (getComputedStyle(child).zIndex === 'auto') {
          child.style.zIndex = 1;
        }
      } // Set canvas and container style


      if (getComputedStyle(this.el).position === 'static') {
        this.el.style.position = 'relative';
      }
    }
  }, {
    key: "applyCanvasStyles",
    value: function applyCanvasStyles(canvasEl) {
      var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      Object.assign(canvasEl.style, {
        position: 'absolute',
        zIndex: 0,
        top: 0,
        left: 0,
        background: ''
      });
      Object.assign(canvasEl.style, opts);
      canvasEl.classList.add('vanta-canvas');
    }
  }, {
    key: "initThree",
    value: function initThree() {
      if (!THREE.WebGLRenderer) {
        console.warn("[VANTA] No THREE defined on window");
        return;
      } // Set renderer


      this.renderer = new THREE.WebGLRenderer({
        alpha: true,
        antialias: true
      });
      this.el.appendChild(this.renderer.domElement);
      this.applyCanvasStyles(this.renderer.domElement);

      if (isNaN(this.options.backgroundAlpha)) {
        this.options.backgroundAlpha = 1;
      }

      this.scene = new THREE.Scene();
    }
  }, {
    key: "getCanvasElement",
    value: function getCanvasElement() {
      if (this.renderer) {
        return this.renderer.domElement; // three.js
      }

      if (this.p5renderer) {
        return this.p5renderer.canvas; // p5
      }
    }
  }, {
    key: "getCanvasRect",
    value: function getCanvasRect() {
      var canvas = this.getCanvasElement();
      if (!canvas) return false;
      return canvas.getBoundingClientRect();
    }
  }, {
    key: "windowMouseMoveWrapper",
    value: function windowMouseMoveWrapper(e) {
      var rect = this.getCanvasRect();
      if (!rect) return false;
      var x = e.clientX - rect.left;
      var y = e.clientY - rect.top;

      if (x >= 0 && y >= 0 && x <= rect.width && y <= rect.height) {
        this.mouseX = x;
        this.mouseY = y;
        if (!this.options.mouseEase) this.triggerMouseMove(x, y);
      }
    }
  }, {
    key: "windowTouchWrapper",
    value: function windowTouchWrapper(e) {
      var rect = this.getCanvasRect();
      if (!rect) return false;

      if (e.touches.length === 1) {
        var x = e.touches[0].clientX - rect.left;
        var y = e.touches[0].clientY - rect.top;

        if (x >= 0 && y >= 0 && x <= rect.width && y <= rect.height) {
          this.mouseX = x;
          this.mouseY = y;
          if (!this.options.mouseEase) this.triggerMouseMove(x, y);
        }
      }
    }
  }, {
    key: "windowGyroWrapper",
    value: function windowGyroWrapper(e) {
      var rect = this.getCanvasRect();
      if (!rect) return false;
      var x = Math.round(e.alpha * 2) - rect.left;
      var y = Math.round(e.beta * 2) - rect.top;

      if (x >= 0 && y >= 0 && x <= rect.width && y <= rect.height) {
        this.mouseX = x;
        this.mouseY = y;
        if (!this.options.mouseEase) this.triggerMouseMove(x, y);
      }
    }
  }, {
    key: "triggerMouseMove",
    value: function triggerMouseMove(x, y) {
      if (x === undefined && y === undefined) {
        // trigger at current position
        if (this.options.mouseEase) {
          x = this.mouseEaseX;
          y = this.mouseEaseY;
        } else {
          x = this.mouseX;
          y = this.mouseY;
        }
      }

      if (this.uniforms) {
        this.uniforms.iMouse.value.x = x / this.scale; // pixel values

        this.uniforms.iMouse.value.y = y / this.scale; // pixel values
      }

      var xNorm = x / this.width; // 0 to 1

      var yNorm = y / this.height; // 0 to 1

      typeof this.onMouseMove === "function" ? this.onMouseMove(xNorm, yNorm) : void 0;
    }
  }, {
    key: "setSize",
    value: function setSize() {
      this.scale || (this.scale = 1);

      if (Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["mobileCheck"])() && this.options.scaleMobile) {
        this.scale = this.options.scaleMobile;
      } else if (this.options.scale) {
        this.scale = this.options.scale;
      }

      this.width = Math.max(this.el.offsetWidth, this.options.minWidth);
      this.height = Math.max(this.el.offsetHeight, this.options.minHeight);
    }
  }, {
    key: "initMouse",
    value: function initMouse() {
      // Init mouseX and mouseY
      if (!this.mouseX && !this.mouseY || this.mouseX === this.options.minWidth / 2 && this.mouseY === this.options.minHeight / 2) {
        this.mouseX = this.width / 2;
        this.mouseY = this.height / 2;
        this.triggerMouseMove(this.mouseX, this.mouseY);
      }
    }
  }, {
    key: "resize",
    value: function resize() {
      this.setSize();

      if (this.camera) {
        this.camera.aspect = this.width / this.height;

        if (typeof this.camera.updateProjectionMatrix === "function") {
          this.camera.updateProjectionMatrix();
        }
      }

      if (this.renderer) {
        this.renderer.setSize(this.width, this.height);
        this.renderer.setPixelRatio(window.devicePixelRatio / this.scale);
      }

      typeof this.onResize === "function" ? this.onResize() : void 0;
    }
  }, {
    key: "isOnScreen",
    value: function isOnScreen() {
      var elHeight = this.el.offsetHeight;
      var elRect = this.el.getBoundingClientRect();
      var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
      var offsetTop = elRect.top + scrollTop;
      var minScrollTop = offsetTop - window.innerHeight;
      var maxScrollTop = offsetTop + elHeight;
      return minScrollTop <= scrollTop && scrollTop <= maxScrollTop;
    }
  }, {
    key: "animationLoop",
    value: function animationLoop() {
      // Step time
      this.t || (this.t = 0);
      this.t += 1; // Uniform time

      this.t2 || (this.t2 = 0);
      this.t2 += this.options.speed || 1;

      if (this.uniforms) {
        this.uniforms.iTime.value = this.t2 * 0.016667; // iTime is in seconds
      }

      if (this.options.mouseEase) {
        this.mouseEaseX = this.mouseEaseX || this.mouseX || 0;
        this.mouseEaseY = this.mouseEaseY || this.mouseY || 0;

        if (Math.abs(this.mouseEaseX - this.mouseX) + Math.abs(this.mouseEaseY - this.mouseY) > 0.1) {
          this.mouseEaseX += (this.mouseX - this.mouseEaseX) * 0.05;
          this.mouseEaseY += (this.mouseY - this.mouseEaseY) * 0.05;
          this.triggerMouseMove(this.mouseEaseX, this.mouseEaseY);
        }
      } // Only animate if element is within view


      if (this.isOnScreen() || this.options.forceAnimate) {
        if (typeof this.onUpdate === "function") {
          this.onUpdate();
        }

        if (this.scene && this.camera) {
          this.renderer.render(this.scene, this.camera);
          this.renderer.setClearColor(this.options.backgroundColor, this.options.backgroundAlpha);
        } // if (this.stats) this.stats.update()
        // if (this.renderStats) this.renderStats.update(this.renderer)


        if (this.fps && this.fps.update) this.fps.update();
        if (typeof this.afterRender === "function") this.afterRender();
      }

      return this.req = window.requestAnimationFrame(this.animationLoop);
    } // setupControls() {
    //   if (DEBUGMODE && THREE.OrbitControls) {
    //     this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement)
    //     Object.assign(this.controls, ORBITCONTROLS)
    //     return this.scene.add(new THREE.AxisHelper(100))
    //   }
    // }

  }, {
    key: "restart",
    value: function restart() {
      // Restart the effect without destroying the renderer
      if (this.scene) {
        while (this.scene.children.length) {
          this.scene.remove(this.scene.children[0]);
        }
      }

      if (typeof this.onRestart === "function") {
        this.onRestart();
      }

      this.init();
    }
  }, {
    key: "init",
    value: function init() {
      if (typeof this.onInit === "function") {
        this.onInit();
      } // this.setupControls()

    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (typeof this.onDestroy === "function") {
        this.onDestroy();
      }

      var rm = window.removeEventListener;
      rm('touchstart', this.windowTouchWrapper);
      rm('touchmove', this.windowTouchWrapper);
      rm('scroll', this.windowMouseMoveWrapper);
      rm('mousemove', this.windowMouseMoveWrapper);
      rm('deviceorientation', this.windowGyroWrapper);
      rm('resize', this.resize);
      window.cancelAnimationFrame(this.req);
      var scene = this.scene;

      if (scene && scene.children) {
        Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["clearThree"])(scene);
      }

      if (this.renderer) {
        if (this.renderer.domElement) {
          this.el.removeChild(this.renderer.domElement);
        }

        this.renderer = null;
        this.scene = null;
      }

      if (VANTA.current === this) {
        VANTA.current = null;
      }
    }
  }]);

  return VantaBase;
}();

/* harmony default export */ __webpack_exports__["default"] = (VANTA.VantaBase);

/***/ }),

/***/ "./resources/js/vanta/helpers.js":
/*!***************************************!*\
  !*** ./resources/js/vanta/helpers.js ***!
  \***************************************/
/*! exports provided: mobileCheck, sample, rn, ri, q, color2Hex, color2Rgb, getBrightness, clearThree */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mobileCheck", function() { return mobileCheck; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sample", function() { return sample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rn", function() { return rn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ri", function() { return ri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "q", function() { return q; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "color2Hex", function() { return color2Hex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "color2Rgb", function() { return color2Rgb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBrightness", function() { return getBrightness; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clearThree", function() { return clearThree; });
Number.prototype.clamp = function (min, max) {
  return Math.min(Math.max(this, min), max);
}; // # module.exports = helpers


function mobileCheck() {
  if (typeof navigator !== 'undefined') {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.innerWidth < 600;
  }

  return null;
}
var sample = function sample(items) {
  return items[Math.floor(Math.random() * items.length)];
};
function rn(start, end) {
  if (start == null) start = 0;
  if (end == null) end = 1;
  return start + Math.random() * (end - start);
}
function ri(start, end) {
  if (start == null) start = 0;
  if (end == null) end = 1;
  return Math.floor(start + Math.random() * (end - start + 1));
}
var q = function q(sel) {
  return document.querySelector(sel);
};
var color2Hex = function color2Hex(color) {
  if (typeof color == 'number') {
    return '#' + ('00000' + color.toString(16)).slice(-6);
  } else return color;
};
var color2Rgb = function color2Rgb(color) {
  var alpha = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var hex = color2Hex(color);
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  var obj = result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
  return 'rgba(' + obj.r + ',' + obj.g + ',' + obj.b + ',' + alpha + ')';
};
var getBrightness = function getBrightness(threeColor) {
  return 0.299 * threeColor.r + 0.587 * threeColor.g + 0.114 * threeColor.b;
};
function clearThree(obj) {
  // https://stackoverflow.com/questions/30359830/how-do-i-clear-three-js-scene/48722282
  while (obj.children && obj.children.length > 0) {
    clearThree(obj.children[0]);
    obj.remove(obj.children[0]);
  }

  if (obj.geometry) obj.geometry.dispose();

  if (obj.material) {
    // in case of map, bumpMap, normalMap, envMap ...
    Object.keys(obj.material).forEach(function (prop) {
      if (!obj.material[prop]) return;

      if (obj.material[prop] !== null && typeof obj.material[prop].dispose === 'function') {
        obj.material[prop].dispose();
      }
    });
    obj.material.dispose();
  }
}

/***/ }),

/***/ "./resources/js/vanta/vanta.net.js":
/*!*****************************************!*\
  !*** ./resources/js/vanta/vanta.net.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_base */ "./resources/js/vanta/_base.js");
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers */ "./resources/js/vanta/helpers.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }



var win = (typeof window === "undefined" ? "undefined" : _typeof(window)) == 'object';
var THREE = win && window.THREE;

var Effect = /*#__PURE__*/function (_VantaBase) {
  _inherits(Effect, _VantaBase);

  var _super = _createSuper(Effect);

  _createClass(Effect, null, [{
    key: "initClass",
    value: function initClass() {
      this.prototype.defaultOptions = {
        color: 0xff3f81,
        backgroundColor: 0x23153c,
        points: 10,
        maxDistance: 20,
        spacing: 15,
        showDots: true
      };
    }
  }]);

  function Effect(userOptions) {
    _classCallCheck(this, Effect);

    THREE = userOptions.THREE || THREE;
    return _super.call(this, userOptions);
  } // onInit() {
  //   this.geometry = new THREE.BoxGeometry( 10, 10, 10 );
  //   this.material = new THREE.MeshLambertMaterial({
  //     color: this.options.color,
  //     emissive: this.options.color,
  //     emissiveIntensity: 0.75
  //   });
  //   this.cube = new THREE.Mesh( this.geometry, this.material );
  //   this.scene.add(this.cube);
  //   const c = this.camera = new THREE.PerspectiveCamera( 75, this.width/this.height, 0.1, 1000 );
  //   c.position.z = 30;
  //   c.lookAt(0,0,0);
  //   this.scene.add(c);
  //   const light = new THREE.HemisphereLight( 0xffffff, this.options.backgroundColor , 1 );
  //   this.scene.add(light);
  // }
  // onUpdate() {
  //   this.cube.rotation.x += 0.01;
  //   this.cube.rotation.y += 0.01;
  // }


  _createClass(Effect, [{
    key: "genPoint",
    value: function genPoint(x, y, z) {
      var sphere;

      if (!this.points) {
        this.points = [];
      }

      if (this.options.showDots) {
        var geometry = new THREE.SphereGeometry(0.25, 12, 12); // radius, width, height

        var material = new THREE.MeshLambertMaterial({
          color: this.options.color
        });
        sphere = new THREE.Mesh(geometry, material);
      } else {
        sphere = new THREE.Object3D();
      }

      this.cont.add(sphere);
      sphere.ox = x;
      sphere.oy = y;
      sphere.oz = z;
      sphere.position.set(x, y, z);
      sphere.r = Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["rn"])(-2, 2); // rotation rate

      return this.points.push(sphere);
    }
  }, {
    key: "onInit",
    value: function onInit() {
      this.cont = new THREE.Group();
      this.cont.position.set(0, 0, 0);
      this.scene.add(this.cont);
      var n = this.options.points;
      var spacing = this.options.spacing;

      if (Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["mobileCheck"])()) {
        n = ~~(n * 0.75);
        spacing = ~~(spacing * 0.65);
      }

      var numPoints = n * n * 2;
      this.linePositions = new Float32Array(numPoints * numPoints * 3);
      this.lineColors = new Float32Array(numPoints * numPoints * 3);
      var colorB = Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["getBrightness"])(new THREE.Color(this.options.color));
      var bgB = Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["getBrightness"])(new THREE.Color(this.options.backgroundColor));
      this.blending = colorB > bgB ? 'additive' : 'subtractive';
      var geometry = new THREE.BufferGeometry();
      geometry.setAttribute('position', new THREE.BufferAttribute(this.linePositions, 3).setUsage(THREE.DynamicDrawUsage));
      geometry.setAttribute('color', new THREE.BufferAttribute(this.lineColors, 3).setUsage(THREE.DynamicDrawUsage));
      geometry.computeBoundingSphere();
      geometry.setDrawRange(0, 0);
      var material = new THREE.LineBasicMaterial({
        vertexColors: THREE.VertexColors,
        blending: this.blending === 'additive' ? THREE.AdditiveBlending : null,
        // blending: THREE.SubtractiveBlending
        transparent: true
      }); // blending: THREE.CustomBlending
      // blendEquation: THREE.SubtractEquation
      // blendSrc: THREE.SrcAlphaFactor
      // blendDst: THREE.OneMinusSrcAlphaFactor

      this.linesMesh = new THREE.LineSegments(geometry, material);
      this.cont.add(this.linesMesh);

      for (var i = 0; i <= n; i++) {
        for (var j = 0; j <= n; j++) {
          var y = Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(-3, 3);
          var x = (i - n / 2) * spacing + Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(-5, 5);
          var z = (j - n / 2) * spacing + Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(-5, 5);

          if (i % 2) {
            z += spacing * 0.5;
          } // offset
          // nexusX = Math.round(x / 20) * 20
          // nexusZ = Math.round(z / 20) * 20
          // x += (nexusX - x) * 0.01
          // z += (nexusZ - z) * 0.01


          this.genPoint(x, y - Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(5, 15), z);
          this.genPoint(x + Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(-5, 5), y + Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(5, 15), z + Object(_helpers__WEBPACK_IMPORTED_MODULE_1__["ri"])(-5, 5));
        }
      } //  # radius
      //   width, # width
      //   rn(0,1000), # startAng
      //   rn(1,6), # ang
      //   rn(0, 50/(radius+1) + 5) + 5/width/(radius+0.5), # y
      //   Math.max(-rn(0.5,2), rn(1, 50-radius/2) - radius/2) * 0.25 # speed
      // )
      // PerspectiveCamera( fov, aspect, near, far )


      this.camera = new THREE.PerspectiveCamera(25, this.width / this.height, 0.01, 10000);
      this.camera.position.set(50, 100, 150);
      this.scene.add(this.camera); // ambience = new THREE.AmbientLight(0xffffff, 0.01)
      // @scene.add(ambience)
      // @pointLight = new THREE.PointLight(0xFFFFFF, 0.01)
      // @pointLight.position.set(0, 150, 200)
      // @scene.add( @pointLight )

      var ambience = new THREE.AmbientLight(0xffffff, 0.75);
      this.scene.add(ambience);
      this.spot = new THREE.SpotLight(0xFFFFFF, 1);
      this.spot.position.set(0, 200, 0);
      this.spot.distance = 400;
      this.spot.target = this.cont;
      return this.scene.add(this.spot);
    }
  }, {
    key: "onDestroy",
    value: function onDestroy() {
      if (this.scene) this.scene.remove(this.linesMesh);
      this.spot = this.points = this.linesMesh = this.lineColors = this.linePositions = null;
    }
  }, {
    key: "setOptions",
    value: function setOptions(userOptions) {
      // allow setOptions to change point colors
      _get(_getPrototypeOf(Effect.prototype), "setOptions", this).call(this, userOptions);

      if (userOptions.color) {
        this.points.forEach(function (p) {
          p.material.color = new THREE.Color(userOptions.color);
        });
      }
    }
  }, {
    key: "onUpdate",
    value: function onUpdate() {
      var diff, t;
      var c = this.camera;

      if (Math.abs(c.tx - c.position.x) > 0.01) {
        diff = c.tx - c.position.x;
        c.position.x += diff * 0.02;
      }

      if (Math.abs(c.ty - c.position.y) > 0.01) {
        diff = c.ty - c.position.y;
        c.position.y += diff * 0.02;
      }

      c.lookAt(new THREE.Vector3(0, 0, 0)); // c.near = 0.01
      // c.updateProjectionMatrix()

      var vertexpos = 0;
      var colorpos = 0;
      var numConnected = 0;
      var bgColor = new THREE.Color(this.options.backgroundColor);
      var color = new THREE.Color(this.options.color);
      var diffColor = color.clone().sub(bgColor);

      if (this.rayCaster) {
        this.rayCaster.setFromCamera(new THREE.Vector2(this.rcMouseX, this.rcMouseY), this.camera);
      } // # TEMPORARY RAY DRAWING
      // pointA = @camera.position
      // direction = @rayCaster.ray.direction
      // direction.normalize()
      // distance = 1000000 # at what distance to determine pointB
      // pointB = new THREE.Vector3()
      // pointB.addVectors( pointA, direction.multiplyScalar( distance ) )
      // geometry = new THREE.Geometry()
      // geometry.vertices.push( pointA )
      // geometry.vertices.push( pointB )
      // material = new THREE.LineBasicMaterial( { color : 0xffffff } )
      // line = new THREE.Line( geometry, material )
      // @scene.add( line )


      for (var i = 0; i < this.points.length; i++) {
        var dist = void 0,
            distToMouse = void 0;
        var p = this.points[i]; // p.position.y += Math.sin(@t * 0.005 - 0.02 * p.ox + 0.015 * p.oz) * 0.02

        if (this.rayCaster) {
          distToMouse = this.rayCaster.ray.distanceToPoint(p.position);
        } else {
          distToMouse = 1000;
        }

        var distClamp = distToMouse.clamp(5, 15);
        p.scale.x = p.scale.y = p.scale.z = ((15 - distClamp) * 0.25).clamp(1, 100);

        if (p.r !== 0) {
          var ang = Math.atan2(p.position.z, p.position.x);
          dist = Math.sqrt(p.position.z * p.position.z + p.position.x * p.position.x);
          ang += 0.00025 * p.r;
          p.position.x = dist * Math.cos(ang);
          p.position.z = dist * Math.sin(ang);
        } // p.position.x += Math.sin(@t * 0.01 + p.position.y) * 0.02
        // p.position.z += Math.sin(@t * 0.01 - p.position.y) * 0.02


        for (var j = i; j < this.points.length; j++) {
          var p2 = this.points[j];
          var dx = p.position.x - p2.position.x;
          var dy = p.position.y - p2.position.y;
          var dz = p.position.z - p2.position.z;
          dist = Math.sqrt(dx * dx + dy * dy + dz * dz);

          if (dist < this.options.maxDistance) {
            var lineColor = void 0;
            var alpha = ((1.0 - dist / this.options.maxDistance) * 2).clamp(0, 1);

            if (this.blending === 'additive') {
              lineColor = new THREE.Color(0x000000).lerp(diffColor, alpha);
            } else {
              lineColor = bgColor.clone().lerp(color, alpha);
            } // if @blending == 'subtractive'
            //   lineColor = new THREE.Color(0x000000).lerp(diffColor, alpha)


            this.linePositions[vertexpos++] = p.position.x;
            this.linePositions[vertexpos++] = p.position.y;
            this.linePositions[vertexpos++] = p.position.z;
            this.linePositions[vertexpos++] = p2.position.x;
            this.linePositions[vertexpos++] = p2.position.y;
            this.linePositions[vertexpos++] = p2.position.z;
            this.lineColors[colorpos++] = lineColor.r;
            this.lineColors[colorpos++] = lineColor.g;
            this.lineColors[colorpos++] = lineColor.b;
            this.lineColors[colorpos++] = lineColor.r;
            this.lineColors[colorpos++] = lineColor.g;
            this.lineColors[colorpos++] = lineColor.b;
            numConnected++;
          }
        }
      }

      this.linesMesh.geometry.setDrawRange(0, numConnected * 2);
      this.linesMesh.geometry.attributes.position.needsUpdate = true;
      this.linesMesh.geometry.attributes.color.needsUpdate = true; // @pointCloud.geometry.attributes.position.needsUpdate = true

      return this.t * 0.001;
    }
  }, {
    key: "onMouseMove",
    value: function onMouseMove(x, y) {
      var c = this.camera;

      if (!c.oy) {
        c.oy = c.position.y;
        c.ox = c.position.x;
        c.oz = c.position.z;
      }

      var ang = Math.atan2(c.oz, c.ox);
      var dist = Math.sqrt(c.oz * c.oz + c.ox * c.ox);
      var tAng = ang + (x - 0.5) * 2 * (this.options.mouseCoeffX || 1);
      c.tz = dist * Math.sin(tAng);
      c.tx = dist * Math.cos(tAng);
      c.ty = c.oy + (y - 0.5) * 50 * (this.options.mouseCoeffY || 1);

      if (!this.rayCaster) {// this.rayCaster = new THREE.Raycaster()
      }

      this.rcMouseX = x * 2 - 1;
      this.rcMouseY = -x * 2 + 1;
    }
  }, {
    key: "onRestart",
    value: function onRestart() {
      if (this.scene) this.scene.remove(this.linesMesh);
      this.points = [];
    }
  }]);

  return Effect;
}(_base__WEBPACK_IMPORTED_MODULE_0__["default"]);

Effect.initClass();
/* harmony default export */ __webpack_exports__["default"] = (_base__WEBPACK_IMPORTED_MODULE_0__["VANTA"].register('NET', Effect));

/***/ }),

/***/ "./resources/js/views/Home.vue":
/*!*************************************!*\
  !*** ./resources/js/views/Home.vue ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_63cd6604_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=63cd6604&scoped=true& */ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&scoped=true&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./resources/js/views/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_63cd6604_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_63cd6604_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "63cd6604",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Home.vue?vue&type=script&lang=js&":
/*!**************************************************************!*\
  !*** ./resources/js/views/Home.vue?vue&type=script&lang=js& ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&scoped=true&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/Home.vue?vue&type=template&id=63cd6604&scoped=true& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=63cd6604&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/Login.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Login.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_12f5395a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=12f5395a&scoped=true& */ "./resources/js/views/Login.vue?vue&type=template&id=12f5395a&scoped=true&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/views/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Login_vue_vue_type_style_index_0_id_12f5395a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css& */ "./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_12f5395a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_12f5395a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "12f5395a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_12f5395a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=style&index=0&id=12f5395a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_12f5395a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_12f5395a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_12f5395a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_12f5395a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=template&id=12f5395a&scoped=true&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=template&id=12f5395a&scoped=true& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_12f5395a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=12f5395a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Login.vue?vue&type=template&id=12f5395a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_12f5395a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_12f5395a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);