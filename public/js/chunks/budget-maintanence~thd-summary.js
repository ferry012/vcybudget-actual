(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["budget-maintanence~thd-summary"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "SummaryBreakDown",
  props: {
    dt_tableHeaders_per_month: {
      type: Array,
      required: true
    },
    dt_tableHeaders_per_month_2: {
      type: Array,
      required: true
    },
    dt_tableClassHeaders: {
      type: String,
      "default": 'text-primary'
    },
    dt_itemEntriesOption: {
      type: Array,
      "default": function _default() {
        return [5, 25, 100];
      }
    },
    dt_classTable: {
      type: String,
      "default": 'table'
    },
    dt_TableWidth: {
      type: String,
      "default": '100'
    },
    dt_TableResponsive: {
      type: Boolean,
      "default": false
    },
    dt_currentSort: {
      type: String,
      "default": 'name'
    },
    dt_currentSortDir: {
      type: String,
      "default": 'asc'
    },
    dt_defaultEntries: {
      type: Number,
      required: true
    },
    dt_Search: {
      type: String
    }
  },
  data: function data() {
    return {
      entrySelected: this.dt_defaultEntries,
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      timer: null
    };
  },
  methods: {
    sort: function sort(s) {
      var currentSortDir = this.dt_currentSortDir;

      if (s === this.dt_currentSort) {
        currentSortDir = this.dt_currentSortDir === 'asc' ? 'desc' : 'asc';

        if (currentSortDir === 'asc') {
          this.sortClassActive.activeAsc = true;
          this.sortClassActive.activeDesc = false;
        } else {
          this.sortClassActive.activeAsc = false;
          this.sortClassActive.activeDesc = true;
        }
      } // this.currentSort = s;


      this.$emit("sortColumn", {
        sortBy: s,
        sortDir: currentSortDir
      });
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.entrySelected = selected;
      this.$emit("perPageEntry", this.entrySelected);
    },
    searchData: function searchData(event) {
      this.$emit("searchItem", event.target.value);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "THDSummaryDataTable",
  props: {
    dt_tableHeaders: {
      type: Array,
      required: true
    },
    dt_tableHeaders_2: {
      type: Array,
      required: true
    },
    dt_tableHeaders_3: {
      type: Array,
      required: true
    },
    dt_tableHeaders_4: {
      type: Array,
      required: true
    },
    table_salaries: {
      type: Array,
      required: true
    },
    table_fuels_and_selling_exp: {
      type: Array,
      required: true
    },
    table_fuels: {
      type: Array,
      required: true
    },
    table_maintenance: {
      type: Array,
      required: true
    },
    table_repairs: {
      type: Array,
      required: true
    },
    table_parking: {
      type: Array,
      required: true
    },
    table_tolling: {
      type: Array,
      required: true
    },
    table_utilities: {
      type: Array,
      required: true
    },
    table_admin_expenses: {
      type: Array,
      required: true
    },
    table_operation_exp: {
      type: Array,
      required: true
    },
    dt_tableClassHeaders: {
      type: String,
      "default": 'text-primary'
    },
    dt_itemEntriesOption: {
      type: Array,
      "default": function _default() {
        return [5, 25, 100];
      }
    },
    dt_classTable: {
      type: String,
      "default": 'table'
    },
    dt_TableWidth: {
      type: String,
      "default": '100'
    },
    dt_TableResponsive: {
      type: Boolean,
      "default": false
    },
    dt_currentSort: {
      type: String,
      "default": 'name'
    },
    dt_currentSortDir: {
      type: String,
      "default": 'asc'
    },
    dt_defaultEntries: {
      type: Number,
      required: true
    },
    dt_Search: {
      type: String
    }
  },
  data: function data() {
    return {
      entrySelected: this.dt_defaultEntries,
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      timer: null,
      allowedCompany: [],
      allowedCompanyIndex: [],
      isIndex: 0,
      totalString: 'TOTAL'
    };
  },
  mounted: function mounted() {
    this.showCompanyByPermission();
  },
  methods: {
    showCompanyByPermission: function showCompanyByPermission() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var arr, arr_index, isLength, hil, sib, rox, sum, diver, jar, kab, cs, hil_index, sib_index, rox_index, sum_index, diver_index, jar_index, kab_index, cs_index, x, y, countArr, arrCheck;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                arr = [];
                arr_index = [];
                isLength = _this.currentPermission.length;
                hil = 0;
                sib = 0;
                rox = 0;
                sum = 0;
                diver = 0;
                jar = 0;
                kab = 0;
                cs = 0;
                hil_index = 0;
                sib_index = 0;
                rox_index = 0;
                sum_index = 0;
                diver_index = 0;
                jar_index = 0;
                kab_index = 0;
                cs_index = 0;
                x = 0;

                do {
                  if (_this.currentPermission[x] === 'hilado_company_thd_summary') {
                    hil = 'HIL'; //1231

                    hil_index = 1;
                  } else if (_this.currentPermission[x] === 'sibulan_company_thd_summary') {
                    sib = 'SIB'; //1232

                    sib_index = 2;
                  } else if (_this.currentPermission[x] === 'roxas_company_thd_summary') {
                    rox = 'ROX'; //1233

                    rox_index = 3;
                  } else if (_this.currentPermission[x] === 'sumag_company_thd_summary') {
                    sum = 'SUM'; //1234

                    sum_index = 4;
                  } else if (_this.currentPermission[x] === 'diversion_company_thd_summary') {
                    diver = 'DIV'; //1235

                    diver_index = 5;
                  } else if (_this.currentPermission[x] === 'jaro_company_thd_summary') {
                    jar = 'JAR'; //1236

                    jar_index = 6;
                  } else if (_this.currentPermission[x] === 'kabankalan_company_thd_summary') {
                    kab = 'KAB'; //1237

                    kab_index = 7;
                  } else if (_this.currentPermission[x] === 'corporate_services_company_thd_summary') {
                    cs = 'CS'; //1901

                    cs_index = 8;
                  }

                  if (hil !== 0 || sib !== 0 || rox !== 0 || sum !== 0 || diver !== 0 || jar !== 0 || kab !== 0 || cs !== 0) {
                    arr = [hil, sib, rox, sum, diver, jar, kab, cs];
                  }

                  if (hil_index !== 0 || sib_index !== 0 || rox_index !== 0 || sum_index !== 0 || diver_index !== 0 || jar_index !== 0 || kab_index !== 0 || cs_index !== 0) {
                    arr_index = [hil_index, sib_index, rox_index, sum_index, diver_index, jar_index, kab_index, cs_index];
                  }

                  x++;
                } while (x <= isLength); // console.log(arr_index,'arr_index');


                console.log(arr, 'arr');
                _context.next = 24;
                return _this.removeCompanyIsNotPermitted(arr, 0);

              case 24:
                _context.next = 26;
                return _this.removeCompanyIsNotPermittedIndex(arr_index, 0);

              case 26:
                y = 0;
                countArr = arr.length; // checking if length is equal to 1 replace the value to 0. Since the loop will execute twice if the value is 1. In the loop it will look like this index[0,1]

                if (countArr === 1) countArr = 0;
                arrCheck = [];

                while (y <= countArr) {
                  if (arr[y] !== 0) {
                    arrCheck[y] = [arr[y]];
                  }

                  y++;
                }

                console.log(arrCheck, 'arrCheck');

                if (arrCheck.length <= 1) {
                  _this.isIndex = 9;
                  _this.totalString = '';
                } // let checkThis = this.dt_tableHeaders_4.includes()
                // let y = 0;
                // let yy = 0;
                //first loop It enters both outer and inner loops, showing the desired output for the first line. You end up with y = 1 and yy = 8.
                //second loop Since y is 1, it doesn't leave the outer loop, but yy is 8, so it doesn't enter the inner loop again.
                //last loop It then keeps adding 1 to y until it doesn't match the outer loop condition anymore and leaves you with that unwanted result.
                // let includeThis = [];
                // let testHeaders = [];
                // let y_1 = null;
                //
                // while(y < this.dt_tableHeaders_4.length){
                //     while(yy < this.allowedCompany.length){
                //         if(this.dt_tableHeaders_4[y].column_name === this.allowedCompany[yy]){
                //             includeThis.push(this.allowedCompany[yy]);
                //         }
                //         yy++;
                //     }
                //     yy = 0;
                //     y++;
                // }
                // console.log(includeThis);


              case 33:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    removeCompanyIsNotPermitted: function removeCompanyIsNotPermitted(arr, value) {
      var i = 0;

      while (i < arr.length) {
        if (arr[i] === value) {
          arr.splice(i, 1);
        } else {
          ++i;
        }
      }

      this.allowedCompany = arr;
      return this.allowedCompany;
    },
    removeCompanyIsNotPermittedIndex: function removeCompanyIsNotPermittedIndex(arr, value) {
      var i = 0;

      while (i < arr.length) {
        if (arr[i] === value) {
          arr.splice(i, 1);
        } else {
          ++i;
        }
      }

      this.allowedCompanyIndex = arr;
      return this.allowedCompanyIndex;
    },
    sort: function sort(s) {
      var currentSortDir = this.dt_currentSortDir;

      if (s === this.dt_currentSort) {
        currentSortDir = this.dt_currentSortDir === 'asc' ? 'desc' : 'asc';

        if (currentSortDir === 'asc') {
          this.sortClassActive.activeAsc = true;
          this.sortClassActive.activeDesc = false;
        } else {
          this.sortClassActive.activeAsc = false;
          this.sortClassActive.activeDesc = true;
        }
      } // this.currentSort = s;


      this.$emit("sortColumn", {
        sortBy: s,
        sortDir: currentSortDir
      });
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.entrySelected = selected;
      this.$emit("perPageEntry", this.entrySelected);
    },
    searchData: function searchData(event) {
      this.$emit("searchItem", event.target.value);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.filter-asc[data-v-ae3088d4] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-ae3088d4], .filter-desc[data-v-ae3088d4] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-ae3088d4] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.active-filter-asc[data-v-ae3088d4] {\n    border-bottom: 5px solid #a3a3a3\n}\n.active-filter-desc[data-v-ae3088d4] {\n    border-top: 5px solid #a3a3a3\n}\n.inline-block[data-v-ae3088d4] {\n    display: inline-block\n}\n.dark_sky_blue_last_four[data-v-ae3088d4]{\n    background-color: #1c7ea6;\n}\n.dark_sky_blue_last_two[data-v-ae3088d4]{\n    background-color: #2197c7;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.filter-asc[data-v-e17b6d7a] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-e17b6d7a], .filter-desc[data-v-e17b6d7a] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-e17b6d7a] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.active-filter-asc[data-v-e17b6d7a] {\n    border-bottom: 5px solid #a3a3a3\n}\n.active-filter-desc[data-v-e17b6d7a] {\n    border-top: 5px solid #a3a3a3\n}\n.inline-block[data-v-e17b6d7a] {\n    display: inline-block\n}\n.dark_sky_blue_last_four[data-v-e17b6d7a]{\n    background-color: #1c7ea6;\n}\n.dark_sky_blue_last_two[data-v-e17b6d7a]{\n    background-color: #2197c7;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row form-group mb-5" }),
    _vm._v(" "),
    _c("div", { staticClass: "table-responsive" }, [
      _c(
        "table",
        { class: _vm.dt_classTable, attrs: { width: _vm.dt_TableWidth + "%" } },
        [
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_per_month, function(th) {
                    return [
                      th.sortable
                        ? _c(
                            "th",
                            {
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass: "inline-block",
                                  staticStyle: { cursor: "pointer" },
                                  on: {
                                    click: function($event) {
                                      return _vm.sort(th.query_name)
                                    }
                                  }
                                },
                                [
                                  _c("div", {
                                    staticClass: "filter-asc",
                                    class: {
                                      "active-filter-asc":
                                        _vm.sortClassActive.activeAsc
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("div", {
                                    staticClass: "filter-desc",
                                    class: {
                                      "active-filter-desc":
                                        _vm.sortClassActive.activeDesc
                                    }
                                  })
                                ]
                              ),
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n\n                    "
                              )
                            ]
                          )
                        : _c(
                            "th",
                            {
                              class: [
                                { "text-center": th.class_text_center },
                                { "table-primary": th.dt_tableClassHeaders },
                                {
                                  dark_sky_blue_last_four:
                                    th.dark_sky_blue_last_four
                                },
                                { "text-white": th.font_text_white },
                                { "bg-info": th.header_bg_info },
                                {
                                  dark_sky_blue_last_two:
                                    th.dark_sky_blue_last_two
                                },
                                { "font-weight-bold": th.font_weight_bold },
                                { "text-right": th.class_text_right }
                              ],
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan,
                                rowspan: th.rowspan
                              }
                            },
                            [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n                    "
                              )
                            ]
                          )
                    ]
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_per_month_2, function(th) {
                    return [
                      th.sortable
                        ? _c(
                            "th",
                            {
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass: "inline-block",
                                  staticStyle: { cursor: "pointer" },
                                  on: {
                                    click: function($event) {
                                      return _vm.sort(th.query_name)
                                    }
                                  }
                                },
                                [
                                  _c("div", {
                                    staticClass: "filter-asc",
                                    class: {
                                      "active-filter-asc":
                                        _vm.sortClassActive.activeAsc
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("div", {
                                    staticClass: "filter-desc",
                                    class: {
                                      "active-filter-desc":
                                        _vm.sortClassActive.activeDesc
                                    }
                                  })
                                ]
                              ),
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n\n                    "
                              )
                            ]
                          )
                        : _c(
                            "th",
                            {
                              class: [
                                { "text-center": th.class_text_center },
                                { "table-primary": th.dt_tableClassHeaders },
                                {
                                  dark_sky_blue_last_four:
                                    th.dark_sky_blue_last_four
                                },
                                { "text-white": th.font_text_white },
                                { "bg-info": th.header_bg_info },
                                {
                                  dark_sky_blue_last_two:
                                    th.dark_sky_blue_last_two
                                },
                                { "font-weight-bold": th.font_weight_bold },
                                { "text-right": th.class_text_right }
                              ],
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan,
                                rowspan: th.rowspan
                              }
                            },
                            [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n                    "
                              )
                            ]
                          )
                    ]
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableData", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    {
                      attrs: { colspan: _vm.dt_tableHeaders_per_month.length }
                    },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          )
        ]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row " }, [
      _c("div", { staticClass: "col-md-6" }, [_vm._t("perPage")], 2)
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-3 offset-md-3 " },
        [_vm._t("pagination")],
        2
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row  form-group" }),
    _vm._v(" "),
    _c("div", { class: [{ "table-responsive": false }] }, [
      _c(
        "table",
        { class: _vm.dt_classTable, attrs: { width: _vm.dt_TableWidth + "%" } },
        [
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ],
              class: _vm.dt_tableClassHeaders
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "VARIANCE" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_2, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "BUDGET" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_3, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "TOTAL OPEX" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_4, function(th, index) {
                    return _vm.allowedCompany.includes(th.column_name) ||
                      th.column_name === "COMPANY" ||
                      th.column_name === _vm.totalString
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.table_salaries, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "SALARIES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c("tbody", [_vm._t("thdSummaryDataTableData", [_vm._m(0)])], 2),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_fuels_and_selling_exp, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "FUEL AND SELLING EXPENSES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.table_fuels, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "FUELS" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableFuels", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_fuels.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_maintenance, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "MAINTENANCE" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableMaintenance", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    { attrs: { colspan: _vm.table_maintenance.length } },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_repairs, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "REPAIRS" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableRepairs", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_repairs.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_parking, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "PARKING" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableParking", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_parking.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_tolling, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "TOLLING" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableTolling", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_tolling.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._t("tableFuelAndSellingExp", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    {
                      attrs: { colspan: _vm.table_fuels_and_selling_exp.length }
                    },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_admin_expenses, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "ADMINISTRATIVE EXPENSES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.table_utilities, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "UTILITIES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableUtilities", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_utilities.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._t("tableAdminExpenses", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    { attrs: { colspan: _vm.table_admin_expenses.length } },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_operation_exp, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "OPERATING EXPENSES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          )
        ]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row " }, [
      _c("div", { staticClass: "col-md-6" }, [_vm._t("perPage")], 2)
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-3 offset-md-3 " },
        [_vm._t("pagination")],
        2
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", { staticClass: "text-center" }, [
      _c("td", { attrs: { colspan: "10" } }, [
        _c("p", [_vm._v("no data available")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue":
/*!********************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& */ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&");
/* harmony import */ var _SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SummaryBreakDown.vue?vue&type=script&lang=js& */ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& */ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "ae3088d4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/tables/SummaryBreakDown.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue":
/*!***********************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& */ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&");
/* harmony import */ var _THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./THDSummaryDataTable.vue?vue&type=script&lang=js& */ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& */ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "e17b6d7a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/tables/THDSummaryDataTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);