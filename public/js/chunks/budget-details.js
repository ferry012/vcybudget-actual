(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["budget-details"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "BudgetDetailsIndex"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _views_tables_BudgetDetailsDataTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../views/tables/BudgetDetailsDataTable */ "./resources/js/views/tables/BudgetDetailsDataTable.vue");
/* harmony import */ var _views_tables_BudgetDetailsDataTablePerMonth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../views/tables/BudgetDetailsDataTablePerMonth */ "./resources/js/views/tables/BudgetDetailsDataTablePerMonth.vue");
/* harmony import */ var _components_ToyDataTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/ToyDataTable */ "./resources/js/components/ToyDataTable.vue");
/* harmony import */ var _users_UserCreate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../users/UserCreate */ "./resources/js/views/users/UserCreate.vue");
/* harmony import */ var _users_UserEdit__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../users/UserEdit */ "./resources/js/views/users/UserEdit.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  name: "BudgetDetailsTable",
  components: {
    BudgetDetailsDataTable: _views_tables_BudgetDetailsDataTable__WEBPACK_IMPORTED_MODULE_1__["default"],
    DataTable: _components_ToyDataTable__WEBPACK_IMPORTED_MODULE_3__["default"],
    UserCreate: _users_UserCreate__WEBPACK_IMPORTED_MODULE_4__["default"],
    UserEdit: _users_UserEdit__WEBPACK_IMPORTED_MODULE_5__["default"],
    BudgetDetailsDataTablePerMonth: _views_tables_BudgetDetailsDataTablePerMonth__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      optionArr: [],
      showCreate: false,
      showEdit: false,
      filterCompany: null,
      tableHeaders: [{
        column_name: 'ACCOUNT',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'ANNUAL',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VS BUDGET',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VS PY ACTUAL',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      tableHeadersBudgetDetailsDataTable: [{
        column_name: 'ACCOUNT',
        query_name: 'name',
        sortable: false,
        rowspan: 2,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'ANNUAL',
        query_name: 'name',
        sortable: false,
        colspan: 3,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VS BUDGET',
        query_name: 'name',
        sortable: false,
        colspan: 2,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VS PY ACTUAL',
        query_name: 'name',
        sortable: false,
        colspan: 2,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        colspan: 3,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        colspan: 3,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      //dark blue sa right
      //sky blue sa left
      tableHeaders_2_BudgetDetailsDataTable: [{
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'CY ACTUAL',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'PY ACTUAL',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '% OF VARIANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'CHANGE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '% OF CHANGE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'ACTUAL',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'ACTUAL',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_salaries: [{
        column_name: 'SALARIES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_fuels_and_selling_exp: [{
        column_name: 'FUEL AND SELLING EXPENSES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_fuels: [{
        column_name: 'FUELS',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_maintenance: [{
        column_name: 'MAINTENANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_repairs: [{
        column_name: 'REPAIRS',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_parking: [{
        column_name: 'PARKING',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_tolling: [{
        column_name: 'TOLLING',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_utilities: [{
        column_name: 'UTILITIES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_admin_expenses: [{
        column_name: 'ADMIN EXPENSES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_operation_exp: [{
        column_name: 'OPERATING EXPENSES',
        query_name: 'name',
        sortable: false,
        rowspan: 2,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_two: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      dt_tableHeaders_per_month: [{
        column_name: '',
        query_name: 'name',
        sortable: false,
        colspan: 3,
        class_text_center: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        colspan: 3,
        class_text_center: true
      }],
      dt_tableHeaders_per_month_2: [{
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        class_text_center: true
      }, {
        column_name: 'ACTUAL',
        query_name: 'name',
        sortable: false,
        class_text_center: true
      }, {
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        class_text_center: true
      }, {
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        class_text_center: true
      }, {
        column_name: 'ACTUAL',
        query_name: 'name',
        sortable: false,
        class_text_center: true
      }, {
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        class_text_center: true
      }],
      totalReg: 0,
      totalFuelAndSellingExp: 0,
      totalFuels: 0,
      totalMain: 0,
      totalRepairs: 0,
      totalPark: 0,
      totalTolling: 0,
      totalAdminExp: 0,
      totalUtilities: 0,
      isSpin: true // company: this.currentUserLocal[0].company,
      // company_name: this.currentUserLocal[0].company_name,

    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapGetters"])({
    tableDefaultEntries: ["user/perPage"],
    tableSortBy: ["user/sortBy"],
    tableSortDir: ["user/sortDir"],
    users: ["user/users"],
    search: ["user/search"],
    perPage: ["user/perPage"],
    budget_total: ["budget_details/budget_total"],
    all_company: ["static_budget/plants"] // company codes

  })), {}, {
    currentUserLocal: function currentUserLocal() {
      return this.$store.getters['auth/users'] || [];
    }
  }),
  watch: {
    "currentUserLocal": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        this.getBudgetTotal();
      }
    },
    "currentPermission": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        if (newVal.length !== 0) {
          this.checkCurrentUserPermission();
        }
      }
    },
    "filterCompany": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        this.getFilteredBudgetTotal();
        this.isSpin = false; // if(newVal !== null){
        //     console.log(this.all_company,'all_company');
        //     this.all_company.forEach((value, index) => {
        //         if(value.EKORG === newVal){
        //             console.log(value,'value');
        //             this.currentUserLocal[0].company = value.EKORG;
        //             this.currentUserLocal[0].company_name = value.NAME1;
        //         }
        //     });
        // }
      }
    },
    "budget_total": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var _this = this;

        var size = 0,
            key;

        for (key in newVal) {
          if (newVal.hasOwnProperty(key)) size++;
        } //console.log(size, 'size');


        if (size !== 0) {
          var x = 0;
          var isLength = this.currentPermission.length;
          var access = null;

          do {
            if (this.currentPermission[x] === 'allowed_salaries_breakdown') {
              access = 'permitted';
            }

            x++;
          } while (x <= isLength); // if(access === null){
          // this.budget_total.REGULAR = 0;
          // this.budget_total.AGENCY = 0;
          // this.budget_total.OTHER_BENEFITS = 0;
          // this.budget_total.NON_MANDATORY = 0;
          //
          // this.budget_total.REGULAR_PY = 0;
          // this.budget_total.AGENCY_PY = 0;
          // this.budget_total.OTHER_BENEFITS_PY = 0;
          // this.budget_total.NON_MANDATORY_PY = 0;
          // }
          //console.log(this.budget_total.REGULAR,this.budget_total.AGENCY,this.budget_total.OTHER_BENEFITS,this.budget_total.NON_MANDATORY,'NON_MANDATORY');
          //BUDGET


          this.table_salaries[1].column_name = this.formatNumber(this.budget_total.SALARIES_TOTAL);
          this.table_fuels_and_selling_exp[1].column_name = this.formatNumber(this.budget_total.FUEL_AND_SELLING_EXP_TOTAL + this.budget_total.FUELS_TOTAL + this.budget_total.MAINTENANCE_TOTAL + this.budget_total.REPAIR_TOTAL + this.budget_total.PARKING_TOTAL + this.budget_total.TOLLING_TOTAL);
          this.table_fuels[1].column_name = this.formatNumber(this.budget_total.FUELS_TOTAL);
          this.table_maintenance[1].column_name = this.formatNumber(this.budget_total.MAINTENANCE_TOTAL);
          this.table_repairs[1].column_name = this.formatNumber(this.budget_total.REPAIR_TOTAL);
          this.table_parking[1].column_name = this.formatNumber(this.budget_total.PARKING_TOTAL);
          this.table_tolling[1].column_name = this.formatNumber(this.budget_total.TOLLING_TOTAL);
          this.table_admin_expenses[1].column_name = this.formatNumber(this.budget_total.ADMIN_EXP_TOTAL + this.budget_total.UTILITIES_TOTAL);
          this.table_utilities[1].column_name = this.formatNumber(this.budget_total.UTILITIES_TOTAL); //CY ACTUAL
          //console.log(this.budget_total.REGULAR , this.budget_total.AGENCY
          //     , this.budget_total.OTHER_BENEFITS , this.budget_total.NON_MANDATORY,'this.budget_total.REGULAR , this.budget_total.AGENCY\n' +
          //     '                        , this.budget_total.OTHER_BENEFITS , this.budget_total.NON_MANDATORY');

          this.table_salaries[2].column_name = this.formatNumber(this.budget_total.REGULAR + this.budget_total.AGENCY + this.budget_total.OTHER_BENEFITS + this.budget_total.NON_MANDATORY);
          this.table_fuels_and_selling_exp[2].column_name = this.formatNumber(this.budget_total.FUEL_VEHICLES + this.budget_total.FUEL_DELIVERY + this.budget_total.FUEL_MACHINERY + this.budget_total.MAIN_TIRE + this.budget_total.MAIN_TIRE_DELIVERY + this.budget_total.MAIN_BATTERY + this.budget_total.MAIN_BATTERY_DELIVERY + this.budget_total.MAIN_MACHINERY + this.budget_total.MAIN_OIL_VEHICLES + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY + this.budget_total.REPAIR_VEHICLES + this.budget_total.REPAIR_VEHICLES_DELIVERY + this.budget_total.PARKING_OFFICE + this.budget_total.PARKING_DELIVERY_VEHICLES + this.budget_total.TOLL_OFFICE + this.budget_total.TOLL_FSP_VEHICLES + this.budget_total.TOLL_DELIVERY_VEHICLES + this.budget_total.ADVERTISING_AND_PROMOTIONS + this.budget_total.TRANSPORTATION_AND_TRAVEL);
          this.table_fuels[2].column_name = this.formatNumber(this.budget_total.FUEL_VEHICLES + this.budget_total.FUEL_DELIVERY + this.budget_total.FUEL_MACHINERY);
          this.table_maintenance[2].column_name = this.formatNumber(this.budget_total.MAIN_TIRE + this.budget_total.MAIN_TIRE_DELIVERY + this.budget_total.MAIN_BATTERY + this.budget_total.MAIN_BATTERY_DELIVERY + this.budget_total.MAIN_MACHINERY + this.budget_total.MAIN_OIL_VEHICLES + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY);
          this.table_repairs[2].column_name = this.formatNumber(this.budget_total.REPAIR_VEHICLES + this.budget_total.REPAIR_VEHICLES_DELIVERY);
          this.table_parking[2].column_name = this.formatNumber(this.budget_total.PARKING_OFFICE + this.budget_total.PARKING_DELIVERY_VEHICLES);
          this.table_tolling[2].column_name = this.formatNumber(this.budget_total.TOLL_OFFICE + this.budget_total.TOLL_FSP_VEHICLES + this.budget_total.TOLL_DELIVERY_VEHICLES);
          this.table_admin_expenses[2].column_name = this.formatNumber(this.budget_total.UTILITIES_ELECTRICITY + this.budget_total.UTILITIES_WATER + this.budget_total.COMMUNICATION + this.budget_total.POSTAGE + this.budget_total.REPAIRS + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT + this.budget_total.PLOG_FEES_OTHERS + this.budget_total.SUPPLIES + this.budget_total.SUBSCRIPTIONS);
          this.table_utilities[2].column_name = this.formatNumber(this.budget_total.UTILITIES_ELECTRICITY + this.budget_total.UTILITIES_WATER); //PY ACTUAL

          this.table_salaries[3].column_name = this.formatNumber(this.budget_total.REGULAR_PY + this.budget_total.AGENCY_PY + this.budget_total.OTHER_BENEFITS_PY + this.budget_total.NON_MANDATORY_PY);
          this.table_fuels_and_selling_exp[3].column_name = this.formatNumber(this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY + this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY + this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY + this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY + this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY + this.budget_total.ADVERTISING_AND_PROMOTIONS_PY + this.budget_total.TRANSPORTATION_AND_TRAVEL_PY);
          this.table_fuels[3].column_name = this.formatNumber(this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY);
          this.table_maintenance[3].column_name = this.formatNumber(this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY);
          this.table_repairs[3].column_name = this.formatNumber(this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY);
          this.table_parking[3].column_name = this.formatNumber(this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY);
          this.table_tolling[3].column_name = this.formatNumber(this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY);
          this.table_admin_expenses[3].column_name = this.formatNumber(this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY + this.budget_total.COMMUNICATION_PY + this.budget_total.POSTAGE_PY + this.budget_total.REPAIRS_PY + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT_PY + this.budget_total.PLOG_FEES_OTHERS_PY + this.budget_total.SUPPLIES_PY + this.budget_total.SUBSCRIPTIONS_PY);
          this.table_utilities[3].column_name = this.formatNumber(this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY); //VARIANCE

          var sal_var = this.table_salaries[2].column_name.replace(/,/g, "") - this.table_salaries[1].column_name.replace(/,/g, "");
          var absVar_sal = Math.abs(sal_var);
          this.table_salaries[4].column_name = sal_var > 0 ? absVar_sal : sal_var === 0 ? absVar_sal : "(".concat(absVar_sal, ")");
          var fuel_and_sell_exp_var = this.table_fuels_and_selling_exp[2].column_name.replace(/,/g, "") - this.table_fuels_and_selling_exp[1].column_name.replace(/,/g, "");
          var absVar_fuel_and_sell_exp = Math.abs(fuel_and_sell_exp_var);
          this.table_fuels_and_selling_exp[4].column_name = fuel_and_sell_exp_var > 0 ? absVar_fuel_and_sell_exp : fuel_and_sell_exp_var === 0 ? absVar_fuel_and_sell_exp : "(".concat(absVar_fuel_and_sell_exp, ")");
          var fuel_var = this.table_fuels[2].column_name.replace(/,/g, "") - this.table_fuels[1].column_name.replace(/,/g, "");
          var absVar_fuel = Math.abs(fuel_var);
          this.table_fuels[4].column_name = fuel_var > 0 ? absVar_fuel : fuel_var === 0 ? absVar_fuel : "(".concat(absVar_fuel, ")");
          var main_var = this.table_maintenance[2].column_name.replace(/,/g, "") - this.table_maintenance[1].column_name.replace(/,/g, "");
          var absVar_main = Math.abs(main_var);
          this.table_maintenance[4].column_name = main_var > 0 ? absVar_main : main_var === 0 ? absVar_main : "(".concat(absVar_main, ")");
          var repair_var = this.table_repairs[2].column_name.replace(/,/g, "") - this.table_repairs[1].column_name.replace(/,/g, "");
          var absVar_repair = Math.abs(repair_var);
          this.table_repairs[4].column_name = repair_var > 0 ? absVar_repair : repair_var === 0 ? absVar_repair : "(".concat(absVar_repair, ")");
          var parking_var = this.table_parking[2].column_name.replace(/,/g, "") - this.table_parking[1].column_name.replace(/,/g, "");
          var absVar_parking = Math.abs(parking_var);
          this.table_parking[4].column_name = parking_var > 0 ? absVar_parking : parking_var === 0 ? absVar_parking : "(".concat(absVar_parking, ")");
          var tolling_var = this.table_tolling[2].column_name.replace(/,/g, "") - this.table_tolling[1].column_name.replace(/,/g, "");
          var absVar_tolling = Math.abs(tolling_var);
          this.table_tolling[4].column_name = tolling_var > 0 ? absVar_tolling : tolling_var === 0 ? absVar_tolling : "(".concat(absVar_tolling, ")");
          var admin_exp_var = this.table_admin_expenses[2].column_name.replace(/,/g, "") - this.table_admin_expenses[1].column_name.replace(/,/g, "");
          var absVar_admin_exp = Math.abs(admin_exp_var);
          this.table_admin_expenses[4].column_name = admin_exp_var > 0 ? absVar_admin_exp : admin_exp_var === 0 ? absVar_admin_exp : "(".concat(absVar_admin_exp, ")");
          var utilities_var = this.table_utilities[2].column_name.replace(/,/g, "") - this.table_utilities[1].column_name.replace(/,/g, "");
          var utilities_exp = Math.abs(utilities_var);
          this.table_utilities[4].column_name = utilities_var > 0 ? utilities_exp : utilities_var === 0 ? utilities_exp : "(".concat(utilities_exp, ")"); //% OF VARIANCE

          var check_var_sal = null;
          var vat_total_percent_sal = null;
          check_var_sal = sal_var / this.budget_total.SALARIES_TOTAL;

          if (!isFinite(check_var_sal)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_sal = '100%';
          } else {
            vat_total_percent_sal = "".concat(Math.round(sal_var / this.budget_total.SALARIES_TOTAL * 100), "%");
          }

          this.table_salaries[5].column_name = vat_total_percent_sal;
          var check_var_fuel_and_sell_exp_var = null;
          var vat_total_percent_fuel_and_sell_exp_var = null;
          check_var_fuel_and_sell_exp_var = fuel_and_sell_exp_var / [this.budget_total.FUEL_AND_SELLING_EXP_TOTAL + this.budget_total.FUELS_TOTAL + this.budget_total.MAINTENANCE_TOTAL + this.budget_total.REPAIR_TOTAL + this.budget_total.PARKING_TOTAL + this.budget_total.TOLLING_TOTAL];

          if (!isFinite(check_var_fuel_and_sell_exp_var)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_fuel_and_sell_exp_var = '100%';
          } else {
            vat_total_percent_fuel_and_sell_exp_var = "".concat(Math.round(fuel_and_sell_exp_var / [this.budget_total.FUEL_AND_SELLING_EXP_TOTAL + this.budget_total.FUELS_TOTAL + this.budget_total.MAINTENANCE_TOTAL + this.budget_total.REPAIR_TOTAL + this.budget_total.PARKING_TOTAL + this.budget_total.TOLLING_TOTAL] * 100), "%");
          }

          this.table_fuels_and_selling_exp[5].column_name = vat_total_percent_fuel_and_sell_exp_var;
          var check_var_fuel = null;
          var vat_total_percent_fuel = null;
          check_var_fuel = fuel_var / this.budget_total.FUELS_TOTAL;

          if (!isFinite(check_var_fuel)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_fuel = '100%';
          } else {
            vat_total_percent_fuel = "".concat(Math.round(fuel_var / this.budget_total.FUELS_TOTAL * 100), "%");
          }

          this.table_fuels[5].column_name = vat_total_percent_fuel;
          var check_var_main = null;
          var vat_total_percent_main = null;
          check_var_main = main_var / this.budget_total.MAINTENANCE_TOTAL;

          if (!isFinite(check_var_main)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_main = '100%';
          } else {
            vat_total_percent_main = "".concat(Math.round(main_var / this.budget_total.MAINTENANCE_TOTAL * 100), "%");
          }

          this.table_maintenance[5].column_name = vat_total_percent_main;
          var check_var_repair = null;
          var vat_total_percent_repair = null;
          check_var_repair = repair_var / this.budget_total.REPAIR_TOTAL;

          if (!isFinite(check_var_repair)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_repair = '100%';
          } else {
            vat_total_percent_repair = "".concat(Math.round(repair_var / this.budget_total.REPAIR_TOTAL * 100), "%");
          }

          this.table_repairs[5].column_name = vat_total_percent_repair;
          var check_var_parking = null;
          var vat_total_percent_parking = null;
          check_var_parking = parking_var / this.budget_total.PARKING_TOTAL;

          if (!isFinite(check_var_parking)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_parking = '100%';
          } else {
            vat_total_percent_parking = "".concat(Math.round(parking_var / this.budget_total.PARKING_TOTAL * 100), "%");
          }

          this.table_parking[5].column_name = vat_total_percent_parking;
          var check_var_tolling = null;
          var vat_total_percent_tolling = null;
          check_var_tolling = tolling_var / this.budget_total.TOLLING_TOTAL;

          if (!isFinite(check_var_tolling)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_tolling = '100%';
          } else {
            vat_total_percent_tolling = "".concat(Math.round(tolling_var / this.budget_total.TOLLING_TOTAL * 100), "%");
          }

          this.table_tolling[5].column_name = vat_total_percent_tolling;
          var check_var_admin_exp = null;
          var vat_total_percent_admin_exp = null;
          check_var_admin_exp = admin_exp_var / [this.budget_total.ADMIN_EXP_TOTAL + this.budget_total.UTILITIES_TOTAL];

          if (!isFinite(check_var_admin_exp)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_admin_exp = '100%';
          } else {
            vat_total_percent_admin_exp = "".concat(Math.round(admin_exp_var / [this.budget_total.ADMIN_EXP_TOTAL + this.budget_total.UTILITIES_TOTAL] * 100), "%");
          }

          this.table_admin_expenses[5].column_name = vat_total_percent_admin_exp;
          var check_var_utilities = null;
          var vat_total_percent_utilities = null;
          check_var_utilities = utilities_var / this.budget_total.UTILITIES_TOTAL;

          if (!isFinite(check_var_utilities)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_percent_utilities = '100%';
          } else {
            vat_total_percent_utilities = "".concat(Math.round(utilities_var / this.budget_total.UTILITIES_TOTAL * 100), "%");
          }

          this.table_utilities[5].column_name = vat_total_percent_utilities; //CHANGE [ CY_ACTUAL - PY_ACTUAL ]

          var sal_change = this.table_salaries[2].column_name.replace(/,/g, "") - this.table_salaries[3].column_name.replace(/,/g, "");
          var absVar_change = Math.abs(sal_change); //prototype only change if error in the future

          this.table_salaries[6].column_name = sal_change > 0 ? this.formatNumber(absVar_change) : sal_change === 0 ? this.formatNumber(absVar_change) : "(".concat(this.formatNumber(absVar_change), ")");
          var fuel_and_sell_exp_change = this.table_fuels_and_selling_exp[2].column_name.replace(/,/g, "") - this.table_fuels_and_selling_exp[3].column_name.replace(/,/g, "");
          var absChange_fuel_and_sell_exp = Math.abs(fuel_and_sell_exp_change);
          this.table_fuels_and_selling_exp[6].column_name = fuel_and_sell_exp_change > 0 ? this.formatNumber(absChange_fuel_and_sell_exp) : fuel_and_sell_exp_change === 0 ? this.formatNumber(absChange_fuel_and_sell_exp) : "(".concat(this.formatNumber(absChange_fuel_and_sell_exp), ")");
          var fuel_change = this.table_fuels[2].column_name.replace(/,/g, "") - this.table_fuels[3].column_name.replace(/,/g, "");
          var absChange_fuel = Math.abs(fuel_change);
          this.table_fuels[6].column_name = fuel_change > 0 ? this.formatNumber(absChange_fuel) : fuel_change === 0 ? this.formatNumber(absChange_fuel) : "(".concat(this.formatNumber(absChange_fuel), ")");
          var main_change = this.table_maintenance[2].column_name.replace(/,/g, "") - this.table_maintenance[3].column_name.replace(/,/g, "");
          var absChange_main = Math.abs(main_change);
          this.table_maintenance[6].column_name = main_change > 0 ? this.formatNumber(absChange_main) : main_change === 0 ? this.formatNumber(absChange_main) : "(".concat(this.formatNumber(absChange_main), ")");
          var repair_change = this.table_repairs[2].column_name.replace(/,/g, "") - this.table_repairs[3].column_name.replace(/,/g, "");
          var absChange_repair = Math.abs(repair_change);
          this.table_repairs[6].column_name = repair_change > 0 ? this.formatNumber(absChange_repair) : repair_change === 0 ? this.formatNumber(absChange_repair) : "(".concat(this.formatNumber(absChange_repair), ")");
          var parking_change = this.table_parking[2].column_name.replace(/,/g, "") - this.table_parking[3].column_name.replace(/,/g, "");
          var absChange_parking = Math.abs(parking_change);
          this.table_parking[6].column_name = parking_change > 0 ? this.formatNumber(absChange_parking) : parking_change === 0 ? this.formatNumber(absChange_parking) : "(".concat(this.formatNumber(absChange_parking), ")");
          var tolling_change = this.table_tolling[2].column_name.replace(/,/g, "") - this.table_tolling[3].column_name.replace(/,/g, "");
          var absChange_tolling = Math.abs(tolling_change);
          this.table_tolling[6].column_name = tolling_change > 0 ? this.formatNumber(absChange_tolling) : tolling_change === 0 ? this.formatNumber(absChange_tolling) : "(".concat(this.formatNumber(absChange_tolling), ")");
          var admin_exp_change = this.table_admin_expenses[2].column_name.replace(/,/g, "") - this.table_admin_expenses[3].column_name.replace(/,/g, "");
          var absChange_admin_exp = Math.abs(admin_exp_change);
          this.table_admin_expenses[6].column_name = admin_exp_change > 0 ? this.formatNumber(absChange_admin_exp) : admin_exp_change === 0 ? this.formatNumber(absChange_admin_exp) : "(".concat(this.formatNumber(absChange_admin_exp), ")");
          var utilities_change = this.table_utilities[2].column_name.replace(/,/g, "") - this.table_utilities[3].column_name.replace(/,/g, "");
          var absChange_utilities = Math.abs(utilities_change);
          this.table_utilities[6].column_name = utilities_change > 0 ? this.formatNumber(absChange_utilities) : utilities_change === 0 ? this.formatNumber(absChange_utilities) : "(".concat(this.formatNumber(absChange_utilities), ")"); //% OF CHANGE [CHANGE / PY ACTUAL] * 100

          var check_var_change_percent_sal = null;
          var vat_total_change_percent_sal = null;
          check_var_change_percent_sal = sal_change / [this.budget_total.REGULAR_PY + this.budget_total.AGENCY_PY + this.budget_total.OTHER_BENEFITS_PY + this.budget_total.NON_MANDATORY_PY];

          if (!isFinite(check_var_change_percent_sal)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_sal = '100%';
          } else {
            vat_total_change_percent_sal = "".concat(Math.round(sal_change / [this.budget_total.REGULAR_PY + this.budget_total.AGENCY_PY + this.budget_total.OTHER_BENEFITS_PY + this.budget_total.NON_MANDATORY_PY] * 100), "%");
          }

          this.table_salaries[7].column_name = vat_total_change_percent_sal;
          var check_var_change_percent_fuel_and_selling_exp = null;
          var vat_total_change_percent_fuel_and_selling_exp = null;
          check_var_change_percent_fuel_and_selling_exp = fuel_and_sell_exp_change / [this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY + this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY + this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY + this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY + this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY + this.budget_total.ADVERTISING_AND_PROMOTIONS_PY + this.budget_total.TRANSPORTATION_AND_TRAVEL_PY]; // !isFinite

          if (!isFinite(check_var_change_percent_fuel_and_selling_exp)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_fuel_and_selling_exp = '100%';
          } else {
            vat_total_change_percent_fuel_and_selling_exp = "".concat(Math.round(fuel_and_sell_exp_change / [this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY + this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY + this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY + this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY + this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY + this.budget_total.ADVERTISING_AND_PROMOTIONS_PY + this.budget_total.TRANSPORTATION_AND_TRAVEL_PY] * 100), "%");
          }

          this.table_fuels_and_selling_exp[7].column_name = vat_total_change_percent_fuel_and_selling_exp;
          var check_var_change_percent_fuel = null;
          var vat_total_change_percent_fuel = null;
          check_var_change_percent_fuel = fuel_change / [this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY];

          if (!isFinite(check_var_change_percent_fuel)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_fuel = '100%';
          } else {
            vat_total_change_percent_fuel = "".concat(Math.round(fuel_change / [this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY] * 100), "%");
          }

          this.table_fuels[7].column_name = vat_total_change_percent_fuel;
          var check_var_change_percent_main = null;
          var vat_total_change_percent_main = null;
          check_var_change_percent_main = main_change / [this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY];

          if (!isFinite(check_var_change_percent_main)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_main = '100%';
          } else {
            vat_total_change_percent_main = "".concat(Math.round(main_change / [this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY] * 100), "%");
          }

          this.table_maintenance[7].column_name = vat_total_change_percent_main;
          var check_var_change_percent_repairs = null;
          var vat_total_change_percent_repairs = null;
          check_var_change_percent_repairs = repair_change / [this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY];

          if (!isFinite(check_var_change_percent_repairs)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_repairs = '100%';
          } else {
            vat_total_change_percent_repairs = "".concat(Math.round(repair_change / [this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY] * 100), "%");
          }

          this.table_repairs[7].column_name = vat_total_change_percent_repairs;
          var check_var_change_percent_parking = null;
          var vat_total_change_percent_parking = null;
          check_var_change_percent_parking = parking_change / [this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY];

          if (!isFinite(check_var_change_percent_parking)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_parking = '100%';
          } else {
            vat_total_change_percent_parking = "".concat(Math.round(parking_change / [this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY] * 100), "%");
          }

          this.table_parking[7].column_name = vat_total_change_percent_parking;
          var check_var_change_percent_tolling = null;
          var vat_total_change_percent_tolling = null;
          check_var_change_percent_tolling = tolling_change / [this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY];

          if (!isFinite(check_var_change_percent_tolling)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_tolling = '100%';
          } else {
            vat_total_change_percent_tolling = "".concat(Math.round(tolling_change / [this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY] * 100), "%");
          }

          this.table_tolling[7].column_name = vat_total_change_percent_tolling;
          var check_var_change_percent_admin_exp = null;
          var vat_total_change_percent_admin_exp = null;
          check_var_change_percent_admin_exp = admin_exp_change / [this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY + this.budget_total.COMMUNICATION_PY + this.budget_total.POSTAGE_PY + this.budget_total.REPAIRS_PY + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT_PY + this.budget_total.PLOG_FEES_OTHERS_PY + this.budget_total.SUPPLIES_PY + this.budget_total.SUBSCRIPTIONS_PY];

          if (!isFinite(check_var_change_percent_admin_exp)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_admin_exp = '100%';
          } else {
            vat_total_change_percent_admin_exp = "".concat(Math.round(admin_exp_change / [this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY + this.budget_total.COMMUNICATION_PY + this.budget_total.POSTAGE_PY + this.budget_total.REPAIRS_PY + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT_PY + this.budget_total.PLOG_FEES_OTHERS_PY + this.budget_total.SUPPLIES_PY + this.budget_total.SUBSCRIPTIONS_PY] * 100), "%");
          }

          this.table_admin_expenses[7].column_name = vat_total_change_percent_admin_exp;
          var check_var_change_percent_utilities = null;
          var vat_total_change_percent_utilities = null;
          check_var_change_percent_utilities = utilities_change / [this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY];

          if (!isFinite(check_var_change_percent_utilities)) {
            // if true then the return in NaN. if false then the return is not NaN; if true then return the default value 100%
            vat_total_change_percent_utilities = '100%';
          } else {
            vat_total_change_percent_utilities = "".concat(Math.round(utilities_change / [this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY] * 100), "%");
          }

          this.table_utilities[7].column_name = vat_total_change_percent_utilities; //OPERATING EXPENSES [ SALARIES + FUEL AND SELLING EXPENSES + ADMINISTRATIVE EXPENSES ]

          this.table_operation_exp[1].column_name = this.formatNumber(this.budget_total.SALARIES_TOTAL + this.budget_total.FUEL_AND_SELLING_EXP_TOTAL + this.budget_total.FUELS_TOTAL + this.budget_total.MAINTENANCE_TOTAL + this.budget_total.REPAIR_TOTAL + this.budget_total.PARKING_TOTAL + this.budget_total.TOLLING_TOTAL + this.budget_total.ADMIN_EXP_TOTAL + this.budget_total.UTILITIES_TOTAL);
          this.table_operation_exp[2].column_name = this.formatNumber(this.budget_total.REGULAR + this.budget_total.AGENCY + this.budget_total.OTHER_BENEFITS + this.budget_total.NON_MANDATORY + this.budget_total.FUEL_VEHICLES + this.budget_total.FUEL_DELIVERY + this.budget_total.FUEL_MACHINERY + this.budget_total.MAIN_TIRE + this.budget_total.MAIN_TIRE_DELIVERY + this.budget_total.MAIN_BATTERY + this.budget_total.MAIN_BATTERY_DELIVERY + this.budget_total.MAIN_MACHINERY + this.budget_total.MAIN_OIL_VEHICLES + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY + this.budget_total.REPAIR_VEHICLES + this.budget_total.REPAIR_VEHICLES_DELIVERY + this.budget_total.PARKING_OFFICE + this.budget_total.PARKING_DELIVERY_VEHICLES + this.budget_total.TOLL_OFFICE + this.budget_total.TOLL_FSP_VEHICLES + this.budget_total.TOLL_DELIVERY_VEHICLES + this.budget_total.ADVERTISING_AND_PROMOTIONS + this.budget_total.TRANSPORTATION_AND_TRAVEL + this.budget_total.UTILITIES_ELECTRICITY + this.budget_total.UTILITIES_WATER + this.budget_total.COMMUNICATION + this.budget_total.POSTAGE + this.budget_total.REPAIRS + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT + this.budget_total.PLOG_FEES_OTHERS + this.budget_total.SUPPLIES + this.budget_total.SUBSCRIPTIONS);
          this.table_operation_exp[3].column_name = this.formatNumber(this.budget_total.REGULAR_PY + this.budget_total.AGENCY_PY + this.budget_total.OTHER_BENEFITS_PY + this.budget_total.NON_MANDATORY_PY + this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY + this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY + this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY + this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY + this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY + this.budget_total.ADVERTISING_AND_PROMOTIONS_PY + this.budget_total.TRANSPORTATION_AND_TRAVEL_PY + this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY + this.budget_total.COMMUNICATION_PY + this.budget_total.POSTAGE_PY + this.budget_total.REPAIRS_PY + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT_PY + this.budget_total.PLOG_FEES_OTHERS_PY + this.budget_total.SUPPLIES_PY + this.budget_total.SUBSCRIPTIONS_PY);

          if (access === null) {
            sal_var = 0;
            sal_change = 0;
          }

          var operation_total = sal_var + fuel_and_sell_exp_var + admin_exp_var;
          var operation_abs = this.formatNumber(Math.abs(operation_total)); // is already negative just remove the sign

          this.table_operation_exp[4].column_name = operation_total > 0 ? "(".concat(operation_abs, ")") : operation_total === 0 ? operation_abs : "(".concat(operation_abs, ")"); // let operation_total_var_percent_sal = Math.round(sal_var / this.budget_total.SALARIES_TOTAL * 100);
          // let operation_total_var_percent_fuel_and_selling_exp = Math.round(fuel_and_sell_exp_var / [this.budget_total.FUEL_AND_SELLING_EXP_TOTAL
          // + this.budget_total.FUELS_TOTAL + this.budget_total.MAINTENANCE_TOTAL + this.budget_total.REPAIR_TOTAL
          // + this.budget_total.PARKING_TOTAL + this.budget_total.TOLLING_TOTAL] * 100);
          // let operation_total_var_percent_admin_exp = Math.round(admin_exp_var / [this.budget_total.ADMIN_EXP_TOTAL + this.budget_total.UTILITIES_TOTAL] * 100);
          // let operation_total_percent = operation_total_var_percent_sal + operation_total_var_percent_fuel_and_selling_exp + operation_total_var_percent_admin_exp;
          // this.table_operation_exp[5].column_name = `${operation_total_percent}%`;

          var sal_budget_total = this.budget_total.SALARIES_TOTAL + this.budget_total.FUEL_AND_SELLING_EXP_TOTAL + this.budget_total.FUELS_TOTAL + this.budget_total.MAINTENANCE_TOTAL + this.budget_total.REPAIR_TOTAL + this.budget_total.PARKING_TOTAL + this.budget_total.TOLLING_TOTAL + this.budget_total.ADMIN_EXP_TOTAL + this.budget_total.UTILITIES_TOTAL;
          var operation_total_percent = operation_total / sal_budget_total * 100;
          this.table_operation_exp[5].column_name = "".concat(Math.round(operation_total_percent), "%");
          var operation_change = sal_change + fuel_and_sell_exp_change + admin_exp_change;
          this.table_operation_exp[6].column_name = this.formatNumber(operation_change); // let operation_change_percent_sal = Math.round(sal_change / [this.budget_total.REGULAR_PY + this.budget_total.AGENCY_PY
          // + this.budget_total.OTHER_BENEFITS_PY + this.budget_total.NON_MANDATORY_PY] * 100);
          // let operation_change_percent_fuel_and_selling_exp = Math.round(fuel_and_sell_exp_change / [this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY
          // + this.budget_total.FUEL_MACHINERY_PY + this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY
          // + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY
          // + this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY + this.budget_total.PARKING_OFFICE_PY
          // + this.budget_total.PARKING_DELIVERY_VEHICLES_PY + this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY
          // + this.budget_total.ADVERTISING_AND_PROMOTIONS_PY + this.budget_total.TRANSPORTATION_AND_TRAVEL_PY] * 100);
          // let operation_change_percent_admin_exp = Math.round(admin_exp_change / [this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY
          // + this.budget_total.COMMUNICATION_PY + this.budget_total.POSTAGE_PY + this.budget_total.REPAIRS_PY + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT_PY
          // + this.budget_total.PLOG_FEES_OTHERS_PY + this.budget_total.SUPPLIES_PY + this.budget_total.SUBSCRIPTIONS_PY] * 100);
          // let operation_change_percent_total = operation_change_percent_sal + operation_change_percent_fuel_and_selling_exp + operation_change_percent_admin_exp;
          // this.table_operation_exp[7].column_name = `${operation_change_percent_total}%`;

          var operation_change_percent_total = operation_change / [this.budget_total.REGULAR_PY + this.budget_total.AGENCY_PY + this.budget_total.OTHER_BENEFITS_PY + this.budget_total.NON_MANDATORY_PY + this.budget_total.FUEL_VEHICLES_PY + this.budget_total.FUEL_DELIVERY_PY + this.budget_total.FUEL_MACHINERY_PY + this.budget_total.MAIN_TIRE_PY + this.budget_total.MAIN_TIRE_DELIVERY_PY + this.budget_total.MAIN_BATTERY_PY + this.budget_total.MAIN_BATTERY_DELIVERY_PY + this.budget_total.MAIN_MACHINERY_PY + this.budget_total.MAIN_OIL_VEHICLES_PY + this.budget_total.MAIN_OIL_VEHICLES_DELIVERY_PY + this.budget_total.REPAIR_VEHICLES_PY + this.budget_total.REPAIR_VEHICLES_DELIVERY_PY + this.budget_total.PARKING_OFFICE_PY + this.budget_total.PARKING_DELIVERY_VEHICLES_PY + this.budget_total.TOLL_OFFICE_PY + this.budget_total.TOLL_FSP_VEHICLES_PY + this.budget_total.TOLL_DELIVERY_VEHICLES_PY + this.budget_total.ADVERTISING_AND_PROMOTIONS_PY + this.budget_total.TRANSPORTATION_AND_TRAVEL_PY + this.budget_total.UTILITIES_ELECTRICITY_PY + this.budget_total.UTILITIES_WATER_PY + this.budget_total.COMMUNICATION_PY + this.budget_total.POSTAGE_PY + this.budget_total.REPAIRS_PY + this.budget_total.REPRESENTATION_AND_ENTERTAINMENT_PY + this.budget_total.PLOG_FEES_OTHERS_PY + this.budget_total.SUPPLIES_PY + this.budget_total.SUBSCRIPTIONS_PY] * 100;

          if (!isFinite(operation_change_percent_total)) {
            operation_change_percent_total = '100%';
            this.table_operation_exp[7].column_name = operation_change_percent_total;
          } else {
            this.table_operation_exp[7].column_name = "".concat(Math.round(operation_change_percent_total), "%");
          } //LEFT SIDE CURRENT MONTH


          this.table_salaries[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_SAL;
          this.table_salaries[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_SAL;
          this.table_salaries[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_SAL;
          this.table_salaries[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_SAL;
          this.table_salaries[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_SAL;
          this.table_salaries[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_SAL;
          this.table_fuels[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_FUEL;
          this.table_fuels[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_FUEL;
          this.table_fuels[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_FUEL;
          this.table_fuels[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_FUEL;
          this.table_fuels[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_FUEL;
          this.table_fuels[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_FUEL;
          this.table_maintenance[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_MAIN;
          this.table_maintenance[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_MAIN;
          this.table_maintenance[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_MAIN;
          this.table_maintenance[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_MAIN;
          this.table_maintenance[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_MAIN;
          this.table_maintenance[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_MAIN;
          this.table_repairs[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_REPAIR;
          this.table_repairs[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_REPAIR;
          this.table_repairs[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_REPAIR;
          this.table_repairs[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_REPAIR;
          this.table_repairs[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_REPAIR;
          this.table_repairs[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_REPAIR;
          this.table_parking[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_PARKING;
          this.table_parking[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_PARKING;
          this.table_parking[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_PARKING;
          this.table_parking[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_PARKING;
          this.table_parking[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_PARKING;
          this.table_parking[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_PARKING;
          this.table_tolling[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_TOLLING;
          this.table_tolling[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_TOLLING;
          this.table_tolling[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_TOLLING;
          this.table_tolling[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_TOLLING;
          this.table_tolling[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_TOLLING;
          this.table_tolling[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_TOLLING;
          this.table_fuels_and_selling_exp[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_FUEL_AND_SELL_EXP;
          this.table_fuels_and_selling_exp[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_FUEL_AND_SELL_EXP;
          this.table_fuels_and_selling_exp[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_FUEL_AND_SELL_EXP;
          this.table_fuels_and_selling_exp[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_FUEL_AND_SELL_EXP;
          this.table_fuels_and_selling_exp[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_FUEL_AND_SELL_EXP;
          this.table_fuels_and_selling_exp[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_FUEL_AND_SELL_EXP;
          this.table_utilities[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_UTILITIES;
          this.table_utilities[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_UTILITIES;
          this.table_utilities[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_UTILITIES;
          this.table_utilities[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_UTILITIES;
          this.table_utilities[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_UTILITIES;
          this.table_utilities[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_UTILITIES;
          this.table_admin_expenses[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_ADMIN_EXP;
          this.table_admin_expenses[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_ADMIN_EXP;
          this.table_admin_expenses[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_ADMIN_EXP;
          this.table_admin_expenses[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_ADMIN_EXP;
          this.table_admin_expenses[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_ADMIN_EXP;
          this.table_admin_expenses[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_ADMIN_EXP;
          this.table_operation_exp[8].column_name = this.budget_total.BUDGET_TOTAL_MONTH_OPERATION;
          this.table_operation_exp[9].column_name = this.budget_total.ACTUAL_TOTAL_CURRENT_MONTH_OPERATION;
          this.table_operation_exp[10].column_name = this.budget_total.VARIANCE_TOTAL_CURRENT_MONTH_OPERATION;
          this.table_operation_exp[11].column_name = this.budget_total.BUDGET_TOTAL_MONTH_OPERATION;
          this.table_operation_exp[12].column_name = this.budget_total.ACTUAL_TOTAL_PAST_MONTH_OPERATION;
          this.table_operation_exp[13].column_name = this.budget_total.VARIANCE_TOTAL_PAST_MONTH_OPERATION;

          var _moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // require


          _moment().format();

          var today = _moment().format('ll');

          var currentMonth = today.split(' ');

          var todayLastMonth = _moment().subtract(1, 'months').format('ll');

          var previousMonth = todayLastMonth.split(' '); //console.log(previousMonth,todayLastMonth,previousMonth[0].toUpperCase());

          this.tableHeadersBudgetDetailsDataTable[4].column_name = currentMonth[0].toUpperCase();
          this.tableHeadersBudgetDetailsDataTable[5].column_name = previousMonth[0].toUpperCase();

          if (this.filterCompany !== null) {
            this.all_company.forEach(function (value, index) {
              if (value.EKORG === _this.filterCompany) {
                console.log(value, 'value');
                _this.currentUserLocal[0].company = value.EKORG;
                _this.currentUserLocal[0].company_name = value.NAME1;
              }
            });
            this.isSpin = true;
          }
        }
      }
    }
  },
  created: function created() {
    //this.$store.commit('permission/PERMISSION_SEARCH', '');
    this.index();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapActions"])({
    getUser: "user/fetchUsersForDatatable",
    editUser: "user/getUser",
    budgetTotal: "budget_details/budgetTotal"
  })), {}, {
    checkCurrentUserPermission: function checkCurrentUserPermission() {
      var arr = [];
      var isLength = this.currentPermission.length;
      var hil = 0;
      var sib = 0;
      var rox = 0;
      var sum = 0;
      var diver = 0;
      var jar = 0;
      var kab = 0;
      var cs = 0;
      var x = 0;

      do {
        if (this.currentPermission[x] === 'hilado_company_budget_details') {
          hil = '1231';
        } else if (this.currentPermission[x] === 'sibulan_company_budget_details') {
          sib = '1232';
        } else if (this.currentPermission[x] === 'roxas_company_budget_details') {
          rox = '1233';
        } else if (this.currentPermission[x] === 'sumag_company_budget_details') {
          sum = '1234';
        } else if (this.currentPermission[x] === 'diversion_company_budget_details') {
          diver = '1235';
        } else if (this.currentPermission[x] === 'jaro_company_budget_details') {
          jar = '1236';
        } else if (this.currentPermission[x] === 'kabankalan_company_budget_details') {
          kab = '1237';
        } else if (this.currentPermission[x] === 'corporate_services_company_budget_details') {
          cs = '1901';
        }

        if (hil !== 0 || sib !== 0 || rox !== 0 || sum !== 0 || diver !== 0 || jar !== 0 || kab !== 0 || cs !== 0) {
          arr = [hil, sib, rox, sum, diver, jar, kab, cs];
        }

        x++;
      } while (x <= isLength); //TODO TOM
      //console.log(arr);


      this.optionArr = arr;
    },
    strictOption: function strictOption(com) {
      if (com.EKORG !== '1231' && com.EKORG !== '1232' && com.EKORG !== '1233' && com.EKORG !== '1234') {
        return true;
      }
    },
    getFilteredBudgetTotal: function getFilteredBudgetTotal() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this2.$store.commit('budget_details/FILTER_COMPANY', _this2.filterCompany);

              case 2:
                _context.next = 4;
                return _this2.budgetTotal();

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    getBudgetTotal: function getBudgetTotal() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var operatingArr;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _this3.$store.commit('budget_details/COMPANY_USER', _this3.currentUserLocal[0].company);

              case 2:
                _context2.next = 4;
                return _this3.budgetTotal();

              case 4:
                operatingArr = [];
                operatingArr.push({
                  'BUDGET': _this3.table_operation_exp[1].column_name,
                  'CY_ACTUAL': _this3.table_operation_exp[2].column_name,
                  'PY_ACTUAL': _this3.table_operation_exp[3].column_name,
                  'VARIANCE': _this3.table_operation_exp[4].column_name,
                  'PERCENT_OF_VARIANCE': _this3.table_operation_exp[5].column_name,
                  'CHANGE': _this3.table_operation_exp[6].column_name,
                  'PERCENT_CHANGE': _this3.table_operation_exp[7].column_name
                });
                _context2.next = 8;
                return _this3.$store.commit('summary/GET_BUDGET_DETAILS_OPERATING_EXP', operatingArr);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    deleteUser: function deleteUser(e, index) {
      var _this4 = this;

      this.$swal({
        title: 'Are you sure to delete this user?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/users/".concat(e);
          axios["delete"](uri).then(function (response) {
            _this4.$swal('Deleted!', 'Your file has been deleted.', 'success');

            _this4.users.data.splice(index, 1);
          });
        }
      });
    },
    showCreateView: function showCreateView() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this5.showCreate = true;
                _this5.showEdit = false;
                _context3.next = 4;
                return _this5.$store.commit('user/USER_CLEAR');

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    closeCreate: function closeCreate(value) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this6.showCreate = value;
                _this6.showEdit = false;
                _context4.next = 4;
                return _this6.$store.commit('user/USER_CLEAR');

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    closeEdit: function closeEdit() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _this7.showCreate = false;
                _this7.showEdit = false;
                _context5.next = 4;
                return _this7.$store.commit('user/USER_CLEAR');

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    edit: function edit(id) {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _this8.showEdit = true;
                _this8.showCreate = false;
                _context6.next = 4;
                return _this8.$store.commit('user/USER_CLEAR');

              case 4:
                _context6.next = 6;
                return _this8.$store.commit('user/USER_ID', id);

              case 6:
                _context6.next = 8;
                return _this8.editUser();

              case 8:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    sortByTable: function sortByTable(value) {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return _this9.$store.commit('user/USER_SORTBY', value.sortBy);

              case 2:
                _context7.next = 4;
                return _this9.$store.commit('user/USER_SORTDIR', value.sortDir);

              case 4:
                _context7.next = 6;
                return _this9.index();

              case 6:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    },
    paginateTable: function paginateTable(value) {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.next = 2;
                return _this10.$store.commit('user/USER_PERPAGE', value);

              case 2:
                _context8.next = 4;
                return _this10.index();

              case 4:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    },
    searchData: function searchData(value) {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.next = 2;
                return _this11.$store.commit('user/USER_SEARCH', value);

              case 2:
                _context9.next = 4;
                return _this11.index();

              case 4:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }))();
    },
    index: function index(page) {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                if (!(typeof page === 'undefined')) {
                  _context10.next = 3;
                  break;
                }

                _context10.next = 3;
                return _this12.$store.commit('user/USER_PAGE', 1);

              case 3:
                _context10.next = 5;
                return _this12.$store.commit('user/USER_PAGE', page);

              case 5:
                _context10.next = 7;
                return _this12.getUser();

              case 7:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    },
    formatNumber: function formatNumber(number) {
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    },
    formatNumberNoAbs: function formatNumberNoAbs(number) {
      return number > 0 ? parseFloat(number).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : parseFloat(number).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [_c("router-view")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row" },
    [
      _c("div", { staticClass: "col-md-12 col-sm-12 col-lg-12" }, [
        _c("div", { staticClass: "card" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6" }, [
                _c("div", { staticClass: "input-group" }, [
                  _vm._m(1),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "bmd-label-floating mt-2 text-primary" },
                    [
                      _vm._v(
                        _vm._s(this.currentUserLocal[0].company) +
                          "\n                            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  !_vm.isSpin
                    ? _c(
                        "div",
                        {
                          staticClass:
                            "spinner-border spinner-border-sm text-danger mt-2",
                          staticStyle: { "margin-left": "5px" },
                          attrs: { role: "status" }
                        },
                        [
                          _c("span", { staticClass: "sr-only" }, [
                            _vm._v("Loading...")
                          ])
                        ]
                      )
                    : _vm._e()
                ])
              ]),
              _vm._v(" "),
              _vm._m(2)
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6" }, [
                _c("div", { staticClass: "input-group" }, [
                  _vm._m(3),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "bmd-label-floating mt-2 text-primary" },
                    [_vm._v(_vm._s(this.currentUserLocal[0].company_name))]
                  ),
                  _vm._v(" "),
                  !_vm.isSpin
                    ? _c(
                        "div",
                        {
                          staticClass:
                            "spinner-border spinner-border-sm text-danger mt-2",
                          staticStyle: { "margin-left": "5px" },
                          attrs: { role: "status" }
                        },
                        [
                          _c("span", { staticClass: "sr-only" }, [
                            _vm._v("Loading...")
                          ])
                        ]
                      )
                    : _vm._e()
                ])
              ]),
              _vm._v(" "),
              _vm._m(4)
            ]),
            _vm._v(" "),
            _vm.$can("filter_company")
              ? _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-6" }, [
                    _c("div", { staticClass: "input-group" }, [
                      _vm._m(5),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.filterCompany,
                              expression: "filterCompany"
                            }
                          ],
                          staticClass: "custom-select",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.filterCompany = $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            }
                          }
                        },
                        [
                          _c("option", { attrs: { value: "", disabled: "" } }, [
                            _vm._v("Please select one...")
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.all_company, function(company) {
                            return _vm.optionArr.includes(company.EKORG)
                              ? _c(
                                  "option",
                                  {
                                    key: company.ID,
                                    domProps: { value: company.EKORG }
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(company.NAME1) +
                                        "\n                                "
                                    )
                                  ]
                                )
                              : _vm._e()
                          })
                        ],
                        2
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" })
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "clearfix" })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 col-sm-12 col-lg-12" }, [
        _c("div", { staticClass: "card" }, [
          _vm._m(6),
          _vm._v(" "),
          _vm.budget_total.length !== 0
            ? _c(
                "div",
                { staticClass: "card-body" },
                [
                  _c("div", { staticClass: "toolbar" }),
                  _vm._v(" "),
                  _c("BudgetDetailsDataTable", {
                    attrs: {
                      dt_tableHeaders: _vm.tableHeadersBudgetDetailsDataTable,
                      dt_tableHeaders_2:
                        _vm.tableHeaders_2_BudgetDetailsDataTable,
                      table_salaries: _vm.table_salaries,
                      table_fuels_and_selling_exp:
                        _vm.table_fuels_and_selling_exp,
                      table_fuels: _vm.table_fuels,
                      table_maintenance: _vm.table_maintenance,
                      table_repairs: _vm.table_repairs,
                      table_parking: _vm.table_parking,
                      table_tolling: _vm.table_tolling,
                      table_admin_expenses: _vm.table_admin_expenses,
                      table_utilities: _vm.table_utilities,
                      table_operation_exp: _vm.table_operation_exp,
                      dt_currentSort: _vm.tableSortBy,
                      dt_currentSortDir: _vm.tableSortDir,
                      dt_defaultEntries: _vm.tableDefaultEntries,
                      dt_tableClassHeaders: "thead-dark",
                      dt_Search: _vm.search,
                      dt_classTable: "table table-bordered",
                      dt_TableWidth: "100"
                    },
                    on: {
                      sortColumn: _vm.sortByTable,
                      perPageEntry: _vm.paginateTable,
                      searchItem: _vm.searchData
                    },
                    scopedSlots: _vm._u(
                      [
                        {
                          key: "tableSalaries",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .SALARIES_CY,
                              function(total, index) {
                                return _vm.$can("allowed_salaries_breakdown")
                                  ? _c("tr", { key: total.id }, [
                                      _c("td", [_vm._v(_vm._s(total.name))]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.budget_total))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.total_cy))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.total_py))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.variance))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.percent_variance))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.change))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.change_percent))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.budget))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.actual_per_month))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.variance_per_month))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.budget))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(_vm._s(total.actual_past_month))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { staticClass: "text-right" }, [
                                        _vm._v(
                                          _vm._s(total.variance_past_month)
                                        )
                                      ])
                                    ])
                                  : _vm._e()
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableFuels",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .FUELS_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableMaintenance",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .MAINTENANCE_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableRepairs",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .REPAIRS_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableParking",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .PARKING_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableTolling",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .TOLLING_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableFuelAndSellingExp",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .FUEL_AND_SELLING_EXP_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableUtilities",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .UTILITIES_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        },
                        {
                          key: "tableAdminExpenses",
                          fn: function() {
                            return _vm._l(
                              _vm.budget_total.ACCOUNT_PARENT.ACCOUNT.DETAILS
                                .ADMIN_EXPENSE_CY,
                              function(total, index) {
                                return _c("tr", { key: total.id }, [
                                  _c("td", [_vm._v(_vm._s(total.name))]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget_total))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_cy))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.total_py))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.percent_variance))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.change_percent))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_per_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.budget))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.actual_past_month))
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "text-right" }, [
                                    _vm._v(_vm._s(total.variance_past_month))
                                  ])
                                ])
                              }
                            )
                          },
                          proxy: true
                        }
                      ],
                      null,
                      false,
                      77080758
                    )
                  })
                ],
                1
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.showCreate,
                expression: "showCreate"
              }
            ],
            staticClass: "col-md-12 col-sm-12 col-lg-6 "
          },
          [_c("UserCreate", { on: { cancelCreate: _vm.closeCreate } })],
          1
        )
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.showEdit,
                expression: "showEdit"
              }
            ],
            ref: "editFormRef",
            staticClass: "col-md-12 col-sm-12 col-lg-6 "
          },
          [_c("UserEdit", { on: { cancelEdit: _vm.closeEdit } })],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header card-header-primary card-header-text" },
      [
        _c("div", { staticClass: "card-icon" }, [
          _c("i", { staticClass: "material-icons" }, [_vm._v("feed")])
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "card-title" }, [
          _vm._v("Budget VS Actual Report")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "label",
        {
          staticClass: "input-group-text input-group-field bmd-label-floating"
        },
        [_vm._v("Company Code\n                                    :")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("div", { staticClass: "input-group" }, [
        _c("div", { staticClass: "input-group-prepend" }, [
          _c(
            "label",
            {
              staticClass:
                "input-group-text input-group-field bmd-label-floating"
            },
            [_vm._v("Currency\n                                    :")]
          )
        ]),
        _vm._v(" "),
        _c("label", { staticClass: "bmd-label-floating mt-2 text-primary" }, [
          _vm._v("PHP")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "label",
        {
          staticClass: "input-group-text input-group-field bmd-label-floating"
        },
        [_vm._v("Company Name\n                                    :")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("div", { staticClass: "input-group" }, [
        _c("div", { staticClass: "input-group-prepend" }, [
          _c(
            "label",
            {
              staticClass:
                "input-group-text input-group-field bmd-label-floating text-primary"
            },
            [
              _vm._v(
                "\n                                    Amounts in '000\n                                "
              )
            ]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "label",
        {
          staticClass: "input-group-text input-group-field bmd-label-floating"
        },
        [_vm._v("Company Filter\n                                    :")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header card-header-primary card-header-icon" },
      [
        _c("div", { staticClass: "card-icon" }, [
          _c("i", { staticClass: "material-icons" }, [
            _vm._v("stacked_bar_chart")
          ])
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "card-title" }, [_vm._v("Budget Details")])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/budget_details/BudgetDetailsIndex.vue":
/*!******************************************************************!*\
  !*** ./resources/js/views/budget_details/BudgetDetailsIndex.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BudgetDetailsIndex_vue_vue_type_template_id_8b85fa26_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true& */ "./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true&");
/* harmony import */ var _BudgetDetailsIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BudgetDetailsIndex.vue?vue&type=script&lang=js& */ "./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BudgetDetailsIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BudgetDetailsIndex_vue_vue_type_template_id_8b85fa26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BudgetDetailsIndex_vue_vue_type_template_id_8b85fa26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "8b85fa26",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/budget_details/BudgetDetailsIndex.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BudgetDetailsIndex.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsIndex_vue_vue_type_template_id_8b85fa26_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsIndex.vue?vue&type=template&id=8b85fa26&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsIndex_vue_vue_type_template_id_8b85fa26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsIndex_vue_vue_type_template_id_8b85fa26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/budget_details/BudgetDetailsTable.vue":
/*!******************************************************************!*\
  !*** ./resources/js/views/budget_details/BudgetDetailsTable.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BudgetDetailsTable_vue_vue_type_template_id_73d837e9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true& */ "./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true&");
/* harmony import */ var _BudgetDetailsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BudgetDetailsTable.vue?vue&type=script&lang=js& */ "./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BudgetDetailsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BudgetDetailsTable_vue_vue_type_template_id_73d837e9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BudgetDetailsTable_vue_vue_type_template_id_73d837e9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "73d837e9",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/budget_details/BudgetDetailsTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BudgetDetailsTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsTable_vue_vue_type_template_id_73d837e9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/budget_details/BudgetDetailsTable.vue?vue&type=template&id=73d837e9&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsTable_vue_vue_type_template_id_73d837e9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BudgetDetailsTable_vue_vue_type_template_id_73d837e9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);