(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["thd-summary"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "SummaryBreakDown",
  props: {
    dt_tableHeaders_per_month: {
      type: Array,
      required: true
    },
    dt_tableHeaders_per_month_2: {
      type: Array,
      required: true
    },
    dt_tableClassHeaders: {
      type: String,
      "default": 'text-primary'
    },
    dt_itemEntriesOption: {
      type: Array,
      "default": function _default() {
        return [5, 25, 100];
      }
    },
    dt_classTable: {
      type: String,
      "default": 'table'
    },
    dt_TableWidth: {
      type: String,
      "default": '100'
    },
    dt_TableResponsive: {
      type: Boolean,
      "default": false
    },
    dt_currentSort: {
      type: String,
      "default": 'name'
    },
    dt_currentSortDir: {
      type: String,
      "default": 'asc'
    },
    dt_defaultEntries: {
      type: Number,
      required: true
    },
    dt_Search: {
      type: String
    }
  },
  data: function data() {
    return {
      entrySelected: this.dt_defaultEntries,
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      timer: null
    };
  },
  methods: {
    sort: function sort(s) {
      var currentSortDir = this.dt_currentSortDir;

      if (s === this.dt_currentSort) {
        currentSortDir = this.dt_currentSortDir === 'asc' ? 'desc' : 'asc';

        if (currentSortDir === 'asc') {
          this.sortClassActive.activeAsc = true;
          this.sortClassActive.activeDesc = false;
        } else {
          this.sortClassActive.activeAsc = false;
          this.sortClassActive.activeDesc = true;
        }
      } // this.currentSort = s;


      this.$emit("sortColumn", {
        sortBy: s,
        sortDir: currentSortDir
      });
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.entrySelected = selected;
      this.$emit("perPageEntry", this.entrySelected);
    },
    searchData: function searchData(event) {
      this.$emit("searchItem", event.target.value);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "THDSummaryDataTable",
  props: {
    dt_tableHeaders: {
      type: Array,
      required: true
    },
    dt_tableHeaders_2: {
      type: Array,
      required: true
    },
    dt_tableHeaders_3: {
      type: Array,
      required: true
    },
    dt_tableHeaders_4: {
      type: Array,
      required: true
    },
    table_salaries: {
      type: Array,
      required: true
    },
    table_fuels_and_selling_exp: {
      type: Array,
      required: true
    },
    table_fuels: {
      type: Array,
      required: true
    },
    table_maintenance: {
      type: Array,
      required: true
    },
    table_repairs: {
      type: Array,
      required: true
    },
    table_parking: {
      type: Array,
      required: true
    },
    table_tolling: {
      type: Array,
      required: true
    },
    table_utilities: {
      type: Array,
      required: true
    },
    table_admin_expenses: {
      type: Array,
      required: true
    },
    table_operation_exp: {
      type: Array,
      required: true
    },
    dt_tableClassHeaders: {
      type: String,
      "default": 'text-primary'
    },
    dt_itemEntriesOption: {
      type: Array,
      "default": function _default() {
        return [5, 25, 100];
      }
    },
    dt_classTable: {
      type: String,
      "default": 'table'
    },
    dt_TableWidth: {
      type: String,
      "default": '100'
    },
    dt_TableResponsive: {
      type: Boolean,
      "default": false
    },
    dt_currentSort: {
      type: String,
      "default": 'name'
    },
    dt_currentSortDir: {
      type: String,
      "default": 'asc'
    },
    dt_defaultEntries: {
      type: Number,
      required: true
    },
    dt_Search: {
      type: String
    }
  },
  data: function data() {
    return {
      entrySelected: this.dt_defaultEntries,
      sortClassActive: {
        'activeDesc': false,
        'activeAsc': true
      },
      timer: null,
      allowedCompany: [],
      allowedCompanyIndex: [],
      isIndex: 0,
      totalString: 'TOTAL'
    };
  },
  mounted: function mounted() {
    this.showCompanyByPermission();
  },
  methods: {
    showCompanyByPermission: function showCompanyByPermission() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var arr, arr_index, isLength, hil, sib, rox, sum, diver, jar, kab, cs, hil_index, sib_index, rox_index, sum_index, diver_index, jar_index, kab_index, cs_index, x, y, countArr, arrCheck;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                arr = [];
                arr_index = [];
                isLength = _this.currentPermission.length;
                hil = 0;
                sib = 0;
                rox = 0;
                sum = 0;
                diver = 0;
                jar = 0;
                kab = 0;
                cs = 0;
                hil_index = 0;
                sib_index = 0;
                rox_index = 0;
                sum_index = 0;
                diver_index = 0;
                jar_index = 0;
                kab_index = 0;
                cs_index = 0;
                x = 0;

                do {
                  if (_this.currentPermission[x] === 'hilado_company_thd_summary') {
                    hil = 'HIL'; //1231

                    hil_index = 1;
                  } else if (_this.currentPermission[x] === 'sibulan_company_thd_summary') {
                    sib = 'SIB'; //1232

                    sib_index = 2;
                  } else if (_this.currentPermission[x] === 'roxas_company_thd_summary') {
                    rox = 'ROX'; //1233

                    rox_index = 3;
                  } else if (_this.currentPermission[x] === 'sumag_company_thd_summary') {
                    sum = 'SUM'; //1234

                    sum_index = 4;
                  } else if (_this.currentPermission[x] === 'diversion_company_thd_summary') {
                    diver = 'DIV'; //1235

                    diver_index = 5;
                  } else if (_this.currentPermission[x] === 'jaro_company_thd_summary') {
                    jar = 'JAR'; //1236

                    jar_index = 6;
                  } else if (_this.currentPermission[x] === 'kabankalan_company_thd_summary') {
                    kab = 'KAB'; //1237

                    kab_index = 7;
                  } else if (_this.currentPermission[x] === 'corporate_services_company_thd_summary') {
                    cs = 'CS'; //1901

                    cs_index = 8;
                  }

                  if (hil !== 0 || sib !== 0 || rox !== 0 || sum !== 0 || diver !== 0 || jar !== 0 || kab !== 0 || cs !== 0) {
                    arr = [hil, sib, rox, sum, diver, jar, kab, cs];
                  }

                  if (hil_index !== 0 || sib_index !== 0 || rox_index !== 0 || sum_index !== 0 || diver_index !== 0 || jar_index !== 0 || kab_index !== 0 || cs_index !== 0) {
                    arr_index = [hil_index, sib_index, rox_index, sum_index, diver_index, jar_index, kab_index, cs_index];
                  }

                  x++;
                } while (x <= isLength); // console.log(arr_index,'arr_index');


                console.log(arr, 'arr');
                _context.next = 24;
                return _this.removeCompanyIsNotPermitted(arr, 0);

              case 24:
                _context.next = 26;
                return _this.removeCompanyIsNotPermittedIndex(arr_index, 0);

              case 26:
                y = 0;
                countArr = arr.length; // checking if length is equal to 1 replace the value to 0. Since the loop will execute twice if the value is 1. In the loop it will look like this index[0,1]

                if (countArr === 1) countArr = 0;
                arrCheck = [];

                while (y <= countArr) {
                  if (arr[y] !== 0) {
                    arrCheck[y] = [arr[y]];
                  }

                  y++;
                }

                console.log(arrCheck, 'arrCheck');

                if (arrCheck.length <= 1) {
                  _this.isIndex = 9;
                  _this.totalString = '';
                } // let checkThis = this.dt_tableHeaders_4.includes()
                // let y = 0;
                // let yy = 0;
                //first loop It enters both outer and inner loops, showing the desired output for the first line. You end up with y = 1 and yy = 8.
                //second loop Since y is 1, it doesn't leave the outer loop, but yy is 8, so it doesn't enter the inner loop again.
                //last loop It then keeps adding 1 to y until it doesn't match the outer loop condition anymore and leaves you with that unwanted result.
                // let includeThis = [];
                // let testHeaders = [];
                // let y_1 = null;
                //
                // while(y < this.dt_tableHeaders_4.length){
                //     while(yy < this.allowedCompany.length){
                //         if(this.dt_tableHeaders_4[y].column_name === this.allowedCompany[yy]){
                //             includeThis.push(this.allowedCompany[yy]);
                //         }
                //         yy++;
                //     }
                //     yy = 0;
                //     y++;
                // }
                // console.log(includeThis);


              case 33:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    removeCompanyIsNotPermitted: function removeCompanyIsNotPermitted(arr, value) {
      var i = 0;

      while (i < arr.length) {
        if (arr[i] === value) {
          arr.splice(i, 1);
        } else {
          ++i;
        }
      }

      this.allowedCompany = arr;
      return this.allowedCompany;
    },
    removeCompanyIsNotPermittedIndex: function removeCompanyIsNotPermittedIndex(arr, value) {
      var i = 0;

      while (i < arr.length) {
        if (arr[i] === value) {
          arr.splice(i, 1);
        } else {
          ++i;
        }
      }

      this.allowedCompanyIndex = arr;
      return this.allowedCompanyIndex;
    },
    sort: function sort(s) {
      var currentSortDir = this.dt_currentSortDir;

      if (s === this.dt_currentSort) {
        currentSortDir = this.dt_currentSortDir === 'asc' ? 'desc' : 'asc';

        if (currentSortDir === 'asc') {
          this.sortClassActive.activeAsc = true;
          this.sortClassActive.activeDesc = false;
        } else {
          this.sortClassActive.activeAsc = false;
          this.sortClassActive.activeDesc = true;
        }
      } // this.currentSort = s;


      this.$emit("sortColumn", {
        sortBy: s,
        sortDir: currentSortDir
      });
    },
    selectPageNumber: function selectPageNumber(selected) {
      this.entrySelected = selected;
      this.$emit("perPageEntry", this.entrySelected);
    },
    searchData: function searchData(event) {
      this.$emit("searchItem", event.target.value);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "THDSummaryIndex"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _views_tables_BudgetDetailsDataTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../views/tables/BudgetDetailsDataTable */ "./resources/js/views/tables/BudgetDetailsDataTable.vue");
/* harmony import */ var _views_tables_BudgetDetailsDataTablePerMonth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../views/tables/BudgetDetailsDataTablePerMonth */ "./resources/js/views/tables/BudgetDetailsDataTablePerMonth.vue");
/* harmony import */ var _views_tables_SummaryOperatingExpensesDataTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../views/tables/SummaryOperatingExpensesDataTable */ "./resources/js/views/tables/SummaryOperatingExpensesDataTable.vue");
/* harmony import */ var _views_tables_SummaryBreakDown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../views/tables/SummaryBreakDown */ "./resources/js/views/tables/SummaryBreakDown.vue");
/* harmony import */ var _views_tables_THDSummaryDataTable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../views/tables/THDSummaryDataTable */ "./resources/js/views/tables/THDSummaryDataTable.vue");
/* harmony import */ var _components_ToyDataTable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/ToyDataTable */ "./resources/js/components/ToyDataTable.vue");
/* harmony import */ var _users_UserCreate__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../users/UserCreate */ "./resources/js/views/users/UserCreate.vue");
/* harmony import */ var _users_UserEdit__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../users/UserEdit */ "./resources/js/views/users/UserEdit.vue");
/* harmony import */ var vue2_datepicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vue2-datepicker */ "./node_modules/vue2-datepicker/index.esm.js");
/* harmony import */ var vue2_datepicker_index_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! vue2-datepicker/index.css */ "./node_modules/vue2-datepicker/index.css");
/* harmony import */ var vue2_datepicker_index_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(vue2_datepicker_index_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_12__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//












/* harmony default export */ __webpack_exports__["default"] = ({
  name: "THDSummaryTable",
  components: {
    BudgetDetailsDataTable: _views_tables_BudgetDetailsDataTable__WEBPACK_IMPORTED_MODULE_1__["default"],
    DataTable: _components_ToyDataTable__WEBPACK_IMPORTED_MODULE_6__["default"],
    UserCreate: _users_UserCreate__WEBPACK_IMPORTED_MODULE_7__["default"],
    UserEdit: _users_UserEdit__WEBPACK_IMPORTED_MODULE_8__["default"],
    BudgetDetailsDataTablePerMonth: _views_tables_BudgetDetailsDataTablePerMonth__WEBPACK_IMPORTED_MODULE_2__["default"],
    SummaryOperatingExpensesDataTable: _views_tables_SummaryOperatingExpensesDataTable__WEBPACK_IMPORTED_MODULE_3__["default"],
    SummaryBreakDown: _views_tables_SummaryBreakDown__WEBPACK_IMPORTED_MODULE_4__["default"],
    THDSummaryDataTable: _views_tables_THDSummaryDataTable__WEBPACK_IMPORTED_MODULE_5__["default"],
    DatePicker: vue2_datepicker__WEBPACK_IMPORTED_MODULE_9__["default"]
  },
  data: function data() {
    return {
      showCreate: false,
      showEdit: false,
      isLoading: false,
      fullPage: true,
      isLoader: null,
      dt_tableHeaders: [{
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      dt_tableHeaders_2: [{
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      dt_tableHeaders_3: [{
        column_name: 'TOTAL OPEX',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      dt_tableHeaders_4: [{
        column_name: 'COMPANY',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'HIL',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'SIB',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'ROX',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'SUM',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'DIV',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'JAR',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'KAB',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'CS',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'TOTAL',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_salaries: [{
        column_name: 'SALARIES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_fuels_and_selling_exp: [{
        column_name: 'FUEL AND SELLING EXPENSES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_fuels: [{
        column_name: 'FUELS',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_maintenance: [{
        column_name: 'MAINTENANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_repairs: [{
        column_name: 'REPAIRS',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_parking: [{
        column_name: 'PARKING',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_tolling: [{
        column_name: 'TOLLING',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_utilities: [{
        column_name: 'UTILITIES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_admin_expenses: [{
        column_name: 'ADMINISTRATIVE EXPENSES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      table_operation_exp: [{
        column_name: 'OPERATING EXPENSES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_right: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      momentFormat: {
        // Date to String
        stringify: function stringify(date) {
          return date ? moment__WEBPACK_IMPORTED_MODULE_12___default()(date).format("YYYY-MM-DD") : "";
        },
        // String to Date
        parse: function parse(value) {
          return value ? moment__WEBPACK_IMPORTED_MODULE_12___default()(value, "YYYY-MM-DD").toDate() : null;
        }
      },
      dateFromAndTo: [],
      //Budget Details data below
      tableHeaders: [{
        column_name: 'COMPANY CODES',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '',
        query_name: 'name',
        sortable: false,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      tableHeadersBudgetDetailsDataTable: [{
        column_name: 'ACCOUNT',
        query_name: 'name',
        sortable: false,
        rowspan: 2,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'ANNUAL',
        query_name: 'name',
        sortable: false,
        colspan: 3,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VS BUDGET',
        query_name: 'name',
        sortable: false,
        colspan: 2,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VS PY ACTUAL',
        query_name: 'name',
        sortable: false,
        colspan: 2,
        class_text_center: true,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      //dark blue sa right
      //sky blue sa left
      tableHeaders_2_BudgetDetailsDataTable: [{
        column_name: 'BUDGET',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'CY ACTUAL',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'PY ACTUAL',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'VARIANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '% OF VARIANCE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: 'CHANGE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }, {
        column_name: '% OF CHANGE',
        query_name: 'name',
        sortable: false,
        dark_sky_blue_last_four: true,
        font_text_white: true,
        font_weight_bold: true
      }],
      totalReg: 0,
      totalFuelAndSellingExp: 0,
      totalFuels: 0,
      totalMain: 0,
      totalRepairs: 0,
      totalPark: 0,
      totalTolling: 0,
      totalAdminExp: 0,
      totalUtilities: 0,
      summary_operating_arr: [],
      thdSummaryHasData: false,
      showIT: false,
      allowedCompany: []
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_11__["mapGetters"])({
    tableDefaultEntries: ["user/perPage"],
    tableSortBy: ["user/sortBy"],
    tableSortDir: ["user/sortDir"],
    users: ["user/users"],
    search: ["user/search"],
    perPage: ["user/perPage"],
    // budget_total: ["budget_details/budget_total"],
    isLogin: ["auth/check"],
    thd_summary: ["thd-summary/thdSummaryOperating"]
  })), {}, {
    currentUserLocal: function currentUserLocal() {
      return this.$store.getters['auth/users'] || [];
    }
  }),
  watch: {
    "currentUserLocal": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        this.getBudgetTotal();
      }
    },
    "thd_summary": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        // console.log(newVal,newVal.ACCOUNTS.DETAILS.SALARIES,this.formatNumber(newVal.ACCOUNTS.DETAILS.SALARIES[0].company_1231),'thd_summary');
        var size = 0,
            key;

        for (key in newVal) {
          if (newVal.hasOwnProperty(key)) size++;
        }

        if (size !== 0) {
          if (newVal.ACCOUNTS.DETAILS.SALARIES.length !== 0 && newVal.ACCOUNTS.DETAILS.FUELS.length !== 0 && newVal.ACCOUNTS.DETAILS.FUEL_AND_SELLING_EXP.length !== 0 && newVal.ACCOUNTS.DETAILS.MAINTENANCE.length !== 0 && newVal.ACCOUNTS.DETAILS.PARKING.length !== 0 && newVal.ACCOUNTS.DETAILS.REPAIRS.length !== 0 && newVal.ACCOUNTS.DETAILS.TOLLING.length !== 0 && newVal.ACCOUNTS.DETAILS.UTILITIES.length !== 0 && newVal.ACCOUNTS.DETAILS.ADMIN_EXPENSE.length !== 0) {
            // console.log(this.dt_tableHeaders_4,'company');
            // this.showCompanyByPermission();
            // let allowedCompany =  this.removeCompanyIsNotPermitted();
            // console.log(this.allowedCompany,'allowedCompany');
            this.isLoader.hide();
            this.showIT = true;
            this.thdSummaryHasData = true;
          }
        }
      }
    },
    "dateFromAndTo": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        if (newVal[0] !== null && newVal[1] !== null) {
          this.getTHDSummaryDetails(); // this.thdSummaryHasData = false;
          // let loader

          this.isLoader = this.$loading.show({
            // Optional parameters
            container: this.fullPage ? null : this.$refs.loadingContainer,
            canCancel: false,
            loader: 'dots',
            color: '#004BAD',
            width: 64,
            height: 64,
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            opacity: 0.5,
            zIndex: 999
          });
        }

        if (newVal[0] === null && newVal[1] === null) {
          this.thdSummaryHasData = false; // the entire table will be hidden
        }
      }
    }
  },
  created: function created() {
    //this.$store.commit('permission/PERMISSION_SEARCH', '');
    this.index();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_11__["mapActions"])({
    getUser: "user/fetchUsersForDatatable",
    editUser: "user/getUser",
    budgetTotal: "budget_details/budgetTotal",
    getTHDSummary: "thd-summary/getTHDSummary"
  })), {}, {
    // async showCompanyByPermission(){
    //      let arr = [];
    //      let isLength = this.currentPermission.length;
    //
    //      let hil = 0;
    //      let sib = 0;
    //      let rox = 0;
    //      let sum = 0;
    //      let diver = 0;
    //      let jar = 0;
    //      let kab = 0;
    //      let cs = 0;
    //
    //      let x = 0;
    //      do{
    //          if(this.currentPermission[x] === 'hilado_company_thd_summary'){
    //              hil = 'HIL'; //1231
    //          }else if(this.currentPermission[x] === 'sibulan_company_thd_summary'){
    //              sib = 'SIB'; //1232
    //          }else if(this.currentPermission[x] === 'roxas_company_thd_summary'){
    //              rox = 'ROX'; //1233
    //          }else if(this.currentPermission[x] === 'sumag_company_thd_summary'){
    //              sum = 'SUM'; //1234
    //          }else if(this.currentPermission[x] === 'diversion_company_thd_summary'){
    //              diver = 'DIV'; //1235
    //          }else if(this.currentPermission[x] === 'jaro_company_thd_summary'){
    //              jar = 'JAR'; //1236
    //          }else if(this.currentPermission[x] === 'kabankalan_company_thd_summary'){
    //              kab = 'KAB'; //1237
    //          }else if(this.currentPermission[x] === 'corporate_services_company_thd_summary'){
    //              cs = 'CS'; //1901
    //          }
    //          if(hil !== 0 || sib !== 0 || rox !== 0
    //              || sum !== 0 || diver !== 0 || jar !== 0
    //              || kab !== 0 || cs !== 0){
    //              arr = [hil,sib,rox,sum,diver,jar,kab,cs];
    //          }
    //          x++;
    //      }while(x <= isLength);
    //     await this.removeCompanyIsNotPermitted(arr,0);
    //     // let checkThis = this.dt_tableHeaders_4.includes()
    //     let y = 0;
    //     let yy = 0;
    //
    //     //first loop It enters both outer and inner loops, showing the desired output for the first line. You end up with y = 1 and yy = 8.
    //     //second loop Since y is 1, it doesn't leave the outer loop, but yy is 8, so it doesn't enter the inner loop again.
    //     //last loop It then keeps adding 1 to y until it doesn't match the outer loop condition anymore and leaves you with that unwanted result.
    //
    //     let includeThis = [];
    //     let testHeaders = [];
    //     let y_1 = null;
    //
    //     while(y < this.dt_tableHeaders_4.length){
    //         while(yy < this.allowedCompany.length){
    //             if(this.dt_tableHeaders_4[y].column_name === this.allowedCompany[yy]){
    //                 includeThis.push(this.allowedCompany[yy]);
    //                  // console.log(this.dt_tableHeaders_4[y].column_name,'checkthis');
    //                 // testHeaders = this.dt_tableHeaders_4.splice(y,5);
    //                 // console.log(this.dt_tableHeaders_4.splice(y,1),'slice');
    //                 // console.log(y,'y');
    //                 // console.log(this.allowedCompany[yy],'checkthis_1');
    //                 // includeThis.push(this.allowedCompany[yy]);
    //                 // console.log(this.allowedCompany[yy],'includeThis123');
    //             }
    //             yy++;
    //         }
    //         yy = 0;
    //         y++;
    //     }
    //     // console.log(includeThis);
    //     // let missingIndex = [];
    //     // for(let x = 0; x < this.dt_tableHeaders_4.length;x++){
    //     //     for(let y = 0; y < includeThis.length;y++){
    //     //         if(this.dt_tableHeaders_4[x].column_name === includeThis[y]){
    //     //              // console.log(x,'x');
    //     //             missingIndex.push(x);
    //     //         }
    //     //     }
    //     // }
    //     // let missingIndex_1 = [];
    //     // for(let x = 0; x < this.dt_tableHeaders_4.length;x++){
    //     //     for(let y = 0; y < missingIndex.length;y++){
    //     //         if(x !== missingIndex[y]){
    //     //             // console.log(missingIndex[y],'test111');
    //     //         }
    //     //     }
    //     // }
    //     // console.log(missingIndex,'missingIndex');
    //     // console.log(testHeaders,'testHeaders');
    //     // console.log(this.dt_tableHeaders_4,'includethis111');
    //     // this.dt_tableHeaders_4 = includeThis;
    //     // console.log(includeThis,'includeThis');
    //     // this.dt_tableHeaders_4.includes()
    //  },
    //  removeCompanyIsNotPermitted(arr, value){
    //      let i = 0;
    //      while (i < arr.length) {
    //          if (arr[i] === value) {
    //              arr.splice(i, 1);
    //          } else {
    //              ++i;
    //          }
    //      }
    //      this.allowedCompany = arr;
    //      return this.allowedCompany;
    //  },
    getBudgetTotal: function getBudgetTotal() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this.$store.commit('budget_details/COMPANY_USER', _this.currentUserLocal[0].company);

              case 2:
                _context.next = 4;
                return _this.budgetTotal();

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    getTHDSummaryDetails: function getTHDSummaryDetails() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _this2.$store.commit('thd-summary/THD_DATE_RANGE', _this2.dateFromAndTo);

              case 2:
                _context2.next = 4;
                return _this2.getTHDSummary();

              case 4:
                _context2.next = 6;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231;

              case 6:
                _this2.table_salaries[1].column_name = _context2.sent;
                _context2.next = 9;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232;

              case 9:
                _this2.table_salaries[2].column_name = _context2.sent;
                _context2.next = 12;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233;

              case 12:
                _this2.table_salaries[3].column_name = _context2.sent;
                _context2.next = 15;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234;

              case 15:
                _this2.table_salaries[4].column_name = _context2.sent;
                _context2.next = 18;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235;

              case 18:
                _this2.table_salaries[5].column_name = _context2.sent;
                _context2.next = 21;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236;

              case 21:
                _this2.table_salaries[6].column_name = _context2.sent;
                _context2.next = 24;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237;

              case 24:
                _this2.table_salaries[7].column_name = _context2.sent;
                _context2.next = 27;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901;

              case 27:
                _this2.table_salaries[8].column_name = _context2.sent;
                _context2.next = 30;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL;

              case 30:
                _this2.table_salaries[9].column_name = _context2.sent;
                _context2.next = 33;
                return _this2.thd_summary.ACCOUNTS.OPEX_1231;

              case 33:
                _this2.dt_tableHeaders_3[1].column_name = _context2.sent;
                _context2.next = 36;
                return _this2.thd_summary.ACCOUNTS.OPEX_1232;

              case 36:
                _this2.dt_tableHeaders_3[2].column_name = _context2.sent;
                _context2.next = 39;
                return _this2.thd_summary.ACCOUNTS.OPEX_1233;

              case 39:
                _this2.dt_tableHeaders_3[3].column_name = _context2.sent;
                _context2.next = 42;
                return _this2.thd_summary.ACCOUNTS.OPEX_1234;

              case 42:
                _this2.dt_tableHeaders_3[4].column_name = _context2.sent;
                _context2.next = 45;
                return _this2.thd_summary.ACCOUNTS.OPEX_1235;

              case 45:
                _this2.dt_tableHeaders_3[5].column_name = _context2.sent;
                _context2.next = 48;
                return _this2.thd_summary.ACCOUNTS.OPEX_1236;

              case 48:
                _this2.dt_tableHeaders_3[6].column_name = _context2.sent;
                _context2.next = 51;
                return _this2.thd_summary.ACCOUNTS.OPEX_1237;

              case 51:
                _this2.dt_tableHeaders_3[7].column_name = _context2.sent;
                _context2.next = 54;
                return _this2.thd_summary.ACCOUNTS.OPEX_1901;

              case 54:
                _this2.dt_tableHeaders_3[8].column_name = _context2.sent;
                _context2.next = 57;
                return _this2.thd_summary.ACCOUNTS.OPEX_TOTAL;

              case 57:
                _this2.dt_tableHeaders_3[9].column_name = _context2.sent;
                _context2.next = 60;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1231;

              case 60:
                _this2.dt_tableHeaders_2[1].column_name = _context2.sent;
                _context2.next = 63;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1232;

              case 63:
                _this2.dt_tableHeaders_2[2].column_name = _context2.sent;
                _context2.next = 66;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1233;

              case 66:
                _this2.dt_tableHeaders_2[3].column_name = _context2.sent;
                _context2.next = 69;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1234;

              case 69:
                _this2.dt_tableHeaders_2[4].column_name = _context2.sent;
                _context2.next = 72;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1235;

              case 72:
                _this2.dt_tableHeaders_2[5].column_name = _context2.sent;
                _context2.next = 75;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1236;

              case 75:
                _this2.dt_tableHeaders_2[6].column_name = _context2.sent;
                _context2.next = 78;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1237;

              case 78:
                _this2.dt_tableHeaders_2[7].column_name = _context2.sent;
                _context2.next = 81;
                return _this2.thd_summary.ACCOUNTS.BUDGET_1901;

              case 81:
                _this2.dt_tableHeaders_2[8].column_name = _context2.sent;
                _context2.next = 84;
                return _this2.thd_summary.ACCOUNTS.BUDGET_TOTAL;

              case 84:
                _this2.dt_tableHeaders_2[9].column_name = _context2.sent;
                _context2.next = 87;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1231;

              case 87:
                _this2.dt_tableHeaders[1].column_name = _context2.sent;
                _context2.next = 90;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1232;

              case 90:
                _this2.dt_tableHeaders[2].column_name = _context2.sent;
                _context2.next = 93;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1233;

              case 93:
                _this2.dt_tableHeaders[3].column_name = _context2.sent;
                _context2.next = 96;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1234;

              case 96:
                _this2.dt_tableHeaders[4].column_name = _context2.sent;
                _context2.next = 99;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1235;

              case 99:
                _this2.dt_tableHeaders[5].column_name = _context2.sent;
                _context2.next = 102;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1236;

              case 102:
                _this2.dt_tableHeaders[6].column_name = _context2.sent;
                _context2.next = 105;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1237;

              case 105:
                _this2.dt_tableHeaders[7].column_name = _context2.sent;
                _context2.next = 108;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_1901;

              case 108:
                _this2.dt_tableHeaders[8].column_name = _context2.sent;
                _context2.next = 111;
                return _this2.thd_summary.ACCOUNTS.VARIANCE_TOTAL;

              case 111:
                _this2.dt_tableHeaders[9].column_name = _context2.sent;
                _context2.next = 114;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_FUELS;

              case 114:
                _this2.table_fuels[1].column_name = _context2.sent;
                _context2.next = 117;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_FUELS;

              case 117:
                _this2.table_fuels[2].column_name = _context2.sent;
                _context2.next = 120;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_FUELS;

              case 120:
                _this2.table_fuels[3].column_name = _context2.sent;
                _context2.next = 123;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_FUELS;

              case 123:
                _this2.table_fuels[4].column_name = _context2.sent;
                _context2.next = 126;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_FUELS;

              case 126:
                _this2.table_fuels[5].column_name = _context2.sent;
                _context2.next = 129;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_FUELS;

              case 129:
                _this2.table_fuels[6].column_name = _context2.sent;
                _context2.next = 132;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_FUELS;

              case 132:
                _this2.table_fuels[7].column_name = _context2.sent;
                _context2.next = 135;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_FUELS;

              case 135:
                _this2.table_fuels[8].column_name = _context2.sent;
                _context2.next = 138;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS;

              case 138:
                _this2.table_fuels[9].column_name = _context2.sent;
                _context2.next = 141;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1231;

              case 141:
                _this2.table_fuels_and_selling_exp[1].column_name = _context2.sent;
                _context2.next = 144;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1232;

              case 144:
                _this2.table_fuels_and_selling_exp[2].column_name = _context2.sent;
                _context2.next = 147;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1233;

              case 147:
                _this2.table_fuels_and_selling_exp[3].column_name = _context2.sent;
                _context2.next = 150;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1234;

              case 150:
                _this2.table_fuels_and_selling_exp[4].column_name = _context2.sent;
                _context2.next = 153;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1235;

              case 153:
                _this2.table_fuels_and_selling_exp[5].column_name = _context2.sent;
                _context2.next = 156;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1236;

              case 156:
                _this2.table_fuels_and_selling_exp[6].column_name = _context2.sent;
                _context2.next = 159;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1237;

              case 159:
                _this2.table_fuels_and_selling_exp[7].column_name = _context2.sent;
                _context2.next = 162;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP_1901;

              case 162:
                _this2.table_fuels_and_selling_exp[8].column_name = _context2.sent;
                _context2.next = 165;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_FUELS_AND_SELLING_EXP;

              case 165:
                _this2.table_fuels_and_selling_exp[9].column_name = _context2.sent;
                _context2.next = 168;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_MAIN;

              case 168:
                _this2.table_maintenance[1].column_name = _context2.sent;
                _context2.next = 171;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_MAIN;

              case 171:
                _this2.table_maintenance[2].column_name = _context2.sent;
                _context2.next = 174;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_MAIN;

              case 174:
                _this2.table_maintenance[3].column_name = _context2.sent;
                _context2.next = 177;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_MAIN;

              case 177:
                _this2.table_maintenance[4].column_name = _context2.sent;
                _context2.next = 180;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_MAIN;

              case 180:
                _this2.table_maintenance[5].column_name = _context2.sent;
                _context2.next = 183;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_MAIN;

              case 183:
                _this2.table_maintenance[6].column_name = _context2.sent;
                _context2.next = 186;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_MAIN;

              case 186:
                _this2.table_maintenance[7].column_name = _context2.sent;
                _context2.next = 189;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_MAIN;

              case 189:
                _this2.table_maintenance[8].column_name = _context2.sent;
                _context2.next = 192;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_MAIN;

              case 192:
                _this2.table_maintenance[9].column_name = _context2.sent;
                _context2.next = 195;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_REPAIRS;

              case 195:
                _this2.table_repairs[1].column_name = _context2.sent;
                _context2.next = 198;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_REPAIRS;

              case 198:
                _this2.table_repairs[2].column_name = _context2.sent;
                _context2.next = 201;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_REPAIRS;

              case 201:
                _this2.table_repairs[3].column_name = _context2.sent;
                _context2.next = 204;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_REPAIRS;

              case 204:
                _this2.table_repairs[4].column_name = _context2.sent;
                _context2.next = 207;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_REPAIRS;

              case 207:
                _this2.table_repairs[5].column_name = _context2.sent;
                _context2.next = 210;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_REPAIRS;

              case 210:
                _this2.table_repairs[6].column_name = _context2.sent;
                _context2.next = 213;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_REPAIRS;

              case 213:
                _this2.table_repairs[7].column_name = _context2.sent;
                _context2.next = 216;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_REPAIRS;

              case 216:
                _this2.table_repairs[8].column_name = _context2.sent;
                _context2.next = 219;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_REPAIRS;

              case 219:
                _this2.table_repairs[9].column_name = _context2.sent;
                _context2.next = 222;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_PARKING;

              case 222:
                _this2.table_parking[1].column_name = _context2.sent;
                _context2.next = 225;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_PARKING;

              case 225:
                _this2.table_parking[2].column_name = _context2.sent;
                _context2.next = 228;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_PARKING;

              case 228:
                _this2.table_parking[3].column_name = _context2.sent;
                _context2.next = 231;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_PARKING;

              case 231:
                _this2.table_parking[4].column_name = _context2.sent;
                _context2.next = 234;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_PARKING;

              case 234:
                _this2.table_parking[5].column_name = _context2.sent;
                _context2.next = 237;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_PARKING;

              case 237:
                _this2.table_parking[6].column_name = _context2.sent;
                _context2.next = 240;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_PARKING;

              case 240:
                _this2.table_parking[7].column_name = _context2.sent;
                _context2.next = 243;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_PARKING;

              case 243:
                _this2.table_parking[8].column_name = _context2.sent;
                _context2.next = 246;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_PARKING;

              case 246:
                _this2.table_parking[9].column_name = _context2.sent;
                _context2.next = 249;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_TOLLING;

              case 249:
                _this2.table_tolling[1].column_name = _context2.sent;
                _context2.next = 252;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_TOLLING;

              case 252:
                _this2.table_tolling[2].column_name = _context2.sent;
                _context2.next = 255;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_TOLLING;

              case 255:
                _this2.table_tolling[3].column_name = _context2.sent;
                _context2.next = 258;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_TOLLING;

              case 258:
                _this2.table_tolling[4].column_name = _context2.sent;
                _context2.next = 261;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_TOLLING;

              case 261:
                _this2.table_tolling[5].column_name = _context2.sent;
                _context2.next = 264;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_TOLLING;

              case 264:
                _this2.table_tolling[6].column_name = _context2.sent;
                _context2.next = 267;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_TOLLING;

              case 267:
                _this2.table_tolling[7].column_name = _context2.sent;
                _context2.next = 270;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_TOLLING;

              case 270:
                _this2.table_tolling[8].column_name = _context2.sent;
                _context2.next = 273;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_TOLLING;

              case 273:
                _this2.table_tolling[9].column_name = _context2.sent;
                _context2.next = 276;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_UTILITIES;

              case 276:
                _this2.table_utilities[1].column_name = _context2.sent;
                _context2.next = 279;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_UTILITIES;

              case 279:
                _this2.table_utilities[2].column_name = _context2.sent;
                _context2.next = 282;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_UTILITIES;

              case 282:
                _this2.table_utilities[3].column_name = _context2.sent;
                _context2.next = 285;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_UTILITIES;

              case 285:
                _this2.table_utilities[4].column_name = _context2.sent;
                _context2.next = 288;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_UTILITIES;

              case 288:
                _this2.table_utilities[5].column_name = _context2.sent;
                _context2.next = 291;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_UTILITIES;

              case 291:
                _this2.table_utilities[6].column_name = _context2.sent;
                _context2.next = 294;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_UTILITIES;

              case 294:
                _this2.table_utilities[7].column_name = _context2.sent;
                _context2.next = 297;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_UTILITIES;

              case 297:
                _this2.table_utilities[8].column_name = _context2.sent;
                _context2.next = 300;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_UTILITIES;

              case 300:
                _this2.table_utilities[9].column_name = _context2.sent;
                _context2.next = 303;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1231_admin_exp;

              case 303:
                _this2.table_admin_expenses[1].column_name = _context2.sent;
                _context2.next = 306;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1232_admin_exp;

              case 306:
                _this2.table_admin_expenses[2].column_name = _context2.sent;
                _context2.next = 309;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1233_admin_exp;

              case 309:
                _this2.table_admin_expenses[3].column_name = _context2.sent;
                _context2.next = 312;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1234_admin_exp;

              case 312:
                _this2.table_admin_expenses[4].column_name = _context2.sent;
                _context2.next = 315;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1235_admin_exp;

              case 315:
                _this2.table_admin_expenses[5].column_name = _context2.sent;
                _context2.next = 318;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1236_admin_exp;

              case 318:
                _this2.table_admin_expenses[6].column_name = _context2.sent;
                _context2.next = 321;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1237_admin_exp;

              case 321:
                _this2.table_admin_expenses[7].column_name = _context2.sent;
                _context2.next = 324;
                return _this2.thd_summary.ACCOUNTS.TOTAL_1901_admin_exp;

              case 324:
                _this2.table_admin_expenses[8].column_name = _context2.sent;
                _context2.next = 327;
                return _this2.thd_summary.ACCOUNTS.TOTAL_ALL_admin_exp;

              case 327:
                _this2.table_admin_expenses[9].column_name = _context2.sent;
                _context2.next = 330;
                return _this2.thd_summary.ACCOUNTS.OPEX_1231;

              case 330:
                _this2.table_operation_exp[1].column_name = _context2.sent;
                _context2.next = 333;
                return _this2.thd_summary.ACCOUNTS.OPEX_1232;

              case 333:
                _this2.table_operation_exp[2].column_name = _context2.sent;
                _context2.next = 336;
                return _this2.thd_summary.ACCOUNTS.OPEX_1233;

              case 336:
                _this2.table_operation_exp[3].column_name = _context2.sent;
                _context2.next = 339;
                return _this2.thd_summary.ACCOUNTS.OPEX_1234;

              case 339:
                _this2.table_operation_exp[4].column_name = _context2.sent;
                _context2.next = 342;
                return _this2.thd_summary.ACCOUNTS.OPEX_1235;

              case 342:
                _this2.table_operation_exp[5].column_name = _context2.sent;
                _context2.next = 345;
                return _this2.thd_summary.ACCOUNTS.OPEX_1236;

              case 345:
                _this2.table_operation_exp[6].column_name = _context2.sent;
                _context2.next = 348;
                return _this2.thd_summary.ACCOUNTS.OPEX_1237;

              case 348:
                _this2.table_operation_exp[7].column_name = _context2.sent;
                _context2.next = 351;
                return _this2.thd_summary.ACCOUNTS.OPEX_1901;

              case 351:
                _this2.table_operation_exp[8].column_name = _context2.sent;
                _context2.next = 354;
                return _this2.thd_summary.ACCOUNTS.OPEX_TOTAL;

              case 354:
                _this2.table_operation_exp[9].column_name = _context2.sent;

              case 355:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    deleteUser: function deleteUser(e, index) {
      var _this3 = this;

      this.$swal({
        title: 'Are you sure to delete this user?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/users/".concat(e);
          axios["delete"](uri).then(function (response) {
            _this3.$swal('Deleted!', 'Your file has been deleted.', 'success');

            _this3.users.data.splice(index, 1);
          });
        }
      });
    },
    showCreateView: function showCreateView() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this4.showCreate = true;
                _this4.showEdit = false;
                _context3.next = 4;
                return _this4.$store.commit('user/USER_CLEAR');

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    closeCreate: function closeCreate(value) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this5.showCreate = value;
                _this5.showEdit = false;
                _context4.next = 4;
                return _this5.$store.commit('user/USER_CLEAR');

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    closeEdit: function closeEdit() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _this6.showCreate = false;
                _this6.showEdit = false;
                _context5.next = 4;
                return _this6.$store.commit('user/USER_CLEAR');

              case 4:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    edit: function edit(id) {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _this7.showEdit = true;
                _this7.showCreate = false;
                _context6.next = 4;
                return _this7.$store.commit('user/USER_CLEAR');

              case 4:
                _context6.next = 6;
                return _this7.$store.commit('user/USER_ID', id);

              case 6:
                _context6.next = 8;
                return _this7.editUser();

              case 8:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    sortByTable: function sortByTable(value) {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return _this8.$store.commit('user/USER_SORTBY', value.sortBy);

              case 2:
                _context7.next = 4;
                return _this8.$store.commit('user/USER_SORTDIR', value.sortDir);

              case 4:
                _context7.next = 6;
                return _this8.index();

              case 6:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    },
    paginateTable: function paginateTable(value) {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.next = 2;
                return _this9.$store.commit('user/USER_PERPAGE', value);

              case 2:
                _context8.next = 4;
                return _this9.index();

              case 4:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    },
    searchData: function searchData(value) {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.next = 2;
                return _this10.$store.commit('user/USER_SEARCH', value);

              case 2:
                _context9.next = 4;
                return _this10.index();

              case 4:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }))();
    },
    index: function index(page) {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                if (!(typeof page === 'undefined')) {
                  _context10.next = 3;
                  break;
                }

                _context10.next = 3;
                return _this11.$store.commit('user/USER_PAGE', 1);

              case 3:
                _context10.next = 5;
                return _this11.$store.commit('user/USER_PAGE', page);

              case 5:
                _context10.next = 7;
                return _this11.getUser();

              case 7:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    },
    formatNumber: function formatNumber(number) {
      // return (number === '' || number === undefined) ? parseFloat('0') : parseFloat(Math.abs(number)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    },
    formatNumberNoAbs: function formatNumberNoAbs(number) {
      return number > 0 ? parseFloat(number).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : parseFloat(number).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.filter-asc[data-v-ae3088d4] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-ae3088d4], .filter-desc[data-v-ae3088d4] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-ae3088d4] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.active-filter-asc[data-v-ae3088d4] {\n    border-bottom: 5px solid #a3a3a3\n}\n.active-filter-desc[data-v-ae3088d4] {\n    border-top: 5px solid #a3a3a3\n}\n.inline-block[data-v-ae3088d4] {\n    display: inline-block\n}\n.dark_sky_blue_last_four[data-v-ae3088d4]{\n    background-color: #1c7ea6;\n}\n.dark_sky_blue_last_two[data-v-ae3088d4]{\n    background-color: #2197c7;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.filter-asc[data-v-e17b6d7a] {\n    border-bottom: 5px solid #ccc;\n    margin-bottom: 1px\n}\n.filter-asc[data-v-e17b6d7a], .filter-desc[data-v-e17b6d7a] {\n    width: 0;\n    height: 0;\n    border-left: 5px solid transparent;\n    border-right: 5px solid transparent\n}\n.filter-desc[data-v-e17b6d7a] {\n    border-top: 5px solid #ccc;\n    margin-top: 1px\n}\n.active-filter-asc[data-v-e17b6d7a] {\n    border-bottom: 5px solid #a3a3a3\n}\n.active-filter-desc[data-v-e17b6d7a] {\n    border-top: 5px solid #a3a3a3\n}\n.inline-block[data-v-e17b6d7a] {\n    display: inline-block\n}\n.dark_sky_blue_last_four[data-v-e17b6d7a]{\n    background-color: #1c7ea6;\n}\n.dark_sky_blue_last_two[data-v-e17b6d7a]{\n    background-color: #2197c7;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row form-group mb-5" }),
    _vm._v(" "),
    _c("div", { staticClass: "table-responsive" }, [
      _c(
        "table",
        { class: _vm.dt_classTable, attrs: { width: _vm.dt_TableWidth + "%" } },
        [
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_per_month, function(th) {
                    return [
                      th.sortable
                        ? _c(
                            "th",
                            {
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass: "inline-block",
                                  staticStyle: { cursor: "pointer" },
                                  on: {
                                    click: function($event) {
                                      return _vm.sort(th.query_name)
                                    }
                                  }
                                },
                                [
                                  _c("div", {
                                    staticClass: "filter-asc",
                                    class: {
                                      "active-filter-asc":
                                        _vm.sortClassActive.activeAsc
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("div", {
                                    staticClass: "filter-desc",
                                    class: {
                                      "active-filter-desc":
                                        _vm.sortClassActive.activeDesc
                                    }
                                  })
                                ]
                              ),
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n\n                    "
                              )
                            ]
                          )
                        : _c(
                            "th",
                            {
                              class: [
                                { "text-center": th.class_text_center },
                                { "table-primary": th.dt_tableClassHeaders },
                                {
                                  dark_sky_blue_last_four:
                                    th.dark_sky_blue_last_four
                                },
                                { "text-white": th.font_text_white },
                                { "bg-info": th.header_bg_info },
                                {
                                  dark_sky_blue_last_two:
                                    th.dark_sky_blue_last_two
                                },
                                { "font-weight-bold": th.font_weight_bold },
                                { "text-right": th.class_text_right }
                              ],
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan,
                                rowspan: th.rowspan
                              }
                            },
                            [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n                    "
                              )
                            ]
                          )
                    ]
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_per_month_2, function(th) {
                    return [
                      th.sortable
                        ? _c(
                            "th",
                            {
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass: "inline-block",
                                  staticStyle: { cursor: "pointer" },
                                  on: {
                                    click: function($event) {
                                      return _vm.sort(th.query_name)
                                    }
                                  }
                                },
                                [
                                  _c("div", {
                                    staticClass: "filter-asc",
                                    class: {
                                      "active-filter-asc":
                                        _vm.sortClassActive.activeAsc
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("div", {
                                    staticClass: "filter-desc",
                                    class: {
                                      "active-filter-desc":
                                        _vm.sortClassActive.activeDesc
                                    }
                                  })
                                ]
                              ),
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n\n                    "
                              )
                            ]
                          )
                        : _c(
                            "th",
                            {
                              class: [
                                { "text-center": th.class_text_center },
                                { "table-primary": th.dt_tableClassHeaders },
                                {
                                  dark_sky_blue_last_four:
                                    th.dark_sky_blue_last_four
                                },
                                { "text-white": th.font_text_white },
                                { "bg-info": th.header_bg_info },
                                {
                                  dark_sky_blue_last_two:
                                    th.dark_sky_blue_last_two
                                },
                                { "font-weight-bold": th.font_weight_bold },
                                { "text-right": th.class_text_right }
                              ],
                              attrs: {
                                width: th.width + "%",
                                colspan: th.colspan,
                                rowspan: th.rowspan
                              }
                            },
                            [
                              _vm._v(
                                "\n                        " +
                                  _vm._s(th.column_name) +
                                  "\n                    "
                              )
                            ]
                          )
                    ]
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableData", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    {
                      attrs: { colspan: _vm.dt_tableHeaders_per_month.length }
                    },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          )
        ]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row " }, [
      _c("div", { staticClass: "col-md-6" }, [_vm._t("perPage")], 2)
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-3 offset-md-3 " },
        [_vm._t("pagination")],
        2
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row  form-group" }),
    _vm._v(" "),
    _c("div", { class: [{ "table-responsive": false }] }, [
      _c(
        "table",
        { class: _vm.dt_classTable, attrs: { width: _vm.dt_TableWidth + "%" } },
        [
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ],
              class: _vm.dt_tableClassHeaders
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "VARIANCE" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_2, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "BUDGET" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_3, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "TOTAL OPEX" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.dt_tableHeaders_4, function(th, index) {
                    return _vm.allowedCompany.includes(th.column_name) ||
                      th.column_name === "COMPANY" ||
                      th.column_name === _vm.totalString
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.table_salaries, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "SALARIES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c("tbody", [_vm._t("thdSummaryDataTableData", [_vm._m(0)])], 2),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_fuels_and_selling_exp, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "FUEL AND SELLING EXPENSES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.table_fuels, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "FUELS" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableFuels", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_fuels.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_maintenance, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "MAINTENANCE" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableMaintenance", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    { attrs: { colspan: _vm.table_maintenance.length } },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_repairs, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "REPAIRS" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableRepairs", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_repairs.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_parking, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "PARKING" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableParking", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_parking.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_tolling, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "TOLLING" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableTolling", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_tolling.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._t("tableFuelAndSellingExp", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    {
                      attrs: { colspan: _vm.table_fuels_and_selling_exp.length }
                    },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_admin_expenses, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "ADMINISTRATIVE EXPENSES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "tr",
                [
                  _vm._l(_vm.table_utilities, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "UTILITIES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._t("tableUtilities", [
                _c("tr", { staticClass: "text-center" }, [
                  _c("td", { attrs: { colspan: _vm.table_utilities.length } }, [
                    _c("p", [_vm._v("no data available")])
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._t("tableAdminExpenses", [
                _c("tr", { staticClass: "text-center" }, [
                  _c(
                    "td",
                    { attrs: { colspan: _vm.table_admin_expenses.length } },
                    [_c("p", [_vm._v("no data available")])]
                  )
                ])
              ])
            ],
            2
          ),
          _vm._v(" "),
          _c(
            "thead",
            {
              directives: [
                { name: "columns-resizable", rawName: "v-columns-resizable" }
              ]
            },
            [
              _c(
                "tr",
                [
                  _vm._l(_vm.table_operation_exp, function(th, index) {
                    return _vm.allowedCompanyIndex.includes(index) ||
                      th.column_name === "OPERATING EXPENSES" ||
                      index === 9 - _vm.isIndex
                      ? [
                          th.sortable
                            ? _c(
                                "th",
                                {
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "inline-block",
                                      staticStyle: { cursor: "pointer" },
                                      on: {
                                        click: function($event) {
                                          return _vm.sort(th.query_name)
                                        }
                                      }
                                    },
                                    [
                                      _c("div", {
                                        staticClass: "filter-asc",
                                        class: {
                                          "active-filter-asc":
                                            _vm.sortClassActive.activeAsc
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "filter-desc",
                                        class: {
                                          "active-filter-desc":
                                            _vm.sortClassActive.activeDesc
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n\n                    "
                                  )
                                ]
                              )
                            : _c(
                                "th",
                                {
                                  class: [
                                    { "text-center": th.class_text_center },
                                    {
                                      "table-primary": th.dt_tableClassHeaders
                                    },
                                    {
                                      dark_sky_blue_last_four:
                                        th.dark_sky_blue_last_four
                                    },
                                    { "text-white": th.font_text_white },
                                    { "bg-info": th.header_bg_info },
                                    {
                                      dark_sky_blue_last_two:
                                        th.dark_sky_blue_last_two
                                    },
                                    { "font-weight-bold": th.font_weight_bold },
                                    { "text-right": th.class_text_right }
                                  ],
                                  attrs: {
                                    width: th.width + "%",
                                    colspan: th.colspan,
                                    rowspan: th.rowspan
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(th.column_name) +
                                      "\n                    "
                                  )
                                ]
                              )
                        ]
                      : _vm._e()
                  })
                ],
                2
              )
            ]
          )
        ]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row " }, [
      _c("div", { staticClass: "col-md-6" }, [_vm._t("perPage")], 2)
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-3 offset-md-3 " },
        [_vm._t("pagination")],
        2
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", { staticClass: "text-center" }, [
      _c("td", { attrs: { colspan: "10" } }, [
        _c("p", [_vm._v("no data available")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [_c("router-view")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row" },
    [
      _c("div", { staticClass: "col-md-12 col-sm-12 col-lg-12" }, [
        _c("div", { staticClass: "card" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6" }, [
                _c(
                  "div",
                  { staticClass: "input-group" },
                  [
                    _vm._m(2),
                    _vm._v(" "),
                    _c(
                      "date-picker",
                      {
                        attrs: {
                          placeholder: "Select date range",
                          formatter: _vm.momentFormat,
                          "value-type": "format",
                          type: "date",
                          range: ""
                        },
                        model: {
                          value: _vm.dateFromAndTo,
                          callback: function($$v) {
                            _vm.dateFromAndTo = $$v
                          },
                          expression: "dateFromAndTo"
                        }
                      },
                      [
                        _vm._v(
                          '\n                                             class="form-control"\n                                '
                        )
                      ]
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "clearfix" })
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.thdSummaryHasData
        ? _c(
            "div",
            {
              ref: "loadingContainer",
              staticClass: "col-md-12 col-sm-12 col-lg-12"
            },
            [
              _c("div", { staticClass: "card" }, [
                _vm._m(3),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("div", { staticClass: "toolbar" }),
                    _vm._v(" "),
                    _c("THDSummaryDataTable", {
                      attrs: {
                        dt_tableHeaders: _vm.dt_tableHeaders,
                        dt_tableHeaders_2: _vm.dt_tableHeaders_2,
                        dt_tableHeaders_3: _vm.dt_tableHeaders_3,
                        dt_tableHeaders_4: _vm.dt_tableHeaders_4,
                        table_salaries: _vm.table_salaries,
                        table_fuels_and_selling_exp:
                          _vm.table_fuels_and_selling_exp,
                        table_fuels: _vm.table_fuels,
                        table_maintenance: _vm.table_maintenance,
                        table_repairs: _vm.table_repairs,
                        table_parking: _vm.table_parking,
                        table_tolling: _vm.table_tolling,
                        table_utilities: _vm.table_utilities,
                        table_admin_expenses: _vm.table_admin_expenses,
                        table_operation_exp: _vm.table_operation_exp,
                        dt_currentSort: _vm.tableSortBy,
                        dt_currentSortDir: _vm.tableSortDir,
                        dt_defaultEntries: _vm.tableDefaultEntries,
                        dt_Search: _vm.search,
                        dt_classTable: "table table-bordered",
                        dt_TableWidth: "100"
                      },
                      on: {
                        sortColumn: _vm.sortByTable,
                        perPageEntry: _vm.paginateTable,
                        searchItem: _vm.searchData
                      },
                      scopedSlots: _vm._u(
                        [
                          _vm.showIT
                            ? {
                                key: "thdSummaryDataTableData",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS.SALARIES,
                                    function(thd, index) {
                                      return _vm.$can(
                                        "allowed_salaries_breakdown"
                                      )
                                        ? _c("tr", [
                                            _c("td", [
                                              _vm._v(_vm._s(thd.name))
                                            ]),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "hilado_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1231)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "sibulan_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1232)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "roxas_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1233)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "sumag_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1234)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "diversion_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1235)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can("jaro_company_thd_summary")
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1236)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "kabankalan_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1237)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can(
                                              "corporate_services_company_thd_summary"
                                            )
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(thd.company_1901)
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.$can("show_total_thd_summary")
                                              ? _c(
                                                  "td",
                                                  { staticClass: "text-right" },
                                                  [_vm._v(_vm._s(thd.total))]
                                                )
                                              : _vm._e()
                                          ])
                                        : _vm._e()
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableFuels",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS.FUELS,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableMaintenance",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS
                                      .MAINTENANCE,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableRepairs",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS.REPAIRS,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableParking",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS.PARKING,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableTolling",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS.TOLLING,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableFuelAndSellingExp",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS
                                      .FUEL_AND_SELLING_EXP,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableUtilities",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS.UTILITIES,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null,
                          _vm.showIT
                            ? {
                                key: "tableAdminExpenses",
                                fn: function() {
                                  return _vm._l(
                                    _vm.thd_summary.ACCOUNTS.DETAILS
                                      .ADMIN_EXPENSE,
                                    function(thd, index) {
                                      return _c("tr", [
                                        _c("td", [_vm._v(_vm._s(thd.name))]),
                                        _vm._v(" "),
                                        _vm.$can("hilado_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1231))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sibulan_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1232))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("roxas_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1233))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("sumag_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1234))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "diversion_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1235))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("jaro_company_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1236))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "kabankalan_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1237))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can(
                                          "corporate_services_company_thd_summary"
                                        )
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.company_1901))]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.$can("show_total_thd_summary")
                                          ? _c(
                                              "td",
                                              { staticClass: "text-right" },
                                              [_vm._v(_vm._s(thd.total))]
                                            )
                                          : _vm._e()
                                      ])
                                    }
                                  )
                                },
                                proxy: true
                              }
                            : null
                        ],
                        null,
                        true
                      )
                    })
                  ],
                  1
                )
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.showCreate,
                expression: "showCreate"
              }
            ],
            staticClass: "col-md-12 col-sm-12 col-lg-6 "
          },
          [_c("UserCreate", { on: { cancelCreate: _vm.closeCreate } })],
          1
        )
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.showEdit,
                expression: "showEdit"
              }
            ],
            ref: "editFormRef",
            staticClass: "col-md-12 col-sm-12 col-lg-6 "
          },
          [_c("UserEdit", { on: { cancelEdit: _vm.closeEdit } })],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header card-header-primary card-header-text" },
      [
        _c("div", { staticClass: "card-icon" }, [
          _c("i", { staticClass: "material-icons" }, [_vm._v("feed")])
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "card-title" }, [
          _vm._v("Budget VS Actual Report THD Summary")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6" }, [
        _c("div", { staticClass: "input-group" }, [
          _c("div", { staticClass: "input-group-prepend" }, [
            _c(
              "label",
              {
                staticClass:
                  "input-group-text input-group-field bmd-label-floating"
              },
              [_vm._v("Currency\n                                        :")]
            )
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "bmd-label-floating mt-2 text-primary" }, [
            _vm._v("PHP")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("div", { staticClass: "input-group" }, [
          _c("div", { staticClass: "input-group-prepend" }, [
            _c(
              "label",
              {
                staticClass:
                  "input-group-text input-group-field bmd-label-floating text-primary"
              },
              [
                _vm._v(
                  "Amounts\n                                        in '000"
                )
              ]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "label",
        {
          staticClass: "input-group-text input-group-field bmd-label-floating"
        },
        [_vm._v("Posting Date\n                                        :")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header card-header-primary card-header-icon" },
      [
        _c("div", { staticClass: "card-icon" }, [
          _c("i", { staticClass: "material-icons" }, [_vm._v("monitor")])
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "card-title" }, [
          _vm._v("THD Summary All Companies")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue":
/*!********************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& */ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&");
/* harmony import */ var _SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SummaryBreakDown.vue?vue&type=script&lang=js& */ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& */ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "ae3088d4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/tables/SummaryBreakDown.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=style&index=0&id=ae3088d4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_style_index_0_id_ae3088d4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/SummaryBreakDown.vue?vue&type=template&id=ae3088d4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SummaryBreakDown_vue_vue_type_template_id_ae3088d4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue":
/*!***********************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& */ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&");
/* harmony import */ var _THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./THDSummaryDataTable.vue?vue&type=script&lang=js& */ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& */ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "e17b6d7a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/tables/THDSummaryDataTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=style&index=0&id=e17b6d7a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_style_index_0_id_e17b6d7a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/tables/THDSummaryDataTable.vue?vue&type=template&id=e17b6d7a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryDataTable_vue_vue_type_template_id_e17b6d7a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/thd_summary/THDSummaryIndex.vue":
/*!************************************************************!*\
  !*** ./resources/js/views/thd_summary/THDSummaryIndex.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _THDSummaryIndex_vue_vue_type_template_id_242d09d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true& */ "./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true&");
/* harmony import */ var _THDSummaryIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./THDSummaryIndex.vue?vue&type=script&lang=js& */ "./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _THDSummaryIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _THDSummaryIndex_vue_vue_type_template_id_242d09d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _THDSummaryIndex_vue_vue_type_template_id_242d09d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "242d09d5",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/thd_summary/THDSummaryIndex.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryIndex.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryIndex_vue_vue_type_template_id_242d09d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryIndex.vue?vue&type=template&id=242d09d5&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryIndex_vue_vue_type_template_id_242d09d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryIndex_vue_vue_type_template_id_242d09d5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/thd_summary/THDSummaryTable.vue":
/*!************************************************************!*\
  !*** ./resources/js/views/thd_summary/THDSummaryTable.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _THDSummaryTable_vue_vue_type_template_id_446f825e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true& */ "./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true&");
/* harmony import */ var _THDSummaryTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./THDSummaryTable.vue?vue&type=script&lang=js& */ "./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _THDSummaryTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _THDSummaryTable_vue_vue_type_template_id_446f825e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _THDSummaryTable_vue_vue_type_template_id_446f825e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "446f825e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/thd_summary/THDSummaryTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryTable_vue_vue_type_template_id_446f825e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/thd_summary/THDSummaryTable.vue?vue&type=template&id=446f825e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryTable_vue_vue_type_template_id_446f825e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_THDSummaryTable_vue_vue_type_template_id_446f825e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);