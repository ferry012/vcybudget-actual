(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["static-budget"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticCreate.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticCreate.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vform__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vform */ "./node_modules/vform/dist/vform.common.js");
/* harmony import */ var vform__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vform__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_Form */ "./resources/js/views/static_budget/_Form.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StaticBudgetCreate",
  components: {
    HasError: vform__WEBPACK_IMPORTED_MODULE_1__["HasError"],
    AlertError: vform__WEBPACK_IMPORTED_MODULE_1__["AlertError"],
    AlertSuccess: vform__WEBPACK_IMPORTED_MODULE_1__["AlertSuccess"],
    StaticBudgetForm: _Form__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])({
    staticBudget: ["static_budget/staticBudget"],
    salaries: ["static_budget/salaries"],
    submitBtn: ["static_budget/submitBtn"] // staticBudgetArr:["static_budget/staticBudgetArr"],

  })), {}, {
    totalExpense: function totalExpense() {
      // <--questionable remove if not use or will cause error.
      //Salary
      var salArr = [];
      var xSal = 0;

      for (var i = 0; i < this.staticBudget.salaries.length; i++) {
        xSal += parseFloat(this.staticBudget.salaries[i].sal_budget);
        salArr.push({
          'salary': xSal
        });
      }

      var salary = 0;

      if (salArr.length !== 0) {
        var lastSal = salArr.length - 1;
        salary = salArr[lastSal].salary;
      } //fuel


      var fuelArr = [];
      var xFuel = 0;

      for (var _i = 0; _i < this.staticBudget.fuels.length; _i++) {
        xFuel += parseFloat(this.staticBudget.fuels[_i].fuel_budget);
        console.log(xFuel, 'x');
        fuelArr.push({
          'fuel': xFuel
        });
      }

      var fuel = 0;

      if (fuelArr.length !== 0) {
        var lastFuel = fuelArr.length - 1;
        fuel = fuelArr[lastFuel].fuel;
      } //utilities


      var admin_expensesArr = [];
      var xAdmin_expenses = 0;

      for (var _i2 = 0; _i2 < this.staticBudget.utilities.length; _i2++) {
        xAdmin_expenses += parseFloat(this.staticBudget.utilities[_i2].utilities_budget);
        admin_expensesArr.push({
          'utilities': xAdmin_expenses
        });
      }

      var utilities = 0;

      if (admin_expensesArr.length !== 0) {
        var lastAdmin_expenses = admin_expensesArr.length - 1;
        utilities = admin_expensesArr[lastAdmin_expenses].utilities;
      } //total of all expenses


      var allTotalExpense = salary + fuel + utilities; //=SUM(D37,D11,D6)

      if (isNaN(allTotalExpense)) {
        // true if NaN
        this.staticBudget.totalNotNaN = false; // hide data
      } else {
        // this.staticBudget.totalExpenses = allTotalExpense;
        this.staticBudget.totalNotNaN = true; // show data
        // return allTotalExpense;
      } // return this.firstName + ' ' + this.lastName

    },
    totalSalaries: function totalSalaries() {
      //Salary
      var salArr = [];
      var xSal = 0; // let no_comma = this.staticBudget.salaries[2].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""); // TODO Fix error with the comma ang backend and font end
      // console.log(this.staticBudget.salaries,no_comma,this.staticBudget.salaries[2].sal_budget,parseFloat(no_comma) + parseFloat(this.staticBudget.salaries[1].sal_budget),parseFloat(no_comma + this.staticBudget.salaries[1].sal_budget),'this.staticBudget.salaries edit');
      // let total = parseFloat(this.staticBudget.salaries[1].sal_budget + this.staticBudget.salaries[2].sal_budget);
      // return total;

      for (var i = 0; i < this.staticBudget.salaries.length; i++) {
        if (this.staticBudget.salaries[i].sal_budget !== null) {
          xSal += parseFloat(this.staticBudget.salaries[i].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        salArr.push({
          'salary': xSal
        });
      }

      var salary = 0;

      if (salArr.length !== 0) {
        var lastSal = salArr.length - 1;
        salary = salArr[lastSal].salary;
        this.staticBudget.totalSalaries = salary;
        return salary;
      }
    },
    totalFuelAndSellingExp: function totalFuelAndSellingExp() {
      //TOTAL FUEL AND SELLING EXPENSES
      //FUEL
      console.log();
      var fuelArr = [];
      var xFuel = 0;

      for (var i = 0; i < this.staticBudget.fuels.length; i++) {
        if (this.staticBudget.fuels[i].fuel_budget !== null) {
          xFuel += parseFloat(this.staticBudget.fuels[i].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        fuelArr.push({
          'fuel': xFuel
        });
      }

      var fuel = 0;

      if (fuelArr.length !== 0) {
        var lastFuel = fuelArr.length - 1;
        fuel = fuelArr[lastFuel].fuel;
      } //MAINTENANCE


      var mainArr = [];
      var xMain = 0;

      for (var _i3 = 0; _i3 < this.staticBudget.maintenance.length; _i3++) {
        if (this.staticBudget.maintenance[_i3].main_budget !== null) {
          xMain += parseFloat(this.staticBudget.maintenance[_i3].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        mainArr.push({
          'maintenance': xMain
        });
      }

      var maintenance = 0;

      if (mainArr.length !== 0) {
        var lastMaintenance = mainArr.length - 1;
        maintenance = mainArr[lastMaintenance].maintenance;
      } //REPAIRS


      var repairArr = [];
      var xRepair = 0;

      for (var _i4 = 0; _i4 < this.staticBudget.repairs.length; _i4++) {
        if (this.staticBudget.repairs[_i4].repairs_budget !== null) {
          xRepair += parseFloat(this.staticBudget.repairs[_i4].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        repairArr.push({
          'repairs': xRepair
        });
      }

      var repairs = 0;

      if (repairArr.length !== 0) {
        var lastRepairs = repairArr.length - 1;
        repairs = repairArr[lastRepairs].repairs;
      } //PARKING


      var parkingArr = [];
      var xParking = 0;

      for (var _i5 = 0; _i5 < this.staticBudget.parking.length; _i5++) {
        if (this.staticBudget.parking[_i5].parking_budget !== null) {
          xParking += parseFloat(this.staticBudget.parking[_i5].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        parkingArr.push({
          'parking': xParking
        });
      }

      var parking = 0;

      if (parkingArr.length !== 0) {
        var lastParking = parkingArr.length - 1;
        parking = parkingArr[lastParking].parking;
      } //TOLLING


      var tollingArr = [];
      var xTolling = 0;

      for (var _i6 = 0; _i6 < this.staticBudget.tolling.length; _i6++) {
        if (this.staticBudget.tolling[_i6].tolling_budget !== null) {
          xTolling += parseFloat(this.staticBudget.tolling[_i6].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        tollingArr.push({
          'tolling': xTolling
        });
      }

      var tolling = 0;

      if (tollingArr.length !== 0) {
        var lastTolling = tollingArr.length - 1;
        tolling = tollingArr[lastTolling].tolling;
      } //Under TOTAL FUEL AND SELLING EXPENSES


      var totalFuelandSellingExpArr = [];
      var xtotalFuelandSellingExp = 0;

      for (var _i7 = 0; _i7 < this.staticBudget.under_total_fuel_and_exp.length; _i7++) {
        if (this.staticBudget.under_total_fuel_and_exp[_i7].under_total_fuel_and_exp_budget !== null) {
          xtotalFuelandSellingExp += parseFloat(this.staticBudget.under_total_fuel_and_exp[_i7].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        totalFuelandSellingExpArr.push({
          'totalFuelandSellingExp': xtotalFuelandSellingExp
        });
      } // console.log(totalFuelandSellingExpArr,'totalFuelandSellingExp');


      var totalFuelandSellingExp = 0;

      if (totalFuelandSellingExpArr.length !== 0) {
        var lastTotalFuelandSellingExp = totalFuelandSellingExpArr.length - 1;
        totalFuelandSellingExp = totalFuelandSellingExpArr[lastTotalFuelandSellingExp].totalFuelandSellingExp;
      }

      console.log(fuel, maintenance, repairs, parking, tolling, totalFuelandSellingExp, 'fuel + maintenance + repairs + parking + tolling + totalFuelandSellingExp');
      var total = fuel + maintenance + repairs + parking + tolling + totalFuelandSellingExp;
      this.staticBudget.totalFuelAndSellingExp = total;
      return total;
    },
    totalAdminExpenses: function totalAdminExpenses() {
      //TOTAL ADMINISTRATIVE EXPENSES
      //UTILITIES
      var utilitiesArr = [];
      var xUtilities = 0;

      for (var i = 0; i < this.staticBudget.utilities.length; i++) {
        if (this.staticBudget.utilities[i].utilities_budget !== null) {
          xUtilities += parseFloat(this.staticBudget.utilities[i].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        utilitiesArr.push({
          'utilities': xUtilities
        });
      }

      var utilities = 0;

      if (utilitiesArr.length !== 0) {
        var lastUtilities = utilitiesArr.length - 1;
        utilities = utilitiesArr[lastUtilities].utilities;
      } //Under Total Admin Expense


      var underTotalAdminExpArr = [];
      var xUnderTotalAdminExp = 0;

      for (var _i8 = 0; _i8 < this.staticBudget.under_total_admin.length; _i8++) {
        if (this.staticBudget.under_total_admin[_i8].under_total_admin_budget !== null) {
          xUnderTotalAdminExp += parseFloat(this.staticBudget.under_total_admin[_i8].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        underTotalAdminExpArr.push({
          'underTotalAdminExp': xUnderTotalAdminExp
        });
      }

      var underTotalAdminExp = 0;

      if (underTotalAdminExpArr.length !== 0) {
        var lastUnderTotalAdminExp = underTotalAdminExpArr.length - 1;
        underTotalAdminExp = underTotalAdminExpArr[lastUnderTotalAdminExp].underTotalAdminExp;
      }

      var total = utilities + underTotalAdminExp;
      this.staticBudget.totalAdminExpenses = total;
      return total;
    },
    totalOperatingExpense: function totalOperatingExpense() {
      //TOTAL SALARIES + TOTAL FUEL AND SELLING EXPENSES + TOTAL ADMINISTRATIVE EXPENSES
      var total = this.staticBudget.totalAdminExpenses + this.staticBudget.totalFuelAndSellingExp + this.staticBudget.totalSalaries;
      this.staticBudget.totalExpenses = total;
      return total;
    }
  }),
  watch: {
    "totalOperatingExpense": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalOperatingExpense');
      }
    },
    "totalAdminExpenses": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalAdminExpenses');
      }
    },
    "totalFuelAndSellingExp": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalFuelAndSellingExp');
      }
    },
    "totalSalaries": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalSalaries');
      }
    },
    "totalExpense": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalExpense');
      }
    },
    "staticBudget.salaries": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'staticBudget.salaries');
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            console.log(newVal[i].sal_name, newVal[i].sal_budget, i, 'valueFunc[i]');

            if (newVal.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].sal_name !== null && newVal[i].sal_budget !== null && newVal[i].sal_name !== '' && newVal[i].sal_budget !== '') {
                if (this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodMaintenace = 0;
                  var goodFuel = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0; //this.staticBudget.salaries[x].sal_name

                  for (var x = 0; x < this.staticBudget.fuels.length; x++) {
                    // console.log(fuels, 'fuels');
                    if (this.staticBudget.fuels[x].fuel_name !== null && this.staticBudget.fuels[x].fuel_budget !== null && this.staticBudget.fuels[x].fuel_name !== null && this.staticBudget.fuels[x].fuel_budget !== '') {
                      goodFuel = 1;
                    } else {
                      goodFuel = 0;
                    }
                  }

                  for (var _x = 0; _x < this.staticBudget.maintenance.length; _x++) {
                    if (this.staticBudget.maintenance[_x].main_name !== null && this.staticBudget.maintenance[_x].main_budget !== null && this.staticBudget.maintenance[_x].main_name !== '' && this.staticBudget.maintenance[_x].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x2 = 0; _x2 < this.staticBudget.repairs.length; _x2++) {
                    if (this.staticBudget.repairs[_x2].repairs_name !== null && this.staticBudget.repairs[_x2].repairs_budget !== null && this.staticBudget.repairs[_x2].repairs_name !== '' && this.staticBudget.repairs[_x2].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x3 = 0; _x3 < this.staticBudget.parking.length; _x3++) {
                    if (this.staticBudget.parking[_x3].parking_name !== null && this.staticBudget.parking[_x3].parking_budget !== null && this.staticBudget.parking[_x3].parking_name !== '' && this.staticBudget.parking[_x3].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x4 = 0; _x4 < this.staticBudget.tolling.length; _x4++) {
                    if (this.staticBudget.tolling[_x4].parking_name !== null && this.staticBudget.tolling[_x4].parking_budget !== null && this.staticBudget.tolling[_x4].parking_name !== '' && this.staticBudget.tolling[_x4].parking_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x5 = 0; _x5 < this.staticBudget.utilities.length; _x5++) {
                    if (this.staticBudget.utilities[_x5].utilities_name !== null && this.staticBudget.utilities[_x5].utilities_budget !== null && this.staticBudget.utilities[_x5].utilities_name !== '' && this.staticBudget.utilities[_x5].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x6 = 0; _x6 < this.staticBudget.under_total_fuel_and_exp.length; _x6++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x7 = 0; _x7 < this.staticBudget.under_total_admin.length; _x7++) {
                    if (this.staticBudget.under_total_admin[_x7].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x7].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x7].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x7].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuel = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodFuel, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodFuel,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUtilities,' + 'goodUnder_total_fuel_and_exp,goodUnder_total_admin,');

                  if (goodFuel === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    // console.log('false 1');
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn);
                    }
                  } else {
                    // console.log('true 4');
                    var _submitBtn2 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn2);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].sal_name === null && n[i].sal_budget === null && n[i].sal_name === '' && n[i].sal_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn3 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn3);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn4 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn4);

                      if (eitherIsNull === 1) {
                        var _submitBtn5 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn5);
                      }
                    }
                  }
                }
              } else {
                //if salaries is null
                if (this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].sal_name !== null && newVal[i].sal_budget !== null && newVal[i].sal_name !== '' && newVal[i].sal_budget !== '') {
                    // console.log('false 1');
                    var _submitBtn6 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn6);
                  } else {
                    // console.log('true 2');
                    eitherIsNull = 1;
                    var _submitBtn7 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn7);
                  }
                } else {
                  // console.log('true 1');
                  eitherIsNull = 1;
                  var _submitBtn8 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn8);
                }
              }
            } else {
              // console.log('true 3');
              var _submitBtn9 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn9);
            }
          }
        } else {
          var _submitBtn10 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn10);
        }
      }
    },
    "staticBudget.fuels": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;
        console.log(newVal, 'staticBudget.fuels');

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.maintenance !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].fuel_name !== null && newVal[i].fuel_budget !== null && newVal[i].fuel_name !== '' && newVal[i].fuel_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodMaintenace = 0;
                  var goodSalaries = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x8 = 0; _x8 < this.staticBudget.maintenance.length; _x8++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.maintenance[_x8].main_name !== null && this.staticBudget.maintenance[_x8].main_budget !== null && this.staticBudget.maintenance[_x8].main_name !== '' && this.staticBudget.maintenance[_x8].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x9 = 0; _x9 < this.staticBudget.repairs.length; _x9++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.repairs[_x9].repairs_name !== null && this.staticBudget.repairs[_x9].repairs_budget !== null && this.staticBudget.repairs[_x9].repairs_name !== '' && this.staticBudget.repairs[_x9].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x10 = 0; _x10 < this.staticBudget.parking.length; _x10++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.parking[_x10].parking_name !== null && this.staticBudget.parking[_x10].parking_budget !== null && this.staticBudget.parking[_x10].parking_name !== '' && this.staticBudget.parking[_x10].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x11 = 0; _x11 < this.staticBudget.tolling.length; _x11++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.tolling[_x11].parking_name !== null && this.staticBudget.tolling[_x11].parking_budget !== null && this.staticBudget.tolling[_x11].parking_name !== '' && this.staticBudget.tolling[_x11].parking_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x12 = 0; _x12 < this.staticBudget.utilities.length; _x12++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.utilities[_x12].utilities_name !== null && this.staticBudget.utilities[_x12].utilities_budget !== null && this.staticBudget.utilities[_x12].utilities_name !== '' && this.staticBudget.utilities[_x12].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x13 = 0; _x13 < this.staticBudget.under_total_fuel_and_exp.length; _x13++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x14 = 0; _x14 < this.staticBudget.under_total_admin.length; _x14++) {
                    if (this.staticBudget.under_total_admin[_x14].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x14].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x14].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x14].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodSalaries,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUtilities,goodUnder_total_fuel_and_exp,' + 'goodUnder_total_admin');

                  if (goodSalaries === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    // console.log('false fuel1');
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn11 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn11);
                    }
                  } else {
                    // console.log('true fuel1');
                    var _submitBtn12 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn12);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].sal_name === null && n[i].sal_budget === null && n[i].sal_name === '' && n[i].sal_budget === '') {
                      // console.log('let submitBtn = true1;');
                      var _submitBtn13 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn13);
                    } else {
                      // console.log('let submitBtn = false1;');
                      var _submitBtn14 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn14);

                      if (eitherIsNull === 1) {
                        var _submitBtn15 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn15);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].fuel_name !== null && newVal[i].fuel_budget !== null && newVal[i].fuel_name !== '' && newVal[i].fuel_budget !== '') {
                    // console.log('let submitBtn = false2;');
                    var _submitBtn16 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn16);
                  } else {
                    // console.log('let submitBtn = true2;');
                    eitherIsNull = 1;
                    var _submitBtn17 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn17);
                  }
                } else {
                  // console.log('let submitBtn = true3;');
                  eitherIsNull = 1;
                  var _submitBtn18 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn18);
                }
              }
            } else {
              // console.log('let submitBtn = true4;');
              var _submitBtn19 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn19);
            }
          }
        } else {
          var _submitBtn20 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn20);
        }
      }
    },
    "staticBudget.maintenance": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].main_name !== null && newVal[i].main_budget !== null && newVal[i].main_name !== '' && newVal[i].main_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x15 = 0; _x15 < this.staticBudget.fuels.length; _x15++) {
                    if (this.staticBudget.fuels[_x15].fuel_name !== null && this.staticBudget.fuels[_x15].fuel_budget !== null && this.staticBudget.fuels[_x15].fuel_name !== '' && this.staticBudget.fuels[_x15].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x16 = 0; _x16 < this.staticBudget.repairs.length; _x16++) {
                    if (this.staticBudget.repairs[_x16].repairs_name !== null && this.staticBudget.repairs[_x16].repairs_budget !== null && this.staticBudget.repairs[_x16].repairs_name !== '' && this.staticBudget.repairs[_x16].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x17 = 0; _x17 < this.staticBudget.parking.length; _x17++) {
                    if (this.staticBudget.parking[_x17].parking_name !== null && this.staticBudget.parking[_x17].parking_budget !== null && this.staticBudget.parking[_x17].parking_name !== '' && this.staticBudget.parking[_x17].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x18 = 0; _x18 < this.staticBudget.tolling.length; _x18++) {
                    if (this.staticBudget.tolling[_x18].tolling_name !== null && this.staticBudget.tolling[_x18].tolling_budget !== null && this.staticBudget.tolling[_x18].tolling_name !== '' && this.staticBudget.tolling[_x18].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x19 = 0; _x19 < this.staticBudget.utilities.length; _x19++) {
                    if (this.staticBudget.utilities[_x19].utilities_name !== null && this.staticBudget.utilities[_x19].utilities_budget !== null && this.staticBudget.utilities[_x19].utilities_name !== '' && this.staticBudget.utilities[_x19].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x20 = 0; _x20 < this.staticBudget.under_total_fuel_and_exp.length; _x20++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x21 = 0; _x21 < this.staticBudget.under_total_admin.length; _x21++) {
                    if (this.staticBudget.under_total_admin[_x21].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x21].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x21].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x21].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_admin, goodUnder_total_fuel_and_exp, 'goodSalaries,goodFuels,goodRepairs,goodParking,goodTolling,goodUtilities,' + 'goodUnder_total_admin,goodUnder_total_fuel_and_exp,');

                  if (goodSalaries === 1 && goodFuels === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_admin === 1 && goodUnder_total_fuel_and_exp === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn21 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn21);
                    }
                  } else {
                    var _submitBtn22 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn22);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].sal_name === null && n[i].sal_budget === null && n[i].sal_name === '' && n[i].sal_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn23 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn23);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn24 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn24);

                      if (eitherIsNull === 1) {
                        var _submitBtn25 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn25);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].main_name !== null && newVal[i].main_budget !== null && newVal[i].main_name !== '' && newVal[i].main_budget !== '') {
                    var _submitBtn26 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn26);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn27 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn27);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn28 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn28);
                }
              }
            } else {
              var _submitBtn29 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn29);
            }
          }
        } else {
          var _submitBtn30 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn30);
        }
      }
    },
    "staticBudget.repairs": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].repairs_name !== null && newVal[i].repairs_budget !== null && newVal[i].repairs_name !== '' && newVal[i].repairs_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x22 = 0; _x22 < this.staticBudget.fuels.length; _x22++) {
                    if (this.staticBudget.fuels[_x22].fuel_name !== null && this.staticBudget.fuels[_x22].fuel_budget !== null && this.staticBudget.fuels[_x22].fuel_name !== '' && this.staticBudget.fuels[_x22].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x23 = 0; _x23 < this.staticBudget.maintenance.length; _x23++) {
                    if (this.staticBudget.maintenance[_x23].main_name !== null && this.staticBudget.maintenance[_x23].main_budget !== null && this.staticBudget.maintenance[_x23].main_name !== '' && this.staticBudget.maintenance[_x23].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x24 = 0; _x24 < this.staticBudget.parking.length; _x24++) {
                    if (this.staticBudget.parking[_x24].parking_name !== null && this.staticBudget.parking[_x24].parking_budget !== null && this.staticBudget.parking[_x24].parking_name !== '' && this.staticBudget.parking[_x24].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x25 = 0; _x25 < this.staticBudget.tolling.length; _x25++) {
                    if (this.staticBudget.tolling[_x25].tolling_name !== null && this.staticBudget.tolling[_x25].tolling_budget !== null && this.staticBudget.tolling[_x25].tolling_name !== '' && this.staticBudget.tolling[_x25].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x26 = 0; _x26 < this.staticBudget.utilities.length; _x26++) {
                    if (this.staticBudget.utilities[_x26].tolling_name !== null && this.staticBudget.utilities[_x26].tolling_budget !== null && this.staticBudget.utilities[_x26].tolling_name !== '' && this.staticBudget.utilities[_x26].tolling_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x27 = 0; _x27 < this.staticBudget.under_total_fuel_and_exp.length; _x27++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x28 = 0; _x28 < this.staticBudget.under_total_admin.length; _x28++) {
                    if (this.staticBudget.under_total_admin[_x28].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x28].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x28].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x28].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodUtilities,goodUnder_total_fuel_and_exp,goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn31 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn31);
                    }
                  } else {
                    var _submitBtn32 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn32);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].repairs_name === null && n[i].repairs_budget === null && n[i].repairs_name === '' && n[i].repairs_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn33 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn33);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn34 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn34);

                      if (eitherIsNull === 1) {
                        var _submitBtn35 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn35);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].repairs_name !== null && newVal[i].repairs_budget !== null && newVal[i].repairs_name !== '' && newVal[i].repairs_budget !== '') {
                    var _submitBtn36 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn36);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn37 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn37);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn38 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn38);
                }
              }
            } else {
              var _submitBtn39 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn39);
            }
          }
        } else {
          var _submitBtn40 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn40);
        }
      }
    },
    "staticBudget.parking": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].parking_name !== null && newVal[i].parking_budget !== null && newVal[i].parking_name !== '' && newVal[i].parking_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x29 = 0; _x29 < this.staticBudget.fuels.length; _x29++) {
                    if (this.staticBudget.fuels[_x29].fuel_name !== null && this.staticBudget.fuels[_x29].fuel_budget !== null && this.staticBudget.fuels[_x29].fuel_name !== '' && this.staticBudget.fuels[_x29].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x30 = 0; _x30 < this.staticBudget.maintenance.length; _x30++) {
                    if (this.staticBudget.maintenance[_x30].main_name !== null && this.staticBudget.maintenance[_x30].main_budget !== null && this.staticBudget.maintenance[_x30].main_name !== '' && this.staticBudget.maintenance[_x30].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x31 = 0; _x31 < this.staticBudget.repairs.length; _x31++) {
                    if (this.staticBudget.repairs[_x31].repairs_name !== null && this.staticBudget.repairs[_x31].repairs_budget !== null && this.staticBudget.repairs[_x31].repairs_name !== '' && this.staticBudget.repairs[_x31].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x32 = 0; _x32 < this.staticBudget.tolling.length; _x32++) {
                    if (this.staticBudget.tolling[_x32].tolling_name !== null && this.staticBudget.tolling[_x32].tolling_budget !== null && this.staticBudget.tolling[_x32].tolling_name !== '' && this.staticBudget.tolling[_x32].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x33 = 0; _x33 < this.staticBudget.utilities.length; _x33++) {
                    if (this.staticBudget.utilities[_x33].utilities_name !== null && this.staticBudget.utilities[_x33].utilities_budget !== null && this.staticBudget.utilities[_x33].utilities_name !== '' && this.staticBudget.utilities[_x33].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x34 = 0; _x34 < this.staticBudget.under_total_fuel_and_exp.length; _x34++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x35 = 0; _x35 < this.staticBudget.under_total_admin.length; _x35++) {
                    if (this.staticBudget.under_total_admin[_x35].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x35].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x35].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x35].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodTolling,goodUtilities,goodUnder_total_fuel_and_exp,' + 'goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn41 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn41);
                    }
                  } else {
                    var _submitBtn42 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn42);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].parking_name === null && n[i].parking_budget === null && n[i].parking_name === '' && n[i].parking_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn43 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn43);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn44 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn44);

                      if (eitherIsNull === 1) {
                        var _submitBtn45 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn45);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].parking_name !== null && newVal[i].parking_budget !== null && newVal[i].parking_name !== '' && newVal[i].parking_budget !== '') {
                    var _submitBtn46 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn46);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn47 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn47);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn48 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn48);
                }
              }
            } else {
              var _submitBtn49 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn49);
            }
          }
        } else {
          var _submitBtn50 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn50);
        }
      }
    },
    "staticBudget.tolling": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].tolling_name !== null && newVal[i].tolling_budget !== null && newVal[i].tolling_name !== '' && newVal[i].tolling_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_admin = 0;
                  var goodUnder_total_fuel_and_exp = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x36 = 0; _x36 < this.staticBudget.fuels.length; _x36++) {
                    if (this.staticBudget.fuels[_x36].fuel_name !== null && this.staticBudget.fuels[_x36].fuel_budget !== null && this.staticBudget.fuels[_x36].fuel_name !== '' && this.staticBudget.fuels[_x36].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x37 = 0; _x37 < this.staticBudget.maintenance.length; _x37++) {
                    if (this.staticBudget.maintenance[_x37].main_name !== null && this.staticBudget.maintenance[_x37].main_budget !== null && this.staticBudget.maintenance[_x37].main_name !== '' && this.staticBudget.maintenance[_x37].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x38 = 0; _x38 < this.staticBudget.repairs.length; _x38++) {
                    if (this.staticBudget.repairs[_x38].repairs_name !== null && this.staticBudget.repairs[_x38].repairs_budget !== null && this.staticBudget.repairs[_x38].repairs_name !== '' && this.staticBudget.repairs[_x38].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x39 = 0; _x39 < this.staticBudget.parking.length; _x39++) {
                    if (this.staticBudget.parking[_x39].parking_name !== null && this.staticBudget.parking[_x39].parking_budget !== null && this.staticBudget.parking[_x39].parking_name !== '' && this.staticBudget.parking[_x39].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x40 = 0; _x40 < this.staticBudget.utilities.length; _x40++) {
                    if (this.staticBudget.utilities[_x40].utilities_name !== null && this.staticBudget.utilities[_x40].utilities_budget !== null && this.staticBudget.utilities[_x40].utilities_name !== '' && this.staticBudget.utilities[_x40].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x41 = 0; _x41 < this.staticBudget.under_total_admin.length; _x41++) {
                    if (this.staticBudget.under_total_admin[_x41].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x41].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x41].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x41].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  for (var _x42 = 0; _x42 < this.staticBudget.under_total_fuel_and_exp.length; _x42++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodUtilities, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodUtilities,goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodUtilities === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn51 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn51);
                    }
                  } else {
                    var _submitBtn52 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn52);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].tolling_name === null && n[i].tolling_budget === null && n[i].tolling_name === '' && n[i].tolling_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn53 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn53);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn54 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn54);

                      if (eitherIsNull === 1) {
                        var _submitBtn55 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn55);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  if (newVal[i].tolling_name !== null && newVal[i].tolling_budget !== null && newVal[i].tolling_name !== '' && newVal[i].tolling_budget !== '') {
                    var _submitBtn56 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn56);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn57 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn57);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn58 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn58);
                }
              }
            } else {
              var _submitBtn59 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn59);
            }
          }
        } else {
          var _submitBtn60 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn60);
        }
      }
    },
    "staticBudget.utilities": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].utilities_name !== null && newVal[i].utilities_budget !== null && newVal[i].utilities_name !== '' && newVal[i].utilities_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUnder_total_admin = 0;
                  var goodUnder_total_fuel_and_exp = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x43 = 0; _x43 < this.staticBudget.fuels.length; _x43++) {
                    if (this.staticBudget.fuels[_x43].fuel_name !== null && this.staticBudget.fuels[_x43].fuel_budget !== null && this.staticBudget.fuels[_x43].fuel_name !== '' && this.staticBudget.fuels[_x43].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x44 = 0; _x44 < this.staticBudget.maintenance.length; _x44++) {
                    if (this.staticBudget.maintenance[_x44].main_name !== null && this.staticBudget.maintenance[_x44].main_budget !== null && this.staticBudget.maintenance[_x44].main_name !== '' && this.staticBudget.maintenance[_x44].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x45 = 0; _x45 < this.staticBudget.repairs.length; _x45++) {
                    if (this.staticBudget.repairs[_x45].repairs_name !== null && this.staticBudget.repairs[_x45].repairs_budget !== null && this.staticBudget.repairs[_x45].repairs_name !== '' && this.staticBudget.repairs[_x45].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x46 = 0; _x46 < this.staticBudget.parking.length; _x46++) {
                    if (this.staticBudget.parking[_x46].parking_name !== null && this.staticBudget.parking[_x46].parking_budget !== null && this.staticBudget.parking[_x46].parking_name !== '' && this.staticBudget.parking[_x46].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x47 = 0; _x47 < this.staticBudget.tolling.length; _x47++) {
                    if (this.staticBudget.tolling[_x47].tolling_name !== null && this.staticBudget.tolling[_x47].tolling_budget !== null && this.staticBudget.tolling[_x47].tolling_name !== '' && this.staticBudget.tolling[_x47].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x48 = 0; _x48 < this.staticBudget.under_total_admin.length; _x48++) {
                    if (this.staticBudget.under_total_admin[_x48].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x48].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x48].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x48].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  for (var _x49 = 0; _x49 < this.staticBudget.under_total_fuel_and_exp.length; _x49++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUnder_total_admin, goodUnder_total_fuel_and_exp, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUnder_total_admin,goodUnder_total_fuel_and_exp');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUnder_total_admin === 1 && goodUnder_total_fuel_and_exp === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn61 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn61);
                    }
                  } else {
                    var _submitBtn62 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn62);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].utilities_name === null && n[i].utilities_budget === null && n[i].utilities_name === '' && n[i].utilities_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn63 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn63);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn64 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn64);

                      if (eitherIsNull === 1) {
                        var _submitBtn65 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn65);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  if (newVal[i].utilities_name !== null && newVal[i].utilities_budget !== null && newVal[i].utilities_name !== '' && newVal[i].utilities_budget !== '') {
                    var _submitBtn66 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn66);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn67 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn67);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn68 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn68);
                }
              }
            } else {
              var _submitBtn69 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn69);
            }
          }
        } else {
          var _submitBtn70 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn70);
        }
      }
    },
    "staticBudget.under_total_admin": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].under_total_admin_name !== null && newVal[i].under_total_admin_budget !== null && newVal[i].under_total_admin_name !== '' && newVal[i].under_total_admin_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x50 = 0; _x50 < this.staticBudget.fuels.length; _x50++) {
                    if (this.staticBudget.fuels[_x50].fuel_name !== null && this.staticBudget.fuels[_x50].fuel_budget !== null && this.staticBudget.fuels[_x50].fuel_name !== '' && this.staticBudget.fuels[_x50].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x51 = 0; _x51 < this.staticBudget.maintenance.length; _x51++) {
                    if (this.staticBudget.maintenance[_x51].main_name !== null && this.staticBudget.maintenance[_x51].main_budget !== null && this.staticBudget.maintenance[_x51].main_name !== '' && this.staticBudget.maintenance[_x51].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x52 = 0; _x52 < this.staticBudget.repairs.length; _x52++) {
                    if (this.staticBudget.repairs[_x52].repairs_name !== null && this.staticBudget.repairs[_x52].repairs_budget !== null && this.staticBudget.repairs[_x52].repairs_name !== '' && this.staticBudget.repairs[_x52].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x53 = 0; _x53 < this.staticBudget.parking.length; _x53++) {
                    if (this.staticBudget.parking[_x53].parking_name !== null && this.staticBudget.parking[_x53].parking_budget !== null && this.staticBudget.parking[_x53].parking_name !== '' && this.staticBudget.parking[_x53].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x54 = 0; _x54 < this.staticBudget.tolling.length; _x54++) {
                    if (this.staticBudget.tolling[_x54].tolling_name !== null && this.staticBudget.tolling[_x54].tolling_budget !== null && this.staticBudget.tolling[_x54].tolling_name !== '' && this.staticBudget.tolling[_x54].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x55 = 0; _x55 < this.staticBudget.utilities.length; _x55++) {
                    if (this.staticBudget.utilities[_x55].utilities_name !== null && this.staticBudget.utilities[_x55].utilities_budget !== null && this.staticBudget.utilities[_x55].utilities_name !== '' && this.staticBudget.utilities[_x55].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x56 = 0; _x56 < this.staticBudget.under_total_fuel_and_exp.length; _x56++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUtilities,goodUnder_total_fuel_and_exp');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn71 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn71);
                    }
                  } else {
                    var _submitBtn72 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn72);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].under_total_admin_name === null && n[i].under_total_admin_budget === null && n[i].under_total_admin_name === '' && n[i].under_total_admin_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn73 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn73);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn74 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn74);

                      if (eitherIsNull === 1) {
                        var _submitBtn75 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn75);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp !== 0) {
                  if (newVal[i].under_total_admin_name !== null && newVal[i].under_total_admin_budget !== null && newVal[i].under_total_admin_name !== '' && newVal[i].under_total_admin_budget !== '') {
                    var _submitBtn76 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn76);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn77 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn77);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn78 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn78);
                }
              }
            } else {
              var _submitBtn79 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn79);
            }
          }
        } else {
          var _submitBtn80 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn80);
        }
      }
    },
    "staticBudget.under_total_fuel_and_exp": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].under_total_fuel_and_exp_name !== null && newVal[i].under_total_fuel_and_exp_budget !== null && newVal[i].under_total_fuel_and_exp_name !== '' && newVal[i].under_total_fuel_and_exp_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x57 = 0; _x57 < this.staticBudget.fuels.length; _x57++) {
                    if (this.staticBudget.fuels[_x57].fuel_name !== null && this.staticBudget.fuels[_x57].fuel_budget !== null && this.staticBudget.fuels[_x57].fuel_name !== '' && this.staticBudget.fuels[_x57].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x58 = 0; _x58 < this.staticBudget.maintenance.length; _x58++) {
                    if (this.staticBudget.maintenance[_x58].main_name !== null && this.staticBudget.maintenance[_x58].main_budget !== null && this.staticBudget.maintenance[_x58].main_name !== '' && this.staticBudget.maintenance[_x58].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x59 = 0; _x59 < this.staticBudget.repairs.length; _x59++) {
                    if (this.staticBudget.repairs[_x59].repairs_name !== null && this.staticBudget.repairs[_x59].repairs_budget !== null && this.staticBudget.repairs[_x59].repairs_name !== '' && this.staticBudget.repairs[_x59].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x60 = 0; _x60 < this.staticBudget.parking.length; _x60++) {
                    if (this.staticBudget.parking[_x60].parking_name !== null && this.staticBudget.parking[_x60].parking_budget !== null && this.staticBudget.parking[_x60].parking_name !== '' && this.staticBudget.parking[_x60].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x61 = 0; _x61 < this.staticBudget.tolling.length; _x61++) {
                    if (this.staticBudget.tolling[_x61].tolling_name !== null && this.staticBudget.tolling[_x61].tolling_budget !== null && this.staticBudget.tolling[_x61].tolling_name !== '' && this.staticBudget.tolling[_x61].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x62 = 0; _x62 < this.staticBudget.utilities.length; _x62++) {
                    if (this.staticBudget.utilities[_x62].utilities_name !== null && this.staticBudget.utilities[_x62].utilities_budget !== null && this.staticBudget.utilities[_x62].utilities_name !== '' && this.staticBudget.utilities[_x62].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x63 = 0; _x63 < this.staticBudget.under_total_admin.length; _x63++) {
                    if (this.staticBudget.under_total_admin[_x63].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x63].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x63].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x63].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodTolling,goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn81 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn81);
                    }
                  } else {
                    var _submitBtn82 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn82);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].under_total_fuel_and_exp_name === null && n[i].under_total_fuel_and_exp_budget === null && n[i].under_total_fuel_and_exp_name === '' && n[i].under_total_fuel_and_exp_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn83 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn83);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn84 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn84);

                      if (eitherIsNull === 1) {
                        var _submitBtn85 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn85);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].under_total_fuel_and_exp_name !== null && newVal[i].under_total_fuel_and_exp_budget !== null && newVal[i].under_total_fuel_and_exp_name !== '' && newVal[i].under_total_fuel_and_exp_budget !== '') {
                    var _submitBtn86 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn86);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn87 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn87);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn88 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn88);
                }
              }
            } else {
              var _submitBtn89 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn89);
            }
          }
        } else {
          var _submitBtn90 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', _submitBtn90);
        }
      }
    } // deep: true

  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])({
    getStaticBudget: "static_budget/fetchStaticBudgetsForDatatable",
    createStaticBudget: "static_budget/createStaticBudget"
  })), {}, {
    cancelCreate: function cancelCreate() {
      this.$emit("cancelCreate", false);
    },
    submitStaticBudget: function submitStaticBudget() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _this.$store.commit('static_budget/STATIC_BUDGET_CREATE', _this.staticBudget);

              case 3:
                _context.next = 5;
                return _this.createStaticBudget();

              case 5:
                _context.next = 7;
                return _this.getStaticBudget();

              case 7:
                _this.$swal({
                  icon: 'success',
                  title: 'Budget was successfully created',
                  showConfirmButton: false,
                  timer: 1500
                });

                _this.$store.commit('static_budget/STATIC_BUDGET_CLEAR_AFTER_SUBMIT', true); //this.form.name = null;


                _context.next = 14;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 11]]);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticEdit.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticEdit.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_Form */ "./resources/js/views/static_budget/_Form.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import UserForm from './_Form'


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StaticBudgetEdit",
  components: {
    StaticBudgetForm: _Form__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])({
    user: ["user/user"],
    staticBudget: ["static_budget/staticBudget"],
    submitBtn: ["static_budget/submitBtn"]
  })), {}, {
    totalSalaries: function totalSalaries() {
      //Salary
      var salArr = [];
      var xSal = 0; // let no_comma = this.staticBudget.salaries[2].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""); // TODO Fix error with the comma ang backend and font end
      // console.log(this.staticBudget.salaries,no_comma,this.staticBudget.salaries[2].sal_budget,parseFloat(no_comma) + parseFloat(this.staticBudget.salaries[1].sal_budget),parseFloat(no_comma + this.staticBudget.salaries[1].sal_budget),'this.staticBudget.salaries edit');
      // let total = parseFloat(this.staticBudget.salaries[1].sal_budget + this.staticBudget.salaries[2].sal_budget);
      // return total;

      for (var i = 0; i < this.staticBudget.salaries.length; i++) {
        if (this.staticBudget.salaries[i].sal_budget !== null) {
          xSal += parseFloat(this.staticBudget.salaries[i].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        salArr.push({
          'salary': xSal
        });
      }

      var salary = 0;

      if (salArr.length !== 0) {
        var lastSal = salArr.length - 1;
        salary = salArr[lastSal].salary;
        this.staticBudget.totalSalaries = salary;
        return salary;
      }
    },
    totalFuelAndSellingExp: function totalFuelAndSellingExp() {
      //TOTAL FUEL AND SELLING EXPENSES
      //FUEL
      console.log();
      var fuelArr = [];
      var xFuel = 0;

      for (var i = 0; i < this.staticBudget.fuels.length; i++) {
        if (this.staticBudget.fuels[i].fuel_budget !== null) {
          xFuel += parseFloat(this.staticBudget.fuels[i].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        fuelArr.push({
          'fuel': xFuel
        });
      }

      var fuel = 0;

      if (fuelArr.length !== 0) {
        var lastFuel = fuelArr.length - 1;
        fuel = fuelArr[lastFuel].fuel;
      } //MAINTENANCE


      var mainArr = [];
      var xMain = 0;

      for (var _i = 0; _i < this.staticBudget.maintenance.length; _i++) {
        if (this.staticBudget.maintenance[_i].main_budget !== null) {
          xMain += parseFloat(this.staticBudget.maintenance[_i].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        mainArr.push({
          'maintenance': xMain
        });
      }

      var maintenance = 0;

      if (mainArr.length !== 0) {
        var lastMaintenance = mainArr.length - 1;
        maintenance = mainArr[lastMaintenance].maintenance;
      } //REPAIRS


      var repairArr = [];
      var xRepair = 0;

      for (var _i2 = 0; _i2 < this.staticBudget.repairs.length; _i2++) {
        if (this.staticBudget.repairs[_i2].repairs_budget !== null) {
          xRepair += parseFloat(this.staticBudget.repairs[_i2].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        repairArr.push({
          'repairs': xRepair
        });
      }

      var repairs = 0;

      if (repairArr.length !== 0) {
        var lastRepairs = repairArr.length - 1;
        repairs = repairArr[lastRepairs].repairs;
      } //PARKING


      var parkingArr = [];
      var xParking = 0;

      for (var _i3 = 0; _i3 < this.staticBudget.parking.length; _i3++) {
        if (this.staticBudget.parking[_i3].parking_budget !== null) {
          xParking += parseFloat(this.staticBudget.parking[_i3].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        parkingArr.push({
          'parking': xParking
        });
      }

      var parking = 0;

      if (parkingArr.length !== 0) {
        var lastParking = parkingArr.length - 1;
        parking = parkingArr[lastParking].parking;
      } //TOLLING


      var tollingArr = [];
      var xTolling = 0;

      for (var _i4 = 0; _i4 < this.staticBudget.tolling.length; _i4++) {
        if (this.staticBudget.tolling[_i4].tolling_budget !== null) {
          xTolling += parseFloat(this.staticBudget.tolling[_i4].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        tollingArr.push({
          'tolling': xTolling
        });
      }

      var tolling = 0;

      if (tollingArr.length !== 0) {
        var lastTolling = tollingArr.length - 1;
        tolling = tollingArr[lastTolling].tolling;
      } //Under TOTAL FUEL AND SELLING EXPENSES


      var totalFuelandSellingExpArr = [];
      var xtotalFuelandSellingExp = 0;

      for (var _i5 = 0; _i5 < this.staticBudget.under_total_fuel_and_exp.length; _i5++) {
        if (this.staticBudget.under_total_fuel_and_exp[_i5].under_total_fuel_and_exp_budget !== null) {
          xtotalFuelandSellingExp += parseFloat(this.staticBudget.under_total_fuel_and_exp[_i5].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        totalFuelandSellingExpArr.push({
          'totalFuelandSellingExp': xtotalFuelandSellingExp
        });
      } // console.log(totalFuelandSellingExpArr,'totalFuelandSellingExp');


      var totalFuelandSellingExp = 0;

      if (totalFuelandSellingExpArr.length !== 0) {
        var lastTotalFuelandSellingExp = totalFuelandSellingExpArr.length - 1;
        totalFuelandSellingExp = totalFuelandSellingExpArr[lastTotalFuelandSellingExp].totalFuelandSellingExp;
      }

      console.log(fuel, maintenance, repairs, parking, tolling, totalFuelandSellingExp, 'fuel + maintenance + repairs + parking + tolling + totalFuelandSellingExp');
      var total = fuel + maintenance + repairs + parking + tolling + totalFuelandSellingExp;
      this.staticBudget.totalFuelAndSellingExp = total;
      return total;
    },
    totalAdminExpenses: function totalAdminExpenses() {
      //TOTAL ADMINISTRATIVE EXPENSES
      //UTILITIES
      var utilitiesArr = [];
      var xUtilities = 0;

      for (var i = 0; i < this.staticBudget.utilities.length; i++) {
        if (this.staticBudget.utilities[i].utilities_budget !== null) {
          xUtilities += parseFloat(this.staticBudget.utilities[i].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        utilitiesArr.push({
          'utilities': xUtilities
        });
      }

      var utilities = 0;

      if (utilitiesArr.length !== 0) {
        var lastUtilities = utilitiesArr.length - 1;
        utilities = utilitiesArr[lastUtilities].utilities;
      } //Under Total Admin Expense


      var underTotalAdminExpArr = [];
      var xUnderTotalAdminExp = 0;

      for (var _i6 = 0; _i6 < this.staticBudget.under_total_admin.length; _i6++) {
        if (this.staticBudget.under_total_admin[_i6].under_total_admin_budget !== null) {
          xUnderTotalAdminExp += parseFloat(this.staticBudget.under_total_admin[_i6].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, ""));
        }

        underTotalAdminExpArr.push({
          'underTotalAdminExp': xUnderTotalAdminExp
        });
      }

      var underTotalAdminExp = 0;

      if (underTotalAdminExpArr.length !== 0) {
        var lastUnderTotalAdminExp = underTotalAdminExpArr.length - 1;
        underTotalAdminExp = underTotalAdminExpArr[lastUnderTotalAdminExp].underTotalAdminExp;
      }

      var total = utilities + underTotalAdminExp;
      this.staticBudget.totalAdminExpenses = total;
      return total;
    },
    totalOperatingExpense: function totalOperatingExpense() {
      //TOTAL SALARIES + TOTAL FUEL AND SELLING EXPENSES + TOTAL ADMINISTRATIVE EXPENSES
      var total = this.staticBudget.totalAdminExpenses + this.staticBudget.totalFuelAndSellingExp + this.staticBudget.totalSalaries;
      this.staticBudget.totalExpenses = total;
      return total;
    }
  }),
  watch: {
    "totalOperatingExpense": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalOperatingExpense');
      }
    },
    "totalAdminExpenses": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalAdminExpenses');
      }
    },
    "totalFuelAndSellingExp": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalFuelAndSellingExp');
      }
    },
    "totalSalaries": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalSalaries');
      }
    },
    "totalExpense": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'totalExpense');
      }
    },
    "staticBudget.salaries": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'staticBudget.salaries');
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            console.log(newVal[i].sal_name, newVal[i].sal_budget, i, 'valueFunc[i]');

            if (newVal.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].sal_name !== null && newVal[i].sal_budget !== null && newVal[i].sal_name !== '' && newVal[i].sal_budget !== '') {
                if (this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodMaintenace = 0;
                  var goodFuel = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0; //this.staticBudget.salaries[x].sal_name

                  for (var x = 0; x < this.staticBudget.fuels.length; x++) {
                    // console.log(fuels, 'fuels');
                    if (this.staticBudget.fuels[x].fuel_name !== null && this.staticBudget.fuels[x].fuel_budget !== null && this.staticBudget.fuels[x].fuel_name !== null && this.staticBudget.fuels[x].fuel_budget !== '') {
                      goodFuel = 1;
                    } else {
                      goodFuel = 0;
                    }
                  }

                  for (var _x = 0; _x < this.staticBudget.maintenance.length; _x++) {
                    if (this.staticBudget.maintenance[_x].main_name !== null && this.staticBudget.maintenance[_x].main_budget !== null && this.staticBudget.maintenance[_x].main_name !== '' && this.staticBudget.maintenance[_x].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x2 = 0; _x2 < this.staticBudget.repairs.length; _x2++) {
                    if (this.staticBudget.repairs[_x2].repairs_name !== null && this.staticBudget.repairs[_x2].repairs_budget !== null && this.staticBudget.repairs[_x2].repairs_name !== '' && this.staticBudget.repairs[_x2].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x3 = 0; _x3 < this.staticBudget.parking.length; _x3++) {
                    if (this.staticBudget.parking[_x3].parking_name !== null && this.staticBudget.parking[_x3].parking_budget !== null && this.staticBudget.parking[_x3].parking_name !== '' && this.staticBudget.parking[_x3].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x4 = 0; _x4 < this.staticBudget.tolling.length; _x4++) {
                    if (this.staticBudget.tolling[_x4].parking_name !== null && this.staticBudget.tolling[_x4].parking_budget !== null && this.staticBudget.tolling[_x4].parking_name !== '' && this.staticBudget.tolling[_x4].parking_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x5 = 0; _x5 < this.staticBudget.utilities.length; _x5++) {
                    if (this.staticBudget.utilities[_x5].utilities_name !== null && this.staticBudget.utilities[_x5].utilities_budget !== null && this.staticBudget.utilities[_x5].utilities_name !== '' && this.staticBudget.utilities[_x5].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x6 = 0; _x6 < this.staticBudget.under_total_fuel_and_exp.length; _x6++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x6].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x7 = 0; _x7 < this.staticBudget.under_total_admin.length; _x7++) {
                    if (this.staticBudget.under_total_admin[_x7].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x7].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x7].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x7].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuel = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodFuel, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodFuel,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUtilities,' + 'goodUnder_total_fuel_and_exp,goodUnder_total_admin,');

                  if (goodFuel === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    // console.log('false 1');
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn);
                    }
                  } else {
                    // console.log('true 4');
                    var _submitBtn2 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn2);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].sal_name === null && n[i].sal_budget === null && n[i].sal_name === '' && n[i].sal_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn3 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn3);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn4 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn4);

                      if (eitherIsNull === 1) {
                        var _submitBtn5 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn5);
                      }
                    }
                  }
                }
              } else {
                //if salaries is null
                if (this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].sal_name !== null && newVal[i].sal_budget !== null && newVal[i].sal_name !== '' && newVal[i].sal_budget !== '') {
                    // console.log('false 1');
                    var _submitBtn6 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn6);
                  } else {
                    // console.log('true 2');
                    eitherIsNull = 1;
                    var _submitBtn7 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn7);
                  }
                } else {
                  // console.log('true 1');
                  eitherIsNull = 1;
                  var _submitBtn8 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn8);
                }
              }
            } else {
              // console.log('true 3');
              var _submitBtn9 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn9);
            }
          }
        } else {
          var _submitBtn10 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn10);
        }
      }
    },
    "staticBudget.fuels": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;
        console.log(newVal, 'staticBudget.fuels');

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.maintenance !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].fuel_name !== null && newVal[i].fuel_budget !== null && newVal[i].fuel_name !== '' && newVal[i].fuel_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodMaintenace = 0;
                  var goodSalaries = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x8 = 0; _x8 < this.staticBudget.maintenance.length; _x8++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.maintenance[_x8].main_name !== null && this.staticBudget.maintenance[_x8].main_budget !== null && this.staticBudget.maintenance[_x8].main_name !== '' && this.staticBudget.maintenance[_x8].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x9 = 0; _x9 < this.staticBudget.repairs.length; _x9++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.repairs[_x9].repairs_name !== null && this.staticBudget.repairs[_x9].repairs_budget !== null && this.staticBudget.repairs[_x9].repairs_name !== '' && this.staticBudget.repairs[_x9].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x10 = 0; _x10 < this.staticBudget.parking.length; _x10++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.parking[_x10].parking_name !== null && this.staticBudget.parking[_x10].parking_budget !== null && this.staticBudget.parking[_x10].parking_name !== '' && this.staticBudget.parking[_x10].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x11 = 0; _x11 < this.staticBudget.tolling.length; _x11++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.tolling[_x11].parking_name !== null && this.staticBudget.tolling[_x11].parking_budget !== null && this.staticBudget.tolling[_x11].parking_name !== '' && this.staticBudget.tolling[_x11].parking_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x12 = 0; _x12 < this.staticBudget.utilities.length; _x12++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.utilities[_x12].utilities_name !== null && this.staticBudget.utilities[_x12].utilities_budget !== null && this.staticBudget.utilities[_x12].utilities_name !== '' && this.staticBudget.utilities[_x12].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x13 = 0; _x13 < this.staticBudget.under_total_fuel_and_exp.length; _x13++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x13].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x14 = 0; _x14 < this.staticBudget.under_total_admin.length; _x14++) {
                    if (this.staticBudget.under_total_admin[_x14].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x14].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x14].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x14].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodSalaries,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUtilities,goodUnder_total_fuel_and_exp,' + 'goodUnder_total_admin');

                  if (goodSalaries === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    // console.log('false fuel1');
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn11 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn11);
                    }
                  } else {
                    // console.log('true fuel1');
                    var _submitBtn12 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn12);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].sal_name === null && n[i].sal_budget === null && n[i].sal_name === '' && n[i].sal_budget === '') {
                      // console.log('let submitBtn = true1;');
                      var _submitBtn13 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn13);
                    } else {
                      // console.log('let submitBtn = false1;');
                      var _submitBtn14 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn14);

                      if (eitherIsNull === 1) {
                        var _submitBtn15 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn15);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].fuel_name !== null && newVal[i].fuel_budget !== null && newVal[i].fuel_name !== '' && newVal[i].fuel_budget !== '') {
                    // console.log('let submitBtn = false2;');
                    var _submitBtn16 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn16);
                  } else {
                    // console.log('let submitBtn = true2;');
                    eitherIsNull = 1;
                    var _submitBtn17 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn17);
                  }
                } else {
                  // console.log('let submitBtn = true3;');
                  eitherIsNull = 1;
                  var _submitBtn18 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn18);
                }
              }
            } else {
              // console.log('let submitBtn = true4;');
              var _submitBtn19 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn19);
            }
          }
        } else {
          var _submitBtn20 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn20);
        }
      }
    },
    "staticBudget.maintenance": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].main_name !== null && newVal[i].main_budget !== null && newVal[i].main_name !== '' && newVal[i].main_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x15 = 0; _x15 < this.staticBudget.fuels.length; _x15++) {
                    if (this.staticBudget.fuels[_x15].fuel_name !== null && this.staticBudget.fuels[_x15].fuel_budget !== null && this.staticBudget.fuels[_x15].fuel_name !== '' && this.staticBudget.fuels[_x15].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x16 = 0; _x16 < this.staticBudget.repairs.length; _x16++) {
                    if (this.staticBudget.repairs[_x16].repairs_name !== null && this.staticBudget.repairs[_x16].repairs_budget !== null && this.staticBudget.repairs[_x16].repairs_name !== '' && this.staticBudget.repairs[_x16].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x17 = 0; _x17 < this.staticBudget.parking.length; _x17++) {
                    if (this.staticBudget.parking[_x17].parking_name !== null && this.staticBudget.parking[_x17].parking_budget !== null && this.staticBudget.parking[_x17].parking_name !== '' && this.staticBudget.parking[_x17].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x18 = 0; _x18 < this.staticBudget.tolling.length; _x18++) {
                    if (this.staticBudget.tolling[_x18].tolling_name !== null && this.staticBudget.tolling[_x18].tolling_budget !== null && this.staticBudget.tolling[_x18].tolling_name !== '' && this.staticBudget.tolling[_x18].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x19 = 0; _x19 < this.staticBudget.utilities.length; _x19++) {
                    if (this.staticBudget.utilities[_x19].utilities_name !== null && this.staticBudget.utilities[_x19].utilities_budget !== null && this.staticBudget.utilities[_x19].utilities_name !== '' && this.staticBudget.utilities[_x19].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x20 = 0; _x20 < this.staticBudget.under_total_fuel_and_exp.length; _x20++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x20].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x21 = 0; _x21 < this.staticBudget.under_total_admin.length; _x21++) {
                    if (this.staticBudget.under_total_admin[_x21].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x21].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x21].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x21].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_admin, goodUnder_total_fuel_and_exp, 'goodSalaries,goodFuels,goodRepairs,goodParking,goodTolling,goodUtilities,' + 'goodUnder_total_admin,goodUnder_total_fuel_and_exp,');

                  if (goodSalaries === 1 && goodFuels === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_admin === 1 && goodUnder_total_fuel_and_exp === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn21 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn21);
                    }
                  } else {
                    var _submitBtn22 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn22);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].sal_name === null && n[i].sal_budget === null && n[i].sal_name === '' && n[i].sal_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn23 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn23);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn24 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn24);

                      if (eitherIsNull === 1) {
                        var _submitBtn25 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn25);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].main_name !== null && newVal[i].main_budget !== null && newVal[i].main_name !== '' && newVal[i].main_budget !== '') {
                    var _submitBtn26 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn26);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn27 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn27);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn28 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn28);
                }
              }
            } else {
              var _submitBtn29 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn29);
            }
          }
        } else {
          var _submitBtn30 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn30);
        }
      }
    },
    "staticBudget.repairs": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].repairs_name !== null && newVal[i].repairs_budget !== null && newVal[i].repairs_name !== '' && newVal[i].repairs_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x22 = 0; _x22 < this.staticBudget.fuels.length; _x22++) {
                    if (this.staticBudget.fuels[_x22].fuel_name !== null && this.staticBudget.fuels[_x22].fuel_budget !== null && this.staticBudget.fuels[_x22].fuel_name !== '' && this.staticBudget.fuels[_x22].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x23 = 0; _x23 < this.staticBudget.maintenance.length; _x23++) {
                    if (this.staticBudget.maintenance[_x23].main_name !== null && this.staticBudget.maintenance[_x23].main_budget !== null && this.staticBudget.maintenance[_x23].main_name !== '' && this.staticBudget.maintenance[_x23].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x24 = 0; _x24 < this.staticBudget.parking.length; _x24++) {
                    if (this.staticBudget.parking[_x24].parking_name !== null && this.staticBudget.parking[_x24].parking_budget !== null && this.staticBudget.parking[_x24].parking_name !== '' && this.staticBudget.parking[_x24].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x25 = 0; _x25 < this.staticBudget.tolling.length; _x25++) {
                    if (this.staticBudget.tolling[_x25].tolling_name !== null && this.staticBudget.tolling[_x25].tolling_budget !== null && this.staticBudget.tolling[_x25].tolling_name !== '' && this.staticBudget.tolling[_x25].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x26 = 0; _x26 < this.staticBudget.utilities.length; _x26++) {
                    if (this.staticBudget.utilities[_x26].tolling_name !== null && this.staticBudget.utilities[_x26].tolling_budget !== null && this.staticBudget.utilities[_x26].tolling_name !== '' && this.staticBudget.utilities[_x26].tolling_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x27 = 0; _x27 < this.staticBudget.under_total_fuel_and_exp.length; _x27++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x27].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x28 = 0; _x28 < this.staticBudget.under_total_admin.length; _x28++) {
                    if (this.staticBudget.under_total_admin[_x28].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x28].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x28].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x28].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodUtilities,goodUnder_total_fuel_and_exp,goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn31 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn31);
                    }
                  } else {
                    var _submitBtn32 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn32);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].repairs_name === null && n[i].repairs_budget === null && n[i].repairs_name === '' && n[i].repairs_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn33 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn33);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn34 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn34);

                      if (eitherIsNull === 1) {
                        var _submitBtn35 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn35);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].repairs_name !== null && newVal[i].repairs_budget !== null && newVal[i].repairs_name !== '' && newVal[i].repairs_budget !== '') {
                    var _submitBtn36 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn36);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn37 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn37);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn38 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn38);
                }
              }
            } else {
              var _submitBtn39 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn39);
            }
          }
        } else {
          var _submitBtn40 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn40);
        }
      }
    },
    "staticBudget.parking": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].parking_name !== null && newVal[i].parking_budget !== null && newVal[i].parking_name !== '' && newVal[i].parking_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;
                  var goodUnder_total_admin = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x29 = 0; _x29 < this.staticBudget.fuels.length; _x29++) {
                    if (this.staticBudget.fuels[_x29].fuel_name !== null && this.staticBudget.fuels[_x29].fuel_budget !== null && this.staticBudget.fuels[_x29].fuel_name !== '' && this.staticBudget.fuels[_x29].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x30 = 0; _x30 < this.staticBudget.maintenance.length; _x30++) {
                    if (this.staticBudget.maintenance[_x30].main_name !== null && this.staticBudget.maintenance[_x30].main_budget !== null && this.staticBudget.maintenance[_x30].main_name !== '' && this.staticBudget.maintenance[_x30].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x31 = 0; _x31 < this.staticBudget.repairs.length; _x31++) {
                    if (this.staticBudget.repairs[_x31].repairs_name !== null && this.staticBudget.repairs[_x31].repairs_budget !== null && this.staticBudget.repairs[_x31].repairs_name !== '' && this.staticBudget.repairs[_x31].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x32 = 0; _x32 < this.staticBudget.tolling.length; _x32++) {
                    if (this.staticBudget.tolling[_x32].tolling_name !== null && this.staticBudget.tolling[_x32].tolling_budget !== null && this.staticBudget.tolling[_x32].tolling_name !== '' && this.staticBudget.tolling[_x32].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x33 = 0; _x33 < this.staticBudget.utilities.length; _x33++) {
                    if (this.staticBudget.utilities[_x33].utilities_name !== null && this.staticBudget.utilities[_x33].utilities_budget !== null && this.staticBudget.utilities[_x33].utilities_name !== '' && this.staticBudget.utilities[_x33].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x34 = 0; _x34 < this.staticBudget.under_total_fuel_and_exp.length; _x34++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x34].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  for (var _x35 = 0; _x35 < this.staticBudget.under_total_admin.length; _x35++) {
                    if (this.staticBudget.under_total_admin[_x35].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x35].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x35].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x35].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodTolling,goodUtilities,goodUnder_total_fuel_and_exp,' + 'goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn41 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn41);
                    }
                  } else {
                    var _submitBtn42 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn42);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].parking_name === null && n[i].parking_budget === null && n[i].parking_name === '' && n[i].parking_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn43 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn43);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn44 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn44);

                      if (eitherIsNull === 1) {
                        var _submitBtn45 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn45);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].parking_name !== null && newVal[i].parking_budget !== null && newVal[i].parking_name !== '' && newVal[i].parking_budget !== '') {
                    var _submitBtn46 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn46);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn47 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn47);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn48 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn48);
                }
              }
            } else {
              var _submitBtn49 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn49);
            }
          }
        } else {
          var _submitBtn50 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn50);
        }
      }
    },
    "staticBudget.tolling": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].tolling_name !== null && newVal[i].tolling_budget !== null && newVal[i].tolling_name !== '' && newVal[i].tolling_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_admin = 0;
                  var goodUnder_total_fuel_and_exp = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x36 = 0; _x36 < this.staticBudget.fuels.length; _x36++) {
                    if (this.staticBudget.fuels[_x36].fuel_name !== null && this.staticBudget.fuels[_x36].fuel_budget !== null && this.staticBudget.fuels[_x36].fuel_name !== '' && this.staticBudget.fuels[_x36].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x37 = 0; _x37 < this.staticBudget.maintenance.length; _x37++) {
                    if (this.staticBudget.maintenance[_x37].main_name !== null && this.staticBudget.maintenance[_x37].main_budget !== null && this.staticBudget.maintenance[_x37].main_name !== '' && this.staticBudget.maintenance[_x37].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x38 = 0; _x38 < this.staticBudget.repairs.length; _x38++) {
                    if (this.staticBudget.repairs[_x38].repairs_name !== null && this.staticBudget.repairs[_x38].repairs_budget !== null && this.staticBudget.repairs[_x38].repairs_name !== '' && this.staticBudget.repairs[_x38].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x39 = 0; _x39 < this.staticBudget.parking.length; _x39++) {
                    if (this.staticBudget.parking[_x39].parking_name !== null && this.staticBudget.parking[_x39].parking_budget !== null && this.staticBudget.parking[_x39].parking_name !== '' && this.staticBudget.parking[_x39].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x40 = 0; _x40 < this.staticBudget.utilities.length; _x40++) {
                    if (this.staticBudget.utilities[_x40].utilities_name !== null && this.staticBudget.utilities[_x40].utilities_budget !== null && this.staticBudget.utilities[_x40].utilities_name !== '' && this.staticBudget.utilities[_x40].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x41 = 0; _x41 < this.staticBudget.under_total_admin.length; _x41++) {
                    if (this.staticBudget.under_total_admin[_x41].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x41].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x41].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x41].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  for (var _x42 = 0; _x42 < this.staticBudget.under_total_fuel_and_exp.length; _x42++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x42].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodUtilities, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodUtilities,goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodUtilities === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn51 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn51);
                    }
                  } else {
                    var _submitBtn52 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn52);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].tolling_name === null && n[i].tolling_budget === null && n[i].tolling_name === '' && n[i].tolling_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn53 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn53);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn54 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn54);

                      if (eitherIsNull === 1) {
                        var _submitBtn55 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn55);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  if (newVal[i].tolling_name !== null && newVal[i].tolling_budget !== null && newVal[i].tolling_name !== '' && newVal[i].tolling_budget !== '') {
                    var _submitBtn56 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn56);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn57 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn57);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn58 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn58);
                }
              }
            } else {
              var _submitBtn59 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn59);
            }
          }
        } else {
          var _submitBtn60 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn60);
        }
      }
    },
    "staticBudget.utilities": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].utilities_name !== null && newVal[i].utilities_budget !== null && newVal[i].utilities_name !== '' && newVal[i].utilities_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUnder_total_admin = 0;
                  var goodUnder_total_fuel_and_exp = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x43 = 0; _x43 < this.staticBudget.fuels.length; _x43++) {
                    if (this.staticBudget.fuels[_x43].fuel_name !== null && this.staticBudget.fuels[_x43].fuel_budget !== null && this.staticBudget.fuels[_x43].fuel_name !== '' && this.staticBudget.fuels[_x43].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x44 = 0; _x44 < this.staticBudget.maintenance.length; _x44++) {
                    if (this.staticBudget.maintenance[_x44].main_name !== null && this.staticBudget.maintenance[_x44].main_budget !== null && this.staticBudget.maintenance[_x44].main_name !== '' && this.staticBudget.maintenance[_x44].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x45 = 0; _x45 < this.staticBudget.repairs.length; _x45++) {
                    if (this.staticBudget.repairs[_x45].repairs_name !== null && this.staticBudget.repairs[_x45].repairs_budget !== null && this.staticBudget.repairs[_x45].repairs_name !== '' && this.staticBudget.repairs[_x45].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x46 = 0; _x46 < this.staticBudget.parking.length; _x46++) {
                    if (this.staticBudget.parking[_x46].parking_name !== null && this.staticBudget.parking[_x46].parking_budget !== null && this.staticBudget.parking[_x46].parking_name !== '' && this.staticBudget.parking[_x46].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x47 = 0; _x47 < this.staticBudget.tolling.length; _x47++) {
                    if (this.staticBudget.tolling[_x47].tolling_name !== null && this.staticBudget.tolling[_x47].tolling_budget !== null && this.staticBudget.tolling[_x47].tolling_name !== '' && this.staticBudget.tolling[_x47].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x48 = 0; _x48 < this.staticBudget.under_total_admin.length; _x48++) {
                    if (this.staticBudget.under_total_admin[_x48].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x48].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x48].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x48].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  for (var _x49 = 0; _x49 < this.staticBudget.under_total_fuel_and_exp.length; _x49++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x49].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUnder_total_admin, goodUnder_total_fuel_and_exp, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUnder_total_admin,goodUnder_total_fuel_and_exp');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUnder_total_admin === 1 && goodUnder_total_fuel_and_exp === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn61 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn61);
                    }
                  } else {
                    var _submitBtn62 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn62);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].utilities_name === null && n[i].utilities_budget === null && n[i].utilities_name === '' && n[i].utilities_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn63 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn63);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn64 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn64);

                      if (eitherIsNull === 1) {
                        var _submitBtn65 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn65);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.under_total_admin.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  if (newVal[i].utilities_name !== null && newVal[i].utilities_budget !== null && newVal[i].utilities_name !== '' && newVal[i].utilities_budget !== '') {
                    var _submitBtn66 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn66);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn67 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn67);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn68 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn68);
                }
              }
            } else {
              var _submitBtn69 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn69);
            }
          }
        } else {
          var _submitBtn70 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn70);
        }
      }
    },
    "staticBudget.under_total_admin": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
              if (newVal[i].under_total_admin_name !== null && newVal[i].under_total_admin_budget !== null && newVal[i].under_total_admin_name !== '' && newVal[i].under_total_admin_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp.length !== 0) {
                  var goodFuels = 0;
                  var goodSalaries = 0;
                  var goodMaintenace = 0;
                  var goodRepairs = 0;
                  var goodParking = 0;
                  var goodTolling = 0;
                  var goodUtilities = 0;
                  var goodUnder_total_fuel_and_exp = 0;

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    // console.log(this.staticBudget.salaries[x].sal_name, 'this.staticBudget.salaries[x].sal_name');
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x50 = 0; _x50 < this.staticBudget.fuels.length; _x50++) {
                    if (this.staticBudget.fuels[_x50].fuel_name !== null && this.staticBudget.fuels[_x50].fuel_budget !== null && this.staticBudget.fuels[_x50].fuel_name !== '' && this.staticBudget.fuels[_x50].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x51 = 0; _x51 < this.staticBudget.maintenance.length; _x51++) {
                    if (this.staticBudget.maintenance[_x51].main_name !== null && this.staticBudget.maintenance[_x51].main_budget !== null && this.staticBudget.maintenance[_x51].main_name !== '' && this.staticBudget.maintenance[_x51].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x52 = 0; _x52 < this.staticBudget.repairs.length; _x52++) {
                    if (this.staticBudget.repairs[_x52].repairs_name !== null && this.staticBudget.repairs[_x52].repairs_budget !== null && this.staticBudget.repairs[_x52].repairs_name !== '' && this.staticBudget.repairs[_x52].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x53 = 0; _x53 < this.staticBudget.parking.length; _x53++) {
                    if (this.staticBudget.parking[_x53].parking_name !== null && this.staticBudget.parking[_x53].parking_budget !== null && this.staticBudget.parking[_x53].parking_name !== '' && this.staticBudget.parking[_x53].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x54 = 0; _x54 < this.staticBudget.tolling.length; _x54++) {
                    if (this.staticBudget.tolling[_x54].tolling_name !== null && this.staticBudget.tolling[_x54].tolling_budget !== null && this.staticBudget.tolling[_x54].tolling_name !== '' && this.staticBudget.tolling[_x54].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x55 = 0; _x55 < this.staticBudget.utilities.length; _x55++) {
                    if (this.staticBudget.utilities[_x55].utilities_name !== null && this.staticBudget.utilities[_x55].utilities_budget !== null && this.staticBudget.utilities[_x55].utilities_name !== '' && this.staticBudget.utilities[_x55].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x56 = 0; _x56 < this.staticBudget.under_total_fuel_and_exp.length; _x56++) {
                    if (this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_name !== null && this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_budget !== null && this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_name !== '' && this.staticBudget.under_total_fuel_and_exp[_x56].under_total_fuel_and_exp_budget !== '') {
                      goodUnder_total_fuel_and_exp = 1;
                    } else {
                      goodUnder_total_fuel_and_exp = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_fuel_and_exp.length === 0) {
                    goodUnder_total_fuel_and_exp = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_fuel_and_exp, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodParking,goodTolling,goodUtilities,goodUnder_total_fuel_and_exp');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_fuel_and_exp === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn71 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn71);
                    }
                  } else {
                    var _submitBtn72 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn72);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].under_total_admin_name === null && n[i].under_total_admin_budget === null && n[i].under_total_admin_name === '' && n[i].under_total_admin_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn73 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn73);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn74 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn74);

                      if (eitherIsNull === 1) {
                        var _submitBtn75 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn75);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_fuel_and_exp !== 0) {
                  if (newVal[i].under_total_admin_name !== null && newVal[i].under_total_admin_budget !== null && newVal[i].under_total_admin_name !== '' && newVal[i].under_total_admin_budget !== '') {
                    var _submitBtn76 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn76);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn77 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn77);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn78 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn78);
                }
              }
            } else {
              var _submitBtn79 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn79);
            }
          }
        } else {
          var _submitBtn80 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn80);
        }
      }
    },
    "staticBudget.under_total_fuel_and_exp": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        var eitherIsNull = 0;

        if (newVal.length !== 0) {
          for (var i = 0; i < newVal.length; i++) {
            if (newVal.length !== 0 || this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
              if (newVal[i].under_total_fuel_and_exp_name !== null && newVal[i].under_total_fuel_and_exp_budget !== null && newVal[i].under_total_fuel_and_exp_name !== '' && newVal[i].under_total_fuel_and_exp_budget !== '') {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  var goodFuels = 0; //

                  var goodSalaries = 0; //

                  var goodMaintenace = 0; //

                  var goodRepairs = 0; //

                  var goodParking = 0; //

                  var goodTolling = 0; //

                  var goodUtilities = 0; //

                  var goodUnder_total_admin = 0; //

                  for (var x = 0; x < this.staticBudget.salaries.length; x++) {
                    if (this.staticBudget.salaries[x].sal_name !== null && this.staticBudget.salaries[x].sal_budget !== null && this.staticBudget.salaries[x].sal_name !== '' && this.staticBudget.salaries[x].sal_budget !== '') {
                      goodSalaries = 1;
                    } else {
                      goodSalaries = 0;
                    }
                  }

                  for (var _x57 = 0; _x57 < this.staticBudget.fuels.length; _x57++) {
                    if (this.staticBudget.fuels[_x57].fuel_name !== null && this.staticBudget.fuels[_x57].fuel_budget !== null && this.staticBudget.fuels[_x57].fuel_name !== '' && this.staticBudget.fuels[_x57].fuel_budget !== '') {
                      goodFuels = 1;
                    } else {
                      goodFuels = 0;
                    }
                  }

                  for (var _x58 = 0; _x58 < this.staticBudget.maintenance.length; _x58++) {
                    if (this.staticBudget.maintenance[_x58].main_name !== null && this.staticBudget.maintenance[_x58].main_budget !== null && this.staticBudget.maintenance[_x58].main_name !== '' && this.staticBudget.maintenance[_x58].main_budget !== '') {
                      goodMaintenace = 1;
                    } else {
                      goodMaintenace = 0;
                    }
                  }

                  for (var _x59 = 0; _x59 < this.staticBudget.repairs.length; _x59++) {
                    if (this.staticBudget.repairs[_x59].repairs_name !== null && this.staticBudget.repairs[_x59].repairs_budget !== null && this.staticBudget.repairs[_x59].repairs_name !== '' && this.staticBudget.repairs[_x59].repairs_budget !== '') {
                      goodRepairs = 1;
                    } else {
                      goodRepairs = 0;
                    }
                  }

                  for (var _x60 = 0; _x60 < this.staticBudget.parking.length; _x60++) {
                    if (this.staticBudget.parking[_x60].parking_name !== null && this.staticBudget.parking[_x60].parking_budget !== null && this.staticBudget.parking[_x60].parking_name !== '' && this.staticBudget.parking[_x60].parking_budget !== '') {
                      goodParking = 1;
                    } else {
                      goodParking = 0;
                    }
                  }

                  for (var _x61 = 0; _x61 < this.staticBudget.tolling.length; _x61++) {
                    if (this.staticBudget.tolling[_x61].tolling_name !== null && this.staticBudget.tolling[_x61].tolling_budget !== null && this.staticBudget.tolling[_x61].tolling_name !== '' && this.staticBudget.tolling[_x61].tolling_budget !== '') {
                      goodTolling = 1;
                    } else {
                      goodTolling = 0;
                    }
                  }

                  for (var _x62 = 0; _x62 < this.staticBudget.utilities.length; _x62++) {
                    if (this.staticBudget.utilities[_x62].utilities_name !== null && this.staticBudget.utilities[_x62].utilities_budget !== null && this.staticBudget.utilities[_x62].utilities_name !== '' && this.staticBudget.utilities[_x62].utilities_budget !== '') {
                      goodUtilities = 1;
                    } else {
                      goodUtilities = 0;
                    }
                  }

                  for (var _x63 = 0; _x63 < this.staticBudget.under_total_admin.length; _x63++) {
                    if (this.staticBudget.under_total_admin[_x63].under_total_admin_name !== null && this.staticBudget.under_total_admin[_x63].under_total_admin_budget !== null && this.staticBudget.under_total_admin[_x63].under_total_admin_name !== '' && this.staticBudget.under_total_admin[_x63].under_total_admin_budget !== '') {
                      goodUnder_total_admin = 1;
                    } else {
                      goodUnder_total_admin = 0;
                    }
                  }

                  if (this.staticBudget.fuels.length === 0) {
                    goodFuels = 1;
                  }

                  if (this.staticBudget.salaries.length === 0) {
                    goodSalaries = 1;
                  }

                  if (this.staticBudget.maintenance.length === 0) {
                    goodMaintenace = 1;
                  }

                  if (this.staticBudget.repairs.length === 0) {
                    goodRepairs = 1;
                  }

                  if (this.staticBudget.parking.length === 0) {
                    goodParking = 1;
                  }

                  if (this.staticBudget.tolling.length === 0) {
                    goodTolling = 1;
                  }

                  if (this.staticBudget.utilities.length === 0) {
                    goodUtilities = 1;
                  }

                  if (this.staticBudget.under_total_admin.length === 0) {
                    goodUnder_total_admin = 1;
                  }

                  console.log(goodSalaries, goodFuels, goodMaintenace, goodRepairs, goodParking, goodTolling, goodUtilities, goodUnder_total_admin, 'goodSalaries,goodFuels,goodMaintenace,goodRepairs,goodTolling,goodUnder_total_admin');

                  if (goodSalaries === 1 && goodFuels === 1 && goodMaintenace === 1 && goodRepairs === 1 && goodParking === 1 && goodTolling === 1 && goodUtilities === 1 && goodUnder_total_admin === 1) {
                    var submitBtn = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', submitBtn);

                    if (eitherIsNull === 1) {
                      var _submitBtn81 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn81);
                    }
                  } else {
                    var _submitBtn82 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn82);
                  }
                } else {
                  var n = newVal;

                  for (var a = 0; a < n.length; a++) {
                    if (n[i].under_total_fuel_and_exp_name === null && n[i].under_total_fuel_and_exp_budget === null && n[i].under_total_fuel_and_exp_name === '' && n[i].under_total_fuel_and_exp_budget === '') {
                      // console.log('let submitBtn = true;');
                      var _submitBtn83 = true;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn83);
                    } else {
                      // console.log('let submitBtn = false;');
                      var _submitBtn84 = false;
                      this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn84);

                      if (eitherIsNull === 1) {
                        var _submitBtn85 = true;
                        this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn85);
                      }
                    }
                  }
                }
              } else {
                if (this.staticBudget.salaries.length !== 0 || this.staticBudget.fuels.length !== 0 || this.staticBudget.maintenance.length !== 0 || this.staticBudget.repairs.length !== 0 || this.staticBudget.parking.length !== 0 || this.staticBudget.tolling.length !== 0 || this.staticBudget.utilities.length !== 0 || this.staticBudget.under_total_admin.length !== 0) {
                  if (newVal[i].under_total_fuel_and_exp_name !== null && newVal[i].under_total_fuel_and_exp_budget !== null && newVal[i].under_total_fuel_and_exp_name !== '' && newVal[i].under_total_fuel_and_exp_budget !== '') {
                    var _submitBtn86 = false;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn86);
                  } else {
                    eitherIsNull = 1;
                    var _submitBtn87 = true;
                    this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn87);
                  }
                } else {
                  eitherIsNull = 1;
                  var _submitBtn88 = true;
                  this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn88);
                }
              }
            } else {
              var _submitBtn89 = true;
              this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn89);
            }
          }
        } else {
          var _submitBtn90 = true;
          this.$store.commit('static_budget/STATIC_BUDGET_EDIT_BTN', _submitBtn90);
        }
      }
    } // deep: true

  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])({
    updateStaticBudget: "static_budget/updateStaticBudget",
    getStaticBudget: "static_budget/fetchStaticBudgetsForDatatable"
  })), {}, {
    cancelEdit: function cancelEdit() {
      this.$emit("cancelEdit");
    },
    update: function update() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _this.$store.commit('static_budget/STATIC_BUDGET_UPDATE', _this.staticBudget);

              case 3:
                _context.next = 5;
                return _this.updateStaticBudget();

              case 5:
                _context.next = 7;
                return _this.$store.commit('static_budget/STATIC_BUDGET_CLEAR_AFTER_SUBMIT', true);

              case 7:
                _context.next = 9;
                return _this.getStaticBudget();

              case 9:
                _this.$swal({
                  icon: 'success',
                  title: 'Budget has been updated',
                  showConfirmButton: false,
                  timer: 1500
                });

                _context.next = 15;
                break;

              case 12:
                _context.prev = 12;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 12]]);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticIndex.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticIndex.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StaticBudgetIndex"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticTable.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticTable.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_ToyDataTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/ToyDataTable */ "./resources/js/components/ToyDataTable.vue");
/* harmony import */ var _static_budget_StaticCreate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../static_budget/StaticCreate */ "./resources/js/views/static_budget/StaticCreate.vue");
/* harmony import */ var _static_budget_StaticEdit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../static_budget/StaticEdit */ "./resources/js/views/static_budget/StaticEdit.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StaticBudgetTable",
  components: {
    DataTable: _components_ToyDataTable__WEBPACK_IMPORTED_MODULE_1__["default"],
    StaticBudgetCreate: _static_budget_StaticCreate__WEBPACK_IMPORTED_MODULE_2__["default"],
    StaticBudgetEdit: _static_budget_StaticEdit__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  data: function data() {
    return {
      showCreate: false,
      showEdit: false,
      tableHeaders: [{
        column_name: 'Company',
        query_name: 'company',
        sortable: true,
        width: 60
      }, {
        column_name: 'Created At',
        query_name: 'created_at',
        sortable: true,
        width: 20
      }, {
        column_name: 'Actions',
        query_name: 'created_at',
        sortable: false,
        width: 20
      }]
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapGetters"])({
    tableDefaultEntries: ["static_budget/perPage"],
    tableSortBy: ["static_budget/sortBy"],
    tableSortDir: ["static_budget/sortDir"],
    static_budgets: ["static_budget/staticBuds"],
    search: ["static_budget/search"],
    perPage: ["static_budget/perPage"]
  })), {}, {
    currentUserLocal: function currentUserLocal() {
      return this.$store.getters['auth/users'] || [];
    }
  }),
  created: function created() {
    //this.$store.commit('permission/PERMISSION_SEARCH', '');
    this.index();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_4__["mapActions"])({
    getStaticBudget: "static_budget/fetchStaticBudgetsForDatatable",
    editStaticBudget: "static_budget/getStaticBudget"
  })), {}, {
    deleteStaticBudget: function deleteStaticBudget(e, index) {
      var _this = this;

      // console.log(this.currentUserLocal[0].company,'currentUserLocal'); // working
      this.$swal({
        title: 'Are you sure to delete this budget for this company?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/static-budget/".concat(e);
          axios["delete"](uri).then(function (response) {
            _this.$swal('Deleted!', 'Your Budget has been deleted.', 'success');

            _this.$store.commit('static_budget/STATIC_BUDGET_CLEAR_AFTER_SUBMIT', true);

            _this.static_budgets.data.splice(index, 1);

            _this.getStaticBudget();
          });
        }
      });
    },
    showCreateView: function showCreateView() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _this2.$store.commit('static_budget/STATIC_BUDGET_CLEAR_AFTER_SUBMIT', true);

              case 2:
                _this2.showCreate = true;
                _this2.showEdit = false;
                _context.next = 6;
                return _this2.$store.commit('static_budget/STATIC_BUDGET_CLEAR');

              case 6:
                _context.next = 8;
                return _this2.$store.commit('static_budget/STATIC_BUDGET_CLEAR_AFTER_SUBMIT', false);

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    closeCreate: function closeCreate(value) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this3.showCreate = value;
                _this3.showEdit = false;
                _context2.next = 4;
                return _this3.$store.commit('static_budget/STATIC_BUDGET_CLEAR');

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    closeEdit: function closeEdit() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this4.showCreate = false;
                _this4.showEdit = false;
                _context3.next = 4;
                return _this4.$store.commit('static_budget/STATIC_BUDGET_CLEAR');

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    edit: function edit(data) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                // console.log(data.id,'data.id');
                _this5.showEdit = true;
                _this5.showCreate = false;
                _context4.next = 4;
                return _this5.$store.commit('static_budget/STATIC_BUDGET_CLEAR');

              case 4:
                _context4.next = 6;
                return _this5.$store.commit('static_budget/STATIC_BUDGET_ID', data.company);

              case 6:
                _context4.next = 8;
                return _this5.$store.commit('static_budget/STATIC_ID', data.id);

              case 8:
                _context4.next = 10;
                return _this5.editStaticBudget();

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    sortByTable: function sortByTable(value) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return _this6.$store.commit('static_budget/STATIC_BUDGET_SORTBY', value.sortBy);

              case 2:
                _context5.next = 4;
                return _this6.$store.commit('static_budget/STATIC_BUDGET_SORTDIR', value.sortDir);

              case 4:
                _context5.next = 6;
                return _this6.index();

              case 6:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    paginateTable: function paginateTable(value) {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return _this7.$store.commit('static_budget/STATIC_BUDGET_PERPAGE', value);

              case 2:
                _context6.next = 4;
                return _this7.index();

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    searchData: function searchData(value) {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return _this8.$store.commit('static_budget/STATIC_BUDGET_SEARCH', value);

              case 2:
                _context7.next = 4;
                return _this8.index();

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    },
    index: function index(page) {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                if (!(typeof page === 'undefined')) {
                  _context8.next = 3;
                  break;
                }

                _context8.next = 3;
                return _this9.$store.commit('static_budget/STATIC_BUDGET_PAGE', 1);

              case 3:
                _context8.next = 5;
                return _this9.$store.commit('static_budget/STATIC_BUDGET_PAGE', page);

              case 5:
                _context8.next = 7;
                return _this9.getStaticBudget();

              case 7:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    }
  })
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/_Form.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/_Form.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vform__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vform */ "./node_modules/vform/dist/vform.common.js");
/* harmony import */ var vform__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vform__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//hover mouse http://jsfiddle.net/1cekfnqw/3017/
// https://jsfiddle.net/kh831crL/5/


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StaticBudgetForm",
  components: {
    HasError: vform__WEBPACK_IMPORTED_MODULE_0__["HasError"],
    AlertError: vform__WEBPACK_IMPORTED_MODULE_0__["AlertError"],
    AlertSuccess: vform__WEBPACK_IMPORTED_MODULE_0__["AlertSuccess"]
  },
  props: {
    staticBudget: {},
    form: {
      type: Object
    },
    editStatic: false // staticBudgetArr:[],

  },
  data: function data() {
    return {
      salaries: [],
      fuels: [],
      maintenance: [],
      repairs: [],
      parking: [],
      tolling: [],
      utilities: [],
      under_total_admin: [],
      under_total_fuel_and_exp: [],
      op_expenses: [],
      company_code: '',
      totalExpenses: 0,
      totalSalaries: 0,
      totalFuelAndSellingExp: 0,
      totalAdminExpenses: 0,
      // <-- questionable
      totalNotNaN: true,
      doneFetch: true,
      visible: false,
      temp: null,
      tempIndex: null,
      // add visible true if create
      storeCompany: null,
      // this will store the current company
      storeCount: 0,
      // for checking
      disableCompany: false
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])({
    plantBranches: ["static_budget/plants"],
    staticBudgetEdit: ["static_budget/staticBudget"],
    clearData: ["static_budget/clear_data_after_submit"]
  })), {}, {
    // userInput(){
    //     return this.form;
    // },
    staticBudgetInput: function staticBudgetInput() {
      return this.staticBudget;
    }
  }),
  watch: {
    "staticBudgetEdit": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        if (this.doneFetch) {
          // first click coz at first click the this.doneFetch will be forever ``false``
          console.log('test editData');
          this.storeCount = 1;
          this.editData();
        }

        if (newVal.company !== this.storeCompany && this.doneFetch === false) {
          console.log(this.storeCompany = this.staticBudgetEdit.company, 'this.storeCompany = this.staticBudgetEdit.company;');
          this.storeCompany = this.staticBudgetEdit.company;
          this.storeCount = 1;
          this.editData();
        }
      }
    },
    "editStatic": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        console.log(newVal, 'editStatic');

        if (newVal) {
          this.disableCompany = true;
        }
      }
    },
    "clearData": {
      deep: true,
      handler: function handler(newVal, oldVal) {
        if (newVal) {
          this.salaries = [];
          this.fuels = [];
          this.maintenance = [];
          this.repairs = [];
          this.parking = [];
          this.tolling = [];
          this.utilities = [];
          this.under_total_admin = [];
          this.under_total_fuel_and_exp = [];
          this.staticBudget.totalSalaries = 0;
          this.staticBudget.totalFuelAndSellingExp = 0;
          this.staticBudget.totalAdminExpenses = 0;
          this.staticBudget.totalExpenses = 0;
          this.staticBudgetInput.company = null;
          this.staticBudgetInput.salaries = [];
          this.staticBudgetInput.fuels = [];
          this.staticBudgetInput.maintenance = [];
          this.staticBudgetInput.repairs = [];
          this.staticBudgetInput.parking = [];
          this.staticBudgetInput.tolling = [];
          this.staticBudgetInput.utilities = [];
          this.staticBudgetInput.under_total_admin = [];
          this.staticBudgetInput.under_total_fuel_and_exp = [];
          this.$store.commit('static_budget/STATIC_BUDGET_CLEAR_AFTER_SUBMIT', false);
          console.log(newVal, 'clearData');
        } // this.$store.commit('static_budget/STATIC_BUDGET_CREATE_BTN', true);

      }
    }
  },
  created: function created() {
    this.getAllRole();
    this.plants();
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])({
    getAllRole: "role/getRoles",
    plants: "static_budget/getPlant"
  })), {}, {
    onBlurNumberSal: function onBlurNumberSal(e, index) {
      // if you will remove your focus in the input // for Salary
      this.visible = false;
      console.log('onBlurNumberSal', index, this.tempIndex);

      if (this.tempIndex !== null) {
        if (this.salaries[this.tempIndex].sal_budget !== null && this.salaries[this.tempIndex].sal_budget !== undefined) {
          for (var i = 0; i < this.salaries.length; i++) {
            console.log(i, 'onBlurNumberSal');
            var addDefaultVal = this.salaries[i].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.salaries[i].sal_budget = addDefaultVal;
            console.log(this.salaries[i].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
          } //default for fuels


          for (var _i = 0; _i < this.fuels.length; _i++) {
            var _addDefaultVal = this.fuels[_i].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i].fuel_budget = _addDefaultVal;
          } //default for maintenance


          for (var _i2 = 0; _i2 < this.maintenance.length; _i2++) {
            var _addDefaultVal2 = this.maintenance[_i2].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i2].main_budget = _addDefaultVal2;
          } //default for repairs


          for (var _i3 = 0; _i3 < this.repairs.length; _i3++) {
            var _addDefaultVal3 = this.repairs[_i3].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i3].repairs_budget = _addDefaultVal3;
          } //default for parking


          for (var _i4 = 0; _i4 < this.parking.length; _i4++) {
            var _addDefaultVal4 = this.parking[_i4].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i4].parking_budget = _addDefaultVal4;
          } //default for tolling


          for (var _i5 = 0; _i5 < this.tolling.length; _i5++) {
            var _addDefaultVal5 = this.tolling[_i5].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i5].tolling_budget = _addDefaultVal5;
          } //default for under_total_fuel_and_exp


          for (var _i6 = 0; _i6 < this.under_total_fuel_and_exp.length; _i6++) {
            var _addDefaultVal6 = this.under_total_fuel_and_exp[_i6].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i6].under_total_fuel_and_exp_budget = _addDefaultVal6;
          } //default for utilities


          for (var _i7 = 0; _i7 < this.utilities.length; _i7++) {
            var _addDefaultVal7 = this.utilities[_i7].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i7].utilities_budget = _addDefaultVal7;
          } //default for under_total_admin


          for (var _i8 = 0; _i8 < this.under_total_admin.length; _i8++) {
            var _addDefaultVal8 = this.under_total_admin[_i8].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i8].under_total_admin_budget = _addDefaultVal8;
          } // this.thousandSeparator(this.salaries[this.tempIndex].sal_budget);


          this.staticBudgetInput.salaries[this.tempIndex].sal_budget = this.salaries[this.tempIndex].sal_budget;
        } else {
          for (var _i9 = 0; _i9 < this.salaries.length; _i9++) {
            if (_i9 !== this.tempIndex) {
              console.log(_i9, 'onBlurNumberSal');

              var _addDefaultVal9 = this.salaries[_i9].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i9].sal_budget = _addDefaultVal9;
              console.log(this.salaries[_i9].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            }
          } //default for fuels


          for (var _i10 = 0; _i10 < this.fuels.length; _i10++) {
            if (_i10 !== this.tempIndex) {
              var _addDefaultVal10 = this.fuels[_i10].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i10].fuel_budget = _addDefaultVal10;
            }
          } //default for maintenance


          for (var _i11 = 0; _i11 < this.maintenance.length; _i11++) {
            if (_i11 !== this.tempIndex) {
              var _addDefaultVal11 = this.maintenance[_i11].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i11].main_budget = _addDefaultVal11;
            }
          } //default for repairs


          for (var _i12 = 0; _i12 < this.repairs.length; _i12++) {
            if (_i12 !== this.tempIndex) {
              var _addDefaultVal12 = this.repairs[_i12].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i12].repairs_budget = _addDefaultVal12;
            }
          } //default for parking


          for (var _i13 = 0; _i13 < this.parking.length; _i13++) {
            if (_i13 !== this.tempIndex) {
              var _addDefaultVal13 = this.parking[_i13].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i13].parking_budget = _addDefaultVal13;
            }
          } //default for tolling


          for (var _i14 = 0; _i14 < this.tolling.length; _i14++) {
            if (_i14 !== this.tempIndex) {
              var _addDefaultVal14 = this.tolling[_i14].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i14].tolling_budget = _addDefaultVal14;
            }
          } //default for under_total_fuel_and_exp


          for (var _i15 = 0; _i15 < this.under_total_fuel_and_exp.length; _i15++) {
            if (_i15 !== this.tempIndex) {
              var _addDefaultVal15 = this.under_total_fuel_and_exp[_i15].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i15].under_total_fuel_and_exp_budget = _addDefaultVal15;
            }
          } //default for utilities


          for (var _i16 = 0; _i16 < this.utilities.length; _i16++) {
            if (_i16 !== this.tempIndex) {
              var _addDefaultVal16 = this.utilities[_i16].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i16].utilities_budget = _addDefaultVal16;
            }
          } //default for under_total_admin


          for (var _i17 = 0; _i17 < this.under_total_admin.length; _i17++) {
            if (_i17 !== this.tempIndex) {
              var _addDefaultVal17 = this.under_total_admin[_i17].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i17].under_total_admin_budget = _addDefaultVal17;
            }
          }
        }
      }
    },
    onBlurNumberFuel: function onBlurNumberFuel(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.fuels[this.tempIndex].fuel_budget !== null && this.fuels[this.tempIndex].fuel_budget !== undefined) {
          for (var i = 0; i < this.fuels.length; i++) {
            var addDefaultVal = this.fuels[i].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.fuels[i].fuel_budget = addDefaultVal;
          } //default for salaries


          for (var _i18 = 0; _i18 < this.salaries.length; _i18++) {
            var _addDefaultVal18 = this.salaries[_i18].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i18].sal_budget = _addDefaultVal18;
          } //default for maintenance


          for (var _i19 = 0; _i19 < this.maintenance.length; _i19++) {
            var _addDefaultVal19 = this.maintenance[_i19].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i19].main_budget = _addDefaultVal19;
          } //default for repairs


          for (var _i20 = 0; _i20 < this.repairs.length; _i20++) {
            var _addDefaultVal20 = this.repairs[_i20].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i20].repairs_budget = _addDefaultVal20;
          } //default for parking


          for (var _i21 = 0; _i21 < this.parking.length; _i21++) {
            var _addDefaultVal21 = this.parking[_i21].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i21].parking_budget = _addDefaultVal21;
          } //default for tolling


          for (var _i22 = 0; _i22 < this.tolling.length; _i22++) {
            var _addDefaultVal22 = this.tolling[_i22].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i22].tolling_budget = _addDefaultVal22;
          } //default for under_total_fuel_and_exp


          for (var _i23 = 0; _i23 < this.under_total_fuel_and_exp.length; _i23++) {
            var _addDefaultVal23 = this.under_total_fuel_and_exp[_i23].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i23].under_total_fuel_and_exp_budget = _addDefaultVal23;
          } //default for utilities


          for (var _i24 = 0; _i24 < this.utilities.length; _i24++) {
            var _addDefaultVal24 = this.utilities[_i24].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i24].utilities_budget = _addDefaultVal24;
          } //default for under_total_admin


          for (var _i25 = 0; _i25 < this.under_total_admin.length; _i25++) {
            var _addDefaultVal25 = this.under_total_admin[_i25].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i25].under_total_admin_budget = _addDefaultVal25;
          }

          this.staticBudgetInput.fuels[this.tempIndex].fuel_budget = this.fuels[this.tempIndex].fuel_budget;
        } else {
          for (var _i26 = 0; _i26 < this.fuels.length; _i26++) {
            if (_i26 !== this.tempIndex) {
              var _addDefaultVal26 = this.fuels[_i26].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i26].fuel_budget = _addDefaultVal26;
            }
          } //default for salaries


          for (var _i27 = 0; _i27 < this.salaries.length; _i27++) {
            if (_i27 !== this.tempIndex) {
              var _addDefaultVal27 = this.salaries[_i27].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i27].sal_budget = _addDefaultVal27;
            }
          } //default for maintenance


          for (var _i28 = 0; _i28 < this.maintenance.length; _i28++) {
            if (_i28 !== this.tempIndex) {
              var _addDefaultVal28 = this.maintenance[_i28].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i28].main_budget = _addDefaultVal28;
            }
          } //default for repairs


          for (var _i29 = 0; _i29 < this.repairs.length; _i29++) {
            if (_i29 !== this.tempIndex) {
              var _addDefaultVal29 = this.repairs[_i29].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i29].repairs_budget = _addDefaultVal29;
            }
          } //default for parking


          for (var _i30 = 0; _i30 < this.parking.length; _i30++) {
            if (_i30 !== this.tempIndex) {
              var _addDefaultVal30 = this.parking[_i30].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i30].parking_budget = _addDefaultVal30;
            }
          } //default for tolling


          for (var _i31 = 0; _i31 < this.tolling.length; _i31++) {
            if (_i31 !== this.tempIndex) {
              var _addDefaultVal31 = this.tolling[_i31].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i31].tolling_budget = _addDefaultVal31;
            }
          } //default for under_total_fuel_and_exp


          for (var _i32 = 0; _i32 < this.under_total_fuel_and_exp.length; _i32++) {
            if (_i32 !== this.tempIndex) {
              var _addDefaultVal32 = this.under_total_fuel_and_exp[_i32].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i32].under_total_fuel_and_exp_budget = _addDefaultVal32;
            }
          } //default for utilities


          for (var _i33 = 0; _i33 < this.utilities.length; _i33++) {
            if (_i33 !== this.tempIndex) {
              var _addDefaultVal33 = this.utilities[_i33].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i33].utilities_budget = _addDefaultVal33;
            }
          } //default for under_total_admin


          for (var _i34 = 0; _i34 < this.under_total_admin.length; _i34++) {
            if (_i34 !== this.tempIndex) {
              var _addDefaultVal34 = this.under_total_admin[_i34].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i34].under_total_admin_budget = _addDefaultVal34;
            }
          }
        }
      }
    },
    onBlurNumberMain: function onBlurNumberMain(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.maintenance[this.tempIndex].main_budget !== null && this.maintenance[this.tempIndex].main_budget !== undefined) {
          for (var i = 0; i < this.maintenance.length; i++) {
            var addDefaultVal = this.maintenance[i].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.maintenance[i].main_budget = addDefaultVal;
          } //default for fuels


          for (var _i35 = 0; _i35 < this.fuels.length; _i35++) {
            var _addDefaultVal35 = this.fuels[_i35].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i35].fuel_budget = _addDefaultVal35;
          } //default for salaries


          for (var _i36 = 0; _i36 < this.salaries.length; _i36++) {
            var _addDefaultVal36 = this.salaries[_i36].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i36].sal_budget = _addDefaultVal36;
          } //default for repairs


          for (var _i37 = 0; _i37 < this.repairs.length; _i37++) {
            var _addDefaultVal37 = this.repairs[_i37].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i37].repairs_budget = _addDefaultVal37;
          } //default for parking


          for (var _i38 = 0; _i38 < this.parking.length; _i38++) {
            var _addDefaultVal38 = this.parking[_i38].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i38].parking_budget = _addDefaultVal38;
          } //default for tolling


          for (var _i39 = 0; _i39 < this.tolling.length; _i39++) {
            var _addDefaultVal39 = this.tolling[_i39].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i39].tolling_budget = _addDefaultVal39;
          } //default for under_total_fuel_and_exp


          for (var _i40 = 0; _i40 < this.under_total_fuel_and_exp.length; _i40++) {
            var _addDefaultVal40 = this.under_total_fuel_and_exp[_i40].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i40].under_total_fuel_and_exp_budget = _addDefaultVal40;
          } //default for utilities


          for (var _i41 = 0; _i41 < this.utilities.length; _i41++) {
            var _addDefaultVal41 = this.utilities[_i41].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i41].utilities_budget = _addDefaultVal41;
          } //default for under_total_admin


          for (var _i42 = 0; _i42 < this.under_total_admin.length; _i42++) {
            var _addDefaultVal42 = this.under_total_admin[_i42].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i42].under_total_admin_budget = _addDefaultVal42;
          }

          this.staticBudgetInput.maintenance[this.tempIndex].main_budget = this.maintenance[this.tempIndex].main_budget;
        } else {
          for (var _i43 = 0; _i43 < this.maintenance.length; _i43++) {
            if (_i43 !== this.tempIndex) {
              var _addDefaultVal43 = this.maintenance[_i43].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i43].main_budget = _addDefaultVal43;
            }
          } //default for fuels


          for (var _i44 = 0; _i44 < this.fuels.length; _i44++) {
            if (_i44 !== this.tempIndex) {
              var _addDefaultVal44 = this.fuels[_i44].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i44].fuel_budget = _addDefaultVal44;
            }
          } //default for salaries


          for (var _i45 = 0; _i45 < this.salaries.length; _i45++) {
            if (_i45 !== this.tempIndex) {
              var _addDefaultVal45 = this.salaries[_i45].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i45].sal_budget = _addDefaultVal45;
            }
          } //default for repairs


          for (var _i46 = 0; _i46 < this.repairs.length; _i46++) {
            if (_i46 !== this.tempIndex) {
              var _addDefaultVal46 = this.repairs[_i46].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i46].repairs_budget = _addDefaultVal46;
            }
          } //default for parking


          for (var _i47 = 0; _i47 < this.parking.length; _i47++) {
            if (_i47 !== this.tempIndex) {
              var _addDefaultVal47 = this.parking[_i47].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i47].parking_budget = _addDefaultVal47;
            }
          } //default for tolling


          for (var _i48 = 0; _i48 < this.tolling.length; _i48++) {
            if (_i48 !== this.tempIndex) {
              var _addDefaultVal48 = this.tolling[_i48].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i48].tolling_budget = _addDefaultVal48;
            }
          } //default for under_total_fuel_and_exp


          for (var _i49 = 0; _i49 < this.under_total_fuel_and_exp.length; _i49++) {
            if (_i49 !== this.tempIndex) {
              var _addDefaultVal49 = this.under_total_fuel_and_exp[_i49].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i49].under_total_fuel_and_exp_budget = _addDefaultVal49;
            }
          } //default for utilities


          for (var _i50 = 0; _i50 < this.utilities.length; _i50++) {
            if (_i50 !== this.tempIndex) {
              var _addDefaultVal50 = this.utilities[_i50].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i50].utilities_budget = _addDefaultVal50;
            }
          } //default for under_total_admin


          for (var _i51 = 0; _i51 < this.under_total_admin.length; _i51++) {
            if (_i51 !== this.tempIndex) {
              var _addDefaultVal51 = this.under_total_admin[_i51].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i51].under_total_admin_budget = _addDefaultVal51;
            }
          }
        }
      }
    },
    onBlurNumberRepair: function onBlurNumberRepair(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.repairs[this.tempIndex].repairs_budget !== null && this.repairs[this.tempIndex].repairs_budget !== undefined) {
          for (var i = 0; i < this.repairs.length; i++) {
            var addDefaultVal = this.repairs[i].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.repairs[i].repairs_budget = addDefaultVal;
          } //default for maintenance


          for (var _i52 = 0; _i52 < this.maintenance.length; _i52++) {
            var _addDefaultVal52 = this.maintenance[_i52].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i52].main_budget = _addDefaultVal52;
          } //default for fuels


          for (var _i53 = 0; _i53 < this.fuels.length; _i53++) {
            var _addDefaultVal53 = this.fuels[_i53].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i53].fuel_budget = _addDefaultVal53;
          } //default for salaries


          for (var _i54 = 0; _i54 < this.salaries.length; _i54++) {
            var _addDefaultVal54 = this.salaries[_i54].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i54].sal_budget = _addDefaultVal54;
          } //default for parking


          for (var _i55 = 0; _i55 < this.parking.length; _i55++) {
            var _addDefaultVal55 = this.parking[_i55].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i55].parking_budget = _addDefaultVal55;
          } //default for tolling


          for (var _i56 = 0; _i56 < this.tolling.length; _i56++) {
            var _addDefaultVal56 = this.tolling[_i56].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i56].tolling_budget = _addDefaultVal56;
          } //default for under_total_fuel_and_exp


          for (var _i57 = 0; _i57 < this.under_total_fuel_and_exp.length; _i57++) {
            var _addDefaultVal57 = this.under_total_fuel_and_exp[_i57].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i57].under_total_fuel_and_exp_budget = _addDefaultVal57;
          } //default for utilities


          for (var _i58 = 0; _i58 < this.utilities.length; _i58++) {
            var _addDefaultVal58 = this.utilities[_i58].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i58].utilities_budget = _addDefaultVal58;
          } //default for under_total_admin


          for (var _i59 = 0; _i59 < this.under_total_admin.length; _i59++) {
            var _addDefaultVal59 = this.under_total_admin[_i59].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i59].under_total_admin_budget = _addDefaultVal59;
          }

          this.staticBudgetInput.repairs[this.tempIndex].repairs_budget = this.repairs[this.tempIndex].repairs_budget;
        } else {
          for (var _i60 = 0; _i60 < this.repairs.length; _i60++) {
            if (_i60 !== this.tempIndex) {
              var _addDefaultVal60 = this.repairs[_i60].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i60].repairs_budget = _addDefaultVal60;
            }
          } //default for maintenance


          for (var _i61 = 0; _i61 < this.maintenance.length; _i61++) {
            if (_i61 !== this.tempIndex) {
              var _addDefaultVal61 = this.maintenance[_i61].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i61].main_budget = _addDefaultVal61;
            }
          } //default for fuels


          for (var _i62 = 0; _i62 < this.fuels.length; _i62++) {
            if (_i62 !== this.tempIndex) {
              var _addDefaultVal62 = this.fuels[_i62].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i62].fuel_budget = _addDefaultVal62;
            }
          } //default for salaries


          for (var _i63 = 0; _i63 < this.salaries.length; _i63++) {
            if (_i63 !== this.tempIndex) {
              var _addDefaultVal63 = this.salaries[_i63].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i63].sal_budget = _addDefaultVal63;
            }
          } //default for parking


          for (var _i64 = 0; _i64 < this.parking.length; _i64++) {
            if (_i64 !== this.tempIndex) {
              var _addDefaultVal64 = this.parking[_i64].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i64].parking_budget = _addDefaultVal64;
            }
          } //default for tolling


          for (var _i65 = 0; _i65 < this.tolling.length; _i65++) {
            if (_i65 !== this.tempIndex) {
              var _addDefaultVal65 = this.tolling[_i65].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i65].tolling_budget = _addDefaultVal65;
            }
          } //default for under_total_fuel_and_exp


          for (var _i66 = 0; _i66 < this.under_total_fuel_and_exp.length; _i66++) {
            if (_i66 !== this.tempIndex) {
              var _addDefaultVal66 = this.under_total_fuel_and_exp[_i66].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i66].under_total_fuel_and_exp_budget = _addDefaultVal66;
            }
          } //default for utilities


          for (var _i67 = 0; _i67 < this.utilities.length; _i67++) {
            if (_i67 !== this.tempIndex) {
              var _addDefaultVal67 = this.utilities[_i67].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i67].utilities_budget = _addDefaultVal67;
            }
          } //default for under_total_admin


          for (var _i68 = 0; _i68 < this.under_total_admin.length; _i68++) {
            if (_i68 !== this.tempIndex) {
              var _addDefaultVal68 = this.under_total_admin[_i68].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i68].under_total_admin_budget = _addDefaultVal68;
            }
          }
        }
      }
    },
    onBlurNumberParking: function onBlurNumberParking(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.parking[this.tempIndex].parking_budget !== null && this.parking[this.tempIndex].parking_budget !== undefined) {
          for (var i = 0; i < this.parking.length; i++) {
            var addDefaultVal = this.parking[i].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.parking[i].parking_budget = addDefaultVal;
          } //default for repairs


          for (var _i69 = 0; _i69 < this.repairs.length; _i69++) {
            var _addDefaultVal69 = this.repairs[_i69].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i69].repairs_budget = _addDefaultVal69;
          } //default for maintenance


          for (var _i70 = 0; _i70 < this.maintenance.length; _i70++) {
            var _addDefaultVal70 = this.maintenance[_i70].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i70].main_budget = _addDefaultVal70;
          } //default for fuels


          for (var _i71 = 0; _i71 < this.fuels.length; _i71++) {
            var _addDefaultVal71 = this.fuels[_i71].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i71].fuel_budget = _addDefaultVal71;
          } //default for salaries


          for (var _i72 = 0; _i72 < this.salaries.length; _i72++) {
            var _addDefaultVal72 = this.salaries[_i72].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i72].sal_budget = _addDefaultVal72;
          } //default for tolling


          for (var _i73 = 0; _i73 < this.tolling.length; _i73++) {
            var _addDefaultVal73 = this.tolling[_i73].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i73].tolling_budget = _addDefaultVal73;
          } //default for under_total_fuel_and_exp


          for (var _i74 = 0; _i74 < this.under_total_fuel_and_exp.length; _i74++) {
            var _addDefaultVal74 = this.under_total_fuel_and_exp[_i74].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i74].under_total_fuel_and_exp_budget = _addDefaultVal74;
          } //default for utilities


          for (var _i75 = 0; _i75 < this.utilities.length; _i75++) {
            var _addDefaultVal75 = this.utilities[_i75].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i75].utilities_budget = _addDefaultVal75;
          } //default for under_total_admin


          for (var _i76 = 0; _i76 < this.under_total_admin.length; _i76++) {
            var _addDefaultVal76 = this.under_total_admin[_i76].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i76].under_total_admin_budget = _addDefaultVal76;
          }

          this.staticBudgetInput.parking[this.tempIndex].parking_budget = this.parking[this.tempIndex].parking_budget;
        } else {
          for (var _i77 = 0; _i77 < this.parking.length; _i77++) {
            if (_i77 !== this.tempIndex) {
              var _addDefaultVal77 = this.parking[_i77].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i77].parking_budget = _addDefaultVal77;
            }
          } //default for repairs


          for (var _i78 = 0; _i78 < this.repairs.length; _i78++) {
            if (_i78 !== this.tempIndex) {
              var _addDefaultVal78 = this.repairs[_i78].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i78].repairs_budget = _addDefaultVal78;
            }
          } //default for maintenance


          for (var _i79 = 0; _i79 < this.maintenance.length; _i79++) {
            if (_i79 !== this.tempIndex) {
              var _addDefaultVal79 = this.maintenance[_i79].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i79].main_budget = _addDefaultVal79;
            }
          } //default for fuels


          for (var _i80 = 0; _i80 < this.fuels.length; _i80++) {
            if (_i80 !== this.tempIndex) {
              var _addDefaultVal80 = this.fuels[_i80].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i80].fuel_budget = _addDefaultVal80;
            }
          } //default for salaries


          for (var _i81 = 0; _i81 < this.salaries.length; _i81++) {
            if (_i81 !== this.tempIndex) {
              var _addDefaultVal81 = this.salaries[_i81].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i81].sal_budget = _addDefaultVal81;
            }
          } //default for tolling


          for (var _i82 = 0; _i82 < this.tolling.length; _i82++) {
            if (_i82 !== this.tempIndex) {
              var _addDefaultVal82 = this.tolling[_i82].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i82].tolling_budget = _addDefaultVal82;
            }
          } //default for under_total_fuel_and_exp


          for (var _i83 = 0; _i83 < this.under_total_fuel_and_exp.length; _i83++) {
            if (_i83 !== this.tempIndex) {
              var _addDefaultVal83 = this.under_total_fuel_and_exp[_i83].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i83].under_total_fuel_and_exp_budget = _addDefaultVal83;
            }
          } //default for utilities


          for (var _i84 = 0; _i84 < this.utilities.length; _i84++) {
            if (_i84 !== this.tempIndex) {
              var _addDefaultVal84 = this.utilities[_i84].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i84].utilities_budget = _addDefaultVal84;
            }
          } //default for under_total_admin


          for (var _i85 = 0; _i85 < this.under_total_admin.length; _i85++) {
            if (_i85 !== this.tempIndex) {
              var _addDefaultVal85 = this.under_total_admin[_i85].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i85].under_total_admin_budget = _addDefaultVal85;
            }
          }
        }
      }
    },
    onBlurNumberTolling: function onBlurNumberTolling(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.tolling[this.tempIndex].tolling_budget !== null && this.tolling[this.tempIndex].tolling_budget !== undefined) {
          for (var i = 0; i < this.tolling.length; i++) {
            var addDefaultVal = this.tolling[i].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.tolling[i].tolling_budget = addDefaultVal;
          } //default for parking


          for (var _i86 = 0; _i86 < this.parking.length; _i86++) {
            var _addDefaultVal86 = this.parking[_i86].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i86].parking_budget = _addDefaultVal86;
          } //default for repairs


          for (var _i87 = 0; _i87 < this.repairs.length; _i87++) {
            var _addDefaultVal87 = this.repairs[_i87].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i87].repairs_budget = _addDefaultVal87;
          } //default for maintenance


          for (var _i88 = 0; _i88 < this.maintenance.length; _i88++) {
            var _addDefaultVal88 = this.maintenance[_i88].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i88].main_budget = _addDefaultVal88;
          } //default for fuels


          for (var _i89 = 0; _i89 < this.fuels.length; _i89++) {
            var _addDefaultVal89 = this.fuels[_i89].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i89].fuel_budget = _addDefaultVal89;
          } //default for salaries


          for (var _i90 = 0; _i90 < this.salaries.length; _i90++) {
            var _addDefaultVal90 = this.salaries[_i90].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i90].sal_budget = _addDefaultVal90;
          } //default for under_total_fuel_and_exp


          for (var _i91 = 0; _i91 < this.under_total_fuel_and_exp.length; _i91++) {
            var _addDefaultVal91 = this.under_total_fuel_and_exp[_i91].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i91].under_total_fuel_and_exp_budget = _addDefaultVal91;
          } //default for utilities


          for (var _i92 = 0; _i92 < this.utilities.length; _i92++) {
            var _addDefaultVal92 = this.utilities[_i92].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i92].utilities_budget = _addDefaultVal92;
          } //default for under_total_admin


          for (var _i93 = 0; _i93 < this.under_total_admin.length; _i93++) {
            var _addDefaultVal93 = this.under_total_admin[_i93].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i93].under_total_admin_budget = _addDefaultVal93;
          }

          this.staticBudgetInput.tolling[this.tempIndex].tolling_budget = this.tolling[this.tempIndex].tolling_budget;
        } else {
          for (var _i94 = 0; _i94 < this.tolling.length; _i94++) {
            if (_i94 !== this.tempIndex) {
              var _addDefaultVal94 = this.tolling[_i94].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i94].tolling_budget = _addDefaultVal94;
            }
          } //default for parking


          for (var _i95 = 0; _i95 < this.parking.length; _i95++) {
            if (_i95 !== this.tempIndex) {
              var _addDefaultVal95 = this.parking[_i95].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i95].parking_budget = _addDefaultVal95;
            }
          } //default for repairs


          for (var _i96 = 0; _i96 < this.repairs.length; _i96++) {
            if (_i96 !== this.tempIndex) {
              var _addDefaultVal96 = this.repairs[_i96].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i96].repairs_budget = _addDefaultVal96;
            }
          } //default for maintenance


          for (var _i97 = 0; _i97 < this.maintenance.length; _i97++) {
            if (_i97 !== this.tempIndex) {
              var _addDefaultVal97 = this.maintenance[_i97].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i97].main_budget = _addDefaultVal97;
            }
          } //default for fuels


          for (var _i98 = 0; _i98 < this.fuels.length; _i98++) {
            if (_i98 !== this.tempIndex) {
              var _addDefaultVal98 = this.fuels[_i98].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i98].fuel_budget = _addDefaultVal98;
            }
          } //default for salaries


          for (var _i99 = 0; _i99 < this.salaries.length; _i99++) {
            if (_i99 !== this.tempIndex) {
              var _addDefaultVal99 = this.salaries[_i99].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i99].sal_budget = _addDefaultVal99;
            }
          } //default for under_total_fuel_and_exp


          for (var _i100 = 0; _i100 < this.under_total_fuel_and_exp.length; _i100++) {
            if (_i100 !== this.tempIndex) {
              var _addDefaultVal100 = this.under_total_fuel_and_exp[_i100].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i100].under_total_fuel_and_exp_budget = _addDefaultVal100;
            }
          } //default for utilities


          for (var _i101 = 0; _i101 < this.utilities.length; _i101++) {
            if (_i101 !== this.tempIndex) {
              var _addDefaultVal101 = this.utilities[_i101].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i101].utilities_budget = _addDefaultVal101;
            }
          } //default for under_total_admin


          for (var _i102 = 0; _i102 < this.under_total_admin.length; _i102++) {
            if (_i102 !== this.tempIndex) {
              var _addDefaultVal102 = this.under_total_admin[_i102].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i102].under_total_admin_budget = _addDefaultVal102;
            }
          }
        }
      }
    },
    onBlurNumberUnderTotalFuelAndExpBudget: function onBlurNumberUnderTotalFuelAndExpBudget(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.under_total_fuel_and_exp[this.tempIndex].under_total_fuel_and_exp_budget !== null && this.under_total_fuel_and_exp[this.tempIndex].under_total_fuel_and_exp_budget !== undefined) {
          for (var i = 0; i < this.under_total_fuel_and_exp.length; i++) {
            var addDefaultVal = this.under_total_fuel_and_exp[i].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.under_total_fuel_and_exp[i].under_total_fuel_and_exp_budget = addDefaultVal;
          } //default for tolling


          for (var _i103 = 0; _i103 < this.tolling.length; _i103++) {
            var _addDefaultVal103 = this.tolling[_i103].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i103].tolling_budget = _addDefaultVal103;
          } //default for parking


          for (var _i104 = 0; _i104 < this.parking.length; _i104++) {
            var _addDefaultVal104 = this.parking[_i104].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i104].parking_budget = _addDefaultVal104;
          } //default for repairs


          for (var _i105 = 0; _i105 < this.repairs.length; _i105++) {
            var _addDefaultVal105 = this.repairs[_i105].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i105].repairs_budget = _addDefaultVal105;
          } //default for maintenance


          for (var _i106 = 0; _i106 < this.maintenance.length; _i106++) {
            var _addDefaultVal106 = this.maintenance[_i106].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i106].main_budget = _addDefaultVal106;
          } //default for fuels


          for (var _i107 = 0; _i107 < this.fuels.length; _i107++) {
            var _addDefaultVal107 = this.fuels[_i107].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i107].fuel_budget = _addDefaultVal107;
          } //default for salaries


          for (var _i108 = 0; _i108 < this.salaries.length; _i108++) {
            var _addDefaultVal108 = this.salaries[_i108].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i108].sal_budget = _addDefaultVal108;
          } //default for utilities


          for (var _i109 = 0; _i109 < this.utilities.length; _i109++) {
            var _addDefaultVal109 = this.utilities[_i109].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i109].utilities_budget = _addDefaultVal109;
          } //default for under_total_admin


          for (var _i110 = 0; _i110 < this.under_total_admin.length; _i110++) {
            var _addDefaultVal110 = this.under_total_admin[_i110].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i110].under_total_admin_budget = _addDefaultVal110;
          }

          this.staticBudgetInput.under_total_fuel_and_exp[this.tempIndex].under_total_fuel_and_exp_budget = this.under_total_fuel_and_exp[this.tempIndex].under_total_fuel_and_exp_budget;
        } else {
          for (var _i111 = 0; _i111 < this.under_total_fuel_and_exp.length; _i111++) {
            if (_i111 !== this.tempIndex) {
              var _addDefaultVal111 = this.under_total_fuel_and_exp[_i111].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i111].under_total_fuel_and_exp_budget = _addDefaultVal111;
            }
          } //default for tolling


          for (var _i112 = 0; _i112 < this.tolling.length; _i112++) {
            if (_i112 !== this.tempIndex) {
              var _addDefaultVal112 = this.tolling[_i112].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i112].tolling_budget = _addDefaultVal112;
            }
          } //default for parking


          for (var _i113 = 0; _i113 < this.parking.length; _i113++) {
            if (_i113 !== this.tempIndex) {
              var _addDefaultVal113 = this.parking[_i113].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i113].parking_budget = _addDefaultVal113;
            }
          } //default for repairs


          for (var _i114 = 0; _i114 < this.repairs.length; _i114++) {
            if (_i114 !== this.tempIndex) {
              var _addDefaultVal114 = this.repairs[_i114].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i114].repairs_budget = _addDefaultVal114;
            }
          } //default for maintenance


          for (var _i115 = 0; _i115 < this.maintenance.length; _i115++) {
            if (_i115 !== this.tempIndex) {
              var _addDefaultVal115 = this.maintenance[_i115].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i115].main_budget = _addDefaultVal115;
            }
          } //default for fuels


          for (var _i116 = 0; _i116 < this.fuels.length; _i116++) {
            if (_i116 !== this.tempIndex) {
              var _addDefaultVal116 = this.fuels[_i116].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i116].fuel_budget = _addDefaultVal116;
            }
          } //default for salaries


          for (var _i117 = 0; _i117 < this.salaries.length; _i117++) {
            if (_i117 !== this.tempIndex) {
              var _addDefaultVal117 = this.salaries[_i117].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i117].sal_budget = _addDefaultVal117;
            }
          } //default for utilities


          for (var _i118 = 0; _i118 < this.utilities.length; _i118++) {
            if (_i118 !== this.tempIndex) {
              var _addDefaultVal118 = this.utilities[_i118].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i118].utilities_budget = _addDefaultVal118;
            }
          } //default for under_total_admin


          for (var _i119 = 0; _i119 < this.under_total_admin.length; _i119++) {
            if (_i119 !== this.tempIndex) {
              var _addDefaultVal119 = this.under_total_admin[_i119].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i119].under_total_admin_budget = _addDefaultVal119;
            }
          }
        }
      }
    },
    onBlurNumberUtilities: function onBlurNumberUtilities(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.utilities[this.tempIndex].utilities_budget !== null && this.utilities[this.tempIndex].utilities_budget !== undefined) {
          for (var i = 0; i < this.utilities.length; i++) {
            var addDefaultVal = this.utilities[i].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.utilities[i].utilities_budget = addDefaultVal;
          } //default for under_total_fuel_and_exp


          for (var _i120 = 0; _i120 < this.under_total_fuel_and_exp.length; _i120++) {
            var _addDefaultVal120 = this.under_total_fuel_and_exp[_i120].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i120].under_total_fuel_and_exp_budget = _addDefaultVal120;
          } //default for tolling


          for (var _i121 = 0; _i121 < this.tolling.length; _i121++) {
            var _addDefaultVal121 = this.tolling[_i121].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i121].tolling_budget = _addDefaultVal121;
          } //default for parking


          for (var _i122 = 0; _i122 < this.parking.length; _i122++) {
            var _addDefaultVal122 = this.parking[_i122].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i122].parking_budget = _addDefaultVal122;
          } //default for repairs


          for (var _i123 = 0; _i123 < this.repairs.length; _i123++) {
            var _addDefaultVal123 = this.repairs[_i123].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i123].repairs_budget = _addDefaultVal123;
          } //default for maintenance


          for (var _i124 = 0; _i124 < this.maintenance.length; _i124++) {
            var _addDefaultVal124 = this.maintenance[_i124].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i124].main_budget = _addDefaultVal124;
          } //default for fuels


          for (var _i125 = 0; _i125 < this.fuels.length; _i125++) {
            var _addDefaultVal125 = this.fuels[_i125].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i125].fuel_budget = _addDefaultVal125;
          } //default for salaries


          for (var _i126 = 0; _i126 < this.salaries.length; _i126++) {
            var _addDefaultVal126 = this.salaries[_i126].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i126].sal_budget = _addDefaultVal126;
          } //default for under_total_admin


          for (var _i127 = 0; _i127 < this.under_total_admin.length; _i127++) {
            var _addDefaultVal127 = this.under_total_admin[_i127].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_admin[_i127].under_total_admin_budget = _addDefaultVal127;
          }

          this.staticBudgetInput.utilities[this.tempIndex].utilities_budget = this.utilities[this.tempIndex].utilities_budget;
        } else {
          for (var _i128 = 0; _i128 < this.utilities.length; _i128++) {
            if (_i128 !== this.tempIndex) {
              var _addDefaultVal128 = this.utilities[_i128].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i128].utilities_budget = _addDefaultVal128;
            }
          } //default for under_total_fuel_and_exp


          for (var _i129 = 0; _i129 < this.under_total_fuel_and_exp.length; _i129++) {
            if (_i129 !== this.tempIndex) {
              var _addDefaultVal129 = this.under_total_fuel_and_exp[_i129].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i129].under_total_fuel_and_exp_budget = _addDefaultVal129;
            }
          } //default for tolling


          for (var _i130 = 0; _i130 < this.tolling.length; _i130++) {
            if (_i130 !== this.tempIndex) {
              var _addDefaultVal130 = this.tolling[_i130].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i130].tolling_budget = _addDefaultVal130;
            }
          } //default for parking


          for (var _i131 = 0; _i131 < this.parking.length; _i131++) {
            if (_i131 !== this.tempIndex) {
              var _addDefaultVal131 = this.parking[_i131].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i131].parking_budget = _addDefaultVal131;
            }
          } //default for repairs


          for (var _i132 = 0; _i132 < this.repairs.length; _i132++) {
            if (_i132 !== this.tempIndex) {
              var _addDefaultVal132 = this.repairs[_i132].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i132].repairs_budget = _addDefaultVal132;
            }
          } //default for maintenance


          for (var _i133 = 0; _i133 < this.maintenance.length; _i133++) {
            if (_i133 !== this.tempIndex) {
              var _addDefaultVal133 = this.maintenance[_i133].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i133].main_budget = _addDefaultVal133;
            }
          } //default for fuels


          for (var _i134 = 0; _i134 < this.fuels.length; _i134++) {
            if (_i134 !== this.tempIndex) {
              var _addDefaultVal134 = this.fuels[_i134].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i134].fuel_budget = _addDefaultVal134;
            }
          } //default for salaries


          for (var _i135 = 0; _i135 < this.salaries.length; _i135++) {
            if (_i135 !== this.tempIndex) {
              var _addDefaultVal135 = this.salaries[_i135].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i135].sal_budget = _addDefaultVal135;
            }
          } //default for under_total_admin


          for (var _i136 = 0; _i136 < this.under_total_admin.length; _i136++) {
            if (_i136 !== this.tempIndex) {
              var _addDefaultVal136 = this.under_total_admin[_i136].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i136].under_total_admin_budget = _addDefaultVal136;
            }
          }
        }
      }
    },
    onBlurNumberUnderAdminExpenses: function onBlurNumberUnderAdminExpenses(e, index) {
      // if you will remove your focus in the input
      this.visible = false;

      if (this.tempIndex !== null) {
        if (this.under_total_admin[this.tempIndex].under_total_admin_budget !== null && this.under_total_admin[this.tempIndex].under_total_admin_budget !== undefined) {
          for (var i = 0; i < this.under_total_admin.length; i++) {
            var addDefaultVal = this.under_total_admin[i].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.under_total_admin[i].under_total_admin_budget = addDefaultVal;
          } //default for utilities


          for (var _i137 = 0; _i137 < this.utilities.length; _i137++) {
            var _addDefaultVal137 = this.utilities[_i137].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.utilities[_i137].utilities_budget = _addDefaultVal137;
          } //default for under_total_fuel_and_exp


          for (var _i138 = 0; _i138 < this.under_total_fuel_and_exp.length; _i138++) {
            var _addDefaultVal138 = this.under_total_fuel_and_exp[_i138].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.under_total_fuel_and_exp[_i138].under_total_fuel_and_exp_budget = _addDefaultVal138;
          } //default for tolling


          for (var _i139 = 0; _i139 < this.tolling.length; _i139++) {
            var _addDefaultVal139 = this.tolling[_i139].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.tolling[_i139].tolling_budget = _addDefaultVal139;
          } //default for parking


          for (var _i140 = 0; _i140 < this.parking.length; _i140++) {
            var _addDefaultVal140 = this.parking[_i140].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.parking[_i140].parking_budget = _addDefaultVal140;
          } //default for repairs


          for (var _i141 = 0; _i141 < this.repairs.length; _i141++) {
            var _addDefaultVal141 = this.repairs[_i141].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.repairs[_i141].repairs_budget = _addDefaultVal141;
          } //default for maintenance


          for (var _i142 = 0; _i142 < this.maintenance.length; _i142++) {
            var _addDefaultVal142 = this.maintenance[_i142].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.maintenance[_i142].main_budget = _addDefaultVal142;
          } //default for fuels


          for (var _i143 = 0; _i143 < this.fuels.length; _i143++) {
            var _addDefaultVal143 = this.fuels[_i143].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.fuels[_i143].fuel_budget = _addDefaultVal143;
          } //default for salaries


          for (var _i144 = 0; _i144 < this.salaries.length; _i144++) {
            var _addDefaultVal144 = this.salaries[_i144].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            this.salaries[_i144].sal_budget = _addDefaultVal144;
          }

          this.staticBudgetInput.under_total_admin[this.tempIndex].under_total_admin_budget = this.under_total_admin[this.tempIndex].under_total_admin_budget;
        } else {
          for (var _i145 = 0; _i145 < this.under_total_admin.length; _i145++) {
            if (_i145 !== this.tempIndex) {
              var _addDefaultVal145 = this.under_total_admin[_i145].under_total_admin_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_admin[_i145].under_total_admin_budget = _addDefaultVal145;
            }
          } //default for utilities


          for (var _i146 = 0; _i146 < this.utilities.length; _i146++) {
            if (_i146 !== this.tempIndex) {
              var _addDefaultVal146 = this.utilities[_i146].utilities_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.utilities[_i146].utilities_budget = _addDefaultVal146;
            }
          } //default for under_total_fuel_and_exp


          for (var _i147 = 0; _i147 < this.under_total_fuel_and_exp.length; _i147++) {
            if (_i147 !== this.tempIndex) {
              var _addDefaultVal147 = this.under_total_fuel_and_exp[_i147].under_total_fuel_and_exp_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.under_total_fuel_and_exp[_i147].under_total_fuel_and_exp_budget = _addDefaultVal147;
            }
          } //default for tolling


          for (var _i148 = 0; _i148 < this.tolling.length; _i148++) {
            if (_i148 !== this.tempIndex) {
              var _addDefaultVal148 = this.tolling[_i148].tolling_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.tolling[_i148].tolling_budget = _addDefaultVal148;
            }
          } //default for parking


          for (var _i149 = 0; _i149 < this.parking.length; _i149++) {
            if (_i149 !== this.tempIndex) {
              var _addDefaultVal149 = this.parking[_i149].parking_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.parking[_i149].parking_budget = _addDefaultVal149;
            }
          } //default for repairs


          for (var _i150 = 0; _i150 < this.repairs.length; _i150++) {
            if (_i150 !== this.tempIndex) {
              var _addDefaultVal150 = this.repairs[_i150].repairs_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.repairs[_i150].repairs_budget = _addDefaultVal150;
            }
          } //default for maintenance


          for (var _i151 = 0; _i151 < this.maintenance.length; _i151++) {
            if (_i151 !== this.tempIndex) {
              var _addDefaultVal151 = this.maintenance[_i151].main_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.maintenance[_i151].main_budget = _addDefaultVal151;
            }
          } //default for fuels


          for (var _i152 = 0; _i152 < this.fuels.length; _i152++) {
            if (_i152 !== this.tempIndex) {
              var _addDefaultVal152 = this.fuels[_i152].fuel_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.fuels[_i152].fuel_budget = _addDefaultVal152;
            }
          } //default for salaries


          for (var _i153 = 0; _i153 < this.salaries.length; _i153++) {
            if (_i153 !== this.tempIndex) {
              var _addDefaultVal153 = this.salaries[_i153].sal_budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

              this.salaries[_i153].sal_budget = _addDefaultVal153;
            }
          }
        }
      }
    },
    //focus event
    onFocusTextSal: function onFocusTextSal(index) {
      // if you will focus the input
      console.log('onFocusTextSal', index);

      for (var i = 0; i < this.salaries.length; i++) {
        if (this.salaries[i].sal_budget !== null) {
          var allVal = this.salaries[i].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.salaries[i].sal_budget = allVal;
        }
      } //default for fuels


      for (var _i154 = 0; _i154 < this.fuels.length; _i154++) {
        if (this.fuels[_i154].fuel_budget !== null) {
          var _allVal = this.fuels[_i154].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i154].fuel_budget = _allVal;
        }
      } //default for maintenance


      for (var _i155 = 0; _i155 < this.maintenance.length; _i155++) {
        if (this.maintenance[_i155].main_budget !== null) {
          var _allVal2 = this.maintenance[_i155].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i155].main_budget = _allVal2;
        }
      } //default for repairs


      for (var _i156 = 0; _i156 < this.repairs.length; _i156++) {
        if (this.repairs[_i156].repairs_budget !== null) {
          var _allVal3 = this.repairs[_i156].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i156].repairs_budget = _allVal3;
        }
      } //default for parking


      for (var _i157 = 0; _i157 < this.parking.length; _i157++) {
        if (this.parking[_i157].parking_budget !== null) {
          var _allVal4 = this.parking[_i157].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i157].parking_budget = _allVal4;
        }
      } //default for tolling


      for (var _i158 = 0; _i158 < this.tolling.length; _i158++) {
        if (this.tolling[_i158].tolling_budget !== null) {
          var _allVal5 = this.tolling[_i158].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i158].tolling_budget = _allVal5;
        }
      } //default for under_total_fuel_and_exp


      for (var _i159 = 0; _i159 < this.under_total_fuel_and_exp.length; _i159++) {
        if (this.under_total_fuel_and_exp[_i159].under_total_fuel_and_exp_budget !== null) {
          var _allVal6 = this.under_total_fuel_and_exp[_i159].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i159].under_total_fuel_and_exp_budget = _allVal6;
        }
      } //default for utilities


      for (var _i160 = 0; _i160 < this.utilities.length; _i160++) {
        if (this.utilities[_i160].utilities_budget !== null) {
          var _allVal7 = this.utilities[_i160].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i160].utilities_budget = _allVal7;
        }
      } //default for under_total_admin


      for (var _i161 = 0; _i161 < this.under_total_admin.length; _i161++) {
        if (this.under_total_admin[_i161].under_total_admin_budget !== null) {
          var _allVal8 = this.under_total_admin[_i161].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i161].under_total_admin_budget = _allVal8;
        }
      }

      if (this.salaries[index].sal_budget !== null) {
        var val = this.salaries[index].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.salaries[index].sal_budget = val;
        console.log(val, 'val onFocusTextSal');
        this.staticBudgetInput.salaries[index].sal_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextFuel: function onFocusTextFuel(index) {
      // if you will focus the input
      for (var i = 0; i < this.fuels.length; i++) {
        if (this.fuels[i].fuel_budget !== null) {
          var allVal = this.fuels[i].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.fuels[i].fuel_budget = allVal;
        }
      } //default for salaries


      for (var _i162 = 0; _i162 < this.salaries.length; _i162++) {
        if (this.salaries[_i162].sal_budget !== null) {
          var _allVal9 = this.salaries[_i162].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i162].sal_budget = _allVal9;
        }
      } //default for maintenance


      for (var _i163 = 0; _i163 < this.maintenance.length; _i163++) {
        if (this.maintenance[_i163].main_budget !== null) {
          var _allVal10 = this.maintenance[_i163].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i163].main_budget = _allVal10;
        }
      } //default for repairs


      for (var _i164 = 0; _i164 < this.repairs.length; _i164++) {
        if (this.repairs[_i164].repairs_budget !== null) {
          var _allVal11 = this.repairs[_i164].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i164].repairs_budget = _allVal11;
        }
      } //default for parking


      for (var _i165 = 0; _i165 < this.parking.length; _i165++) {
        if (this.parking[_i165].parking_budget !== null) {
          var _allVal12 = this.parking[_i165].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i165].parking_budget = _allVal12;
        }
      } //default for tolling


      for (var _i166 = 0; _i166 < this.tolling.length; _i166++) {
        if (this.tolling[_i166].tolling_budget !== null) {
          var _allVal13 = this.tolling[_i166].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i166].tolling_budget = _allVal13;
        }
      } //default for under_total_fuel_and_exp


      for (var _i167 = 0; _i167 < this.under_total_fuel_and_exp.length; _i167++) {
        if (this.under_total_fuel_and_exp[_i167].under_total_fuel_and_exp_budget !== null) {
          var _allVal14 = this.under_total_fuel_and_exp[_i167].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i167].under_total_fuel_and_exp_budget = _allVal14;
        }
      } //default for utilities


      for (var _i168 = 0; _i168 < this.utilities.length; _i168++) {
        if (this.utilities[_i168].utilities_budget !== null) {
          var _allVal15 = this.utilities[_i168].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i168].utilities_budget = _allVal15;
        }
      } //default for under_total_admin


      for (var _i169 = 0; _i169 < this.under_total_admin.length; _i169++) {
        if (this.under_total_admin[_i169].under_total_admin_budget !== null) {
          var _allVal16 = this.under_total_admin[_i169].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i169].under_total_admin_budget = _allVal16;
        }
      }

      if (this.fuels[index].fuel_budget !== null) {
        var val = this.fuels[index].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.fuels[index].fuel_budget = val;
        this.staticBudgetInput.fuels[index].fuel_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextMain: function onFocusTextMain(index) {
      // if you will focus the input
      for (var i = 0; i < this.maintenance.length; i++) {
        if (this.maintenance[i].main_budget !== null) {
          var allVal = this.maintenance[i].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.maintenance[i].main_budget = allVal;
        }
      } //default for fuels


      for (var _i170 = 0; _i170 < this.fuels.length; _i170++) {
        if (this.fuels[_i170].fuel_budget !== null) {
          var _allVal17 = this.fuels[_i170].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i170].fuel_budget = _allVal17;
        }
      } //default for salaries


      for (var _i171 = 0; _i171 < this.salaries.length; _i171++) {
        if (this.salaries[_i171].sal_budget !== null) {
          var _allVal18 = this.salaries[_i171].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i171].sal_budget = _allVal18;
        }
      } //default for repairs


      for (var _i172 = 0; _i172 < this.repairs.length; _i172++) {
        if (this.repairs[_i172].repairs_budget !== null) {
          var _allVal19 = this.repairs[_i172].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i172].repairs_budget = _allVal19;
        }
      } //default for parking


      for (var _i173 = 0; _i173 < this.parking.length; _i173++) {
        if (this.parking[_i173].parking_budget !== null) {
          var _allVal20 = this.parking[_i173].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i173].parking_budget = _allVal20;
        }
      } //default for tolling


      for (var _i174 = 0; _i174 < this.tolling.length; _i174++) {
        if (this.tolling[_i174].tolling_budget !== null) {
          var _allVal21 = this.tolling[_i174].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i174].tolling_budget = _allVal21;
        }
      } //default for under_total_fuel_and_exp


      for (var _i175 = 0; _i175 < this.under_total_fuel_and_exp.length; _i175++) {
        if (this.under_total_fuel_and_exp[_i175].under_total_fuel_and_exp_budget !== null) {
          var _allVal22 = this.under_total_fuel_and_exp[_i175].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i175].under_total_fuel_and_exp_budget = _allVal22;
        }
      } //default for utilities


      for (var _i176 = 0; _i176 < this.utilities.length; _i176++) {
        if (this.utilities[_i176].utilities_budget !== null) {
          var _allVal23 = this.utilities[_i176].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i176].utilities_budget = _allVal23;
        }
      } //default for under_total_admin


      for (var _i177 = 0; _i177 < this.under_total_admin.length; _i177++) {
        if (this.under_total_admin[_i177].under_total_admin_budget !== null) {
          var _allVal24 = this.under_total_admin[_i177].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i177].under_total_admin_budget = _allVal24;
        }
      }

      if (this.maintenance[index].main_budget !== null) {
        var val = this.maintenance[index].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.maintenance[index].main_budget = val;
        this.staticBudgetInput.maintenance[index].main_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextRepair: function onFocusTextRepair(index) {
      // if you will focus the input
      for (var i = 0; i < this.repairs.length; i++) {
        if (this.repairs[i].repairs_budget !== null) {
          var allVal = this.repairs[i].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.repairs[i].repairs_budget = allVal;
        }
      } //default for maintenance


      for (var _i178 = 0; _i178 < this.maintenance.length; _i178++) {
        if (this.maintenance[_i178].main_budget !== null) {
          var _allVal25 = this.maintenance[_i178].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i178].main_budget = _allVal25;
        }
      } //default for fuels


      for (var _i179 = 0; _i179 < this.fuels.length; _i179++) {
        if (this.fuels[_i179].fuel_budget !== null) {
          var _allVal26 = this.fuels[_i179].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i179].fuel_budget = _allVal26;
        }
      } //default for salaries


      for (var _i180 = 0; _i180 < this.salaries.length; _i180++) {
        if (this.salaries[_i180].sal_budget !== null) {
          var _allVal27 = this.salaries[_i180].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i180].sal_budget = _allVal27;
        }
      } //default for parking


      for (var _i181 = 0; _i181 < this.parking.length; _i181++) {
        if (this.parking[_i181].parking_budget !== null) {
          var _allVal28 = this.parking[_i181].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i181].parking_budget = _allVal28;
        }
      } //default for tolling


      for (var _i182 = 0; _i182 < this.tolling.length; _i182++) {
        if (this.tolling[_i182].tolling_budget !== null) {
          var _allVal29 = this.tolling[_i182].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i182].tolling_budget = _allVal29;
        }
      } //default for under_total_fuel_and_exp


      for (var _i183 = 0; _i183 < this.under_total_fuel_and_exp.length; _i183++) {
        if (this.under_total_fuel_and_exp[_i183].under_total_fuel_and_exp_budget !== null) {
          var _allVal30 = this.under_total_fuel_and_exp[_i183].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i183].under_total_fuel_and_exp_budget = _allVal30;
        }
      } //default for utilities


      for (var _i184 = 0; _i184 < this.utilities.length; _i184++) {
        if (this.utilities[_i184].utilities_budget !== null) {
          var _allVal31 = this.utilities[_i184].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i184].utilities_budget = _allVal31;
        }
      } //default for under_total_admin


      for (var _i185 = 0; _i185 < this.under_total_admin.length; _i185++) {
        if (this.under_total_admin[_i185].under_total_admin_budget !== null) {
          var _allVal32 = this.under_total_admin[_i185].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i185].under_total_admin_budget = _allVal32;
        }
      }

      if (this.repairs[index].repairs_budget !== null) {
        var val = this.repairs[index].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.repairs[index].repairs_budget = val;
        this.staticBudgetInput.repairs[index].repairs_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextParking: function onFocusTextParking(index) {
      // if you will focus the input
      for (var i = 0; i < this.parking.length; i++) {
        if (this.parking[i].parking_budget !== null) {
          var allVal = this.parking[i].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.parking[i].parking_budget = allVal;
        }
      } //default for repairs


      for (var _i186 = 0; _i186 < this.repairs.length; _i186++) {
        if (this.repairs[_i186].repairs_budget !== null) {
          var _allVal33 = this.repairs[_i186].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i186].repairs_budget = _allVal33;
        }
      } //default for maintenance


      for (var _i187 = 0; _i187 < this.maintenance.length; _i187++) {
        if (this.maintenance[_i187].main_budget !== null) {
          var _allVal34 = this.maintenance[_i187].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i187].main_budget = _allVal34;
        }
      } //default for fuels


      for (var _i188 = 0; _i188 < this.fuels.length; _i188++) {
        if (this.fuels[_i188].fuel_budget !== null) {
          var _allVal35 = this.fuels[_i188].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i188].fuel_budget = _allVal35;
        }
      } //default for salaries


      for (var _i189 = 0; _i189 < this.salaries.length; _i189++) {
        if (this.salaries[_i189].sal_budget !== null) {
          var _allVal36 = this.salaries[_i189].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i189].sal_budget = _allVal36;
        }
      } //default for tolling


      for (var _i190 = 0; _i190 < this.tolling.length; _i190++) {
        if (this.tolling[_i190].tolling_budget !== null) {
          var _allVal37 = this.tolling[_i190].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i190].tolling_budget = _allVal37;
        }
      } //default for under_total_fuel_and_exp


      for (var _i191 = 0; _i191 < this.under_total_fuel_and_exp.length; _i191++) {
        if (this.under_total_fuel_and_exp[_i191].under_total_fuel_and_exp_budget !== null) {
          var _allVal38 = this.under_total_fuel_and_exp[_i191].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i191].under_total_fuel_and_exp_budget = _allVal38;
        }
      } //default for utilities


      for (var _i192 = 0; _i192 < this.utilities.length; _i192++) {
        if (this.utilities[_i192].utilities_budget !== null) {
          var _allVal39 = this.utilities[_i192].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i192].utilities_budget = _allVal39;
        }
      } //default for under_total_admin


      for (var _i193 = 0; _i193 < this.under_total_admin.length; _i193++) {
        if (this.under_total_admin[_i193].under_total_admin_budget !== null) {
          var _allVal40 = this.under_total_admin[_i193].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i193].under_total_admin_budget = _allVal40;
        }
      }

      if (this.parking[index].parking_budget !== null) {
        var val = this.parking[index].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.parking[index].parking_budget = val;
        this.staticBudgetInput.parking[index].parking_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextTolling: function onFocusTextTolling(index) {
      // if you will focus the input
      for (var i = 0; i < this.tolling.length; i++) {
        if (this.tolling[i].tolling_budget !== null) {
          var allVal = this.tolling[i].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.tolling[i].tolling_budget = allVal;
        }
      } //default for parking


      for (var _i194 = 0; _i194 < this.parking.length; _i194++) {
        if (this.parking[_i194].parking_budget !== null) {
          var _allVal41 = this.parking[_i194].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i194].parking_budget = _allVal41;
        }
      } //default for repairs


      for (var _i195 = 0; _i195 < this.repairs.length; _i195++) {
        if (this.repairs[_i195].repairs_budget !== null) {
          var _allVal42 = this.repairs[_i195].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i195].repairs_budget = _allVal42;
        }
      } //default for maintenance


      for (var _i196 = 0; _i196 < this.maintenance.length; _i196++) {
        if (this.maintenance[_i196].main_budget !== null) {
          var _allVal43 = this.maintenance[_i196].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i196].main_budget = _allVal43;
        }
      } //default for fuels


      for (var _i197 = 0; _i197 < this.fuels.length; _i197++) {
        if (this.fuels[_i197].fuel_budget !== null) {
          var _allVal44 = this.fuels[_i197].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i197].fuel_budget = _allVal44;
        }
      } //default for salaries


      for (var _i198 = 0; _i198 < this.salaries.length; _i198++) {
        if (this.salaries[_i198].sal_budget !== null) {
          var _allVal45 = this.salaries[_i198].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i198].sal_budget = _allVal45;
        }
      } //default for under_total_fuel_and_exp


      for (var _i199 = 0; _i199 < this.under_total_fuel_and_exp.length; _i199++) {
        if (this.under_total_fuel_and_exp[_i199].under_total_fuel_and_exp_budget !== null) {
          var _allVal46 = this.under_total_fuel_and_exp[_i199].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i199].under_total_fuel_and_exp_budget = _allVal46;
        }
      } //default for utilities


      for (var _i200 = 0; _i200 < this.utilities.length; _i200++) {
        if (this.utilities[_i200].utilities_budget !== null) {
          var _allVal47 = this.utilities[_i200].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i200].utilities_budget = _allVal47;
        }
      } //default for under_total_admin


      for (var _i201 = 0; _i201 < this.under_total_admin.length; _i201++) {
        if (this.under_total_admin[_i201].under_total_admin_budget !== null) {
          var _allVal48 = this.under_total_admin[_i201].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i201].under_total_admin_budget = _allVal48;
        }
      }

      if (this.tolling[index].tolling_budget !== null) {
        var val = this.tolling[index].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.tolling[index].tolling_budget = val;
        this.staticBudgetInput.tolling[index].tolling_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextUnderTotalFuelAndExpBudget: function onFocusTextUnderTotalFuelAndExpBudget(index) {
      // if you will focus the input
      for (var i = 0; i < this.under_total_fuel_and_exp.length; i++) {
        if (this.under_total_fuel_and_exp[i].under_total_fuel_and_exp_budget !== null) {
          var allVal = this.under_total_fuel_and_exp[i].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.under_total_fuel_and_exp[i].under_total_fuel_and_exp_budget = allVal;
        }
      } //default for tolling


      for (var _i202 = 0; _i202 < this.tolling.length; _i202++) {
        if (this.tolling[_i202].tolling_budget !== null) {
          var _allVal49 = this.tolling[_i202].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i202].tolling_budget = _allVal49;
        }
      } //default for parking


      for (var _i203 = 0; _i203 < this.parking.length; _i203++) {
        if (this.parking[_i203].parking_budget !== null) {
          var _allVal50 = this.parking[_i203].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i203].parking_budget = _allVal50;
        }
      } //default for repairs


      for (var _i204 = 0; _i204 < this.repairs.length; _i204++) {
        if (this.repairs[_i204].repairs_budget !== null) {
          var _allVal51 = this.repairs[_i204].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i204].repairs_budget = _allVal51;
        }
      } //default for maintenance


      for (var _i205 = 0; _i205 < this.maintenance.length; _i205++) {
        if (this.maintenance[_i205].main_budget !== null) {
          var _allVal52 = this.maintenance[_i205].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i205].main_budget = _allVal52;
        }
      } //default for fuels


      for (var _i206 = 0; _i206 < this.fuels.length; _i206++) {
        if (this.fuels[_i206].fuel_budget !== null) {
          var _allVal53 = this.fuels[_i206].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i206].fuel_budget = _allVal53;
        }
      } //default for salaries


      for (var _i207 = 0; _i207 < this.salaries.length; _i207++) {
        if (this.salaries[_i207].sal_budget !== null) {
          var _allVal54 = this.salaries[_i207].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i207].sal_budget = _allVal54;
        }
      } //default for utilities


      for (var _i208 = 0; _i208 < this.utilities.length; _i208++) {
        if (this.utilities[_i208].utilities_budget !== null) {
          var _allVal55 = this.utilities[_i208].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i208].utilities_budget = _allVal55;
        }
      } //default for under_total_admin


      for (var _i209 = 0; _i209 < this.under_total_admin.length; _i209++) {
        if (this.under_total_admin[_i209].under_total_admin_budget !== null) {
          var _allVal56 = this.under_total_admin[_i209].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i209].under_total_admin_budget = _allVal56;
        }
      }

      if (this.under_total_fuel_and_exp[index].under_total_fuel_and_exp_budget !== null) {
        var val = this.under_total_fuel_and_exp[index].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.under_total_fuel_and_exp[index].under_total_fuel_and_exp_budget = val;
        this.staticBudgetInput.under_total_fuel_and_exp[index].under_total_fuel_and_exp_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextUtilities: function onFocusTextUtilities(index) {
      // if you will focus the input
      for (var i = 0; i < this.utilities.length; i++) {
        if (this.utilities[i].utilities_budget !== null) {
          var allVal = this.utilities[i].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.utilities[i].utilities_budget = allVal;
        }
      } //default for under_total_fuel_and_exp


      for (var _i210 = 0; _i210 < this.under_total_fuel_and_exp.length; _i210++) {
        if (this.under_total_fuel_and_exp[_i210].under_total_fuel_and_exp_budget !== null) {
          var _allVal57 = this.under_total_fuel_and_exp[_i210].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i210].under_total_fuel_and_exp_budget = _allVal57;
        }
      } //default for tolling


      for (var _i211 = 0; _i211 < this.tolling.length; _i211++) {
        if (this.tolling[_i211].tolling_budget !== null) {
          var _allVal58 = this.tolling[_i211].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i211].tolling_budget = _allVal58;
        }
      } //default for parking


      for (var _i212 = 0; _i212 < this.parking.length; _i212++) {
        if (this.parking[_i212].parking_budget !== null) {
          var _allVal59 = this.parking[_i212].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i212].parking_budget = _allVal59;
        }
      } //default for repairs


      for (var _i213 = 0; _i213 < this.repairs.length; _i213++) {
        if (this.repairs[_i213].repairs_budget !== null) {
          var _allVal60 = this.repairs[_i213].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i213].repairs_budget = _allVal60;
        }
      } //default for maintenance


      for (var _i214 = 0; _i214 < this.maintenance.length; _i214++) {
        if (this.maintenance[_i214].main_budget !== null) {
          var _allVal61 = this.maintenance[_i214].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i214].main_budget = _allVal61;
        }
      } //default for fuels


      for (var _i215 = 0; _i215 < this.fuels.length; _i215++) {
        if (this.fuels[_i215].fuel_budget !== null) {
          var _allVal62 = this.fuels[_i215].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i215].fuel_budget = _allVal62;
        }
      } //default for salaries


      for (var _i216 = 0; _i216 < this.salaries.length; _i216++) {
        if (this.salaries[_i216].sal_budget !== null) {
          var _allVal63 = this.salaries[_i216].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i216].sal_budget = _allVal63;
        }
      } //default for under_total_admin


      for (var _i217 = 0; _i217 < this.under_total_admin.length; _i217++) {
        if (this.under_total_admin[_i217].under_total_admin_budget !== null) {
          var _allVal64 = this.under_total_admin[_i217].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_admin[_i217].under_total_admin_budget = _allVal64;
        }
      }

      if (this.utilities[index].utilities_budget !== null) {
        var val = this.utilities[index].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.utilities[index].utilities_budget = val;
        this.staticBudgetInput.utilities[index].utilities_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    onFocusTextUnderAdminExpenses: function onFocusTextUnderAdminExpenses(index) {
      // if you will focus the input
      for (var i = 0; i < this.under_total_admin.length; i++) {
        if (this.under_total_admin[i].under_total_admin_budget !== null) {
          var allVal = this.under_total_admin[i].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
          this.under_total_admin[i].under_total_admin_budget = allVal;
        }
      } //default for utilities


      for (var _i218 = 0; _i218 < this.utilities.length; _i218++) {
        if (this.utilities[_i218].utilities_budget !== null) {
          var _allVal65 = this.utilities[_i218].utilities_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.utilities[_i218].utilities_budget = _allVal65;
        }
      } //default for under_total_fuel_and_exp


      for (var _i219 = 0; _i219 < this.under_total_fuel_and_exp.length; _i219++) {
        if (this.under_total_fuel_and_exp[_i219].under_total_fuel_and_exp_budget !== null) {
          var _allVal66 = this.under_total_fuel_and_exp[_i219].under_total_fuel_and_exp_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.under_total_fuel_and_exp[_i219].under_total_fuel_and_exp_budget = _allVal66;
        }
      } //default for tolling


      for (var _i220 = 0; _i220 < this.tolling.length; _i220++) {
        if (this.tolling[_i220].tolling_budget !== null) {
          var _allVal67 = this.tolling[_i220].tolling_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.tolling[_i220].tolling_budget = _allVal67;
        }
      } //default for parking


      for (var _i221 = 0; _i221 < this.parking.length; _i221++) {
        if (this.parking[_i221].parking_budget !== null) {
          var _allVal68 = this.parking[_i221].parking_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.parking[_i221].parking_budget = _allVal68;
        }
      } //default for repairs


      for (var _i222 = 0; _i222 < this.repairs.length; _i222++) {
        if (this.repairs[_i222].repairs_budget !== null) {
          var _allVal69 = this.repairs[_i222].repairs_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.repairs[_i222].repairs_budget = _allVal69;
        }
      } //default for maintenance


      for (var _i223 = 0; _i223 < this.maintenance.length; _i223++) {
        if (this.maintenance[_i223].main_budget !== null) {
          var _allVal70 = this.maintenance[_i223].main_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.maintenance[_i223].main_budget = _allVal70;
        }
      } //default for fuels


      for (var _i224 = 0; _i224 < this.fuels.length; _i224++) {
        if (this.fuels[_i224].fuel_budget !== null) {
          var _allVal71 = this.fuels[_i224].fuel_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.fuels[_i224].fuel_budget = _allVal71;
        }
      } //default for salaries


      for (var _i225 = 0; _i225 < this.salaries.length; _i225++) {
        if (this.salaries[_i225].sal_budget !== null) {
          var _allVal72 = this.salaries[_i225].sal_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");

          this.salaries[_i225].sal_budget = _allVal72;
        }
      }

      if (this.under_total_admin[index].under_total_admin_budget !== null) {
        var val = this.under_total_admin[index].under_total_admin_budget.replace(/^[, ]+|[, ]+$|[, ]+/g, "");
        this.temp = val;
        this.under_total_admin[index].under_total_admin_budget = val;
        this.staticBudgetInput.under_total_admin[index].under_total_admin_budget = val;
      }

      this.tempIndex = index;
      this.visible = true;
    },
    thousandSeparator: function thousandSeparator(budget) {
      if (budget !== '' || budget !== undefined || budget !== 0 || budget !== '0' || budget !== null) {
        var bud = budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        console.log(bud, 'thousandSeparator');
        this.salaries[this.tempIndex].sal_budget = bud;
        return bud;
      } else {
        return budget;
      }
    },
    thousandRemover: function thousandRemover(budget) {
      if (budget !== '' || budget !== undefined || budget !== 0 || budget !== '0' || budget !== null) {
        var bud = budget.toString().replace(",", "");
        console.log(bud, 'thousandRemover');
        return bud;
      } else {
        return budget;
      }
    },
    editData: function editData() {
      if (this.storeCount === 1) {
        this.storeCompany = this.staticBudgetEdit.company;
        this.storeCount = 0; // return;
      }

      var doneFuel = 0;
      var doneSalaries = 0;
      var doneMaintenance = 0;
      var doneRepairs = 0;
      var doneParking = 0;
      var doneTolling = 0;
      var doneUtilities = 0;
      var doneUnder_total_admin = 0;
      var doneUnder_total_fuel_and_exp = 0;
      this.salaries = [];
      this.fuels = [];
      this.maintenance = [];
      this.repairs = [];
      this.parking = [];
      this.tolling = [];
      this.utilities = [];
      this.under_total_admin = [];
      this.under_total_fuel_and_exp = [];

      for (var x = 0; x < this.staticBudgetEdit.fuels.length; x++) {
        this.fuels.push({
          'fuel_name': this.staticBudgetEdit.fuels[x].fuel_name,
          'fuel_budget': this.staticBudgetEdit.fuels[x].fuel_budget
        });

        if (this.fuels.length === this.staticBudgetEdit.fuels.length) {
          doneFuel = 1;
          break;
        }
      }

      console.log(this.staticBudgetEdit.salaries, 'this.staticBudgetEdit.salaries check');

      for (var _x = 0; _x < this.staticBudgetEdit.salaries.length; _x++) {
        this.salaries.push({
          'sal_name': this.staticBudgetEdit.salaries[_x].sal_name,
          'sal_budget': this.staticBudgetEdit.salaries[_x].sal_budget
        });

        if (this.salaries.length === this.staticBudgetEdit.salaries.length) {
          doneSalaries = 1;
          break;
        }
      }

      for (var _x2 = 0; _x2 < this.staticBudgetEdit.maintenance.length; _x2++) {
        this.maintenance.push({
          'main_name': this.staticBudgetEdit.maintenance[_x2].main_name,
          'main_budget': this.staticBudgetEdit.maintenance[_x2].main_budget
        });

        if (this.maintenance.length === this.staticBudgetEdit.maintenance.length) {
          doneMaintenance = 1;
          break;
        }
      }

      for (var _x3 = 0; _x3 < this.staticBudgetEdit.repairs.length; _x3++) {
        this.repairs.push({
          'repairs_name': this.staticBudgetEdit.repairs[_x3].repairs_name,
          'repairs_budget': this.staticBudgetEdit.repairs[_x3].repairs_budget
        });

        if (this.repairs.length === this.staticBudgetEdit.repairs.length) {
          doneRepairs = 1;
          break;
        }
      }

      for (var _x4 = 0; _x4 < this.staticBudgetEdit.parking.length; _x4++) {
        this.parking.push({
          'parking_name': this.staticBudgetEdit.parking[_x4].parking_name,
          'parking_budget': this.staticBudgetEdit.parking[_x4].parking_budget
        });

        if (this.parking.length === this.staticBudgetEdit.parking.length) {
          doneParking = 1;
          break;
        }
      }

      for (var _x5 = 0; _x5 < this.staticBudgetEdit.tolling.length; _x5++) {
        this.tolling.push({
          'tolling_name': this.staticBudgetEdit.tolling[_x5].tolling_name,
          'tolling_budget': this.staticBudgetEdit.tolling[_x5].tolling_budget
        });

        if (this.tolling.length === this.staticBudgetEdit.tolling.length) {
          doneTolling = 1;
          break;
        }
      }

      for (var _x6 = 0; _x6 < this.staticBudgetEdit.utilities.length; _x6++) {
        this.utilities.push({
          'utilities_name': this.staticBudgetEdit.utilities[_x6].utilities_name,
          'utilities_budget': this.staticBudgetEdit.utilities[_x6].utilities_budget
        });

        if (this.utilities.length === this.staticBudgetEdit.utilities.length) {
          doneUtilities = 1;
          break;
        }
      }

      for (var _x7 = 0; _x7 < this.staticBudgetEdit.under_total_admin.length; _x7++) {
        this.under_total_admin.push({
          'under_total_admin_name': this.staticBudgetEdit.under_total_admin[_x7].under_total_admin_name,
          'under_total_admin_budget': this.staticBudgetEdit.under_total_admin[_x7].under_total_admin_budget
        });

        if (this.under_total_admin.length === this.staticBudgetEdit.under_total_admin.length) {
          doneUnder_total_admin = 1;
          break;
        }
      }

      console.log(this.staticBudgetEdit.under_total_fuel_and_exp, 'this.staticBudgetEdit.under_total_fuel_and_exp');

      for (var _x8 = 0; _x8 < this.staticBudgetEdit.under_total_fuel_and_exp.length; _x8++) {
        this.under_total_fuel_and_exp.push({
          'under_total_fuel_and_exp_name': this.staticBudgetEdit.under_total_fuel_and_exp[_x8].under_total_fuel_and_exp_name,
          'under_total_fuel_and_exp_budget': this.staticBudgetEdit.under_total_fuel_and_exp[_x8].under_total_fuel_and_exp_budget
        });

        if (this.under_total_fuel_and_exp.length === this.staticBudgetEdit.under_total_fuel_and_exp.length) {
          doneUnder_total_fuel_and_exp = 1;
          break;
        }
      }

      this.staticBudget.company = this.staticBudgetEdit.company;
      this.staticBudget.totalSalaries = this.staticBudgetEdit.totalSalaries;
      this.staticBudget.totalFuelAndSellingExp = this.staticBudgetEdit.totalFuelAndSellingExp;
      this.staticBudget.totalAdminExpenses = this.staticBudgetEdit.totalAdminExpenses;
      this.staticBudget.totalExpenses = this.staticBudgetEdit.totalExpenses;

      if (doneFuel === 1 || doneSalaries === 1 || doneMaintenance === 1 || doneRepairs === 1 || doneParking === 1 || doneTolling === 1 || doneUtilities === 1 || doneUnder_total_admin === 1 || doneUnder_total_fuel_and_exp === 1) {
        this.doneFetch = false;
      }
    },
    //Add function
    addRowSalaries: function addRowSalaries() {
      var elem = document.createElement('tr');
      this.salaries.push({
        'sal_name': null,
        'sal_budget': null
      });
      console.log(this.salaries.length, 'addRowSalaries');
      this.onFocusTextSal(this.salaries.length - 1); //if edit is true

      this.staticBudgetInput.salaries = this.salaries;
    },
    removeElementSalaries: function removeElementSalaries(index) {
      this.salaries.splice(index, 1);
      this.staticBudgetInput.salaries = this.salaries;
    },
    addRowFuels: function addRowFuels() {
      var elem = document.createElement('tr');
      this.fuels.push({
        'fuel_name': null,
        'fuel_budget': null
      });
      this.onFocusTextFuel(this.fuels.length - 1);
      this.staticBudgetInput.fuels = this.fuels;
    },
    removeElementFuels: function removeElementFuels(index) {
      this.fuels.splice(index, 1);
      this.staticBudgetInput.fuels = this.fuels;
    },
    addRowMaintenace: function addRowMaintenace() {
      var elem = document.createElement('tr');
      this.maintenance.push({
        'main_name': null,
        'main_budget': null
      });
      this.staticBudgetInput.maintenance = this.maintenance;
    },
    removeElementMaintenace: function removeElementMaintenace(index) {
      this.maintenance.splice(index, 1);
      this.staticBudgetInput.maintenance = this.maintenance;
    },
    addRowRepairs: function addRowRepairs() {
      var elem = document.createElement('tr');
      this.repairs.push({
        'repairs_name': null,
        'repairs_budget': null
      });
      this.staticBudgetInput.repairs = this.repairs;
    },
    removeElementRepairs: function removeElementRepairs(index) {
      this.repairs.splice(index, 1);
      this.staticBudgetInput.repairs = this.repairs;
    },
    addRowParking: function addRowParking() {
      var elem = document.createElement('tr');
      this.parking.push({
        'parking_name': null,
        'parking_budget': null
      });
      this.staticBudgetInput.parking = this.parking;
    },
    removeElementParking: function removeElementParking(index) {
      this.parking.splice(index, 1);
      this.staticBudgetInput.parking = this.parking;
    },
    addRowTolling: function addRowTolling() {
      var elem = document.createElement('tr');
      this.tolling.push({
        'tolling_name': null,
        'tolling_budget': null
      });
      this.staticBudgetInput.tolling = this.tolling;
    },
    removeElementTolling: function removeElementTolling(index) {
      this.tolling.splice(index, 1);
      this.staticBudgetInput.tolling = this.tolling;
    },
    addRowUtilities: function addRowUtilities() {
      var elem = document.createElement('tr');
      this.utilities.push({
        'utilities_name': null,
        'utilities_budget': null
      });
      this.staticBudgetInput.utilities = this.utilities;
    },
    removeElementUtilities: function removeElementUtilities(index) {
      this.utilities.splice(index, 1);
      this.staticBudgetInput.utilities = this.utilities;
    },
    addRowTotalAdmin: function addRowTotalAdmin() {
      var elem = document.createElement('tr');
      this.under_total_admin.push({
        'under_total_admin_name': null,
        'under_total_admin_budget': null
      });
      this.staticBudgetInput.under_total_admin = this.under_total_admin;
    },
    removeElementTotalAdmin: function removeElementTotalAdmin(index) {
      this.under_total_admin.splice(index, 1);
      this.staticBudgetInput.under_total_admin = this.under_total_admin;
    },
    addRowTotalFuelAndExp: function addRowTotalFuelAndExp() {
      var elem = document.createElement('tr');
      this.under_total_fuel_and_exp.push({
        'under_total_fuel_and_exp_name': null,
        'under_total_fuel_and_exp_budget': null
      });
      this.staticBudgetInput.under_total_fuel_and_exp = this.under_total_fuel_and_exp;
    },
    removeElementTotalFuelAndExp: function removeElementTotalFuelAndExp(index) {
      this.under_total_fuel_and_exp.splice(index, 1);
      this.staticBudgetInput.under_total_fuel_and_exp = this.under_total_fuel_and_exp;
    },
    formatNumber: function formatNumber(number) {
      return number === '' ? '' : parseFloat(Math.abs(number)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticCreate.vue?vue&type=template&id=55a49322&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticCreate.vue?vue&type=template&id=55a49322&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "card" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "form",
        {
          directives: [
            {
              name: "promise-btn",
              rawName: "v-promise-btn",
              value: { action: "submit" },
              expression: "{action: 'submit'}"
            }
          ],
          staticClass: "form-horizontal",
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.submitStaticBudget($event)
            },
            keydown: function($event) {
              return _vm.staticBudget.onKeydown($event)
            }
          }
        },
        [
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("StaticBudgetForm", {
                attrs: { staticBudget: _vm.staticBudget, editStatic: false }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "card-footer" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-fill btn-success",
                attrs: { type: "submit", disabled: _vm.submitBtn }
              },
              [_vm._v("Submit")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-fill btn-danger",
                attrs: { type: "button" },
                on: { click: _vm.cancelCreate }
              },
              [_vm._v("Cancel")]
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header card-header-primary card-header-icon" },
      [
        _c("h4", { staticClass: "card-title" }, [
          _vm._v("Create Account Budget")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "card" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "form",
        {
          directives: [
            {
              name: "promise-btn",
              rawName: "v-promise-btn",
              value: { action: "submit" },
              expression: "{action: 'submit'}"
            }
          ],
          staticClass: "form-horizontal",
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.update($event)
            },
            keydown: function($event) {
              return _vm.staticBudget.onKeydown($event)
            }
          }
        },
        [
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("StaticBudgetForm", {
                attrs: { staticBudget: _vm.staticBudget, editStatic: true }
              })
            ],
            1
          ),
          _vm._v(" "),
          !_vm.user.roles.includes("super_administrator")
            ? _c("div", { staticClass: "card-footer " }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-fill btn-success",
                    attrs: { type: "submit", disabled: _vm.submitBtn }
                  },
                  [_vm._v("Submit")]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-fill btn-danger",
                    attrs: { type: "button" },
                    on: { click: _vm.cancelEdit }
                  },
                  [_vm._v("Cancel")]
                )
              ])
            : _vm._e()
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "card-header card-header-primary card-header-icon" },
      [
        _c("h4", { staticClass: "card-title" }, [
          _vm._v("Create Account Budget Edit")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [_c("router-view")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row" },
    [
      _c("div", { staticClass: "col-md-12 col-sm-12 col-lg-6 " }, [
        _c("div", { staticClass: "card" }, [
          _c(
            "div",
            { staticClass: "card-header card-header-primary card-header-icon" },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("h4", { staticClass: "card-title" }, [
                _vm._v("Company Static Budget List")
              ]),
              _vm._v(" "),
              !_vm.showCreate && _vm.$can("static_budget_create")
                ? _c(
                    "button",
                    {
                      staticClass: "btn btn-primary float-right btn-sm",
                      on: {
                        click: function($event) {
                          return _vm.showCreateView()
                        }
                      }
                    },
                    [_vm._v("CREATE ACCOUNT BUDGET\n                ")]
                  )
                : _vm._e()
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("div", { staticClass: "toolbar" }),
              _vm._v(" "),
              _c("DataTable", {
                attrs: {
                  dt_tableHeaders: _vm.tableHeaders,
                  dt_currentSort: _vm.tableSortBy,
                  dt_currentSortDir: _vm.tableSortDir,
                  dt_defaultEntries: _vm.tableDefaultEntries,
                  dt_tableClassHeaders: "thead-dark",
                  dt_Search: _vm.search,
                  dt_classTable: "table table-bordered",
                  dt_TableWidth: "100"
                },
                on: {
                  sortColumn: _vm.sortByTable,
                  perPageEntry: _vm.paginateTable,
                  searchItem: _vm.searchData
                },
                scopedSlots: _vm._u(
                  [
                    {
                      key: "tableData",
                      fn: function() {
                        return _vm._l(_vm.static_budgets.data, function(
                          data,
                          index
                        ) {
                          return _c("tr", { key: data.id }, [
                            _c("td", [_vm._v(_vm._s(data.company))]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(data.created_at))]),
                            _vm._v(" "),
                            _c("td", [
                              _vm.$can("static_budget_edit")
                                ? _c(
                                    "button",
                                    {
                                      staticClass: "btn btn btn-link btn-info",
                                      on: {
                                        click: function($event) {
                                          return _vm.edit(data)
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "i",
                                        { staticClass: "material-icons" },
                                        [_vm._v("edit")]
                                      )
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.$can("static_budget_delete")
                                ? _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn btn-link btn-danger",
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteStaticBudget(
                                            data.id,
                                            index
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "i",
                                        { staticClass: "material-icons" },
                                        [_vm._v("delete")]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ])
                          ])
                        })
                      },
                      proxy: true
                    },
                    _vm.static_budgets
                      ? {
                          key: "perPage",
                          fn: function() {
                            return [
                              _c("p", { staticClass: "float-left" }, [
                                _vm._v(
                                  "Showing " +
                                    _vm._s(_vm.static_budgets.from || 0) +
                                    " to " +
                                    _vm._s(_vm.static_budgets.to || 0) +
                                    "\n                            of\n                            " +
                                    _vm._s(_vm.static_budgets.total || 0) +
                                    " entries"
                                )
                              ])
                            ]
                          },
                          proxy: true
                        }
                      : null,
                    {
                      key: "pagination",
                      fn: function() {
                        return [
                          _c(
                            "pagination",
                            {
                              attrs: {
                                data: _vm.static_budgets,
                                limit: 2,
                                alight: "right"
                              },
                              on: { "pagination-change-page": _vm.index }
                            },
                            [
                              _c(
                                "span",
                                {
                                  attrs: { slot: "prev-nav" },
                                  slot: "prev-nav"
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-angle-double-left",
                                    attrs: { "aria-hidden": "true" }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  attrs: { slot: "next-nav" },
                                  slot: "next-nav"
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-angle-double-right",
                                    attrs: { "aria-hidden": "true" }
                                  })
                                ]
                              )
                            ]
                          )
                        ]
                      },
                      proxy: true
                    }
                  ],
                  null,
                  true
                )
              })
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.showCreate,
                expression: "showCreate"
              }
            ],
            staticClass: "col-md-12 col-sm-12 col-lg-6 "
          },
          [_c("StaticBudgetCreate", { on: { cancelCreate: _vm.closeCreate } })],
          1
        )
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.showEdit,
                expression: "showEdit"
              }
            ],
            ref: "editFormRef",
            staticClass: "col-md-12 col-sm-12 col-lg-6 "
          },
          [_c("StaticBudgetEdit", { on: { cancelEdit: _vm.closeEdit } })],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-icon" }, [
      _c("i", { staticClass: "material-icons" }, [_vm._v("assignment")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/_Form.vue?vue&type=template&id=4a021e7b&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/static_budget/_Form.vue?vue&type=template&id=4a021e7b&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "col-sm-12" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "table-responsive" }, [
          _c(
            "table",
            {
              staticClass: "table align-items-center table-flush table-hover",
              attrs: { id: "itemsTable" }
            },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("tbody", [
                _c("tr", [
                  _c("td", { attrs: { colspan: "3" } }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", { staticClass: "col-form-label" }, [
                          _vm._v("Company Name")
                        ]),
                        _vm._v(" "),
                        _c(
                          "b-form-select",
                          {
                            class: {
                              "is-invalid": _vm.staticBudgetInput.errors.has(
                                "company"
                              )
                            },
                            attrs: { disabled: _vm.editStatic },
                            model: {
                              value: _vm.staticBudgetInput.company,
                              callback: function($$v) {
                                _vm.$set(_vm.staticBudgetInput, "company", $$v)
                              },
                              expression: "staticBudgetInput.company"
                            }
                          },
                          _vm._l(_vm.plantBranches, function(plant, index) {
                            return _c(
                              "b-form-select-option",
                              { key: index, attrs: { value: plant.EKORG } },
                              [
                                _vm._v(
                                  _vm._s(plant.EKORG) +
                                    " - " +
                                    _vm._s(plant.NAME1) +
                                    "\n                                    "
                                )
                              ]
                            )
                          }),
                          1
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          attrs: {
                            form: _vm.staticBudgetInput,
                            field: "company"
                          }
                        })
                      ],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _c("b", [_vm._v("TOTAL SALARIES")]),
                    _vm._v("  \n                            "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowSalaries }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }, [
                    _vm._v(
                      _vm._s(
                        this.formatNumber(_vm.staticBudgetInput.totalSalaries)
                      )
                    )
                  ]),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.salaries, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c("label", { staticClass: "col-form-label" }, [
                            _vm._v("Salaries Account Name")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.sal_name,
                                expression: "staticBud.sal_name"
                              }
                            ],
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.staticBudgetInput.errors.has(
                                "sal_name"
                              )
                            },
                            attrs: { type: "text" },
                            domProps: { value: staticBud.sal_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "sal_name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("has-error", {
                            attrs: {
                              form: _vm.staticBudgetInput,
                              field: "sal_name"
                            }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("Salaries Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.sal_budget,
                                expression: "staticBud.sal_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.sal_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberSal(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "sal_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.sal_budget,
                                expression: "staticBud.sal_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.sal_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextSal(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "sal_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementSalaries(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _c("b", [_vm._v("TOTAL FUEL AND SELLING EXPENSES  ")]),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowTotalFuelAndExp }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }, [
                    _vm._v(
                      "\n                            " +
                        _vm._s(
                          this.formatNumber(
                            _vm.staticBudgetInput.totalFuelAndSellingExp
                          )
                        ) +
                        "\n                        "
                    )
                  ]),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _vm._v("FUEL  "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowFuels }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.fuels, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("FUEL Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.fuel_name,
                            expression: "staticBud.fuel_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.fuel_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "fuel_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("FUEL Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.fuel_budget,
                                expression: "staticBud.fuel_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.fuel_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberFuel(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "fuel_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.fuel_budget,
                                expression: "staticBud.fuel_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.fuel_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextFuel(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "fuel_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementFuels(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _vm._v("MAINTENANCE  "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowMaintenace }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.maintenance, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("MAINTENANCE Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.main_name,
                            expression: "staticBud.main_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.main_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "main_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("MAINTENANCE Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.main_budget,
                                expression: "staticBud.main_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.main_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberMain(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "main_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.main_budget,
                                expression: "staticBud.main_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.main_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextMain(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "main_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementMaintenace(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _vm._v("REPAIRS  "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowRepairs }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.repairs, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("REPAIRS Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.repairs_name,
                            expression: "staticBud.repairs_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.repairs_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "repairs_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("REPAIRS Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.repairs_budget,
                                expression: "staticBud.repairs_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.repairs_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberRepair(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "repairs_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.repairs_budget,
                                expression: "staticBud.repairs_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.repairs_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextRepair(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "repairs_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementRepairs(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _vm._v("PARKING  "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowParking }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.parking, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("PARKING Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.parking_name,
                            expression: "staticBud.parking_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.parking_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "parking_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("PARKING Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.parking_budget,
                                expression: "staticBud.parking_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.parking_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberParking(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "parking_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.parking_budget,
                                expression: "staticBud.parking_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.parking_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextParking(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "parking_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementParking(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _vm._v("TOLLING  "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowTolling }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.tolling, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("TOLLING Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.tolling_name,
                            expression: "staticBud.tolling_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.tolling_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "tolling_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("TOLLING Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.tolling_budget,
                                expression: "staticBud.tolling_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.tolling_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberTolling(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "tolling_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.tolling_budget,
                                expression: "staticBud.tolling_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.tolling_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextTolling(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "tolling_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementTolling(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.under_total_fuel_and_exp, function(
                  staticBud,
                  index
                ) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("FUEL AND SELLING EXPENSES Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.under_total_fuel_and_exp_name,
                            expression:
                              "staticBud.under_total_fuel_and_exp_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: {
                          value: staticBud.under_total_fuel_and_exp_name
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "under_total_fuel_and_exp_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("FUEL AND SELLING EXPENSES Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value:
                                  staticBud.under_total_fuel_and_exp_budget,
                                expression:
                                  "staticBud.under_total_fuel_and_exp_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: {
                              value: staticBud.under_total_fuel_and_exp_budget
                            },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberUnderTotalFuelAndExpBudget(
                                  index
                                )
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "under_total_fuel_and_exp_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value:
                                  staticBud.under_total_fuel_and_exp_budget,
                                expression:
                                  "staticBud.under_total_fuel_and_exp_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: {
                              value: staticBud.under_total_fuel_and_exp_budget
                            },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextUnderTotalFuelAndExpBudget(
                                  index
                                )
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "under_total_fuel_and_exp_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementTotalFuelAndExp(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _c("b", [_vm._v("TOTAL ADMINISTRATIVE EXPENSES  ")]),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowTotalAdmin }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }, [
                    _vm._v(
                      "\n                            " +
                        _vm._s(
                          this.formatNumber(
                            _vm.staticBudgetInput.totalAdminExpenses
                          )
                        ) +
                        "\n                        "
                    )
                  ]),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _c("th", [
                    _vm._v("UTILITIES  "),
                    _c(
                      "i",
                      {
                        staticClass: "material-icons",
                        staticStyle: { color: "#47a44b" },
                        on: { click: _vm.addRowUtilities }
                      },
                      [_vm._v("post_add")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.utilities, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("UTILITIES Account Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.utilities_name,
                            expression: "staticBud.utilities_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.utilities_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "utilities_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("UTILITIES Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.utilities_budget,
                                expression: "staticBud.utilities_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: { value: staticBud.utilities_budget },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberUtilities(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "utilities_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.utilities_budget,
                                expression: "staticBud.utilities_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: { value: staticBud.utilities_budget },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextUtilities(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "utilities_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementUtilities(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.under_total_admin, function(staticBud, index) {
                  return _c("tr", [
                    _c("td", [
                      _vm._m(2, true),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: staticBud.under_total_admin_name,
                            expression: "staticBud.under_total_admin_name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: staticBud.under_total_admin_name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              staticBud,
                              "under_total_admin_name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("label", { staticClass: "col-form-label" }, [
                        _vm._v("ADMINISTRATIVE EXPENSES Budget Amount")
                      ]),
                      _vm._v(" "),
                      _vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.under_total_admin_budget,
                                expression: "staticBud.under_total_admin_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "number", step: ".01" },
                            domProps: {
                              value: staticBud.under_total_admin_budget
                            },
                            on: {
                              blur: function($event) {
                                return _vm.onBlurNumberUnderAdminExpenses(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "under_total_admin_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.visible
                        ? _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: staticBud.under_total_admin_budget,
                                expression: "staticBud.under_total_admin_budget"
                              }
                            ],
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: { type: "text", step: ".01" },
                            domProps: {
                              value: staticBud.under_total_admin_budget
                            },
                            on: {
                              focus: function($event) {
                                return _vm.onFocusTextUnderAdminExpenses(index)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  staticBud,
                                  "under_total_admin_budget",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "a",
                        {
                          staticStyle: { cursor: "pointer", color: "#E10000" },
                          on: {
                            click: function($event) {
                              return _vm.removeElementTotalAdmin(index)
                            }
                          }
                        },
                        [_c("b", [_vm._v("Remove")])]
                      )
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c("thead", { staticClass: "thead-light" }, [
                _c("tr", [
                  _vm._m(3),
                  _vm._v(" "),
                  _c("th", { staticStyle: { "text-align": "right" } }, [
                    _vm._v(
                      _vm._s(
                        this.formatNumber(_vm.staticBudgetInput.totalExpenses)
                      )
                    )
                  ]),
                  _vm._v(" "),
                  _c("th")
                ])
              ])
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "thead-light" }, [
      _c("tr", [
        _c("th", [_vm._v("COMPANY")]),
        _vm._v(" "),
        _c("th"),
        _vm._v(" "),
        _c("th")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "thead-light" }, [
      _c("tr", [
        _c("th", [_vm._v("ACCOUNT")]),
        _vm._v(" "),
        _c("th", { staticStyle: { "text-align": "right" } }, [
          _vm._v("MONTHLY  "),
          _c("span", [_vm._v("(₱ 1000)")])
        ]),
        _vm._v(" "),
        _c("th", [_vm._v("ACTIONS")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { staticClass: "col-form-label" }, [
      _vm._v("ADMINISTRATIVE EXPENSES Account "),
      _c("br"),
      _vm._v("Name")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("th", [_c("b", [_vm._v("OPERATING EXPENSES")])])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/static_budget/StaticCreate.vue":
/*!***********************************************************!*\
  !*** ./resources/js/views/static_budget/StaticCreate.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StaticCreate_vue_vue_type_template_id_55a49322_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StaticCreate.vue?vue&type=template&id=55a49322&scoped=true& */ "./resources/js/views/static_budget/StaticCreate.vue?vue&type=template&id=55a49322&scoped=true&");
/* harmony import */ var _StaticCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StaticCreate.vue?vue&type=script&lang=js& */ "./resources/js/views/static_budget/StaticCreate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StaticCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StaticCreate_vue_vue_type_template_id_55a49322_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StaticCreate_vue_vue_type_template_id_55a49322_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "55a49322",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/static_budget/StaticCreate.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/static_budget/StaticCreate.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticCreate.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticCreate.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticCreate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/static_budget/StaticCreate.vue?vue&type=template&id=55a49322&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticCreate.vue?vue&type=template&id=55a49322&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticCreate_vue_vue_type_template_id_55a49322_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticCreate.vue?vue&type=template&id=55a49322&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticCreate.vue?vue&type=template&id=55a49322&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticCreate_vue_vue_type_template_id_55a49322_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticCreate_vue_vue_type_template_id_55a49322_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/static_budget/StaticEdit.vue":
/*!*********************************************************!*\
  !*** ./resources/js/views/static_budget/StaticEdit.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StaticEdit_vue_vue_type_template_id_36ab34b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true& */ "./resources/js/views/static_budget/StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true&");
/* harmony import */ var _StaticEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StaticEdit.vue?vue&type=script&lang=js& */ "./resources/js/views/static_budget/StaticEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StaticEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StaticEdit_vue_vue_type_template_id_36ab34b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StaticEdit_vue_vue_type_template_id_36ab34b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "36ab34b0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/static_budget/StaticEdit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/static_budget/StaticEdit.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticEdit.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticEdit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/static_budget/StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticEdit_vue_vue_type_template_id_36ab34b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticEdit.vue?vue&type=template&id=36ab34b0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticEdit_vue_vue_type_template_id_36ab34b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticEdit_vue_vue_type_template_id_36ab34b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/static_budget/StaticIndex.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/static_budget/StaticIndex.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StaticIndex_vue_vue_type_template_id_3f19c9c8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true& */ "./resources/js/views/static_budget/StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true&");
/* harmony import */ var _StaticIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StaticIndex.vue?vue&type=script&lang=js& */ "./resources/js/views/static_budget/StaticIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StaticIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StaticIndex_vue_vue_type_template_id_3f19c9c8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StaticIndex_vue_vue_type_template_id_3f19c9c8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "3f19c9c8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/static_budget/StaticIndex.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/static_budget/StaticIndex.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticIndex.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticIndex.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/static_budget/StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticIndex_vue_vue_type_template_id_3f19c9c8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticIndex.vue?vue&type=template&id=3f19c9c8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticIndex_vue_vue_type_template_id_3f19c9c8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticIndex_vue_vue_type_template_id_3f19c9c8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/static_budget/StaticTable.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/static_budget/StaticTable.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StaticTable_vue_vue_type_template_id_cbe35fd0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true& */ "./resources/js/views/static_budget/StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true&");
/* harmony import */ var _StaticTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StaticTable.vue?vue&type=script&lang=js& */ "./resources/js/views/static_budget/StaticTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StaticTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StaticTable_vue_vue_type_template_id_cbe35fd0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StaticTable_vue_vue_type_template_id_cbe35fd0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "cbe35fd0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/static_budget/StaticTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/static_budget/StaticTable.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticTable.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/static_budget/StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/static_budget/StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticTable_vue_vue_type_template_id_cbe35fd0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/StaticTable.vue?vue&type=template&id=cbe35fd0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticTable_vue_vue_type_template_id_cbe35fd0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaticTable_vue_vue_type_template_id_cbe35fd0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/static_budget/_Form.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/static_budget/_Form.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Form_vue_vue_type_template_id_4a021e7b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_Form.vue?vue&type=template&id=4a021e7b&scoped=true& */ "./resources/js/views/static_budget/_Form.vue?vue&type=template&id=4a021e7b&scoped=true&");
/* harmony import */ var _Form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_Form.vue?vue&type=script&lang=js& */ "./resources/js/views/static_budget/_Form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Form_vue_vue_type_template_id_4a021e7b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Form_vue_vue_type_template_id_4a021e7b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4a021e7b",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/static_budget/_Form.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/static_budget/_Form.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/static_budget/_Form.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./_Form.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/_Form.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Form_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/static_budget/_Form.vue?vue&type=template&id=4a021e7b&scoped=true&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/static_budget/_Form.vue?vue&type=template&id=4a021e7b&scoped=true& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Form_vue_vue_type_template_id_4a021e7b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./_Form.vue?vue&type=template&id=4a021e7b&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/static_budget/_Form.vue?vue&type=template&id=4a021e7b&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Form_vue_vue_type_template_id_4a021e7b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Form_vue_vue_type_template_id_4a021e7b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);