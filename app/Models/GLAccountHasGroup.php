<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GLAccountHasGroup extends Model
{
    use HasFactory;

    protected $table = 'gl_accounts_has_group';
    protected $fillable = ['group_id','gl_id'];
}
