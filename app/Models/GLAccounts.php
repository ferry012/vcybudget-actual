<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GLAccounts extends Model
{
    use HasFactory;

    protected $table = 'gl_accounts';
    protected $fillable = ['gl_account','description'];
}
