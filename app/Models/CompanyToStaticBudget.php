<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyToStaticBudget extends Model
{
    use HasFactory;

    protected $table = 'company_to_static_budget_accounts';
    protected $fillable = ['static_budget_id','salaries_id','fuels_id','maintenance_id','repairs_id','parking_id'];
}
