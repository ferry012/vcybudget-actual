<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminExpense extends Model
{
    use HasFactory;

    protected $table = 'admin_expenses';
    protected $fillable = ['static_budget_id','name','budget'];
}
