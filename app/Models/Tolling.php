<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tolling extends Model
{
    use HasFactory;

    protected $table = 'tolling';
    protected $fillable = ['static_budget_id','name','budget'];
}
