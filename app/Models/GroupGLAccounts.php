<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupGLAccounts extends Model
{
    use HasFactory;

    protected $table = 'group_gl_accounts';
    protected $fillable = ['name'];

    public function gl_accounts()
    {
        return $this->belongsToMany(GLAccounts::class, GLAccountHasGroup::class,'group_id','gl_id');
    }

    protected $casts = [
        'created_at' => 'datetime:M d, Y',
    ];
}
