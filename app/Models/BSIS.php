<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BSIS extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv2';
    protected $table = 'BSIS';

//    protected $casts = [
//        'DMBTR' => 'integer',
//    ];
}
