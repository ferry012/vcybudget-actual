<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuelAndSellingExp extends Model
{
    use HasFactory;

    protected $table = 'fuel_and_selling_exp';
    protected $fillable = ['static_budget_id','name','budget'];
}
