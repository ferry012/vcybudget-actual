<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticBudget extends Model
{
    use HasFactory;
    protected $casts = [
        'created_at' => 'datetime:M d, Y',
    ];

    protected $table = 'static_budgets';
    protected $fillable = ['company','total_salaries','total_fuel_and_selling','total_operating','total_admin'];

    public function AdminExpense()
    {
        return $this->hasMany('App\Models\AdminExpense', 'static_budget_id');
    }

    public function FuelAndSellingExp()
    {
        return $this->hasMany('App\Models\FuelAndSellingExp', 'static_budget_id');
    }

    public function Fuels()
    {
        return $this->hasMany('App\Models\Fuels', 'static_budget_id');
    }

    public function Maintenance()
    {
        return $this->hasMany('App\Models\Maintenance', 'static_budget_id');
    }

    public function Parking()
    {
        return $this->hasMany('App\Models\Parking', 'static_budget_id');
    }

    public function Repairs()
    {
        return $this->hasMany('App\Models\Repairs', 'static_budget_id');
    }

    public function Salary()
    {
        return $this->hasMany('App\Models\Salary', 'static_budget_id');
    }

    public function Tolling()
    {
        return $this->hasMany('App\Models\Tolling', 'static_budget_id');
    }

    public function Utilities()
    {
        return $this->hasMany('App\Models\Utilities', 'static_budget_id');
    }
}
