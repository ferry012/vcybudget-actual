<?php

namespace App\Http\Controllers;

use App\Http\Resources\GLAccountCollection;
use App\Models\Company;
use App\Models\GLAccounts;
use App\Models\GroupGLAccounts;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GLAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return GLAccountCollection|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->showAll) {
            $query = GroupGLAccounts::orderBy('name','asc')->get();
        }
        elseif ($request->budgetMaintenance){
            $query = GroupGLAccounts::with('gl_accounts')->orderBy('group_gl_accounts.name', 'desc')->get();
            $company = Company::orderBy('EKORG','asc')
                ->where('EKORG','!=','1351')
                ->where('EKORG','!=','1601')
                ->where('EKORG','!=','1902')
                ->get();

            $comCount = count($company);
            for($i = 0;$i < count($query);$i++){
                for($x = 0;$x < count($query[$i]['gl_accounts']);$x++){
                    for($y = 0;$y < count($company);$y++){
                        $query[$i]['gl_accounts'][$x][(string)$company[$y]['EKORG']] = null;
                        if($comCount === $y){
                            break;
                        }
                    }
                }
            }
        }
        else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            $query = GroupGLAccounts::with('gl_accounts')->where('name', 'LIKE', "%$searchValue%")
                ->orderBy('group_gl_accounts.name', 'desc')->paginate($perPage);
        }

        return new GLAccountCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:group_gl_accounts,name,regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);

        DB::transaction(function () use($request) {
            $group_gl = GroupGLAccounts::create($request->except('gl_accounts'));
            $accID = [];
            foreach ($request->gl_accounts as $key => $value){
                $gl = GLAccounts::create([
                    'gl_account' => $value['gl_account'],
                    'description' => $value['description'],
                ]);
                $accID[] = $gl['id'];
            }

            foreach ($accID as $key => $account){
                DB::table('gl_accounts_has_group')
                    ->insert([
                        'group_id' => $group_gl['id'],
                        'gl_id' =>$account,
                    ]);
            }

            return response()->json([
                'status'=>'success',
                'message'=>'Group GL Accounts Successfully Added'
            ]);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
