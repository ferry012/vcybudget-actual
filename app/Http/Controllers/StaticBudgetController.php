<?php

namespace App\Http\Controllers;

use App\Http\Resources\StaticBudgetResource;
use App\Http\Resources\UserResource;
use App\Models\AdminExpense;
use App\Models\Company;
use App\Models\CompanyToStaticBudget;
use App\Models\FuelAndSellingExp;
use App\Models\Fuels;
use App\Models\Maintenance;
use App\Models\Parking;
use App\Models\Repairs;
use App\Models\Salary;
use App\Models\StaticBudget;
use App\Models\Tolling;
use App\Models\Utilities;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class StaticBudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return StaticBudgetResource
     */
    public function index(Request $request)
    {
//        dd($request->search,$request->sortby,$request->sortdir,intVal($request->currentpage));
        abort_if(Gate::denies('static_budget_view'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->getAll) {
            $query = StaticBudget::orderBy('created_at', 'asc')->get();
        } else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = intVal($request->currentpage);

            $query = StaticBudget::where(function ($q) use ($searchValue) {
                    $q->orwhere('company', 'LIKE', "%$searchValue%")
                        ->orwhereDate('created_at', 'LIKE', "%$searchValue%");
                })
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }

        return new StaticBudgetResource($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        abort_if(Gate::denies('static_budget_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        DB::transaction(function () use ($request) {

            $request->validate([
                'company' => ['required', 'unique:static_budgets'],
            ]);

            $staticBudget = StaticBudget::create([
                'company' => $request->company,
                'total_salaries' => $request->totalSalaries,
                'total_fuel_and_selling' => $request->totalFuelAndSellingExp,
                'total_admin' => $request->totalAdminExpenses,
                'total_operating' => $request->totalExpenses, //<- OPERATING EXPENSES
            ]);

            //salaries
            if ($request->salaries !== []) {
                foreach ($request->salaries as $key => $salary) {
                    $replace = str_replace(',','',$salary['sal_budget']);
                    Salary::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $salary['sal_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }

            //under total fuel and exp
            if ($request->under_total_fuel_and_exp !== []) {
                foreach ($request->under_total_fuel_and_exp as $key => $under_total_fuel_and_exp) {
                    $replace = str_replace(',','',$under_total_fuel_and_exp['under_total_fuel_and_exp_budget']);
                    FuelAndSellingExp::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $under_total_fuel_and_exp['under_total_fuel_and_exp_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }

            //fuel
            if ($request->fuels !== []) {
                foreach ($request->fuels as $key => $fuel) {
                    $replace = str_replace(',','',$fuel['fuel_budget']);
                    Fuels::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $fuel['fuel_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }
            //maintenance
            if ($request->maintenance !== []) {
                foreach ($request->maintenance as $key => $maintenance) {
                    $replace = str_replace(',','',$maintenance['main_budget']);
                    Maintenance::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $maintenance['main_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }
            //repairs
            if ($request->repairs !== []) {
                foreach ($request->repairs as $key => $repairs) {
                    $replace = str_replace(',','',$repairs['repairs_budget']);
                    Repairs::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $repairs['repairs_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }
            //parking
            if ($request->parking !== []) {
                foreach ($request->parking as $key => $parking) {
                    $replace = str_replace(',','',$parking['parking_budget']);
                    Parking::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $parking['parking_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }
            //tolling
            if ($request->tolling !== []) {
                foreach ($request->tolling as $key => $tolling) {
                    $replace = str_replace(',','',$tolling['tolling_budget']);
                    Tolling::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $tolling['tolling_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }

            //admin_expenses, under in total admin_expense
            if ($request->under_total_admin !== []) {
                foreach ($request->under_total_admin as $key => $under_total_admin) {
                    $replace = str_replace(',','',$under_total_admin['under_total_admin_budget']);
                    AdminExpense::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $under_total_admin['under_total_admin_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }

            //utilities
            if ($request->utilities !== []) {
                foreach ($request->utilities as $key => $utilities) {
                    $replace = str_replace(',','',$utilities['utilities_budget']);
                    Utilities::create([
                        'static_budget_id' => $staticBudget['id'],
                        'name' => $utilities['utilities_name'],
                        'budget' => number_format($replace, 2),
                    ]);
                }
            }

        });
    }

    /**
     * Display the specified resource.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return StaticBudgetResource
     */
    public function edit($id)
    {
        abort_if(Gate::denies('static_budget_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');
//        $query = StaticBudget::with('AdminExpense','FuelAndSellingExp','Fuels','Maintenance','Parking',
//            'Repairs','Salary','Tolling','Utilities')
//            ->where('static_budgets.company',$id)
//            ->first();

//        $query = StaticBudget::from('static_budgets as a')
//        ->leftjoin('admin_expenses as b','a.id','=','b.static_budget_id')
//            ->leftjoin('fuels as c','a.id','=','c.static_budget_id')
//            ->leftjoin('fuel_and_selling_exp as d','a.id','=','d.static_budget_id')
//            ->leftjoin('maintenance as e','a.id','=','e.static_budget_id')
//            ->leftjoin('parking as f','a.id','=','f.static_budget_id')
//            ->leftjoin('repairs as g','a.id','=','g.static_budget_id')
//            ->leftjoin('salaries as h','a.id','=','h.static_budget_id')
//            ->leftjoin('tolling as i','a.id','=','i.static_budget_id')
//            ->leftjoin('utilities as j','a.id','=','j.static_budget_id')
//            ->where('a.company',$id)
//            ->get();
        $getID = StaticBudget::where('company',$id)->first();
        $admin_expenses = AdminExpense::where('static_budget_id',$getID['id'])
            ->select('name as under_total_admin_name','budget as under_total_admin_budget')
            ->get();

        $fuels = Fuels::where('static_budget_id',$getID['id'])
            ->select('name as fuel_name','budget as fuel_budget')
            ->get();

        $FuelAndSellingExp = FuelAndSellingExp::where('static_budget_id',$getID['id'])
            ->select('name as under_total_fuel_and_exp_name','budget as under_total_fuel_and_exp_budget')
            ->get();

        $maintenance = Maintenance::where('static_budget_id',$getID['id'])
            ->select('name as main_name','budget as main_budget')
            ->get();

        $parking = Parking::where('static_budget_id',$getID['id'])
            ->select('name as parking_name','budget as parking_budget')
            ->get();

        $repairs = Repairs::where('static_budget_id',$getID['id'])
            ->select('name as repairs_name','budget as repairs_budget')
            ->get();

        $salaries = Salary::where('static_budget_id',$getID['id'])
            ->select('name as sal_name','budget as sal_budget')
            ->get();

        $tolling = Tolling::where('static_budget_id',$getID['id'])
            ->select('name as tolling_name','budget as tolling_budget')
            ->get();

        $utilities = Utilities::where('static_budget_id',$getID['id'])
            ->select('name as utilities_name','budget as utilities_budget')
            ->get();
//        dd($getID['total_salaries'],$getID['total_operating']);
        return new StaticBudgetResource([
            'fuels' => $fuels,
            'under_total_admin' => $admin_expenses,
            'under_total_fuel_and_exp' => $FuelAndSellingExp,
            'maintenance' => $maintenance,
            'parking' => $parking,
            'repairs' => $repairs,
            'salaries' => $salaries,
            'tolling' => $tolling,
            'utilities' => $utilities,
            'totalSalaries' =>(float)$getID['total_salaries'],
            'totalExpenses' =>(float)$getID['total_operating'],
            'totalFuelAndSellingExp' => (float)$getID['total_fuel_and_selling'],
            'totalAdminExpenses' => (float)$getID['total_admin'],
            'company' => $getID['company'],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        abort_if(Gate::denies('static_budget_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        DB::transaction(function () use ($request,$id) {

            $request->validate([
                'company' => 'required|string|max:25|unique:static_budgets,company,' . $id,
            ]);

            $getID = StaticBudget::where('company',$request->company)->first();

            if(isset($getID['id'])){

                $admin_expenses = AdminExpense::where('static_budget_id',$getID['id']);
                $admin_expenses->delete();

                $fuels = Fuels::where('static_budget_id',$getID['id']);
                $fuels->delete();

                $FuelAndSellingExp = FuelAndSellingExp::where('static_budget_id',$getID['id']);
                $FuelAndSellingExp->delete();

                $maintenance = Maintenance::where('static_budget_id',$getID['id']);
                $maintenance->delete();

                $parking = Parking::where('static_budget_id',$getID['id']);
                $parking->delete();

                $repairs = Repairs::where('static_budget_id',$getID['id']);
                $repairs->delete();

                $salaries = Salary::where('static_budget_id',$getID['id']);
                $salaries->delete();

                $tolling = Tolling::where('static_budget_id',$getID['id']);
                $tolling->delete();

                $utilities = Utilities::where('static_budget_id',$getID['id']);
                $utilities->delete();

                $removeStatic = StaticBudget::where('id',$getID['id']);
                $removeStatic->delete();

                //if done remove
                $staticBudget = StaticBudget::create([
                    'company' => $request->company,
                    'total_salaries' => $request->totalSalaries,
                    'total_fuel_and_selling' => $request->totalFuelAndSellingExp,
                    'total_admin' => $request->totalAdminExpenses,
                    'total_operating' => $request->totalExpenses, //<- OPERATING EXPENSES
                ]);

                //salaries
                if ($request->salaries !== []) {
                    foreach ($request->salaries as $key => $salary) {
                        $replace = str_replace(',','',$salary['sal_budget']);
                        Salary::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $salary['sal_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }

                //under total fuel and exp
                if ($request->under_total_fuel_and_exp !== []) {
                    foreach ($request->under_total_fuel_and_exp as $key => $under_total_fuel_and_exp) {
                        $replace = str_replace(',','',$under_total_fuel_and_exp['under_total_fuel_and_exp_budget']);
                        FuelAndSellingExp::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $under_total_fuel_and_exp['under_total_fuel_and_exp_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }

                //fuel
                if ($request->fuels !== []) {
                    foreach ($request->fuels as $key => $fuel) {
                        $replace = str_replace(',','',$fuel['fuel_budget']);
                        Fuels::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $fuel['fuel_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }
                //maintenance
                if ($request->maintenance !== []) {
                    foreach ($request->maintenance as $key => $maintenance) {
                        $replace = str_replace(',','',$maintenance['main_budget']);
                        Maintenance::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $maintenance['main_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }
                //repairs
                if ($request->repairs !== []) {
                    foreach ($request->repairs as $key => $repairs) {
                        $replace = str_replace(',','',$repairs['repairs_budget']);
                        Repairs::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $repairs['repairs_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }
                //parking
                if ($request->parking !== []) {
                    foreach ($request->parking as $key => $parking) {
                        $replace = str_replace(',','',$parking['parking_budget']);
                        Parking::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $parking['parking_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }
                //tolling
                if ($request->tolling !== []) {
                    foreach ($request->tolling as $key => $tolling) {
                        $replace = str_replace(',','',$tolling['tolling_budget']);
                        Tolling::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $tolling['tolling_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }

                //admin_expenses, under in total admin_expense
                if ($request->under_total_admin !== []) {
                    foreach ($request->under_total_admin as $key => $under_total_admin) {
                        $replace = str_replace(',','',$under_total_admin['under_total_admin_budget']);
                        AdminExpense::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $under_total_admin['under_total_admin_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }

                //utilities
                if ($request->utilities !== []) {
                    foreach ($request->utilities as $key => $utilities) {
                        $replace = str_replace(',','',$utilities['utilities_budget']);
                        Utilities::create([
                            'static_budget_id' => $staticBudget['id'],
                            'name' => $utilities['utilities_name'],
                            'budget' => number_format($replace, 2),
                        ]);
                    }
                }
            }
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        abort_if(Gate::denies('static_budget_delete') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        $static_budget = StaticBudget::findorfail($id);
        $static_budget->delete();

        $admin_expenses = AdminExpense::where('static_budget_id',$id);
        $admin_expenses->delete();

        $fuels = Fuels::where('static_budget_id',$id);
        $fuels->delete();

        $FuelAndSellingExp = FuelAndSellingExp::where('static_budget_id',$id);
        $FuelAndSellingExp->delete();

        $maintenance = Maintenance::where('static_budget_id',$id);
        $maintenance->delete();

        $parking = Parking::where('static_budget_id',$id);
        $parking->delete();

        $repairs = Repairs::where('static_budget_id',$id);
        $repairs->delete();

        $salaries = Salary::where('static_budget_id',$id);
        $salaries->delete();

        $tolling = Tolling::where('static_budget_id',$id);
        $tolling->delete();

        $utilities = Utilities::where('static_budget_id',$id);
        $utilities->delete();

        return response()->json($static_budget);
    }
}
