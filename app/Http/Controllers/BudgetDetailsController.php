<?php

namespace App\Http\Controllers;

use App\Models\AdminExpense;
use App\Models\BSIS;
use App\Models\FuelAndSellingExp;
use App\Models\Fuels;
use App\Models\Maintenance;
use App\Models\Parking;
use App\Models\Repairs;
use App\Models\Salary;
use App\Models\StaticBudget;
use App\Models\Tolling;
use App\Models\User;
use App\Models\Utilities;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BudgetDetailsController extends Controller
{
    public function budgetTotal(Request $request)
    {
        //removed 604001
        //removed  602701 22/11/2021

        $search = $request->filter;
        $company = $request->company;
        $currentDate = Carbon::now()->format('Y-m-d');
        $currentMonth = Carbon::now()->format('m');
        $currentYear = Carbon::now()->format('Y');
        $currentDay = Carbon::now()->format('d');

        $currentPastMonth = Carbon::now()->format('m') - 1;
        $currentPastDate = $currentYear . '-' . $currentPastMonth . '-' . $currentDay;

        $get_PY_DATE = explode("-", $currentDate);
        foreach ($get_PY_DATE as $key => $value) {
            if ($key === 0) {
                $get_PY_DATE[0] -= 1;
                break;
            }
        }
//      *note => remove below after debug
//        $currentUser = auth()->user()->id;
        $currentUser = 1;
        $isUser = null;
        $User = User::permission('filter_company')->get();
        //check if current user has permission
        foreach ($User as $key => $value) {
            if ($value['id'] === $currentUser) {
                $isUser = $value['name'];
                break;
            }
        }

        $isUserSal = null;
        $UserSal = User::permission('allowed_salaries_breakdown')->get();
        //check if current user has permission to allow salaries
        foreach ($UserSal as $key => $value) {
            if ($value['id'] === $currentUser) {
                $isUserSal = $value['name'];
                break;
            }
        }
        //Removing an item in budget data just comment the index in total moth and by month
        $PY_DATE = implode("-", $get_PY_DATE);

        if ($search !== 'null' && $isUser !== null) {
            //CY_ACTUAL
            $regular = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $search)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $search)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            //PY_ACTUAL
            $regular_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
//                $query->where('BUKRS', $search)
//                    ->where('BUDAT', '<=', $PY_DATE)
//                    ->where('GJAHR', $currentYear - 1)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations_PY = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
//                $query->where('BUKRS', $search)
//                    ->where('BUDAT', '<=', $PY_DATE)
//                    ->where('GJAHR', $currentYear - 1)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $static_budgets = StaticBudget::where('company', $search)->first();
            $admin_expenses = AdminExpense::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $fuels = Fuels::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $fuel_and_selling_exp = FuelAndSellingExp::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $maintenance = Maintenance::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $parking = Parking::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $repairs = Repairs::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $salaries = Salary::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $tolling = Tolling::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $utilities = Utilities::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();

            $sal_budget_total = 0;
            $fuel_budget_total = 0;
            $main_budget_total = 0;
            $repair_budget_total = 0;
            $parking_budget_total = 0;
            $tolling_budget_total = 0;
            $fuel_and_selling_exp_budget_total = 0;
            $utilities_budget_total = 0;
            $admin_exp_budget_total = 0;
            //VARIANCE
            $total_var_sal = 0;
            $total_var_fuel = 0;
            $total_var_main = 0;
            $total_var_repair = 0;
            $total_var_parking = 0;
            $total_var_tolling = 0;
            $total_var_fuel_and_selling_exp = 0;
            $total_var_utilities = 0;
            $total_var_admin_exp = 0;

            for ($i = 0; $i < count($salaries); $i++) {
//                    return response()->json(round((float)$agency->DMBTR / 1000));
                $salaries[0]['total_cy'] = number_format(round((float)$regular->DMBTR / 1000));
                $salaries[1]['total_cy'] = number_format(round((float)$agency->DMBTR / 1000));
                $salaries[2]['total_cy'] = number_format(round((float)$other_benefits->DMBTR / 1000));
                $salaries[3]['total_cy'] = number_format(round((float)$non_mandatory->DMBTR / 1000));
                $salaries[$i]['budget_total'] = number_format($salaries[$i]['budget'] * (int)$currentMonth);

                $total = $salaries[$i]['budget'] * (int)$currentMonth;

                $salaries[0]['total_py'] = number_format(round((float)$regular_PY->DMBTR / 1000));
                $salaries[1]['total_py'] = number_format(round((float)$agency_PY->DMBTR / 1000));
                $salaries[2]['total_py'] = number_format(round((float)$other_benefits_PY->DMBTR / 1000));
                $salaries[3]['total_py'] = number_format(round((float)$non_mandatory_PY->DMBTR / 1000));

                //VARIANCE = CY - BUDGET
                $convert_cy = str_replace(',', '', $salaries[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $salaries[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $salaries[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_sal += $var_total; // variance
                $sal_budget_total += $total; // add all value inside in salaries array

                //% OF VARIANCE
                // VARIANCE / BUDGET * 100 to get the percentage(%)
                //$convert_variance = str_replace([',','(',')'], '', $salaries[$i]['variance']);
                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $salaries[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $salaries[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $salaries[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $salaries[$i]['change_percent'] = $var_total_change_percent . "%";
            }

            for ($i = 0; $i < count($fuels); $i++) {
                $fuels[0]['total_cy'] = number_format(round((float)$fuel_vehicles->DMBTR / 1000));
                $fuels[1]['total_cy'] = number_format(round((float)$fuel_delivery->DMBTR / 1000));
                $fuels[2]['total_cy'] = number_format(round((float)$fuel_machinery->DMBTR / 1000));
                $fuels[$i]['budget_total'] = number_format($fuels[$i]['budget'] * (int)$currentMonth);
                $total = $fuels[$i]['budget'] * (int)$currentMonth;

                $fuels[0]['total_py'] = number_format(round((float)$fuel_vehicles_PY->DMBTR / 1000));
                $fuels[1]['total_py'] = number_format(round((float)$fuel_delivery_PY->DMBTR / 1000));
                $fuels[2]['total_py'] = number_format(round((float)$fuel_machinery_PY->DMBTR / 1000));

                $convert_cy = str_replace(',', '', $fuels[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $fuels[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $fuels[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_fuel += $var_total; // variance
                $fuel_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $fuels[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $fuels[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $fuels[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $fuels[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($maintenance); $i++) {
                $maintenance[0]['total_cy'] = number_format(round((float)$main_tire->DMBTR / 1000));
                $maintenance[1]['total_cy'] = number_format(round((float)$main_tire_delivery->DMBTR / 1000));
                $maintenance[2]['total_cy'] = number_format(round((float)$main_battery->DMBTR / 1000));
                $maintenance[3]['total_cy'] = number_format(round((float)$main_battery_delivery->DMBTR / 1000));
                $maintenance[4]['total_cy'] = number_format(round((float)$main_machinery->DMBTR / 1000));
                $maintenance[5]['total_cy'] = number_format(round((float)$main_oil_vehicles->DMBTR / 1000));
                $maintenance[6]['total_cy'] = number_format(round((float)$main_oil_vehicles_delivery->DMBTR / 1000));
                $maintenance[$i]['budget_total'] = number_format($maintenance[$i]['budget'] * (int)$currentMonth);
                $total = $maintenance[$i]['budget'] * (int)$currentMonth;

                $maintenance[0]['total_py'] = number_format(round((float)$main_tire_PY->DMBTR / 1000));
                $maintenance[1]['total_py'] = number_format(round((float)$main_tire_delivery_PY->DMBTR / 1000));
                $maintenance[2]['total_py'] = number_format(round((float)$main_battery_PY->DMBTR / 1000));
                $maintenance[3]['total_py'] = number_format(round((float)$main_battery_delivery_PY->DMBTR / 1000));
                $maintenance[4]['total_py'] = number_format(round((float)$main_machinery_PY->DMBTR / 1000));
                $maintenance[5]['total_py'] = number_format(round((float)$main_oil_vehicles_PY->DMBTR / 1000));
                $maintenance[6]['total_py'] = number_format(round((float)$main_oil_vehicles_delivery_PY->DMBTR / 1000));

                $convert_cy = str_replace(',', '', $maintenance[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $maintenance[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $maintenance[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_main += $var_total; // variance
                $main_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $maintenance[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $maintenance[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $maintenance[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $maintenance[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($repairs); $i++) {
                $repairs[0]['total_cy'] = number_format(round((float)$repair_vehicles->DMBTR / 1000));
                $repairs[1]['total_cy'] = number_format(round((float)$repair_vehicles_delivery->DMBTR / 1000));
                $repairs[$i]['budget_total'] = number_format($repairs[$i]['budget'] * (int)$currentMonth);
                $total = $repairs[$i]['budget'] * (int)$currentMonth;

                $repairs[0]['total_py'] = number_format(round((float)$repair_vehicles_PY->DMBTR / 1000));
                $repairs[1]['total_py'] = number_format(round((float)$repair_vehicles_delivery_PY->DMBTR / 1000));

                $convert_cy = str_replace(',', '', $repairs[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $repairs[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $repairs[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_repair += $var_total; // variance
                $repair_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $repairs[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $repairs[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $repairs[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $repairs[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($parking); $i++) {
                $parking[0]['total_cy'] = number_format(round((float)$parking_office->DMBTR / 1000));
//                $parking[1]['total_cy'] = number_format(round((float)$parking_fsp_vehicles->DMBTR / 1000));
                $parking[1]['total_cy'] = number_format(round((float)$parking_delivery_vehicles->DMBTR / 1000));
                $parking[$i]['budget_total'] = number_format($parking[$i]['budget'] * (int)$currentMonth);

                $parking[0]['total_py'] = number_format(round((float)$parking_office_PY->DMBTR / 1000));
//                $parking[1]['total_py'] = number_format(round((float)$parking_fsp_vehicles_PY->DMBTR / 1000));
                $parking[1]['total_py'] = number_format(round((float)$parking_delivery_vehicles_PY->DMBTR / 1000));
                $total = $parking[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $parking[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $parking[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $parking[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_parking += $var_total; // variance
                $parking_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $parking[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $parking[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $parking[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $parking[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($tolling); $i++) {
                $tolling[0]['total_cy'] = number_format(round((float)$toll_office->DMBTR / 1000));
                $tolling[1]['total_cy'] = number_format(round((float)$toll_fsp_vehicles->DMBTR / 1000));
                $tolling[2]['total_cy'] = number_format(round((float)$toll_delivery_vehicles->DMBTR / 1000));
                $tolling[$i]['budget_total'] = number_format($tolling[$i]['budget'] * (int)$currentMonth);

                $tolling[0]['total_py'] = number_format(round((float)$toll_office_PY->DMBTR / 1000));
                $tolling[1]['total_py'] = number_format(round((float)$toll_fsp_vehicles_PY->DMBTR / 1000));
                $tolling[2]['total_py'] = number_format(round((float)$toll_delivery_vehicles_PY->DMBTR / 1000));
                $total = $tolling[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $tolling[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $tolling[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $tolling[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_tolling += $var_total; // variance
                $tolling_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $tolling[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $tolling[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $tolling[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $tolling[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($fuel_and_selling_exp); $i++) {
                $fuel_and_selling_exp[0]['total_cy'] = number_format(round((float)$ad_and_promotions->DMBTR / 1000));
                $fuel_and_selling_exp[1]['total_cy'] = number_format(round((float)$trans_and_travel->DMBTR / 1000));
                $fuel_and_selling_exp[$i]['budget_total'] = number_format($fuel_and_selling_exp[$i]['budget'] * (int)$currentMonth);

                $fuel_and_selling_exp[0]['total_py'] = number_format(round((float)$ad_and_promotions_PY->DMBTR / 1000));
                $fuel_and_selling_exp[1]['total_py'] = number_format(round((float)$trans_and_travel_PY->DMBTR / 1000));
                $total = $fuel_and_selling_exp[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $fuel_and_selling_exp[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $fuel_and_selling_exp[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $fuel_and_selling_exp[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_fuel_and_selling_exp += $var_total; // variance
                $fuel_and_selling_exp_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $fuel_and_selling_exp[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $fuel_and_selling_exp[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $fuel_and_selling_exp[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $fuel_and_selling_exp[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($utilities); $i++) {
                $utilities[0]['total_cy'] = number_format(round((float)$utilities_electricity->DMBTR / 1000));
                $utilities[1]['total_cy'] = number_format(round((float)$utilities_water->DMBTR / 1000));
                $utilities[$i]['budget_total'] = number_format($utilities[$i]['budget'] * (int)$currentMonth);

                $utilities[0]['total_py'] = number_format(round((float)$utilities_electricity_PY->DMBTR / 1000));
                $utilities[1]['total_py'] = number_format(round((float)$utilities_water_PY->DMBTR / 1000));
                $total = $utilities[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $utilities[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $utilities[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $utilities[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_utilities += $var_total; // variance
                $utilities_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $utilities[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $utilities[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $utilities[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $utilities[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($admin_expenses); $i++) {
                $admin_expenses[0]['total_cy'] = number_format(round((float)$communication->DMBTR / 1000));
                $admin_expenses[1]['total_cy'] = number_format(round((float)$postage->DMBTR / 1000));
                $admin_expenses[2]['total_cy'] = number_format(round((float)$repairs_total->DMBTR / 1000));
                $admin_expenses[3]['total_cy'] = number_format(round((float)$represent_and_entertainment->DMBTR / 1000));
                $admin_expenses[4]['total_cy'] = number_format(round((float)$plog_fees_others->DMBTR / 1000));
                $admin_expenses[5]['total_cy'] = number_format(round((float)$supplies->DMBTR / 1000));
                $admin_expenses[6]['total_cy'] = number_format(round((float)$subscriptions->DMBTR / 1000));
//            $admin_expenses[7]['total_cy'] = number_format(round((float)$solicit_and_donations->DMBTR / 1000));
                $admin_expenses[$i]['budget_total'] = number_format($admin_expenses[$i]['budget'] * (int)$currentMonth);

                $admin_expenses[0]['total_py'] = number_format(round((float)$communication_PY->DMBTR / 1000));
                $admin_expenses[1]['total_py'] = number_format(round((float)$postage_PY->DMBTR / 1000));
                $admin_expenses[2]['total_py'] = number_format(round((float)$repairs_total_PY->DMBTR / 1000));
                $admin_expenses[3]['total_py'] = number_format(round((float)$represent_and_entertainment_PY->DMBTR / 1000));
                $admin_expenses[4]['total_py'] = number_format(round((float)$plog_fees_others_PY->DMBTR / 1000));
                $admin_expenses[5]['total_py'] = number_format(round((float)$supplies_PY->DMBTR / 1000));
                $admin_expenses[6]['total_py'] = number_format(round((float)$subscriptions_PY->DMBTR / 1000));
//            $admin_expenses[7]['total_py'] = number_format(round((float)$solicit_and_donations_PY->DMBTR / 1000));
                $total = $admin_expenses[$i]['budget'] * (int)$currentMonth;
                $convert_cy = str_replace(',', '', $admin_expenses[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $admin_expenses[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $admin_expenses[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_admin_exp += $var_total; // variance
                $admin_exp_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $admin_expenses[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $admin_expenses[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $admin_expenses[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $admin_expenses[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            $accArr = [
                'SALARIES_CY' => $salaries ?? [],
                'FUELS_CY' => $fuels ?? [],
                'MAINTENANCE_CY' => $maintenance ?? [],
                'REPAIRS_CY' => $repairs ?? [],
                'PARKING_CY' => $parking ?? [],
                'TOLLING_CY' => $tolling ?? [],
                'FUEL_AND_SELLING_EXP_CY' => $fuel_and_selling_exp ?? [],
                'UTILITIES_CY' => $utilities ?? [],
                'ADMIN_EXPENSE_CY' => $admin_expenses ?? [],
            ];
            $arrRegular = [
                'ACCOUNT' => [
                    'DETAILS' => $accArr,
                ],
            ];

            //BUDGET DETAILS PER MONTH
            $regular_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $search)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $search)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations_per_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $search)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            //BUDGET DETAILS PAST MONTH
            $regular_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
//                $query->where('BUKRS', $search)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth - 1)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $search)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations_past_month = BSIS::where(function ($query) use ($search, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
//                $query->where('BUKRS', $search)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth - 1)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $total_month_budget_sal = 0;
            $total_current_month_actual_sal = 0;
            $total_current_month_variance_sal = 0;
            $total_past_month_actual_sal = 0;
            $total_past_month_variance_sal = 0;

            $total_month_budget_fuel = 0;
            $total_current_month_actual_fuel = 0;
            $total_current_month_variance_fuel = 0;
            $total_past_month_actual_fuel = 0;
            $total_past_month_variance_fuel = 0;

            $total_month_budget_main = 0;
            $total_current_month_actual_main = 0;
            $total_current_month_variance_main = 0;
            $total_past_month_actual_main = 0;
            $total_past_month_variance_main = 0;

            $total_month_budget_repair = 0;
            $total_current_month_actual_repair = 0;
            $total_current_month_variance_repair = 0;
            $total_past_month_actual_repair = 0;
            $total_past_month_variance_repair = 0;

            $total_month_budget_parking = 0;
            $total_current_month_actual_parking = 0;
            $total_current_month_variance_parking = 0;
            $total_past_month_actual_parking = 0;
            $total_past_month_variance_parking = 0;

            $total_month_budget_tolling = 0;
            $total_current_month_actual_tolling = 0;
            $total_current_month_variance_tolling = 0;
            $total_past_month_actual_tolling = 0;
            $total_past_month_variance_tolling = 0;

            $total_month_budget_fuel_and_sell_exp = 0;
            $total_current_month_actual_fuel_and_sell_exp = 0;
            $total_current_month_variance_fuel_and_sell_exp = 0;
            $total_past_month_actual_fuel_and_sell_exp = 0;
            $total_past_month_variance_fuel_and_sell_exp = 0;

            $total_month_budget_utilities = 0;
            $total_current_month_actual_utilities = 0;
            $total_current_month_variance_utilities = 0;
            $total_past_month_actual_utilities = 0;
            $total_past_month_variance_utilities = 0;

            $total_month_budget_admin_exp = 0;
            $total_current_month_actual_admin_exp = 0;
            $total_current_month_variance_admin_exp = 0;
            $total_past_month_actual_admin_exp = 0;
            $total_past_month_variance_admin_exp = 0;

            for ($i = 0; $i < count($salaries); $i++) {
                //CURRENT MONTH
                $salaries[0]['actual_per_month'] = number_format(round((float)$regular_per_month->DMBTR / 1000));
                $salaries[1]['actual_per_month'] = number_format(round((float)$agency_per_month->DMBTR / 1000));
                $salaries[2]['actual_per_month'] = number_format(round((float)$other_benefits_per_month->DMBTR / 1000));
                $salaries[3]['actual_per_month'] = number_format(round((float)$non_mandatory_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $salaries[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $salaries[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $salaries[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $salaries[0]['actual_past_month'] = number_format(round((float)$regular_past_month->DMBTR / 1000));
                $salaries[1]['actual_past_month'] = number_format(round((float)$agency_past_month->DMBTR / 1000));
                $salaries[2]['actual_past_month'] = number_format(round((float)$other_benefits_past_month->DMBTR / 1000));
                $salaries[3]['actual_past_month'] = number_format(round((float)$non_mandatory_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $salaries[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $salaries[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_sal += $salaries[$i]['budget'];
                $total_current_month_actual_sal += (int)$salaries[$i]['actual_per_month'];
                $total_current_month_variance_sal += $var_total_per_month;
                $total_past_month_actual_sal += (int)$salaries[$i]['actual_past_month'];
                $total_past_month_variance_sal += $var_total_past_month;
            }
            for ($i = 0; $i < count($fuels); $i++) {
                //CURRENT MONTH
                $fuels[0]['actual_per_month'] = number_format(round((float)$fuel_vehicles_per_month->DMBTR / 1000));
                $fuels[1]['actual_per_month'] = number_format(round((float)$fuel_delivery_per_month->DMBTR / 1000));
                $fuels[2]['actual_per_month'] = number_format(round((float)$fuel_machinery_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $fuels[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $fuels[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $fuels[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $fuels[0]['actual_past_month'] = number_format(round((float)$fuel_vehicles_past_month->DMBTR / 1000));
                $fuels[1]['actual_past_month'] = number_format(round((float)$fuel_delivery_past_month->DMBTR / 1000));
                $fuels[2]['actual_past_month'] = number_format(round((float)$fuel_machinery_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $fuels[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $fuels[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_fuel += $fuels[$i]['budget'];

                $total_current_month_actual_fuel += $fuels[$i]['actual_per_month'];
                $total_current_month_variance_fuel += $var_total_per_month;
                $total_past_month_actual_fuel += $fuels[$i]['actual_past_month'];
                $total_past_month_variance_fuel += $var_total_past_month;
            }
            for ($i = 0; $i < count($maintenance); $i++) {
                $maintenance[0]['actual_per_month'] = number_format(round((float)$main_tire_per_month->DMBTR / 1000));
                $maintenance[1]['actual_per_month'] = number_format(round((float)$main_tire_delivery_per_month->DMBTR / 1000));
                $maintenance[2]['actual_per_month'] = number_format(round((float)$main_battery_per_month->DMBTR / 1000));
                $maintenance[3]['actual_per_month'] = number_format(round((float)$main_battery_delivery_per_month->DMBTR / 1000));
                $maintenance[4]['actual_per_month'] = number_format(round((float)$main_machinery_per_month->DMBTR / 1000));
                $maintenance[5]['actual_per_month'] = number_format(round((float)$main_oil_vehicles_per_month->DMBTR / 1000));
                $maintenance[6]['actual_per_month'] = number_format(round((float)$main_oil_vehicles_delivery_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $maintenance[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $maintenance[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $maintenance[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $maintenance[0]['actual_past_month'] = number_format(round((float)$main_tire_past_month->DMBTR / 1000));
                $maintenance[1]['actual_past_month'] = number_format(round((float)$main_tire_delivery_past_month->DMBTR / 1000));
                $maintenance[2]['actual_past_month'] = number_format(round((float)$main_battery_past_month->DMBTR / 1000));
                $maintenance[3]['actual_past_month'] = number_format(round((float)$main_battery_delivery_past_month->DMBTR / 1000));
                $maintenance[4]['actual_past_month'] = number_format(round((float)$main_machinery_past_month->DMBTR / 1000));
                $maintenance[5]['actual_past_month'] = number_format(round((float)$main_oil_vehicles_past_month->DMBTR / 1000));
                $maintenance[6]['actual_past_month'] = number_format(round((float)$main_oil_vehicles_delivery_past_month->DMBTR / 1000));;

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $maintenance[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $maintenance[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_main += $maintenance[$i]['budget'];

                $total_current_month_actual_main += $maintenance[$i]['actual_per_month'];
                $total_current_month_variance_main += $var_total_per_month;
                $total_past_month_actual_main += $maintenance[$i]['actual_past_month'];
                $total_past_month_variance_main += $var_total_past_month;
            }
            for ($i = 0; $i < count($repairs); $i++) {
                $repairs[0]['actual_per_month'] = number_format(round((float)$repair_vehicles_per_month->DMBTR / 1000));
                $repairs[1]['actual_per_month'] = number_format(round((float)$repair_vehicles_delivery_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $repairs[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $repairs[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $repairs[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $repairs[0]['actual_past_month'] = number_format(round((float)$repair_vehicles_past_month->DMBTR / 1000));
                $repairs[1]['actual_past_month'] = number_format(round((float)$repair_vehicles_delivery_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $repairs[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $repairs[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_repair += $repairs[$i]['budget'];

                $total_current_month_actual_repair += $repairs[$i]['actual_per_month'];
                $total_current_month_variance_repair += $var_total_per_month;
                $total_past_month_actual_repair += $repairs[$i]['actual_past_month'];
                $total_past_month_variance_repair += $var_total_past_month;
            }
            for ($i = 0; $i < count($parking); $i++) {
                $parking[0]['actual_per_month'] = number_format(round((float)$parking_office_per_month->DMBTR / 1000));
//                $parking[1]['actual_per_month'] = number_format(round((float)$parking_fsp_vehicles_per_month->DMBTR / 1000));
                $parking[1]['actual_per_month'] = number_format(round((float)$parking_delivery_vehicles_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $parking[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $parking[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $parking[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $parking[0]['actual_past_month'] = number_format(round((float)$parking_office_past_month->DMBTR / 1000));
//                $parking[1]['actual_past_month'] = number_format(round((float)$parking_fsp_vehicles_past_month->DMBTR / 1000));
                $parking[1]['actual_past_month'] = number_format(round((float)$parking_delivery_vehicles_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $parking[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $parking[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_parking += $parking[$i]['budget'];

                $total_current_month_actual_parking += $parking[$i]['actual_per_month'];
                $total_current_month_variance_parking += $var_total_per_month;
                $total_past_month_actual_parking += $parking[$i]['actual_past_month'];
                $total_past_month_variance_parking += $var_total_past_month;
            }
            for ($i = 0; $i < count($tolling); $i++) {
                $tolling[0]['actual_per_month'] = number_format(round((float)$toll_office_per_month->DMBTR / 1000));
                $tolling[1]['actual_per_month'] = number_format(round((float)$toll_fsp_vehicles_per_month->DMBTR / 1000));
                $tolling[2]['actual_per_month'] = number_format(round((float)$toll_delivery_vehicles_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $tolling[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $tolling[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $tolling[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $tolling[0]['actual_past_month'] = number_format(round((float)$toll_office_past_month->DMBTR / 1000));
                $tolling[1]['actual_past_month'] = number_format(round((float)$toll_fsp_vehicles_past_month->DMBTR / 1000));
                $tolling[2]['actual_past_month'] = number_format(round((float)$toll_delivery_vehicles_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $tolling[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $tolling[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_tolling += $tolling[$i]['budget'];

                $total_current_month_actual_tolling += $tolling[$i]['actual_per_month'];
                $total_current_month_variance_tolling += $var_total_per_month;
                $total_past_month_actual_tolling += $tolling[$i]['actual_past_month'];
                $total_past_month_variance_tolling += $var_total_past_month;
            }
            for ($i = 0; $i < count($fuel_and_selling_exp); $i++) {
                $fuel_and_selling_exp[0]['actual_per_month'] = number_format(round((float)$ad_and_promotions_per_month->DMBTR / 1000));
                $fuel_and_selling_exp[1]['actual_per_month'] = number_format(round((float)$trans_and_travel_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $fuel_and_selling_exp[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $fuel_and_selling_exp[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $fuel_and_selling_exp[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $fuel_and_selling_exp[0]['actual_past_month'] = number_format(round((float)$ad_and_promotions_past_month->DMBTR / 1000));
                $fuel_and_selling_exp[1]['actual_past_month'] = number_format(round((float)$trans_and_travel_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $fuel_and_selling_exp[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $fuel_and_selling_exp[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_fuel_and_sell_exp += $fuel_and_selling_exp[$i]['budget'];

                $total_current_month_actual_fuel_and_sell_exp += $fuel_and_selling_exp[$i]['actual_per_month'];
                $total_current_month_variance_fuel_and_sell_exp += $var_total_per_month;
                $total_past_month_actual_fuel_and_sell_exp += $fuel_and_selling_exp[$i]['actual_past_month'];
                $total_past_month_variance_fuel_and_sell_exp += $var_total_past_month;
            }
            for ($i = 0; $i < count($utilities); $i++) {
                $utilities[0]['actual_per_month'] = number_format(round((float)$utilities_electricity_per_month->DMBTR / 1000));
                $utilities[1]['actual_per_month'] = number_format(round((float)$utilities_water_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $utilities[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $utilities[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $utilities[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $utilities[0]['actual_past_month'] = number_format(round((float)$utilities_electricity_past_month->DMBTR / 1000));
                $utilities[1]['actual_past_month'] = number_format(round((float)$utilities_water_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $utilities[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $utilities[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_admin_exp += $utilities[$i]['budget'];

                $total_current_month_actual_admin_exp += $utilities[$i]['actual_per_month'];
                $total_current_month_variance_admin_exp += $var_total_per_month;
                $total_past_month_actual_admin_exp += $utilities[$i]['actual_past_month'];
                $total_past_month_variance_admin_exp += $var_total_past_month;
            }
            for ($i = 0; $i < count($admin_expenses); $i++) {
                $admin_expenses[0]['actual_per_month'] = number_format(round((float)$communication_per_month->DMBTR / 1000));
                $admin_expenses[1]['actual_per_month'] = number_format(round((float)$postage_per_month->DMBTR / 1000));
                $admin_expenses[2]['actual_per_month'] = number_format(round((float)$repairs_total_per_month->DMBTR / 1000));
                $admin_expenses[3]['actual_per_month'] = number_format(round((float)$represent_and_entertainment_per_month->DMBTR / 1000));
                $admin_expenses[4]['actual_per_month'] = number_format(round((float)$plog_fees_others_per_month->DMBTR / 1000));
                $admin_expenses[5]['actual_per_month'] = number_format(round((float)$supplies_per_month->DMBTR / 1000));
                $admin_expenses[6]['actual_per_month'] = number_format(round((float)$subscriptions_per_month->DMBTR / 1000));
//            $admin_expenses[7]['actual_per_month'] = number_format(round((float)$solicit_and_donations_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $admin_expenses[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $admin_expenses[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $admin_expenses[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $admin_expenses[0]['actual_past_month'] = number_format(round((float)$communication_past_month->DMBTR / 1000));
                $admin_expenses[1]['actual_past_month'] = number_format(round((float)$postage_past_month->DMBTR / 1000));
                $admin_expenses[2]['actual_past_month'] = number_format(round((float)$repairs_total_past_month->DMBTR / 1000));
                $admin_expenses[3]['actual_past_month'] = number_format(round((float)$represent_and_entertainment_past_month->DMBTR / 1000));
                $admin_expenses[4]['actual_past_month'] = number_format(round((float)$plog_fees_others_past_month->DMBTR / 1000));
                $admin_expenses[5]['actual_past_month'] = number_format(round((float)$supplies_past_month->DMBTR / 1000));
                $admin_expenses[6]['actual_past_month'] = number_format(round((float)$subscriptions_past_month->DMBTR / 1000));
//            $admin_expenses[7]['actual_past_month'] = number_format(round((float)$solicit_and_donations_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $admin_expenses[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $admin_expenses[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_utilities += $admin_expenses[$i]['budget'];

                $total_current_month_actual_utilities += $admin_expenses[$i]['actual_per_month'];
                $total_current_month_variance_utilities += $var_total_per_month;
                $total_past_month_actual_utilities += $admin_expenses[$i]['actual_past_month'];
                $total_past_month_variance_utilities += $var_total_past_month;
            }

            if ($isUserSal === null) {
                $sal_budget_total = 0;
            }
//            dd($sal_budget_total);
            return response()->json([
                'ACCOUNT_PARENT' => $arrRegular,

                'REGULAR' => round((float)$regular->DMBTR / 1000),
                'AGENCY' => round((float)$agency->DMBTR / 1000),
                'OTHER_BENEFITS' => round((float)$other_benefits->DMBTR / 1000),
                'NON_MANDATORY' => round((float)$non_mandatory->DMBTR / 1000),

                'FUEL_VEHICLES' => round((float)$fuel_vehicles->DMBTR / 1000),
                'FUEL_DELIVERY' => round((float)$fuel_delivery->DMBTR / 1000),
                'FUEL_MACHINERY' => round((float)$fuel_machinery->DMBTR / 1000),

                'MAIN_TIRE' => round((float)$main_tire->DMBTR / 1000),
                'MAIN_TIRE_DELIVERY' => round((float)$main_tire_delivery->DMBTR / 1000),
                'MAIN_BATTERY' => round((float)$main_battery->DMBTR / 1000),
                'MAIN_BATTERY_DELIVERY' => round((float)$main_battery_delivery->DMBTR / 1000),
                'MAIN_MACHINERY' => round((float)$main_machinery->DMBTR / 1000),
                'MAIN_OIL_VEHICLES' => round((float)$main_oil_vehicles->DMBTR / 1000),
                'MAIN_OIL_VEHICLES_DELIVERY' => round((float)$main_oil_vehicles_delivery->DMBTR / 1000),

                'REPAIR_VEHICLES' => round((float)$repair_vehicles->DMBTR / 1000),
                'REPAIR_VEHICLES_DELIVERY' => round((float)$repair_vehicles_delivery->DMBTR / 1000),

                'PARKING_OFFICE' => round((float)$parking_office->DMBTR / 1000),
//                'PARKING_FSP_VEHICLES' => round((float)$parking_fsp_vehicles->DMBTR / 1000),
                'PARKING_DELIVERY_VEHICLES' => round((float)$parking_delivery_vehicles->DMBTR / 1000),

                'TOLL_OFFICE' => round((float)$toll_office->DMBTR / 1000),
                'TOLL_FSP_VEHICLES' => round((float)$toll_fsp_vehicles->DMBTR / 1000),
                'TOLL_DELIVERY_VEHICLES' => round((float)$toll_delivery_vehicles->DMBTR / 1000),

                'ADVERTISING_AND_PROMOTIONS' => round((float)$ad_and_promotions->DMBTR / 1000),
                'TRANSPORTATION_AND_TRAVEL' => round((float)$trans_and_travel->DMBTR / 1000),

                'UTILITIES_ELECTRICITY' => round((float)$utilities_electricity->DMBTR / 1000),
                'UTILITIES_WATER' => round((float)$utilities_water->DMBTR / 1000),

                'COMMUNICATION' => round((float)$communication->DMBTR / 1000),
                'POSTAGE' => round((float)$postage->DMBTR / 1000),
                'REPAIRS' => round((float)$repairs_total->DMBTR / 1000),
                'REPRESENTATION_AND_ENTERTAINMENT' => round((float)$represent_and_entertainment->DMBTR / 1000),
                'PLOG_FEES_OTHERS' => round((float)$plog_fees_others->DMBTR / 1000),
                'SUPPLIES' => round((float)$supplies->DMBTR / 1000),
                'SUBSCRIPTIONS' => round((float)$subscriptions->DMBTR / 1000),
//                'SOLICIT_AND_DONATIONS' => round((float)$solicit_and_donations->DMBTR / 1000),

                'REGULAR_PY' => round((float)$regular_PY->DMBTR / 1000),
                'AGENCY_PY' => round((float)$agency_PY->DMBTR / 1000),
                'OTHER_BENEFITS_PY' => round((float)$other_benefits_PY->DMBTR / 1000),
                'NON_MANDATORY_PY' => round((float)$non_mandatory_PY->DMBTR / 1000),

                'FUEL_VEHICLES_PY' => round((float)$fuel_vehicles_PY->DMBTR / 1000),
                'FUEL_DELIVERY_PY' => round((float)$fuel_delivery_PY->DMBTR / 1000),
                'FUEL_MACHINERY_PY' => round((float)$fuel_machinery_PY->DMBTR / 1000),

                'MAIN_TIRE_PY' => round((float)$main_tire_PY->DMBTR / 1000),
                'MAIN_TIRE_DELIVERY_PY' => round((float)$main_tire_delivery_PY->DMBTR / 1000),
                'MAIN_BATTERY_PY' => round((float)$main_battery_PY->DMBTR / 1000),
                'MAIN_BATTERY_DELIVERY_PY' => round((float)$main_battery_delivery_PY->DMBTR / 1000),
                'MAIN_MACHINERY_PY' => round((float)$main_machinery_PY->DMBTR / 1000),
                'MAIN_OIL_VEHICLES_PY' => round((float)$main_oil_vehicles_PY->DMBTR / 1000),
                'MAIN_OIL_VEHICLES_DELIVERY_PY' => round((float)$main_oil_vehicles_delivery_PY->DMBTR / 1000),

                'REPAIR_VEHICLES_PY' => round((float)$repair_vehicles_PY->DMBTR / 1000),
                'REPAIR_VEHICLES_DELIVERY_PY' => round((float)$repair_vehicles_delivery_PY->DMBTR / 1000),

                'PARKING_OFFICE_PY' => round((float)$parking_office_PY->DMBTR / 1000),
//                'PARKING_FSP_VEHICLES_PY' => round((float)$parking_fsp_vehicles_PY->DMBTR / 1000),
                'PARKING_DELIVERY_VEHICLES_PY' => round((float)$parking_delivery_vehicles_PY->DMBTR / 1000),

                'TOLL_OFFICE_PY' => round((float)$toll_office_PY->DMBTR / 1000),
                'TOLL_FSP_VEHICLES_PY' => round((float)$toll_fsp_vehicles_PY->DMBTR / 1000),
                'TOLL_DELIVERY_VEHICLES_PY' => round((float)$toll_delivery_vehicles_PY->DMBTR / 1000),

                'ADVERTISING_AND_PROMOTIONS_PY' => round((float)$ad_and_promotions_PY->DMBTR / 1000),
                'TRANSPORTATION_AND_TRAVEL_PY' => round((float)$trans_and_travel_PY->DMBTR / 1000),

                'UTILITIES_ELECTRICITY_PY' => round((float)$utilities_electricity_PY->DMBTR / 1000),
                'UTILITIES_WATER_PY' => round((float)$utilities_water_PY->DMBTR / 1000),

                'COMMUNICATION_PY' => round((float)$communication_PY->DMBTR / 1000),
                'POSTAGE_PY' => round((float)$postage_PY->DMBTR / 1000),
                'REPAIRS_PY' => round((float)$repairs_total_PY->DMBTR / 1000),
                'REPRESENTATION_AND_ENTERTAINMENT_PY' => round((float)$represent_and_entertainment_PY->DMBTR / 1000),
                'PLOG_FEES_OTHERS_PY' => round((float)$plog_fees_others_PY->DMBTR / 1000),
                'SUPPLIES_PY' => round((float)$supplies_PY->DMBTR / 1000),
                'SUBSCRIPTIONS_PY' => round((float)$subscriptions_PY->DMBTR / 1000),
//                'SOLICIT_AND_DONATIONS_PY' => round((float)$solicit_and_donations_PY->DMBTR / 1000),

                'SALARIES_TOTAL' => $sal_budget_total,
                'FUELS_TOTAL' => $fuel_budget_total,
                'MAINTENANCE_TOTAL' => $main_budget_total,
                'REPAIR_TOTAL' => $repair_budget_total,
                'PARKING_TOTAL' => $parking_budget_total,
                'TOLLING_TOTAL' => $tolling_budget_total,
                'FUEL_AND_SELLING_EXP_TOTAL' => $fuel_and_selling_exp_budget_total,
                'UTILITIES_TOTAL' => $utilities_budget_total,
                'ADMIN_EXP_TOTAL' => $admin_exp_budget_total,

                //VARIANCE
                'SALARIES_TOTAL_VAR' => $total_var_sal,
                'FUELS_TOTAL_VAR' => $total_var_fuel,
                'MAINTENANCE_TOTAL_VAR' => $total_var_main,
                'REPAIR_TOTAL_VAR' => $total_var_repair,
                'PARKING_TOTAL_VAR' => $total_var_parking,
                'TOLLING_TOTAL_VAR' => $total_var_tolling,
                'FUEL_AND_SELLING_EXP_TOTAL_VAR' => $total_var_fuel_and_selling_exp,
                'UTILITIES_TOTAL_VAR' => $total_var_utilities,
                'ADMIN_EXP_TOTAL_VAR' => $total_var_admin_exp,

                //CURRENT MONTH
                'BUDGET_TOTAL_MONTH_SAL' => $total_month_budget_sal,
                'ACTUAL_TOTAL_CURRENT_MONTH_SAL' => $total_current_month_actual_sal,
                'VARIANCE_TOTAL_CURRENT_MONTH_SAL' => $total_current_month_variance_sal,
                'ACTUAL_TOTAL_PAST_MONTH_SAL' => $total_past_month_actual_sal,
                'VARIANCE_TOTAL_PAST_MONTH_SAL' => $total_past_month_variance_sal,

                'BUDGET_TOTAL_MONTH_FUEL' => $total_month_budget_fuel,
                'ACTUAL_TOTAL_CURRENT_MONTH_FUEL' => $total_current_month_actual_fuel,
                'VARIANCE_TOTAL_CURRENT_MONTH_FUEL' => $total_current_month_variance_fuel,
                'ACTUAL_TOTAL_PAST_MONTH_FUEL' => $total_past_month_actual_fuel,
                'VARIANCE_TOTAL_PAST_MONTH_FUEL' => $total_past_month_variance_fuel,

                'BUDGET_TOTAL_MONTH_MAIN' => $total_month_budget_main,
                'ACTUAL_TOTAL_CURRENT_MONTH_MAIN' => $total_current_month_actual_main,
                'VARIANCE_TOTAL_CURRENT_MONTH_MAIN' => $total_current_month_variance_main,
                'ACTUAL_TOTAL_PAST_MONTH_MAIN' => $total_past_month_actual_main,
                'VARIANCE_TOTAL_PAST_MONTH_MAIN' => $total_past_month_variance_main,

                'BUDGET_TOTAL_MONTH_REPAIR' => $total_month_budget_repair,
                'ACTUAL_TOTAL_CURRENT_MONTH_REPAIR' => $total_current_month_actual_repair,
                'VARIANCE_TOTAL_CURRENT_MONTH_REPAIR' => $total_current_month_variance_repair,
                'ACTUAL_TOTAL_PAST_MONTH_REPAIR' => $total_past_month_actual_repair,
                'VARIANCE_TOTAL_PAST_MONTH_REPAIR' => $total_past_month_variance_repair,

                'BUDGET_TOTAL_MONTH_PARKING' => $total_month_budget_parking,
                'ACTUAL_TOTAL_CURRENT_MONTH_PARKING' => $total_current_month_actual_parking,
                'VARIANCE_TOTAL_CURRENT_MONTH_PARKING' => $total_current_month_variance_parking,
                'ACTUAL_TOTAL_PAST_MONTH_PARKING' => $total_past_month_actual_parking,
                'VARIANCE_TOTAL_PAST_MONTH_PARKING' => $total_past_month_variance_parking,

                'BUDGET_TOTAL_MONTH_TOLLING' => $total_month_budget_tolling,
                'ACTUAL_TOTAL_CURRENT_MONTH_TOLLING' => $total_current_month_actual_tolling,
                'VARIANCE_TOTAL_CURRENT_MONTH_TOLLING' => $total_current_month_variance_tolling,
                'ACTUAL_TOTAL_PAST_MONTH_TOLLING' => $total_past_month_actual_tolling,
                'VARIANCE_TOTAL_PAST_MONTH_TOLLING' => $total_past_month_variance_tolling,

                'BUDGET_TOTAL_MONTH_FUEL_AND_SELL_EXP' => $total_month_budget_fuel_and_sell_exp + $total_month_budget_tolling
                    + $total_month_budget_parking + $total_month_budget_repair + $total_month_budget_main + $total_month_budget_fuel,
                'ACTUAL_TOTAL_CURRENT_MONTH_FUEL_AND_SELL_EXP' => $total_current_month_actual_fuel_and_sell_exp + $total_current_month_actual_tolling
                    + $total_current_month_actual_parking + $total_current_month_actual_repair + $total_current_month_actual_main + $total_current_month_actual_fuel,
                'VARIANCE_TOTAL_CURRENT_MONTH_FUEL_AND_SELL_EXP' => $total_current_month_variance_fuel_and_sell_exp + $total_current_month_variance_tolling + $total_current_month_variance_parking
                    + $total_current_month_variance_repair + $total_current_month_variance_main + $total_current_month_variance_fuel,
                'ACTUAL_TOTAL_PAST_MONTH_FUEL_AND_SELL_EXP' => $total_past_month_actual_fuel_and_sell_exp + $total_past_month_actual_tolling + $total_past_month_actual_parking
                    + $total_past_month_actual_repair + $total_past_month_actual_main + $total_past_month_actual_fuel,
                'VARIANCE_TOTAL_PAST_MONTH_FUEL_AND_SELL_EXP' => $total_past_month_variance_fuel_and_sell_exp + $total_past_month_variance_tolling + $total_past_month_variance_parking
                    + $total_past_month_variance_repair + $total_past_month_variance_main + $total_past_month_variance_fuel,

                'BUDGET_TOTAL_MONTH_UTILITIES' => $total_month_budget_utilities,
                'ACTUAL_TOTAL_CURRENT_MONTH_UTILITIES' => $total_current_month_actual_utilities,
                'VARIANCE_TOTAL_CURRENT_MONTH_UTILITIES' => $total_current_month_variance_utilities,
                'ACTUAL_TOTAL_PAST_MONTH_UTILITIES' => $total_past_month_actual_utilities,
                'VARIANCE_TOTAL_PAST_MONTH_UTILITIES' => $total_past_month_variance_utilities,

                'BUDGET_TOTAL_MONTH_ADMIN_EXP' => $total_month_budget_admin_exp + $total_month_budget_utilities,
                'ACTUAL_TOTAL_CURRENT_MONTH_ADMIN_EXP' => $total_current_month_actual_admin_exp + $total_current_month_actual_utilities,
                'VARIANCE_TOTAL_CURRENT_MONTH_ADMIN_EXP' => $total_current_month_variance_admin_exp + $total_current_month_variance_utilities,
                'ACTUAL_TOTAL_PAST_MONTH_ADMIN_EXP' => $total_past_month_actual_admin_exp + $total_past_month_actual_utilities,
                'VARIANCE_TOTAL_PAST_MONTH_ADMIN_EXP' => $total_past_month_variance_admin_exp + $total_past_month_variance_utilities,

                'BUDGET_TOTAL_MONTH_OPERATION' => $total_month_budget_sal + $total_month_budget_fuel_and_sell_exp + $total_month_budget_tolling
                    + $total_month_budget_parking + $total_month_budget_repair + $total_month_budget_main + $total_month_budget_fuel
                    + $total_current_month_actual_admin_exp + $total_current_month_actual_utilities,
                'ACTUAL_TOTAL_CURRENT_MONTH_OPERATION' => $total_current_month_actual_admin_exp + $total_current_month_actual_utilities + $total_current_month_actual_sal
                    + $total_current_month_actual_fuel_and_sell_exp + $total_current_month_actual_tolling
                    + $total_current_month_actual_parking + $total_current_month_actual_repair + $total_current_month_actual_main + $total_current_month_actual_fuel,
                'VARIANCE_TOTAL_CURRENT_MONTH_OPERATION' => $total_current_month_variance_admin_exp + $total_current_month_variance_utilities + $total_current_month_variance_sal
                    + $total_current_month_variance_fuel_and_sell_exp + $total_current_month_variance_tolling + $total_current_month_variance_parking
                    + $total_current_month_variance_repair + $total_current_month_variance_main + $total_current_month_variance_fuel,
                'ACTUAL_TOTAL_PAST_MONTH_OPERATION' => $total_past_month_actual_admin_exp + $total_past_month_actual_utilities + $total_past_month_actual_sal
                    + $total_past_month_actual_fuel_and_sell_exp + $total_past_month_actual_tolling + $total_past_month_actual_parking
                    + $total_past_month_actual_repair + $total_past_month_actual_main + $total_past_month_actual_fuel,
                'VARIANCE_TOTAL_PAST_MONTH_OPERATION' => $total_past_month_variance_admin_exp + $total_past_month_variance_utilities + $total_past_month_variance_sal
                    + $total_past_month_variance_fuel_and_sell_exp + $total_past_month_variance_tolling + $total_past_month_variance_parking
                    + $total_past_month_variance_repair + $total_past_month_variance_main + $total_past_month_variance_fuel,
            ]);
        }
        else {
            //CY_ACTUAL
            $regular = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            //PY_ACTUAL
            $regular_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $PY_DATE)
//                    ->where('GJAHR', $currentYear - 1)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $PY_DATE)
                    ->where('GJAHR', $currentYear - 1)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $PY_DATE)
//                    ->where('GJAHR', $currentYear - 1)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $static_budgets = StaticBudget::where('company', $company)->first();
            $admin_expenses = AdminExpense::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $fuels = Fuels::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $fuel_and_selling_exp = FuelAndSellingExp::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $maintenance = Maintenance::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $parking = Parking::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $repairs = Repairs::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $salaries = Salary::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $tolling = Tolling::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
            $utilities = Utilities::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
//        $static_budgets = StaticBudget::from('static_budgets as stat')
//             ->join('admin_expenses as ad','stat.id','=','ad.static_budget_id')
//            ->leftjoin('fuels as fu','stat.id','=','fu.static_budget_id')
//            ->leftjoin('fuel_and_selling_exp as fu_and_sell','stat.id','=','fu_and_sell.static_budget_id')
//            ->leftjoin('maintenance as main','stat.id','=','main.static_budget_id')
//            ->leftjoin('parking as pa','stat.id','=','pa.static_budget_id')
//            ->leftjoin('repairs as re','stat.id','=','re.static_budget_id')
//            ->leftjoin('salaries as sal','stat.id','=','sal.static_budget_id')
//            ->leftjoin('tolling as tol','stat.id','=','tol.static_budget_id')
//            ->leftjoin('utilities as ut','stat.id','=','ut.static_budget_id')
//            ->where('stat.company',$company)
//            ->get();
//        dd($static_budgets);

            $sal_budget_total = 0;
            $fuel_budget_total = 0;
            $main_budget_total = 0;
            $repair_budget_total = 0;
            $parking_budget_total = 0;
            $tolling_budget_total = 0;
            $fuel_and_selling_exp_budget_total = 0;
            $utilities_budget_total = 0;
            $admin_exp_budget_total = 0;
            //VARIANCE
            $total_var_sal = 0;
            $total_var_fuel = 0;
            $total_var_main = 0;
            $total_var_repair = 0;
            $total_var_parking = 0;
            $total_var_tolling = 0;
            $total_var_fuel_and_selling_exp = 0;
            $total_var_utilities = 0;
            $total_var_admin_exp = 0;

            for ($i = 0; $i < count($salaries); $i++) {
//                    return response()->json(round((float)$agency->DMBTR / 1000));
                $salaries[0]['total_cy'] = number_format(round((float)$regular->DMBTR / 1000));
                $salaries[1]['total_cy'] = number_format(round((float)$agency->DMBTR / 1000));
                $salaries[2]['total_cy'] = number_format(round((float)$other_benefits->DMBTR / 1000));
                $salaries[3]['total_cy'] = number_format(round((float)$non_mandatory->DMBTR / 1000));
                $salaries[$i]['budget_total'] = number_format($salaries[$i]['budget'] * (int)$currentMonth);

                $total = $salaries[$i]['budget'] * (int)$currentMonth;

                $salaries[0]['total_py'] = number_format(round((float)$regular_PY->DMBTR / 1000));
                $salaries[1]['total_py'] = number_format(round((float)$agency_PY->DMBTR / 1000));
                $salaries[2]['total_py'] = number_format(round((float)$other_benefits_PY->DMBTR / 1000));
                $salaries[3]['total_py'] = number_format(round((float)$non_mandatory_PY->DMBTR / 1000));

                //VARIANCE = CY - BUDGET
                $convert_cy = str_replace(',', '', $salaries[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $salaries[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $salaries[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_sal += $var_total; // variance
                $sal_budget_total += $total; // add all value inside in salaries array

                //% OF VARIANCE
                // VARIANCE / BUDGET * 100 to get the percentage(%)
                //$convert_variance = str_replace([',','(',')'], '', $salaries[$i]['variance']);
                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $salaries[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $salaries[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $salaries[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $salaries[$i]['change_percent'] = $var_total_change_percent . "%";
            }

            for ($i = 0; $i < count($fuels); $i++) {
                $fuels[0]['total_cy'] = number_format(round((float)$fuel_vehicles->DMBTR / 1000));
                $fuels[1]['total_cy'] = number_format(round((float)$fuel_delivery->DMBTR / 1000));
                $fuels[2]['total_cy'] = number_format(round((float)$fuel_machinery->DMBTR / 1000));
                $fuels[$i]['budget_total'] = number_format($fuels[$i]['budget'] * (int)$currentMonth);
                $total = $fuels[$i]['budget'] * (int)$currentMonth;

                $fuels[0]['total_py'] = number_format(round((float)$fuel_vehicles_PY->DMBTR / 1000));
                $fuels[1]['total_py'] = number_format(round((float)$fuel_delivery_PY->DMBTR / 1000));
                $fuels[2]['total_py'] = number_format(round((float)$fuel_machinery_PY->DMBTR / 1000));

                $convert_cy = str_replace(',', '', $fuels[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $fuels[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $fuels[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_fuel += $var_total; // variance
                $fuel_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $fuels[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $fuels[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $fuels[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $fuels[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($maintenance); $i++) {
                $maintenance[0]['total_cy'] = number_format(round((float)$main_tire->DMBTR / 1000));
                $maintenance[1]['total_cy'] = number_format(round((float)$main_tire_delivery->DMBTR / 1000));
                $maintenance[2]['total_cy'] = number_format(round((float)$main_battery->DMBTR / 1000));
                $maintenance[3]['total_cy'] = number_format(round((float)$main_battery_delivery->DMBTR / 1000));
                $maintenance[4]['total_cy'] = number_format(round((float)$main_machinery->DMBTR / 1000));
                $maintenance[5]['total_cy'] = number_format(round((float)$main_oil_vehicles->DMBTR / 1000));
                $maintenance[6]['total_cy'] = number_format(round((float)$main_oil_vehicles_delivery->DMBTR / 1000));
                $maintenance[$i]['budget_total'] = number_format($maintenance[$i]['budget'] * (int)$currentMonth);
                $total = $maintenance[$i]['budget'] * (int)$currentMonth;

                $maintenance[0]['total_py'] = number_format(round((float)$main_tire_PY->DMBTR / 1000));
                $maintenance[1]['total_py'] = number_format(round((float)$main_tire_delivery_PY->DMBTR / 1000));
                $maintenance[2]['total_py'] = number_format(round((float)$main_battery_PY->DMBTR / 1000));
                $maintenance[3]['total_py'] = number_format(round((float)$main_battery_delivery_PY->DMBTR / 1000));
                $maintenance[4]['total_py'] = number_format(round((float)$main_machinery_PY->DMBTR / 1000));
                $maintenance[5]['total_py'] = number_format(round((float)$main_oil_vehicles_PY->DMBTR / 1000));
                $maintenance[6]['total_py'] = number_format(round((float)$main_oil_vehicles_delivery_PY->DMBTR / 1000));

                $convert_cy = str_replace(',', '', $maintenance[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $maintenance[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $maintenance[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_main += $var_total; // variance
                $main_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $maintenance[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $maintenance[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $maintenance[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $maintenance[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($repairs); $i++) {
                $repairs[0]['total_cy'] = number_format(round((float)$repair_vehicles->DMBTR / 1000));
                $repairs[1]['total_cy'] = number_format(round((float)$repair_vehicles_delivery->DMBTR / 1000));
                $repairs[$i]['budget_total'] = number_format($repairs[$i]['budget'] * (int)$currentMonth);
                $total = $repairs[$i]['budget'] * (int)$currentMonth;

                $repairs[0]['total_py'] = number_format(round((float)$repair_vehicles_PY->DMBTR / 1000));
                $repairs[1]['total_py'] = number_format(round((float)$repair_vehicles_delivery_PY->DMBTR / 1000));

                $convert_cy = str_replace(',', '', $repairs[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $repairs[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $repairs[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_repair += $var_total; // variance
                $repair_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $repairs[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $repairs[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $repairs[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $repairs[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($parking); $i++) {
                $parking[0]['total_cy'] = number_format(round((float)$parking_office->DMBTR / 1000));
//                $parking[1]['total_cy'] = number_format(round((float)$parking_fsp_vehicles->DMBTR / 1000));
                $parking[1]['total_cy'] = number_format(round((float)$parking_delivery_vehicles->DMBTR / 1000));
                $parking[$i]['budget_total'] = number_format($parking[$i]['budget'] * (int)$currentMonth);

                $parking[0]['total_py'] = number_format(round((float)$parking_office_PY->DMBTR / 1000));
//                $parking[1]['total_py'] = number_format(round((float)$parking_fsp_vehicles_PY->DMBTR / 1000));
                $parking[1]['total_py'] = number_format(round((float)$parking_delivery_vehicles_PY->DMBTR / 1000));
                $total = $parking[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $parking[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $parking[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $parking[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_parking += $var_total; // variance
                $parking_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $parking[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $parking[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $parking[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $parking[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($tolling); $i++) {
                $tolling[0]['total_cy'] = number_format(round((float)$toll_office->DMBTR / 1000));
                $tolling[1]['total_cy'] = number_format(round((float)$toll_fsp_vehicles->DMBTR / 1000));
                $tolling[2]['total_cy'] = number_format(round((float)$toll_delivery_vehicles->DMBTR / 1000));
                $tolling[$i]['budget_total'] = number_format($tolling[$i]['budget'] * (int)$currentMonth);

                $tolling[0]['total_py'] = number_format(round((float)$toll_office_PY->DMBTR / 1000));
                $tolling[1]['total_py'] = number_format(round((float)$toll_fsp_vehicles_PY->DMBTR / 1000));
                $tolling[2]['total_py'] = number_format(round((float)$toll_delivery_vehicles_PY->DMBTR / 1000));
                $total = $tolling[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $tolling[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $tolling[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $tolling[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_tolling += $var_total; // variance
                $tolling_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $tolling[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $tolling[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $tolling[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $tolling[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($fuel_and_selling_exp); $i++) {
                $fuel_and_selling_exp[0]['total_cy'] = number_format(round((float)$ad_and_promotions->DMBTR / 1000));
                $fuel_and_selling_exp[1]['total_cy'] = number_format(round((float)$trans_and_travel->DMBTR / 1000));
                $fuel_and_selling_exp[$i]['budget_total'] = number_format($fuel_and_selling_exp[$i]['budget'] * (int)$currentMonth);

                $fuel_and_selling_exp[0]['total_py'] = number_format(round((float)$ad_and_promotions_PY->DMBTR / 1000));
                $fuel_and_selling_exp[1]['total_py'] = number_format(round((float)$trans_and_travel_PY->DMBTR / 1000));
                $total = $fuel_and_selling_exp[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $fuel_and_selling_exp[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $fuel_and_selling_exp[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $fuel_and_selling_exp[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_fuel_and_selling_exp += $var_total; // variance
                $fuel_and_selling_exp_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $fuel_and_selling_exp[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $fuel_and_selling_exp[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $fuel_and_selling_exp[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $fuel_and_selling_exp[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($utilities); $i++) {
                $utilities[0]['total_cy'] = number_format(round((float)$utilities_electricity->DMBTR / 1000));
                $utilities[1]['total_cy'] = number_format(round((float)$utilities_water->DMBTR / 1000));
                $utilities[$i]['budget_total'] = number_format($utilities[$i]['budget'] * (int)$currentMonth);

                $utilities[0]['total_py'] = number_format(round((float)$utilities_electricity_PY->DMBTR / 1000));
                $utilities[1]['total_py'] = number_format(round((float)$utilities_water_PY->DMBTR / 1000));
                $total = $utilities[$i]['budget'] * (int)$currentMonth;

                $convert_cy = str_replace(',', '', $utilities[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $utilities[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $utilities[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_utilities += $var_total; // variance
                $utilities_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $utilities[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $utilities[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $utilities[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $utilities[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            for ($i = 0; $i < count($admin_expenses); $i++) {
                $admin_expenses[0]['total_cy'] = number_format(round((float)$communication->DMBTR / 1000));
                $admin_expenses[1]['total_cy'] = number_format(round((float)$postage->DMBTR / 1000));
                $admin_expenses[2]['total_cy'] = number_format(round((float)$repairs_total->DMBTR / 1000));
                $admin_expenses[3]['total_cy'] = number_format(round((float)$represent_and_entertainment->DMBTR / 1000));
                $admin_expenses[4]['total_cy'] = number_format(round((float)$plog_fees_others->DMBTR / 1000));
                $admin_expenses[5]['total_cy'] = number_format(round((float)$supplies->DMBTR / 1000));
                $admin_expenses[6]['total_cy'] = number_format(round((float)$subscriptions->DMBTR / 1000));
//            $admin_expenses[7]['total_cy'] = number_format(round((float)$solicit_and_donations->DMBTR / 1000));
                $admin_expenses[$i]['budget_total'] = number_format($admin_expenses[$i]['budget'] * (int)$currentMonth);

                $admin_expenses[0]['total_py'] = number_format(round((float)$communication_PY->DMBTR / 1000));
                $admin_expenses[1]['total_py'] = number_format(round((float)$postage_PY->DMBTR / 1000));
                $admin_expenses[2]['total_py'] = number_format(round((float)$repairs_total_PY->DMBTR / 1000));
                $admin_expenses[3]['total_py'] = number_format(round((float)$represent_and_entertainment_PY->DMBTR / 1000));
                $admin_expenses[4]['total_py'] = number_format(round((float)$plog_fees_others_PY->DMBTR / 1000));
                $admin_expenses[5]['total_py'] = number_format(round((float)$supplies_PY->DMBTR / 1000));
                $admin_expenses[6]['total_py'] = number_format(round((float)$subscriptions_PY->DMBTR / 1000));
//            $admin_expenses[7]['total_py'] = number_format(round((float)$solicit_and_donations_PY->DMBTR / 1000));
                $total = $admin_expenses[$i]['budget'] * (int)$currentMonth;
                $convert_cy = str_replace(',', '', $admin_expenses[$i]['total_cy']);
                $convert_num_cy = (int)$convert_cy;

                $convert_budget_total = str_replace(',', '', $admin_expenses[$i]['budget_total']);
                $convert_num_budget_total = (int)$convert_budget_total;

                $var_total = $convert_num_cy - $convert_num_budget_total;
                $admin_expenses[$i]['variance'] = $var_total > 0 ? "(" . abs($var_total) . ")" : $var_total == 0 ? abs($var_total) : "(" . abs($var_total) . ")";
                //Is "$var_total": greater than zero? Yes? Return $var_total with parenthesis to "sign".
                //Otherwise, does "$var_total" equal zero?  Yes?  Return abs($var_total) / 0 to "sign".
                //Otherwise, return "(" . abs($var_total) . ")" to "sign".
                $total_var_admin_exp += $var_total; // variance
                $admin_exp_budget_total += $total;

                $catch_it = null;
                $check_var = null;
                try {
                    $check_var = (int)round($var_total / (int)$convert_budget_total * 100);
                } catch (\Exception $e) {
                    $catch_it = $e->getMessage();
                }
                $var_total_percent = 0;
                if ($catch_it !== null && $check_var === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_percent = 100;
                } else {
                    $var_total_percent = (int)round($var_total / (int)$convert_budget_total * 100);
                }
                $admin_expenses[$i]['percent_variance'] = $var_total_percent . "%";

                //CHANGE [ CY_ACTUAL - PY_ACTUAL ]
                $convert_py = str_replace(',', '', $admin_expenses[$i]['total_py']);
                $convert_num_py = (int)$convert_py;

                $change_total = $convert_num_cy - $convert_num_py;
                $admin_expenses[$i]['change'] = $change_total > 0 ? "(" . abs($change_total) . ")" : $change_total == 0 ? abs($change_total) : "(" . abs($change_total) . ")";

                //% OF CHANGE [CHANGE / PY ACTUAL] * 100
                $catch_it_change_percent = null;
                $check_var_change_percent = null;
                try {
                    $check_var_change_percent = (int)round($change_total / $convert_num_py * 100);
                } catch (\Exception $e) {
                    $catch_it_change_percent = $e->getMessage();
                }
                $var_total_change_percent = 0;
                if ($catch_it_change_percent !== null && $check_var_change_percent === null) {
                    // not null which means it has an error, so the default value will be 100%
                    //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                    $var_total_change_percent = 100;
                } else {
                    $var_total_change_percent = (int)round($change_total / $convert_num_py * 100);
                }
                $admin_expenses[$i]['change_percent'] = $var_total_change_percent . "%";
            }
            $accArr = [
                'SALARIES_CY' => $salaries ?? [],
                'FUELS_CY' => $fuels ?? [],
                'MAINTENANCE_CY' => $maintenance ?? [],
                'REPAIRS_CY' => $repairs ?? [],
                'PARKING_CY' => $parking ?? [],
                'TOLLING_CY' => $tolling ?? [],
                'FUEL_AND_SELLING_EXP_CY' => $fuel_and_selling_exp ?? [],
                'UTILITIES_CY' => $utilities ?? [],
                'ADMIN_EXPENSE_CY' => $admin_expenses ?? [],
            ];
            $arrRegular = [
                'ACCOUNT' => [
                    'DETAILS' => $accArr,
                ],
            ];

            //BUDGET DETAILS PER MONTH
            $regular_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                $query->where('BUKRS', $company)
                    ->where('BUDAT', '<=', $currentDate)
                    ->where('GJAHR', $currentYear)
                    ->where('MONAT', $currentMonth)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations_per_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            //BUDGET DETAILS PAST MONTH
            $regular_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600001, 600006]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $agency_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600004, 600005, 600008]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $other_benefits_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $non_mandatory_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600500,600501,600502,600503]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602500]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602501]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $fuel_machinery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602400]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_tire_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602401]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602403]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_battery_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602404]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_machinery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602405]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602406]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $main_oil_vehicles_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602407]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602302]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repair_vehicles_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602303]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $parking_office_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $parking_fsp_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
//                $query->where('BUKRS', $company)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth - 1)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $parking_delivery_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602702]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_office_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602800]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_fsp_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $toll_delivery_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602802]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $ad_and_promotions_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $trans_and_travel_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_water_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [601701]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $communication_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $postage_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602100, 602101]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $repairs_total_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $represent_and_entertainment_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603500, 603501, 603502]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $plog_fees_others_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603002]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $supplies_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $subscriptions_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                    ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                    ->whereIn('HKONT', [603800, 603801]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

//            $solicit_and_donations_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
//                $query->where('BUKRS', $company)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth - 1)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

            $total_month_budget_sal = 0;
            $total_current_month_actual_sal = 0;
            $total_current_month_variance_sal = 0;
            $total_past_month_actual_sal = 0;
            $total_past_month_variance_sal = 0;

            $total_month_budget_fuel = 0;
            $total_current_month_actual_fuel = 0;
            $total_current_month_variance_fuel = 0;
            $total_past_month_actual_fuel = 0;
            $total_past_month_variance_fuel = 0;

            $total_month_budget_main = 0;
            $total_current_month_actual_main = 0;
            $total_current_month_variance_main = 0;
            $total_past_month_actual_main = 0;
            $total_past_month_variance_main = 0;

            $total_month_budget_repair = 0;
            $total_current_month_actual_repair = 0;
            $total_current_month_variance_repair = 0;
            $total_past_month_actual_repair = 0;
            $total_past_month_variance_repair = 0;

            $total_month_budget_parking = 0;
            $total_current_month_actual_parking = 0;
            $total_current_month_variance_parking = 0;
            $total_past_month_actual_parking = 0;
            $total_past_month_variance_parking = 0;

            $total_month_budget_tolling = 0;
            $total_current_month_actual_tolling = 0;
            $total_current_month_variance_tolling = 0;
            $total_past_month_actual_tolling = 0;
            $total_past_month_variance_tolling = 0;

            $total_month_budget_fuel_and_sell_exp = 0;
            $total_current_month_actual_fuel_and_sell_exp = 0;
            $total_current_month_variance_fuel_and_sell_exp = 0;
            $total_past_month_actual_fuel_and_sell_exp = 0;
            $total_past_month_variance_fuel_and_sell_exp = 0;

            $total_month_budget_utilities = 0;
            $total_current_month_actual_utilities = 0;
            $total_current_month_variance_utilities = 0;
            $total_past_month_actual_utilities = 0;
            $total_past_month_variance_utilities = 0;

            $total_month_budget_admin_exp = 0;
            $total_current_month_actual_admin_exp = 0;
            $total_current_month_variance_admin_exp = 0;
            $total_past_month_actual_admin_exp = 0;
            $total_past_month_variance_admin_exp = 0;

            for ($i = 0; $i < count($salaries); $i++) {
                //CURRENT MONTH
                $salaries[0]['actual_per_month'] = number_format(round((float)$regular_per_month->DMBTR / 1000));
                $salaries[1]['actual_per_month'] = number_format(round((float)$agency_per_month->DMBTR / 1000));
                $salaries[2]['actual_per_month'] = number_format(round((float)$other_benefits_per_month->DMBTR / 1000));
                $salaries[3]['actual_per_month'] = number_format(round((float)$non_mandatory_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $salaries[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $salaries[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $salaries[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $salaries[0]['actual_past_month'] = number_format(round((float)$regular_past_month->DMBTR / 1000));
                $salaries[1]['actual_past_month'] = number_format(round((float)$agency_past_month->DMBTR / 1000));
                $salaries[2]['actual_past_month'] = number_format(round((float)$other_benefits_past_month->DMBTR / 1000));
                $salaries[3]['actual_past_month'] = number_format(round((float)$non_mandatory_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $salaries[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $salaries[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_sal += $salaries[$i]['budget'];

                $total_current_month_actual_sal += $salaries[$i]['actual_per_month'];
                $total_current_month_variance_sal += $var_total_per_month;
                $total_past_month_actual_sal += $salaries[$i]['actual_past_month'];
                $total_past_month_variance_sal += $var_total_past_month;
            }

            for ($i = 0; $i < count($fuels); $i++) {
                //CURRENT MONTH
                $fuels[0]['actual_per_month'] = number_format(round((float)$fuel_vehicles_per_month->DMBTR / 1000));
                $fuels[1]['actual_per_month'] = number_format(round((float)$fuel_delivery_per_month->DMBTR / 1000));
                $fuels[2]['actual_per_month'] = number_format(round((float)$fuel_machinery_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $fuels[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $fuels[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $fuels[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $fuels[0]['actual_past_month'] = number_format(round((float)$fuel_vehicles_past_month->DMBTR / 1000));
                $fuels[1]['actual_past_month'] = number_format(round((float)$fuel_delivery_past_month->DMBTR / 1000));
                $fuels[2]['actual_past_month'] = number_format(round((float)$fuel_machinery_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $fuels[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $fuels[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_fuel += $fuels[$i]['budget'];

                $total_current_month_actual_fuel += $fuels[$i]['actual_per_month'];
                $total_current_month_variance_fuel += $var_total_per_month;
                $total_past_month_actual_fuel += $fuels[$i]['actual_past_month'];
                $total_past_month_variance_fuel += $var_total_past_month;
            }
            for ($i = 0; $i < count($maintenance); $i++) {
                $maintenance[0]['actual_per_month'] = number_format(round((float)$main_tire_per_month->DMBTR / 1000));
                $maintenance[1]['actual_per_month'] = number_format(round((float)$main_tire_delivery_per_month->DMBTR / 1000));
                $maintenance[2]['actual_per_month'] = number_format(round((float)$main_battery_per_month->DMBTR / 1000));
                $maintenance[3]['actual_per_month'] = number_format(round((float)$main_battery_delivery_per_month->DMBTR / 1000));
                $maintenance[4]['actual_per_month'] = number_format(round((float)$main_machinery_per_month->DMBTR / 1000));
                $maintenance[5]['actual_per_month'] = number_format(round((float)$main_oil_vehicles_per_month->DMBTR / 1000));
                $maintenance[6]['actual_per_month'] = number_format(round((float)$main_oil_vehicles_delivery_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $maintenance[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $maintenance[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $maintenance[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $maintenance[0]['actual_past_month'] = number_format(round((float)$main_tire_past_month->DMBTR / 1000));
                $maintenance[1]['actual_past_month'] = number_format(round((float)$main_tire_delivery_past_month->DMBTR / 1000));
                $maintenance[2]['actual_past_month'] = number_format(round((float)$main_battery_past_month->DMBTR / 1000));
                $maintenance[3]['actual_past_month'] = number_format(round((float)$main_battery_delivery_past_month->DMBTR / 1000));
                $maintenance[4]['actual_past_month'] = number_format(round((float)$main_machinery_past_month->DMBTR / 1000));
                $maintenance[5]['actual_past_month'] = number_format(round((float)$main_oil_vehicles_past_month->DMBTR / 1000));
                $maintenance[6]['actual_past_month'] = number_format(round((float)$main_oil_vehicles_delivery_past_month->DMBTR / 1000));;

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $maintenance[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $maintenance[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_main += $maintenance[$i]['budget'];

                $total_current_month_actual_main += $maintenance[$i]['actual_per_month'];
                $total_current_month_variance_main += $var_total_per_month;
                $total_past_month_actual_main += $maintenance[$i]['actual_past_month'];
                $total_past_month_variance_main += $var_total_past_month;
            }
            for ($i = 0; $i < count($repairs); $i++) {
                $repairs[0]['actual_per_month'] = number_format(round((float)$repair_vehicles_per_month->DMBTR / 1000));
                $repairs[1]['actual_per_month'] = number_format(round((float)$repair_vehicles_delivery_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $repairs[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $repairs[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $repairs[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $repairs[0]['actual_past_month'] = number_format(round((float)$repair_vehicles_past_month->DMBTR / 1000));
                $repairs[1]['actual_past_month'] = number_format(round((float)$repair_vehicles_delivery_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $repairs[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $repairs[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_repair += $repairs[$i]['budget'];

                $total_current_month_actual_repair += $repairs[$i]['actual_per_month'];
                $total_current_month_variance_repair += $var_total_per_month;
                $total_past_month_actual_repair += $repairs[$i]['actual_past_month'];
                $total_past_month_variance_repair += $var_total_past_month;
            }
            for ($i = 0; $i < count($parking); $i++) {
                $parking[0]['actual_per_month'] = number_format(round((float)$parking_office_per_month->DMBTR / 1000));
//                $parking[1]['actual_per_month'] = number_format(round((float)$parking_fsp_vehicles_per_month->DMBTR / 1000));
                $parking[1]['actual_per_month'] = number_format(round((float)$parking_delivery_vehicles_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $parking[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $parking[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $parking[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $parking[0]['actual_past_month'] = number_format(round((float)$parking_office_past_month->DMBTR / 1000));
//                $parking[1]['actual_past_month'] = number_format(round((float)$parking_fsp_vehicles_past_month->DMBTR / 1000));
                $parking[1]['actual_past_month'] = number_format(round((float)$parking_delivery_vehicles_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $parking[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $parking[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_parking += $parking[$i]['budget'];

                $total_current_month_actual_parking += $parking[$i]['actual_per_month'];
                $total_current_month_variance_parking += $var_total_per_month;
                $total_past_month_actual_parking += $parking[$i]['actual_past_month'];
                $total_past_month_variance_parking += $var_total_past_month;
            }
            for ($i = 0; $i < count($tolling); $i++) {
                $tolling[0]['actual_per_month'] = number_format(round((float)$toll_office_per_month->DMBTR / 1000));
                $tolling[1]['actual_per_month'] = number_format(round((float)$toll_fsp_vehicles_per_month->DMBTR / 1000));
                $tolling[2]['actual_per_month'] = number_format(round((float)$toll_delivery_vehicles_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $tolling[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $tolling[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $tolling[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $tolling[0]['actual_past_month'] = number_format(round((float)$toll_office_past_month->DMBTR / 1000));
                $tolling[1]['actual_past_month'] = number_format(round((float)$toll_fsp_vehicles_past_month->DMBTR / 1000));
                $tolling[2]['actual_past_month'] = number_format(round((float)$toll_delivery_vehicles_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $tolling[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $tolling[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_tolling += $tolling[$i]['budget'];

                $total_current_month_actual_tolling += $tolling[$i]['actual_per_month'];
                $total_current_month_variance_tolling += $var_total_per_month;
                $total_past_month_actual_tolling += $tolling[$i]['actual_past_month'];
                $total_past_month_variance_tolling += $var_total_past_month;
            }
            for ($i = 0; $i < count($fuel_and_selling_exp); $i++) {
                $fuel_and_selling_exp[0]['actual_per_month'] = number_format(round((float)$ad_and_promotions_per_month->DMBTR / 1000));
                $fuel_and_selling_exp[1]['actual_per_month'] = number_format(round((float)$trans_and_travel_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $fuel_and_selling_exp[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $fuel_and_selling_exp[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $fuel_and_selling_exp[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $fuel_and_selling_exp[0]['actual_past_month'] = number_format(round((float)$ad_and_promotions_past_month->DMBTR / 1000));
                $fuel_and_selling_exp[1]['actual_past_month'] = number_format(round((float)$trans_and_travel_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $fuel_and_selling_exp[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $fuel_and_selling_exp[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_fuel_and_sell_exp += $fuel_and_selling_exp[$i]['budget'];

                $total_current_month_actual_fuel_and_sell_exp += $fuel_and_selling_exp[$i]['actual_per_month'];
                $total_current_month_variance_fuel_and_sell_exp += $var_total_per_month;
                $total_past_month_actual_fuel_and_sell_exp += $fuel_and_selling_exp[$i]['actual_past_month'];
                $total_past_month_variance_fuel_and_sell_exp += $var_total_past_month;
            }
            for ($i = 0; $i < count($utilities); $i++) {
                $utilities[0]['actual_per_month'] = number_format(round((float)$utilities_electricity_per_month->DMBTR / 1000));
                $utilities[1]['actual_per_month'] = number_format(round((float)$utilities_water_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $utilities[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $utilities[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $utilities[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $utilities[0]['actual_past_month'] = number_format(round((float)$utilities_electricity_past_month->DMBTR / 1000));
                $utilities[1]['actual_past_month'] = number_format(round((float)$utilities_water_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $utilities[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $utilities[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_utilities += $utilities[$i]['budget'];

                $total_current_month_actual_utilities += $utilities[$i]['actual_per_month'];
                $total_current_month_variance_utilities += $var_total_per_month;
                $total_past_month_actual_utilities += $utilities[$i]['actual_past_month'];
                $total_past_month_variance_utilities += $var_total_past_month;
            }
            for ($i = 0; $i < count($admin_expenses); $i++) {
                $admin_expenses[0]['actual_per_month'] = number_format(round((float)$communication_per_month->DMBTR / 1000));
                $admin_expenses[1]['actual_per_month'] = number_format(round((float)$postage_per_month->DMBTR / 1000));
                $admin_expenses[2]['actual_per_month'] = number_format(round((float)$repairs_total_per_month->DMBTR / 1000));
                $admin_expenses[3]['actual_per_month'] = number_format(round((float)$represent_and_entertainment_per_month->DMBTR / 1000));
                $admin_expenses[4]['actual_per_month'] = number_format(round((float)$plog_fees_others_per_month->DMBTR / 1000));
                $admin_expenses[5]['actual_per_month'] = number_format(round((float)$supplies_per_month->DMBTR / 1000));
                $admin_expenses[6]['actual_per_month'] = number_format(round((float)$subscriptions_per_month->DMBTR / 1000));
//            $admin_expenses[7]['actual_per_month'] = number_format(round((float)$solicit_and_donations_per_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_budget = str_replace(',', '', $admin_expenses[$i]['budget']);
                $convert_num_budget = (int)$convert_budget;

                $convert_actual = str_replace(',', '', $admin_expenses[$i]['actual_per_month']);
                $convert_num_actual = (int)$convert_actual;

                $var_total_per_month = $convert_num_budget - $convert_num_actual;
                $admin_expenses[$i]['variance_per_month'] = $var_total_per_month > 0 ? "(" . abs($var_total_per_month) . ")" : $var_total_per_month == 0 ? abs($var_total_per_month) : "(" . abs($var_total_per_month) . ")";

                //PAST MONTH
                $admin_expenses[0]['actual_past_month'] = number_format(round((float)$communication_past_month->DMBTR / 1000));
                $admin_expenses[1]['actual_past_month'] = number_format(round((float)$postage_past_month->DMBTR / 1000));
                $admin_expenses[2]['actual_past_month'] = number_format(round((float)$repairs_total_past_month->DMBTR / 1000));
                $admin_expenses[3]['actual_past_month'] = number_format(round((float)$represent_and_entertainment_past_month->DMBTR / 1000));
                $admin_expenses[4]['actual_past_month'] = number_format(round((float)$plog_fees_others_past_month->DMBTR / 1000));
                $admin_expenses[5]['actual_past_month'] = number_format(round((float)$supplies_past_month->DMBTR / 1000));
                $admin_expenses[6]['actual_past_month'] = number_format(round((float)$subscriptions_past_month->DMBTR / 1000));
//            $admin_expenses[7]['actual_past_month'] = number_format(round((float)$solicit_and_donations_past_month->DMBTR / 1000));

                //VARIANCE [BUDGET - ACTUAL]
                $convert_actual_past = str_replace(',', '', $admin_expenses[$i]['actual_past_month']);
                $convert_num_actual_past = (int)$convert_actual_past;

                $var_total_past_month = $convert_budget - $convert_num_actual_past;
                $admin_expenses[$i]['variance_past_month'] = $var_total_past_month > 0 ? "(" . abs($var_total_past_month) . ")" : $var_total_past_month == 0 ? abs($var_total_past_month) : "(" . abs($var_total_past_month) . ")";

                $total_month_budget_admin_exp += $admin_expenses[$i]['budget'];

                $total_current_month_actual_admin_exp += $admin_expenses[$i]['actual_per_month'];
                $total_current_month_variance_admin_exp += $var_total_per_month;
                $total_past_month_actual_admin_exp += $admin_expenses[$i]['actual_past_month'];
                $total_past_month_variance_admin_exp += $var_total_past_month;
            }
//            dd($total_current_month_actual_admin_exp  , $total_current_month_actual_utilities  , $total_current_month_actual_sal
//                , $total_current_month_actual_fuel_and_sell_exp  , $total_current_month_actual_tolling
//                , $total_current_month_actual_parking  , $total_current_month_actual_repair  , $total_current_month_actual_main  , $total_current_month_actual_fuel);
            return response()->json([
                'ACCOUNT_PARENT' => $arrRegular,

                'REGULAR' => round((float)$regular->DMBTR / 1000),
                'AGENCY' => round((float)$agency->DMBTR / 1000),
                'OTHER_BENEFITS' => round((float)$other_benefits->DMBTR / 1000),
                'NON_MANDATORY' => round((float)$non_mandatory->DMBTR / 1000),

                'FUEL_VEHICLES' => round((float)$fuel_vehicles->DMBTR / 1000),
                'FUEL_DELIVERY' => round((float)$fuel_delivery->DMBTR / 1000),
                'FUEL_MACHINERY' => round((float)$fuel_machinery->DMBTR / 1000),

                'MAIN_TIRE' => round((float)$main_tire->DMBTR / 1000),
                'MAIN_TIRE_DELIVERY' => round((float)$main_tire_delivery->DMBTR / 1000),
                'MAIN_BATTERY' => round((float)$main_battery->DMBTR / 1000),
                'MAIN_BATTERY_DELIVERY' => round((float)$main_battery_delivery->DMBTR / 1000),
                'MAIN_MACHINERY' => round((float)$main_machinery->DMBTR / 1000),
                'MAIN_OIL_VEHICLES' => round((float)$main_oil_vehicles->DMBTR / 1000),
                'MAIN_OIL_VEHICLES_DELIVERY' => round((float)$main_oil_vehicles_delivery->DMBTR / 1000),

                'REPAIR_VEHICLES' => round((float)$repair_vehicles->DMBTR / 1000),
                'REPAIR_VEHICLES_DELIVERY' => round((float)$repair_vehicles_delivery->DMBTR / 1000),

                'PARKING_OFFICE' => round((float)$parking_office->DMBTR / 1000),
//                'PARKING_FSP_VEHICLES' => round((float)$parking_fsp_vehicles->DMBTR / 1000),
                'PARKING_DELIVERY_VEHICLES' => round((float)$parking_delivery_vehicles->DMBTR / 1000),

                'TOLL_OFFICE' => round((float)$toll_office->DMBTR / 1000),
                'TOLL_FSP_VEHICLES' => round((float)$toll_fsp_vehicles->DMBTR / 1000),
                'TOLL_DELIVERY_VEHICLES' => round((float)$toll_delivery_vehicles->DMBTR / 1000),

                'ADVERTISING_AND_PROMOTIONS' => round((float)$ad_and_promotions->DMBTR / 1000),
                'TRANSPORTATION_AND_TRAVEL' => round((float)$trans_and_travel->DMBTR / 1000),

                'UTILITIES_ELECTRICITY' => round((float)$utilities_electricity->DMBTR / 1000),
                'UTILITIES_WATER' => round((float)$utilities_water->DMBTR / 1000),

                'COMMUNICATION' => round((float)$communication->DMBTR / 1000),
                'POSTAGE' => round((float)$postage->DMBTR / 1000),
                'REPAIRS' => round((float)$repairs_total->DMBTR / 1000),
                'REPRESENTATION_AND_ENTERTAINMENT' => round((float)$represent_and_entertainment->DMBTR / 1000),
                'PLOG_FEES_OTHERS' => round((float)$plog_fees_others->DMBTR / 1000),
                'SUPPLIES' => round((float)$supplies->DMBTR / 1000),
                'SUBSCRIPTIONS' => round((float)$subscriptions->DMBTR / 1000),
//                'SOLICIT_AND_DONATIONS' => round((float)$solicit_and_donations->DMBTR / 1000),

                'REGULAR_PY' => round((float)$regular_PY->DMBTR / 1000),
                'AGENCY_PY' => round((float)$agency_PY->DMBTR / 1000),
                'OTHER_BENEFITS_PY' => round((float)$other_benefits_PY->DMBTR / 1000),
                'NON_MANDATORY_PY' => round((float)$non_mandatory_PY->DMBTR / 1000),

                'FUEL_VEHICLES_PY' => round((float)$fuel_vehicles_PY->DMBTR / 1000),
                'FUEL_DELIVERY_PY' => round((float)$fuel_delivery_PY->DMBTR / 1000),
                'FUEL_MACHINERY_PY' => round((float)$fuel_machinery_PY->DMBTR / 1000),

                'MAIN_TIRE_PY' => round((float)$main_tire_PY->DMBTR / 1000),
                'MAIN_TIRE_DELIVERY_PY' => round((float)$main_tire_delivery_PY->DMBTR / 1000),
                'MAIN_BATTERY_PY' => round((float)$main_battery_PY->DMBTR / 1000),
                'MAIN_BATTERY_DELIVERY_PY' => round((float)$main_battery_delivery_PY->DMBTR / 1000),
                'MAIN_MACHINERY_PY' => round((float)$main_machinery_PY->DMBTR / 1000),
                'MAIN_OIL_VEHICLES_PY' => round((float)$main_oil_vehicles_PY->DMBTR / 1000),
                'MAIN_OIL_VEHICLES_DELIVERY_PY' => round((float)$main_oil_vehicles_delivery_PY->DMBTR / 1000),

                'REPAIR_VEHICLES_PY' => round((float)$repair_vehicles_PY->DMBTR / 1000),
                'REPAIR_VEHICLES_DELIVERY_PY' => round((float)$repair_vehicles_delivery_PY->DMBTR / 1000),

                'PARKING_OFFICE_PY' => round((float)$parking_office_PY->DMBTR / 1000),
//                'PARKING_FSP_VEHICLES_PY' => round((float)$parking_fsp_vehicles_PY->DMBTR / 1000),
                'PARKING_DELIVERY_VEHICLES_PY' => round((float)$parking_delivery_vehicles_PY->DMBTR / 1000),

                'TOLL_OFFICE_PY' => round((float)$toll_office_PY->DMBTR / 1000),
                'TOLL_FSP_VEHICLES_PY' => round((float)$toll_fsp_vehicles_PY->DMBTR / 1000),
                'TOLL_DELIVERY_VEHICLES_PY' => round((float)$toll_delivery_vehicles_PY->DMBTR / 1000),

                'ADVERTISING_AND_PROMOTIONS_PY' => round((float)$ad_and_promotions_PY->DMBTR / 1000),
                'TRANSPORTATION_AND_TRAVEL_PY' => round((float)$trans_and_travel_PY->DMBTR / 1000),

                'UTILITIES_ELECTRICITY_PY' => round((float)$utilities_electricity_PY->DMBTR / 1000),
                'UTILITIES_WATER_PY' => round((float)$utilities_water_PY->DMBTR / 1000),

                'COMMUNICATION_PY' => round((float)$communication_PY->DMBTR / 1000),
                'POSTAGE_PY' => round((float)$postage_PY->DMBTR / 1000),
                'REPAIRS_PY' => round((float)$repairs_total_PY->DMBTR / 1000),
                'REPRESENTATION_AND_ENTERTAINMENT_PY' => round((float)$represent_and_entertainment_PY->DMBTR / 1000),
                'PLOG_FEES_OTHERS_PY' => round((float)$plog_fees_others_PY->DMBTR / 1000),
                'SUPPLIES_PY' => round((float)$supplies_PY->DMBTR / 1000),
                'SUBSCRIPTIONS_PY' => round((float)$subscriptions_PY->DMBTR / 1000),
//                'SOLICIT_AND_DONATIONS_PY' => round((float)$solicit_and_donations_PY->DMBTR / 1000),

                'SALARIES_TOTAL' => $sal_budget_total,
                'FUELS_TOTAL' => $fuel_budget_total,
                'MAINTENANCE_TOTAL' => $main_budget_total,
                'REPAIR_TOTAL' => $repair_budget_total,
                'PARKING_TOTAL' => $parking_budget_total,
                'TOLLING_TOTAL' => $tolling_budget_total,
                'FUEL_AND_SELLING_EXP_TOTAL' => $fuel_and_selling_exp_budget_total,
                'UTILITIES_TOTAL' => $utilities_budget_total,
                'ADMIN_EXP_TOTAL' => $admin_exp_budget_total,

                //VARIANCE
                'SALARIES_TOTAL_VAR' => $total_var_sal,
                'FUELS_TOTAL_VAR' => $total_var_fuel,
                'MAINTENANCE_TOTAL_VAR' => $total_var_main,
                'REPAIR_TOTAL_VAR' => $total_var_repair,
                'PARKING_TOTAL_VAR' => $total_var_parking,
                'TOLLING_TOTAL_VAR' => $total_var_tolling,
                'FUEL_AND_SELLING_EXP_TOTAL_VAR' => $total_var_fuel_and_selling_exp,
                'UTILITIES_TOTAL_VAR' => $total_var_utilities,
                'ADMIN_EXP_TOTAL_VAR' => $total_var_admin_exp,

                //CURRENT MONTH
                'BUDGET_TOTAL_MONTH_SAL' => $total_month_budget_sal,
                'ACTUAL_TOTAL_CURRENT_MONTH_SAL' => $total_current_month_actual_sal,
                'VARIANCE_TOTAL_CURRENT_MONTH_SAL' => $total_current_month_variance_sal,
                'ACTUAL_TOTAL_PAST_MONTH_SAL' => $total_past_month_actual_sal,
                'VARIANCE_TOTAL_PAST_MONTH_SAL' => $total_past_month_variance_sal,

                'BUDGET_TOTAL_MONTH_FUEL' => $total_month_budget_fuel,
                'ACTUAL_TOTAL_CURRENT_MONTH_FUEL' => $total_current_month_actual_fuel,
                'VARIANCE_TOTAL_CURRENT_MONTH_FUEL' => $total_current_month_variance_fuel,
                'ACTUAL_TOTAL_PAST_MONTH_FUEL' => $total_past_month_actual_fuel,
                'VARIANCE_TOTAL_PAST_MONTH_FUEL' => $total_past_month_variance_fuel,

                'BUDGET_TOTAL_MONTH_MAIN' => $total_month_budget_main,
                'ACTUAL_TOTAL_CURRENT_MONTH_MAIN' => $total_current_month_actual_main,
                'VARIANCE_TOTAL_CURRENT_MONTH_MAIN' => $total_current_month_variance_main,
                'ACTUAL_TOTAL_PAST_MONTH_MAIN' => $total_past_month_actual_main,
                'VARIANCE_TOTAL_PAST_MONTH_MAIN' => $total_past_month_variance_main,

                'BUDGET_TOTAL_MONTH_REPAIR' => $total_month_budget_repair,
                'ACTUAL_TOTAL_CURRENT_MONTH_REPAIR' => $total_current_month_actual_repair,
                'VARIANCE_TOTAL_CURRENT_MONTH_REPAIR' => $total_current_month_variance_repair,
                'ACTUAL_TOTAL_PAST_MONTH_REPAIR' => $total_past_month_actual_repair,
                'VARIANCE_TOTAL_PAST_MONTH_REPAIR' => $total_past_month_variance_repair,

                'BUDGET_TOTAL_MONTH_PARKING' => $total_month_budget_parking,
                'ACTUAL_TOTAL_CURRENT_MONTH_PARKING' => $total_current_month_actual_parking,
                'VARIANCE_TOTAL_CURRENT_MONTH_PARKING' => $total_current_month_variance_parking,
                'ACTUAL_TOTAL_PAST_MONTH_PARKING' => $total_past_month_actual_parking,
                'VARIANCE_TOTAL_PAST_MONTH_PARKING' => $total_past_month_variance_parking,

                'BUDGET_TOTAL_MONTH_TOLLING' => $total_month_budget_tolling,
                'ACTUAL_TOTAL_CURRENT_MONTH_TOLLING' => $total_current_month_actual_tolling,
                'VARIANCE_TOTAL_CURRENT_MONTH_TOLLING' => $total_current_month_variance_tolling,
                'ACTUAL_TOTAL_PAST_MONTH_TOLLING' => $total_past_month_actual_tolling,
                'VARIANCE_TOTAL_PAST_MONTH_TOLLING' => $total_past_month_variance_tolling,

                'BUDGET_TOTAL_MONTH_FUEL_AND_SELL_EXP' => $total_month_budget_fuel_and_sell_exp + $total_month_budget_tolling
                    + $total_month_budget_parking + $total_month_budget_repair + $total_month_budget_main + $total_month_budget_fuel,
                'ACTUAL_TOTAL_CURRENT_MONTH_FUEL_AND_SELL_EXP' => $total_current_month_actual_fuel_and_sell_exp + $total_current_month_actual_tolling
                    + $total_current_month_actual_parking + $total_current_month_actual_repair + $total_current_month_actual_main + $total_current_month_actual_fuel,
                'VARIANCE_TOTAL_CURRENT_MONTH_FUEL_AND_SELL_EXP' => $total_current_month_variance_fuel_and_sell_exp + $total_current_month_variance_tolling + $total_current_month_variance_parking
                    + $total_current_month_variance_repair + $total_current_month_variance_main + $total_current_month_variance_fuel,
                'ACTUAL_TOTAL_PAST_MONTH_FUEL_AND_SELL_EXP' => $total_past_month_actual_fuel_and_sell_exp + $total_past_month_actual_tolling + $total_past_month_actual_parking
                    + $total_past_month_actual_repair + $total_past_month_actual_main + $total_past_month_actual_fuel,
                'VARIANCE_TOTAL_PAST_MONTH_FUEL_AND_SELL_EXP' => $total_past_month_variance_fuel_and_sell_exp + $total_past_month_variance_tolling + $total_past_month_variance_parking
                    + $total_past_month_variance_repair + $total_past_month_variance_main + $total_past_month_variance_fuel,

                'BUDGET_TOTAL_MONTH_UTILITIES' => $total_month_budget_utilities,
                'ACTUAL_TOTAL_CURRENT_MONTH_UTILITIES' => $total_current_month_actual_utilities,
                'VARIANCE_TOTAL_CURRENT_MONTH_UTILITIES' => $total_current_month_variance_utilities,
                'ACTUAL_TOTAL_PAST_MONTH_UTILITIES' => $total_past_month_actual_utilities,
                'VARIANCE_TOTAL_PAST_MONTH_UTILITIES' => $total_past_month_variance_utilities,

                'BUDGET_TOTAL_MONTH_ADMIN_EXP' => $total_month_budget_admin_exp + $total_month_budget_utilities,
                'ACTUAL_TOTAL_CURRENT_MONTH_ADMIN_EXP' => $total_current_month_actual_admin_exp + $total_current_month_actual_utilities,
                'VARIANCE_TOTAL_CURRENT_MONTH_ADMIN_EXP' => $total_current_month_variance_admin_exp + $total_current_month_variance_utilities,
                'ACTUAL_TOTAL_PAST_MONTH_ADMIN_EXP' => $total_past_month_actual_admin_exp + $total_past_month_actual_utilities,
                'VARIANCE_TOTAL_PAST_MONTH_ADMIN_EXP' => $total_past_month_variance_admin_exp + $total_past_month_variance_utilities,

                'BUDGET_TOTAL_MONTH_OPERATION' => $total_month_budget_sal + $total_month_budget_fuel_and_sell_exp + $total_month_budget_tolling
                    + $total_month_budget_parking + $total_month_budget_repair + $total_month_budget_main + $total_month_budget_fuel
                    + $total_month_budget_admin_exp + $total_month_budget_utilities,
                'ACTUAL_TOTAL_CURRENT_MONTH_OPERATION' => $total_current_month_actual_admin_exp + $total_current_month_actual_utilities + $total_current_month_actual_sal
                    + $total_current_month_actual_fuel_and_sell_exp + $total_current_month_actual_tolling
                    + $total_current_month_actual_parking + $total_current_month_actual_repair + $total_current_month_actual_main + $total_current_month_actual_fuel,
                'VARIANCE_TOTAL_CURRENT_MONTH_OPERATION' => $total_current_month_variance_admin_exp + $total_current_month_variance_utilities + $total_current_month_variance_sal
                    + $total_current_month_variance_fuel_and_sell_exp + $total_current_month_variance_tolling + $total_current_month_variance_parking
                    + $total_current_month_variance_repair + $total_current_month_variance_main + $total_current_month_variance_fuel,
                'ACTUAL_TOTAL_PAST_MONTH_OPERATION' => $total_past_month_actual_admin_exp + $total_past_month_actual_utilities + $total_past_month_actual_sal
                    + $total_past_month_actual_fuel_and_sell_exp + $total_past_month_actual_tolling + $total_past_month_actual_parking
                    + $total_past_month_actual_repair + $total_past_month_actual_main + $total_past_month_actual_fuel,
                'VARIANCE_TOTAL_PAST_MONTH_OPERATION' => $total_past_month_variance_admin_exp + $total_past_month_variance_utilities + $total_past_month_variance_sal
                    + $total_past_month_variance_fuel_and_sell_exp + $total_past_month_variance_tolling + $total_past_month_variance_parking
                    + $total_past_month_variance_repair + $total_past_month_variance_main + $total_past_month_variance_fuel,
            ]);
        }

    }
}
