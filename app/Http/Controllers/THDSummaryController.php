<?php

namespace App\Http\Controllers;

use App\Models\BSIS;
use App\Models\Company;
use App\Models\StaticBudget;
use App\Models\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class THDSummaryController extends Controller
{
    public function thdSummary(Request $request)
    {
        if ($request->from !== 'null' && $request->to !== 'null') {
            //TODO if zero check the $user var
            //removed SOLICITATION AND DONATION 604001
            //removed $parking_fsp_vehicles 602701 22/11/2021
            $user = auth()->user()->name;
            $databaseName = Config::get('database.connections')['sqlsrv2']['database'];
//            $user = 'Super admin';

            $isFrom = $request->from;
            $isTo = $request->to;

            $User_HIL = User::permission('hilado_company_thd_summary')->get();
            $isUser_HIL = null;

            foreach ($User_HIL as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_HIL = $value['name'];
                    break;
                }
            }

            $User_SIB = User::permission('sibulan_company_thd_summary')->get();
            $isUser_SIB = null;

            foreach ($User_SIB as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_SIB = $value['name'];
                    break;
                }
            }

            $User_ROX = User::permission('roxas_company_thd_summary')->get();
            $isUser_ROX = null;

            foreach ($User_ROX as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_ROX = $value['name'];
                    break;
                }
            }
//            return response()->json($isUser_ROX);

            $User_SUM = User::permission('sumag_company_thd_summary')->get();
            $isUser_SUM = null;

            foreach ($User_SUM as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_SUM = $value['name'];
                    break;
                }
            }

            $User_DIV = User::permission('diversion_company_thd_summary')->get();
            $isUser_DIV = null;

            foreach ($User_DIV as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_DIV = $value['name'];
                    break;
                }
            }

            $User_JARO = User::permission('jaro_company_thd_summary')->get();
            $isUser_JARO = null;

            foreach ($User_JARO as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_JARO = $value['name'];
                    break;
                }
            }

            $User_KAB = User::permission('kabankalan_company_thd_summary')->get();
            $isUser_KAB = null;

            foreach ($User_KAB as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_KAB = $value['name'];
                    break;
                }
            }

            $User_CS = User::permission('corporate_services_company_thd_summary')->get();
            $isUser_CS = null;

            foreach ($User_CS as $key => $value) {
                if ($user === $value['name']) {
                    $isUser_CS = $value['name'];
                    break;
                }
            }

            $isUserSal = null;
            $UserSal = User::permission('allowed_salaries_breakdown')->get();
            //check if current user has permission to allow salaries
            foreach ($UserSal as $key => $value) {
                if ($user === $value['name']) {
                    $isUserSal = $value['name'];
                    break;
                }
            }


            $regular = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (600001,600006)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");

            $agency = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (600004,600005,600008)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");

            $other_benefits = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $non_mandatory = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (600500, 600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (600500, 600501,600502,600503)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $fuel_vehicles = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602500)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $fuel_delivery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602501)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $fuel_machinery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602502)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $main_tire = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602400)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");

            $main_tire_delivery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602401)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $main_battery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602403)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");

            $main_battery_delivery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602404)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $main_machinery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602405)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $main_oil_vehicles = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602406)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $main_oil_vehicles_delivery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602407)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");

            $repair_vehicles = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602302)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $repair_vehicles_delivery = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602303)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $parking_office = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602700)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $parking_delivery_vehicles = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo'  AND HKONT IN (602702)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $toll_office = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602800)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $toll_fsp_vehicles = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602801)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $toll_delivery_vehicles = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602802)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $ad_and_promotions = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $trans_and_travel = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602600, 602601, 602602, 602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602600,602601,602602,602603)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $utilities_electricity = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (601700)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $utilities_water = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (601701)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $communication = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (601800, 601801, 601802, 601803, 601804, 601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (601800, 601801, 601802, 601803, 601804, 601805)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $postage = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602100,602101)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $repairs_total = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (602300, 602301, 602304, 602305, 602306, 602307, 602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (602300, 602301, 602304, 602305, 602306, 602307, 602308)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $represent_and_entertainment = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (603500,603501,603502)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $plog_fees_others = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (603002)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $supplies = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (603700, 603701, 603702, 603703, 603704, 603705, 603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (603700, 603701, 603702, 603703, 603704, 603705, 603706)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");


            $subscriptions = DB::connection('sqlsrv2')
                ->select(
                "SELECT SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR,pl.EKORG FROM $databaseName.dbo.PLANT_BRANCHES AS pl
                                    LEFT JOIN $databaseName.dbo.BSIS AS bs ON pl.EKORG = bs.BUKRS
                                        AND BUDAT >= '$isFrom' AND BUDAT <= '$isTo' AND HKONT IN (603800,603801)
                                            WHERE EKORG != '1351' AND EKORG != '1601' AND EKORG != '1902' AND EKORG != '1238'
                                                GROUP BY pl.EKORG
                                                    ORDER BY pl.EKORG asc");

//            $solicit_and_donations = BSIS::where(function ($query) use ($isFrom,$isTo) {
//                $query->where('BUKRS', '!=', '1351')
//                    ->where('BUKRS', '!=', '1601')
//                    ->where('BUKRS', '!=', '1902')
//                    ->where('BUDAT', '>=', $isFrom)
//                    ->where('BUDAT', '<=', $isTo)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->groupBy('BUKRS')
//                ->get();


            $budget = StaticBudget::select('total_operating', 'company')->groupBy('total_operating', 'company')->get();
            $getPerCompany = [];
            for ($i = 0; $i < count($budget); $i++) {
                $getPerCompany[$budget[$i]['company']] =
                    [
                        'company' => $budget[$i]['company'],
                        'total_operating' => $budget[$i]['total_operating'],
                    ];
            }

            $salaries[] = [];
            $fuels[] = [];
            $maintenance[] = [];
            $repairs[] = [];
            $parking[] = [];
            $tolling[] = [];
            $fuel_and_selling_exp[] = [];
            $utilities[] = [];
            $admin_exp[] = [];

            //salaries
            $company_1231 = null;
            $company_1232 = null;
            $company_1233 = null;
            $company_1234 = null;
            $company_1235 = null;
            $company_1236 = null;
            $company_1237 = null;
            $company_1901 = null;
            $total_all = null;

            $company_1231_fuel = null;
            $company_1232_fuel = null;
            $company_1233_fuel = null;
            $company_1234_fuel = null;
            $company_1235_fuel = null;
            $company_1236_fuel = null;
            $company_1237_fuel = null;
            $company_1901_fuel = null;
            $total_all_fuel = null;

            $company_1231_main = null;
            $company_1232_main = null;
            $company_1233_main = null;
            $company_1234_main = null;
            $company_1235_main = null;
            $company_1236_main = null;
            $company_1237_main = null;
            $company_1901_main = null;
            $total_all_main = null;

            $company_1231_repairs = null;
            $company_1232_repairs = null;
            $company_1233_repairs = null;
            $company_1234_repairs = null;
            $company_1235_repairs = null;
            $company_1236_repairs = null;
            $company_1237_repairs = null;
            $company_1901_repairs = null;
            $total_all_repairs = null;

            $company_1231_parking = null;
            $company_1232_parking = null;
            $company_1233_parking = null;
            $company_1234_parking = null;
            $company_1235_parking = null;
            $company_1236_parking = null;
            $company_1237_parking = null;
            $company_1901_parking = null;
            $total_all_parking = null;

            $company_1231_tolling = null;
            $company_1232_tolling = null;
            $company_1233_tolling = null;
            $company_1234_tolling = null;
            $company_1235_tolling = null;
            $company_1236_tolling = null;
            $company_1237_tolling = null;
            $company_1901_tolling = null;
            $total_all_tolling = null;

            $company_1231_fuel_and_selling_exp = null;
            $company_1232_fuel_and_selling_exp = null;
            $company_1233_fuel_and_selling_exp = null;
            $company_1234_fuel_and_selling_exp = null;
            $company_1235_fuel_and_selling_exp = null;
            $company_1236_fuel_and_selling_exp = null;
            $company_1237_fuel_and_selling_exp = null;
            $company_1901_fuel_and_selling_exp = null;
            $total_details_fuel_and_selling_exp = null;

            $company_1231_utilities = null;
            $company_1232_utilities = null;
            $company_1233_utilities = null;
            $company_1234_utilities = null;
            $company_1235_utilities = null;
            $company_1236_utilities = null;
            $company_1237_utilities = null;
            $company_1901_utilities = null;
            $total_all_utilities = null;

            $company_1231_admin_exp = null;
            $company_1232_admin_exp = null;
            $company_1233_admin_exp = null;
            $company_1234_admin_exp = null;
            $company_1235_admin_exp = null;
            $company_1236_admin_exp = null;
            $company_1237_admin_exp = null;
            $company_1901_admin_exp = null;
            $total_all_admin_exp = null;

            for ($i = 0; $i < count($salaries); $i++) {
                $salaries[0]['name'] = 'REGULAR';
                $salaries[1]['name'] = 'AGENCY';
                $salaries[2]['name'] = 'OTHER BENEFITS';
                $salaries[3]['name'] = 'NON-MANDATORY BONUSES AND INCENTIVES';
                if ($regular !== []) {
                    for ($x = 0; $x < count($regular); $x++) {
                        if ($regular[$x]->DMBTR !== null) {
                            $salaries[0]['company_1231'] = isset($regular[0]->DMBTR) === true ? number_format((int)round($regular[0]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1232'] = isset($regular[1]->DMBTR) === true ? number_format((int)round($regular[1]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1233'] = isset($regular[2]->DMBTR) === true ? number_format((int)round($regular[2]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1234'] = isset($regular[3]->DMBTR) === true ? number_format((int)round($regular[3]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1235'] = isset($regular[4]->DMBTR) === true ? number_format((int)round($regular[4]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1236'] = isset($regular[5]->DMBTR) === true ? number_format((int)round($regular[5]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1237'] = isset($regular[6]->DMBTR) === true ? number_format((int)round($regular[6]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1901'] = isset($regular[7]->DMBTR) === true ? number_format((int)round($regular[7]->DMBTR) / 1000) : 0;
//                             return response()->json($salaries[0]['company_1901']);
                            if ($isUser_HIL === null) {
                                $salaries[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $salaries[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $salaries[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $salaries[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $salaries[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $salaries[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $salaries[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $salaries[0]['company_1901'] = 0;
                            }
                            $salaries[0]['total'] = number_format((int)str_replace(',', '', $salaries[0]['company_1231']) + (int)str_replace(',', '', $salaries[0]['company_1232'])
                                + (int)str_replace(',', '', $salaries[0]['company_1233']) + (int)str_replace(',', '', $salaries[0]['company_1234'])
                                + (int)str_replace(',', '', $salaries[0]['company_1235']) + (int)str_replace(',', '', $salaries[0]['company_1236'])
                                + (int)str_replace(',', '', $salaries[0]['company_1237']) + (int)str_replace(',', '', $salaries[0]['company_1901']));
                        }
                        else {
                            $salaries[0]['company_1231'] = isset($regular[0]->DMBTR) === true ? number_format((int)round($regular[0]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1232'] = isset($regular[1]->DMBTR) === true ? number_format((int)round($regular[1]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1233'] = isset($regular[2]->DMBTR) === true ? number_format((int)round($regular[2]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1234'] = isset($regular[3]->DMBTR) === true ? number_format((int)round($regular[3]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1235'] = isset($regular[4]->DMBTR) === true ? number_format((int)round($regular[4]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1236'] = isset($regular[5]->DMBTR) === true ? number_format((int)round($regular[5]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1237'] = isset($regular[6]->DMBTR) === true ? number_format((int)round($regular[6]->DMBTR) / 1000) : 0;
                            $salaries[0]['company_1901'] = isset($regular[7]->DMBTR) === true ? number_format((int)round($regular[7]->DMBTR) / 1000) : 0;

//                            $salaries[0]['total'] = 0;
                        }
                    }
                }
//                return response()->json($salaries);
                if ($agency !== []) {
                    for ($x = 0; $x < count($agency); $x++) {
                        if ($agency[$x]->DMBTR !== null) {
                            $salaries[1]['company_1231'] = isset($agency[0]->DMBTR) === true ? number_format((int)round($agency[0]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1232'] = isset($agency[1]->DMBTR) === true ? number_format((int)round($agency[1]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1233'] = isset($agency[2]->DMBTR) === true ? number_format((int)round($agency[2]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1234'] = isset($agency[3]->DMBTR) === true ? number_format((int)round($agency[3]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1235'] = isset($agency[4]->DMBTR) === true ? number_format((int)round($agency[4]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1236'] = isset($agency[5]->DMBTR) === true ? number_format((int)round($agency[5]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1237'] = isset($agency[6]->DMBTR) === true ? number_format((int)round($agency[6]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1901'] = isset($agency[7]->DMBTR) === true ? number_format((int)round($agency[7]->DMBTR) / 1000) : 0;

//                            if ($isUser_HIL === null) {
//                                $salaries[1]['company_1231'] = 0;
//                            }
//                            if ($isUser_SIB === null) {
//                                $salaries[1]['company_1232'] = 0;
//                            }
//                            if ($isUser_ROX === null) {
//                                $salaries[1]['company_1233'] = 0;
//                            }
//                            if ($isUser_SUM === null) {
//                                $salaries[1]['company_1234'] = 0;
//                            }
//                            if ($isUser_DIV === null) {
//                                $salaries[1]['company_1235'] = 0;
//                            }
//                            if ($isUser_JARO === null) {
//                                $salaries[1]['company_1236'] = 0;
//                            }
//                            if ($isUser_KAB === null) {
//                                $salaries[1]['company_1237'] = 0;
//                            }
//                            if ($isUser_CS === null) {
//                                $salaries[1]['company_1901'] = 0;
//                            }
                            $salaries[1]['total'] = number_format((int)str_replace(',', '', $salaries[1]['company_1231']) + (int)str_replace(',', '', $salaries[1]['company_1232'])
                                + (int)str_replace(',', '', $salaries[1]['company_1233']) + (int)str_replace(',', '', $salaries[1]['company_1234'])
                                + (int)str_replace(',', '', $salaries[1]['company_1235']) + (int)str_replace(',', '', $salaries[1]['company_1236'])
                                + (int)str_replace(',', '', $salaries[1]['company_1237']) + (int)str_replace(',', '', $salaries[1]['company_1901']));
                        }
                        else {
                            $salaries[1]['company_1231'] = isset($agency[0]->DMBTR) === true ? number_format((int)round($agency[0]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1232'] = isset($agency[1]->DMBTR) === true ? number_format((int)round($agency[1]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1233'] = isset($agency[2]->DMBTR) === true ? number_format((int)round($agency[2]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1234'] = isset($agency[3]->DMBTR) === true ? number_format((int)round($agency[3]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1235'] = isset($agency[4]->DMBTR) === true ? number_format((int)round($agency[4]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1236'] = isset($agency[5]->DMBTR) === true ? number_format((int)round($agency[5]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1237'] = isset($agency[6]->DMBTR) === true ? number_format((int)round($agency[6]->DMBTR) / 1000) : 0;
                            $salaries[1]['company_1901'] = isset($agency[7]->DMBTR) === true ? number_format((int)round($agency[7]->DMBTR) / 1000) : 0;
//                            $salaries[1]['total'] = 0;
                        }
                    }
                }
                if ($other_benefits !== []) {
                    for ($x = 0; $x < count($other_benefits); $x++) {

                        if ($other_benefits[$x]->DMBTR !== null) {
                            $salaries[2]['company_1231'] = isset($other_benefits[0]->DMBTR) === true ? number_format((int)round($other_benefits[0]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1232'] = isset($other_benefits[1]->DMBTR) === true ? number_format((int)round($other_benefits[1]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1233'] = isset($other_benefits[2]->DMBTR) === true ? number_format((int)round($other_benefits[2]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1234'] = isset($other_benefits[3]->DMBTR) === true ? number_format((int)round($other_benefits[3]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1235'] = isset($other_benefits[4]->DMBTR) === true ? number_format((int)round($other_benefits[4]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1236'] = isset($other_benefits[5]->DMBTR) === true ? number_format((int)round($other_benefits[5]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1237'] = isset($other_benefits[6]->DMBTR) === true ? number_format((int)round($other_benefits[6]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1901'] = isset($other_benefits[7]->DMBTR) === true ? number_format((int)round($other_benefits[7]->DMBTR) / 1000) : 0;

//                            if ($isUser_HIL === null) {
//                                $salaries[2]['company_1231'] = 0;
//                            }
//                            if ($isUser_SIB === null) {
//                                $salaries[2]['company_1232'] = 0;
//                            }
//                            if ($isUser_ROX === null) {
//                                $salaries[2]['company_1233'] = 0;
//                            }
//                            if ($isUser_SUM === null) {
//                                $salaries[2]['company_1234'] = 0;
//                            }
//                            if ($isUser_DIV === null) {
//                                $salaries[2]['company_1235'] = 0;
//                            }
//                            if ($isUser_JARO === null) {
//                                $salaries[2]['company_1236'] = 0;
//                            }
//                            if ($isUser_KAB === null) {
//                                $salaries[2]['company_1237'] = 0;
//                            }
//                            if ($isUser_CS === null) {
//                                $salaries[2]['company_1901'] = 0;
//                            }
                            $salaries[2]['total'] = number_format((int)str_replace(',', '', $salaries[2]['company_1231']) + (int)str_replace(',', '', $salaries[2]['company_1232'])
                                + (int)str_replace(',', '', $salaries[2]['company_1233']) + (int)str_replace(',', '', $salaries[2]['company_1234'])
                                + (int)str_replace(',', '', $salaries[2]['company_1235']) + (int)str_replace(',', '', $salaries[2]['company_1236'])
                                + (int)str_replace(',', '', $salaries[2]['company_1237']) + (int)str_replace(',', '', $salaries[2]['company_1901']));
                        }
                        else {
                            $salaries[2]['company_1231'] = isset($other_benefits[0]->DMBTR) === true ? number_format((int)round($other_benefits[0]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1232'] = isset($other_benefits[1]->DMBTR) === true ? number_format((int)round($other_benefits[1]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1233'] = isset($other_benefits[2]->DMBTR) === true ? number_format((int)round($other_benefits[2]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1234'] = isset($other_benefits[3]->DMBTR) === true ? number_format((int)round($other_benefits[3]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1235'] = isset($other_benefits[4]->DMBTR) === true ? number_format((int)round($other_benefits[4]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1236'] = isset($other_benefits[5]->DMBTR) === true ? number_format((int)round($other_benefits[5]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1237'] = isset($other_benefits[6]->DMBTR) === true ? number_format((int)round($other_benefits[6]->DMBTR) / 1000) : 0;
                            $salaries[2]['company_1901'] = isset($other_benefits[7]->DMBTR) === true ? number_format((int)round($other_benefits[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($non_mandatory !== []) {
                    for ($x = 0; $x < count($non_mandatory); $x++) {

                        if ($non_mandatory[$x]->DMBTR !== null) {
                            $salaries[3]['company_1231'] = isset($non_mandatory[0]->DMBTR) === true ? number_format((int)round($non_mandatory[0]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1232'] = isset($non_mandatory[1]->DMBTR) === true ? number_format((int)round($non_mandatory[1]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1233'] = isset($non_mandatory[2]->DMBTR) === true ? number_format((int)round($non_mandatory[2]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1234'] = isset($non_mandatory[3]->DMBTR) === true ? number_format((int)round($non_mandatory[3]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1235'] = isset($non_mandatory[4]->DMBTR) === true ? number_format((int)round($non_mandatory[4]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1236'] = isset($non_mandatory[5]->DMBTR) === true ? number_format((int)round($non_mandatory[5]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1237'] = isset($non_mandatory[6]->DMBTR) === true ? number_format((int)round($non_mandatory[6]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1901'] = isset($non_mandatory[7]->DMBTR) === true ? number_format((int)round($non_mandatory[7]->DMBTR) / 1000) : 0;

//                            if ($isUser_HIL === null) {
//                                $salaries[3]['company_1231'] = 0;
//                            }
//                            if ($isUser_SIB === null) {
//                                $salaries[3]['company_1232'] = 0;
//                            }
//                            if ($isUser_ROX === null) {
//                                $salaries[3]['company_1233'] = 0;
//                            }
//                            if ($isUser_SUM === null) {
//                                $salaries[3]['company_1234'] = 0;
//                            }
//                            if ($isUser_DIV === null) {
//                                $salaries[3]['company_1235'] = 0;
//                            }
//                            if ($isUser_JARO === null) {
//                                $salaries[3]['company_1236'] = 0;
//                            }
//                            if ($isUser_KAB === null) {
//                                $salaries[3]['company_1237'] = 0;
//                            }
//                            if ($isUser_CS === null) {
//                                $salaries[3]['company_1901'] = 0;
//                            }
                            $salaries[3]['total'] = number_format((int)str_replace(',', '', $salaries[3]['company_1231']) + (int)str_replace(',', '', $salaries[3]['company_1232'])
                                + (int)str_replace(',', '', $salaries[3]['company_1233']) + (int)str_replace(',', '', $salaries[3]['company_1234'])
                                + (int)str_replace(',', '', $salaries[3]['company_1235']) + (int)str_replace(',', '', $salaries[3]['company_1236'])
                                + (int)str_replace(',', '', $salaries[3]['company_1237']) + (int)str_replace(',', '', $salaries[3]['company_1901']));

                        }
                        else {
                            $salaries[3]['company_1231'] = isset($non_mandatory[0]->DMBTR) === true ? number_format((int)round($non_mandatory[0]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1232'] = isset($non_mandatory[1]->DMBTR) === true ? number_format((int)round($non_mandatory[1]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1233'] = isset($non_mandatory[2]->DMBTR) === true ? number_format((int)round($non_mandatory[2]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1234'] = isset($non_mandatory[3]->DMBTR) === true ? number_format((int)round($non_mandatory[3]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1235'] = isset($non_mandatory[4]->DMBTR) === true ? number_format((int)round($non_mandatory[4]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1236'] = isset($non_mandatory[5]->DMBTR) === true ? number_format((int)round($non_mandatory[5]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1237'] = isset($non_mandatory[6]->DMBTR) === true ? number_format((int)round($non_mandatory[6]->DMBTR) / 1000) : 0;
                            $salaries[3]['company_1901'] = isset($non_mandatory[7]->DMBTR) === true ? number_format((int)round($non_mandatory[7]->DMBTR) / 1000) : 0;

                        }

                    }
                }

                if ($regular === []) {
                    $salaries[0]['company_1231'] = 0;
                    $salaries[0]['company_1232'] = 0;
                    $salaries[0]['company_1233'] = 0;
                    $salaries[0]['company_1234'] = 0;
                    $salaries[0]['company_1235'] = 0;
                    $salaries[0]['company_1236'] = 0;
                    $salaries[0]['company_1237'] = 0;
                    $salaries[0]['company_1901'] = 0;
                    $salaries[0]['total'] = 0;
                }
                if ($agency === []) {
                    $salaries[1]['company_1231'] = 0;
                    $salaries[1]['company_1232'] = 0;
                    $salaries[1]['company_1233'] = 0;
                    $salaries[1]['company_1234'] = 0;
                    $salaries[1]['company_1235'] = 0;
                    $salaries[1]['company_1236'] = 0;
                    $salaries[1]['company_1237'] = 0;
                    $salaries[1]['company_1901'] = 0;
                    $salaries[1]['total'] = 0;
                }
                if ($other_benefits === []) {
                    $salaries[2]['company_1231'] = 0;
                    $salaries[2]['company_1232'] = 0;
                    $salaries[2]['company_1233'] = 0;
                    $salaries[2]['company_1234'] = 0;
                    $salaries[2]['company_1235'] = 0;
                    $salaries[2]['company_1236'] = 0;
                    $salaries[2]['company_1237'] = 0;
                    $salaries[2]['company_1901'] = 0;
                    $salaries[2]['total'] = 0;
                }
                if ($non_mandatory === []) {
                    $salaries[3]['company_1231'] = 0;
                    $salaries[3]['company_1232'] = 0;
                    $salaries[3]['company_1233'] = 0;
                    $salaries[3]['company_1234'] = 0;
                    $salaries[3]['company_1235'] = 0;
                    $salaries[3]['company_1236'] = 0;
                    $salaries[3]['company_1237'] = 0;
                    $salaries[3]['company_1901'] = 0;
                    $salaries[3]['total'] = 0;
                }
                //salaries

                $company_1231 = (int)str_replace(',', '', $salaries[0]['company_1231']) + (int)str_replace(',', '', $salaries[1]['company_1231'])
                    + (int)str_replace(',', '', $salaries[2]['company_1231']) + (int)str_replace(',', '', $salaries[3]['company_1231']);
                $company_1232 = (int)str_replace(',', '', $salaries[0]['company_1232']) + (int)str_replace(',', '', $salaries[1]['company_1232'])
                    + (int)str_replace(',', '', $salaries[2]['company_1232']) + (int)str_replace(',', '', $salaries[3]['company_1232']);
                $company_1233 = (int)str_replace(',', '', $salaries[0]['company_1233']) + (int)str_replace(',', '', $salaries[1]['company_1233'])
                    + (int)str_replace(',', '', $salaries[2]['company_1233']) + (int)str_replace(',', '', $salaries[3]['company_1233']);
                $company_1234 = (int)str_replace(',', '', $salaries[0]['company_1234']) + (int)str_replace(',', '', $salaries[1]['company_1234'])
                    + (int)str_replace(',', '', $salaries[2]['company_1234']) + (int)str_replace(',', '', $salaries[3]['company_1234']);
                $company_1235 = (int)str_replace(',', '', $salaries[0]['company_1235']) + (int)str_replace(',', '', $salaries[1]['company_1235'])
                    + (int)str_replace(',', '', $salaries[2]['company_1235']) + (int)str_replace(',', '', $salaries[3]['company_1235']);
                $company_1236 = (int)str_replace(',', '', $salaries[0]['company_1236']) + (int)str_replace(',', '', $salaries[1]['company_1236'])
                    + (int)str_replace(',', '', $salaries[2]['company_1236']) + (int)str_replace(',', '', $salaries[3]['company_1236']);
                $company_1237 = (int)str_replace(',', '', $salaries[0]['company_1237']) + (int)str_replace(',', '', $salaries[1]['company_1237'])
                    + (int)str_replace(',', '', $salaries[2]['company_1237']) + (int)str_replace(',', '', $salaries[3]['company_1237']);
                $company_1901 = (int)str_replace(',', '', $salaries[0]['company_1901']) + (int)str_replace(',', '', $salaries[1]['company_1901'])
                    + (int)str_replace(',', '', $salaries[2]['company_1901']) + (int)str_replace(',', '', $salaries[3]['company_1901']);

//                if ($isUser_HIL === null) {
//                    $company_1231 = 0;
//                }
//                if ($isUser_SIB === null) {
//                    $company_1232 = 0;
//                }
//                if ($isUser_ROX === null) {
//                    $company_1233 = 0;
//                }
//                if ($isUser_SUM === null) {
//                    $company_1234 = 0;
//                }
//                if ($isUser_DIV === null) {
//                    $company_1235 = 0;
//                }
//                if ($isUser_JARO === null) {
//                    $company_1236 = 0;
//                }
//                if ($isUser_KAB === null) {
//                    $company_1237 = 0;
//                }
//                if ($isUser_CS === null) {
//                    $company_1901 = 0;
//                }

                $total_all = $company_1231 + $company_1232 + $company_1233 + $company_1234 + $company_1235 + $company_1236 + $company_1237 + $company_1901;
//                dd($company_1231 , $company_1232 , $company_1233 , $company_1234 , $company_1235 , $company_1236 , $company_1237 , $company_1901,$total_all);
            }

            for ($i = 0; $i < count($fuels); $i++) {
                $fuels[0]['name'] = 'FUEL - VEHICLES';
                $fuels[1]['name'] = 'FUEL - DELIVERY VEHICLES';
                $fuels[2]['name'] = 'FUEL - MACHINERY & EQUIPMENT';
                if ($fuel_vehicles !== []) {
                    for ($x = 0; $x < count($fuel_vehicles); $x++) {
                        if ($fuel_vehicles[$x]->DMBTR !== null) {
                            $fuels[0]['company_1231'] = isset($fuel_vehicles[0]->DMBTR) === true ? number_format((int)round($fuel_vehicles[0]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1232'] = isset($fuel_vehicles[1]->DMBTR) === true ? number_format((int)round($fuel_vehicles[1]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1233'] = isset($fuel_vehicles[2]->DMBTR) === true ? number_format((int)round($fuel_vehicles[2]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1234'] = isset($fuel_vehicles[3]->DMBTR) === true ? number_format((int)round($fuel_vehicles[3]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1235'] = isset($fuel_vehicles[4]->DMBTR) === true ? number_format((int)round($fuel_vehicles[4]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1236'] = isset($fuel_vehicles[5]->DMBTR) === true ? number_format((int)round($fuel_vehicles[5]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1237'] = isset($fuel_vehicles[6]->DMBTR) === true ? number_format((int)round($fuel_vehicles[6]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1901'] = isset($fuel_vehicles[7]->DMBTR) === true ? number_format((int)round($fuel_vehicles[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $fuels[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $fuels[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $fuels[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $fuels[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $fuels[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $fuels[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $fuels[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $fuels[0]['company_1901'] = 0;
                            }
                            $fuels[0]['total'] = number_format((int)str_replace(',', '', $fuels[0]['company_1231']) + (int)str_replace(',', '', $fuels[0]['company_1232'])
                                + (int)str_replace(',', '', $fuels[0]['company_1233']) + (int)str_replace(',', '', $fuels[0]['company_1234'])
                                + (int)str_replace(',', '', $fuels[0]['company_1235']) + (int)str_replace(',', '', $fuels[0]['company_1236'])
                                + (int)str_replace(',', '', $fuels[0]['company_1237']) + (int)str_replace(',', '', $fuels[0]['company_1901']));
                        }
                        else {
                            $fuels[0]['company_1231'] = isset($fuel_vehicles[0]->DMBTR) === true ? number_format((int)round($fuel_vehicles[0]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1232'] = isset($fuel_vehicles[1]->DMBTR) === true ? number_format((int)round($fuel_vehicles[1]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1233'] = isset($fuel_vehicles[2]->DMBTR) === true ? number_format((int)round($fuel_vehicles[2]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1234'] = isset($fuel_vehicles[3]->DMBTR) === true ? number_format((int)round($fuel_vehicles[3]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1235'] = isset($fuel_vehicles[4]->DMBTR) === true ? number_format((int)round($fuel_vehicles[4]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1236'] = isset($fuel_vehicles[5]->DMBTR) === true ? number_format((int)round($fuel_vehicles[5]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1237'] = isset($fuel_vehicles[6]->DMBTR) === true ? number_format((int)round($fuel_vehicles[6]->DMBTR) / 1000) : 0;
                            $fuels[0]['company_1901'] = isset($fuel_vehicles[7]->DMBTR) === true ? number_format((int)round($fuel_vehicles[7]->DMBTR) / 1000) : 0;
                        }
                    }
                }
                if ($fuel_delivery !== []) {
                    for ($x = 0; $x < count($fuel_delivery); $x++) {
                        if ($fuel_delivery[$x]->DMBTR !== null) {
                            $fuels[1]['company_1231'] = isset($fuel_delivery[0]->DMBTR) === true ? number_format((int)round($fuel_delivery[0]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1232'] = isset($fuel_delivery[1]->DMBTR) === true ? number_format((int)round($fuel_delivery[1]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1233'] = isset($fuel_delivery[2]->DMBTR) === true ? number_format((int)round($fuel_delivery[2]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1234'] = isset($fuel_delivery[3]->DMBTR) === true ? number_format((int)round($fuel_delivery[3]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1235'] = isset($fuel_delivery[4]->DMBTR) === true ? number_format((int)round($fuel_delivery[4]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1236'] = isset($fuel_delivery[5]->DMBTR) === true ? number_format((int)round($fuel_delivery[5]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1237'] = isset($fuel_delivery[6]->DMBTR) === true ? number_format((int)round($fuel_delivery[6]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1901'] = isset($fuel_delivery[7]->DMBTR) === true ? number_format((int)round($fuel_delivery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $fuels[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $fuels[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $fuels[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $fuels[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $fuels[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $fuels[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $fuels[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $fuels[1]['company_1901'] = 0;
                            }
                            $fuels[1]['total'] = number_format((int)str_replace(',', '', $fuels[1]['company_1231']) + (int)str_replace(',', '', $fuels[1]['company_1232'])
                                + (int)str_replace(',', '', $fuels[1]['company_1233']) + (int)str_replace(',', '', $fuels[1]['company_1234'])
                                + (int)str_replace(',', '', $fuels[1]['company_1235']) + (int)str_replace(',', '', $fuels[1]['company_1236'])
                                + (int)str_replace(',', '', $fuels[1]['company_1237']) + (int)str_replace(',', '', $fuels[1]['company_1901']));
                        }
                        else {
                            $fuels[1]['company_1231'] = isset($fuel_delivery[0]->DMBTR) === true ? number_format((int)round($fuel_delivery[0]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1232'] = isset($fuel_delivery[1]->DMBTR) === true ? number_format((int)round($fuel_delivery[1]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1233'] = isset($fuel_delivery[2]->DMBTR) === true ? number_format((int)round($fuel_delivery[2]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1234'] = isset($fuel_delivery[3]->DMBTR) === true ? number_format((int)round($fuel_delivery[3]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1235'] = isset($fuel_delivery[4]->DMBTR) === true ? number_format((int)round($fuel_delivery[4]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1236'] = isset($fuel_delivery[5]->DMBTR) === true ? number_format((int)round($fuel_delivery[5]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1237'] = isset($fuel_delivery[6]->DMBTR) === true ? number_format((int)round($fuel_delivery[6]->DMBTR) / 1000) : 0;
                            $fuels[1]['company_1901'] = isset($fuel_delivery[7]->DMBTR) === true ? number_format((int)round($fuel_delivery[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($fuel_machinery !== []) {
                    for ($x = 0; $x < count($fuel_machinery); $x++) {
                        if ($fuel_machinery[$x]->DMBTR !== null) {
                            $fuels[2]['company_1231'] = isset($fuel_machinery[0]->DMBTR) === true ? number_format((int)round($fuel_machinery[0]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1232'] = isset($fuel_machinery[1]->DMBTR) === true ? number_format((int)round($fuel_machinery[1]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1233'] = isset($fuel_machinery[2]->DMBTR) === true ? number_format((int)round($fuel_machinery[2]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1234'] = isset($fuel_machinery[3]->DMBTR) === true ? number_format((int)round($fuel_machinery[3]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1235'] = isset($fuel_machinery[4]->DMBTR) === true ? number_format((int)round($fuel_machinery[4]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1236'] = isset($fuel_machinery[5]->DMBTR) === true ? number_format((int)round($fuel_machinery[5]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1237'] = isset($fuel_machinery[6]->DMBTR) === true ? number_format((int)round($fuel_machinery[6]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1901'] = isset($fuel_machinery[7]->DMBTR) === true ? number_format((int)round($fuel_machinery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $fuels[2]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $fuels[2]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $fuels[2]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $fuels[2]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $fuels[2]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $fuels[2]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $fuels[2]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $fuels[2]['company_1901'] = 0;
                            }
                            $fuels[2]['total'] = number_format((int)str_replace(',', '', $fuels[2]['company_1231']) + (int)str_replace(',', '', $fuels[2]['company_1232'])
                                + (int)str_replace(',', '', $fuels[2]['company_1233']) + (int)str_replace(',', '', $fuels[2]['company_1234'])
                                + (int)str_replace(',', '', $fuels[2]['company_1235']) + (int)str_replace(',', '', $fuels[2]['company_1236'])
                                + (int)str_replace(',', '', $fuels[2]['company_1237']) + (int)str_replace(',', '', $fuels[2]['company_1901']));
                        }
                        else {
                            $fuels[2]['company_1231'] = isset($fuel_machinery[0]->DMBTR) === true ? number_format((int)round($fuel_machinery[0]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1232'] = isset($fuel_machinery[1]->DMBTR) === true ? number_format((int)round($fuel_machinery[1]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1233'] = isset($fuel_machinery[2]->DMBTR) === true ? number_format((int)round($fuel_machinery[2]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1234'] = isset($fuel_machinery[3]->DMBTR) === true ? number_format((int)round($fuel_machinery[3]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1235'] = isset($fuel_machinery[4]->DMBTR) === true ? number_format((int)round($fuel_machinery[4]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1236'] = isset($fuel_machinery[5]->DMBTR) === true ? number_format((int)round($fuel_machinery[5]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1237'] = isset($fuel_machinery[6]->DMBTR) === true ? number_format((int)round($fuel_machinery[6]->DMBTR) / 1000) : 0;
                            $fuels[2]['company_1901'] = isset($fuel_machinery[7]->DMBTR) === true ? number_format((int)round($fuel_machinery[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($fuel_vehicles === []) {
                    $fuels[0]['company_1231'] = 0;
                    $fuels[0]['company_1232'] = 0;
                    $fuels[0]['company_1233'] = 0;
                    $fuels[0]['company_1234'] = 0;
                    $fuels[0]['company_1235'] = 0;
                    $fuels[0]['company_1236'] = 0;
                    $fuels[0]['company_1237'] = 0;
                    $fuels[0]['company_1901'] = 0;
                    $fuels[0]['total'] = 0;
                }
                if ($fuel_delivery === []) {
                    $fuels[1]['company_1231'] = 0;
                    $fuels[1]['company_1232'] = 0;
                    $fuels[1]['company_1233'] = 0;
                    $fuels[1]['company_1234'] = 0;
                    $fuels[1]['company_1235'] = 0;
                    $fuels[1]['company_1236'] = 0;
                    $fuels[1]['company_1237'] = 0;
                    $fuels[1]['company_1901'] = 0;
                    $fuels[1]['total'] = 0;
                }
                if ($fuel_machinery === []) {
                    $fuels[2]['company_1231'] = 0;
                    $fuels[2]['company_1232'] = 0;
                    $fuels[2]['company_1233'] = 0;
                    $fuels[2]['company_1234'] = 0;
                    $fuels[2]['company_1235'] = 0;
                    $fuels[2]['company_1236'] = 0;
                    $fuels[2]['company_1237'] = 0;
                    $fuels[2]['company_1901'] = 0;
                    $fuels[2]['total'] = 0;
                }

                //fuels
                $company_1231_fuel = (int)str_replace(',', '', $fuels[0]['company_1231']) + (int)str_replace(',', '', $fuels[1]['company_1231'])
                    + (int)str_replace(',', '', $fuels[2]['company_1231']);
                $company_1232_fuel = (int)str_replace(',', '', $fuels[0]['company_1232']) + (int)str_replace(',', '', $fuels[1]['company_1232'])
                    + (int)str_replace(',', '', $fuels[2]['company_1232']);
                $company_1233_fuel = (int)str_replace(',', '', $fuels[0]['company_1233']) + (int)str_replace(',', '', $fuels[1]['company_1233'])
                    + (int)str_replace(',', '', $fuels[2]['company_1233']);
                $company_1234_fuel = (int)str_replace(',', '', $fuels[0]['company_1234']) + (int)str_replace(',', '', $fuels[1]['company_1234'])
                    + (int)str_replace(',', '', $fuels[2]['company_1234']);
                $company_1235_fuel = (int)str_replace(',', '', $fuels[0]['company_1235']) + (int)str_replace(',', '', $fuels[1]['company_1235'])
                    + (int)str_replace(',', '', $fuels[2]['company_1235']);
                $company_1236_fuel = (int)str_replace(',', '', $fuels[0]['company_1236']) + (int)str_replace(',', '', $fuels[1]['company_1236'])
                    + (int)str_replace(',', '', $fuels[2]['company_1236']);
                $company_1237_fuel = (int)str_replace(',', '', $fuels[0]['company_1237']) + (int)str_replace(',', '', $fuels[1]['company_1237'])
                    + (int)str_replace(',', '', $fuels[2]['company_1237']);
                $company_1901_fuel = (int)str_replace(',', '', $fuels[0]['company_1901']) + (int)str_replace(',', '', $fuels[1]['company_1901'])
                    + (int)str_replace(',', '', $fuels[2]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_fuel = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_fuel = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_fuel = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_fuel = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_fuel = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_fuel = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_fuel = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_fuel = 0;
                }
                $total_all_fuel = $company_1231_fuel + $company_1232_fuel + $company_1233_fuel + $company_1234_fuel + $company_1235_fuel + $company_1236_fuel + $company_1237_fuel + $company_1901_fuel;
            }

            for ($i = 0; $i < count($maintenance); $i++) {
                $maintenance[0]['name'] = 'MAINTENANCE - TIRE VEHICLES';
                $maintenance[1]['name'] = 'MAINTENANCE - TIRE DELIVERY VEHICLES';
                $maintenance[2]['name'] = 'MAINTENANCE - BATTERY VEHICLES';
                $maintenance[3]['name'] = 'MAINTENANCE - BATTERY DELIVERY VEHICLES';
                $maintenance[4]['name'] = 'MAINTENANCE - MACHINERY AND EQUIPMENT';
                $maintenance[5]['name'] = 'MAINTENANCE - OIL AND LUBRICANTS VEHICLES';
                $maintenance[6]['name'] = 'MAINTENANCE - OIL AND LUBRICANTS DELIVERY VEH';

                if ($main_tire !== []) {
                    for ($x = 0; $x < count($main_tire); $x++) {
                        if ($main_tire[$x]->DMBTR !== null) {
                            $maintenance[0]['company_1231'] = isset($main_tire[0]->DMBTR) === true ? number_format((int)round($main_tire[0]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1232'] = isset($main_tire[1]->DMBTR) === true ? number_format((int)round($main_tire[1]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1233'] = isset($main_tire[2]->DMBTR) === true ? number_format((int)round($main_tire[2]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1234'] = isset($main_tire[3]->DMBTR) === true ? number_format((int)round($main_tire[3]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1235'] = isset($main_tire[4]->DMBTR) === true ? number_format((int)round($main_tire[4]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1236'] = isset($main_tire[5]->DMBTR) === true ? number_format((int)round($main_tire[5]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1237'] = isset($main_tire[6]->DMBTR) === true ? number_format((int)round($main_tire[6]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1901'] = isset($main_tire[7]->DMBTR) === true ? number_format((int)round($main_tire[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[0]['company_1901'] = 0;
                            }
                            $maintenance[0]['total'] = number_format((int)str_replace(',', '', $maintenance[0]['company_1231']) + (int)str_replace(',', '', $maintenance[0]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[0]['company_1233']) + (int)str_replace(',', '', $maintenance[0]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[0]['company_1235']) + (int)str_replace(',', '', $maintenance[0]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[0]['company_1237']) + (int)str_replace(',', '', $maintenance[0]['company_1901']));
                        }
                        else {
                            $maintenance[0]['company_1231'] = isset($main_tire[0]->DMBTR) === true ? number_format((int)round($main_tire[0]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1232'] = isset($main_tire[1]->DMBTR) === true ? number_format((int)round($main_tire[1]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1233'] = isset($main_tire[2]->DMBTR) === true ? number_format((int)round($main_tire[2]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1234'] = isset($main_tire[3]->DMBTR) === true ? number_format((int)round($main_tire[3]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1235'] = isset($main_tire[4]->DMBTR) === true ? number_format((int)round($main_tire[4]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1236'] = isset($main_tire[5]->DMBTR) === true ? number_format((int)round($main_tire[5]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1237'] = isset($main_tire[6]->DMBTR) === true ? number_format((int)round($main_tire[6]->DMBTR) / 1000) : 0;
                            $maintenance[0]['company_1901'] = isset($main_tire[7]->DMBTR) === true ? number_format((int)round($main_tire[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($main_tire_delivery !== []) {
                    for ($x = 0; $x < count($main_tire_delivery); $x++) {
                        if ($main_tire_delivery[$x]->DMBTR !== null) {
                            $maintenance[1]['company_1231'] = isset($main_tire_delivery[0]->DMBTR) === true ? number_format((int)round($main_tire_delivery[0]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1232'] = isset($main_tire_delivery[1]->DMBTR) === true ? number_format((int)round($main_tire_delivery[1]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1233'] = isset($main_tire_delivery[2]->DMBTR) === true ? number_format((int)round($main_tire_delivery[2]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1234'] = isset($main_tire_delivery[3]->DMBTR) === true ? number_format((int)round($main_tire_delivery[3]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1235'] = isset($main_tire_delivery[4]->DMBTR) === true ? number_format((int)round($main_tire_delivery[4]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1236'] = isset($main_tire_delivery[5]->DMBTR) === true ? number_format((int)round($main_tire_delivery[5]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1237'] = isset($main_tire_delivery[6]->DMBTR) === true ? number_format((int)round($main_tire_delivery[6]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1901'] = isset($main_tire_delivery[7]->DMBTR) === true ? number_format((int)round($main_tire_delivery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[1]['company_1901'] = 0;
                            }
                            $maintenance[1]['total'] = number_format((int)str_replace(',', '', $maintenance[1]['company_1231']) + (int)str_replace(',', '', $maintenance[1]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[1]['company_1233']) + (int)str_replace(',', '', $maintenance[1]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[1]['company_1235']) + (int)str_replace(',', '', $maintenance[1]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[1]['company_1237']) + (int)str_replace(',', '', $maintenance[1]['company_1901']));
                        }
                        else {
                            $maintenance[1]['company_1231'] = isset($main_tire_delivery[0]->DMBTR) === true ? number_format((int)round($main_tire_delivery[0]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1232'] = isset($main_tire_delivery[1]->DMBTR) === true ? number_format((int)round($main_tire_delivery[1]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1233'] = isset($main_tire_delivery[2]->DMBTR) === true ? number_format((int)round($main_tire_delivery[2]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1234'] = isset($main_tire_delivery[3]->DMBTR) === true ? number_format((int)round($main_tire_delivery[3]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1235'] = isset($main_tire_delivery[4]->DMBTR) === true ? number_format((int)round($main_tire_delivery[4]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1236'] = isset($main_tire_delivery[5]->DMBTR) === true ? number_format((int)round($main_tire_delivery[5]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1237'] = isset($main_tire_delivery[6]->DMBTR) === true ? number_format((int)round($main_tire_delivery[6]->DMBTR) / 1000) : 0;
                            $maintenance[1]['company_1901'] = isset($main_tire_delivery[7]->DMBTR) === true ? number_format((int)round($main_tire_delivery[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($main_battery !== []) {
                    for ($x = 0; $x < count($main_battery); $x++) {

                        if ($main_battery[$x]->DMBTR !== null) {
                            $maintenance[2]['company_1231'] = isset($main_battery[0]->DMBTR) === true ? number_format((int)round($main_battery[0]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1232'] = isset($main_battery[1]->DMBTR) === true ? number_format((int)round($main_battery[1]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1233'] = isset($main_battery[2]->DMBTR) === true ? number_format((int)round($main_battery[2]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1234'] = isset($main_battery[3]->DMBTR) === true ? number_format((int)round($main_battery[3]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1235'] = isset($main_battery[4]->DMBTR) === true ? number_format((int)round($main_battery[4]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1236'] = isset($main_battery[5]->DMBTR) === true ? number_format((int)round($main_battery[5]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1237'] = isset($main_battery[6]->DMBTR) === true ? number_format((int)round($main_battery[6]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1901'] = isset($main_battery[7]->DMBTR) === true ? number_format((int)round($main_battery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[2]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[2]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[2]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[2]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[2]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[2]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[2]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[2]['company_1901'] = 0;
                            }
                            $maintenance[2]['total'] = number_format((int)str_replace(',', '', $maintenance[2]['company_1231']) + (int)str_replace(',', '', $maintenance[2]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[2]['company_1233']) + (int)str_replace(',', '', $maintenance[2]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[2]['company_1235']) + (int)str_replace(',', '', $maintenance[2]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[2]['company_1237']) + (int)str_replace(',', '', $maintenance[2]['company_1901']));
                        }
                        else {
                            $maintenance[2]['company_1231'] = isset($main_battery[0]->DMBTR) === true ? number_format((int)round($main_battery[0]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1232'] = isset($main_battery[1]->DMBTR) === true ? number_format((int)round($main_battery[1]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1233'] = isset($main_battery[2]->DMBTR) === true ? number_format((int)round($main_battery[2]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1234'] = isset($main_battery[3]->DMBTR) === true ? number_format((int)round($main_battery[3]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1235'] = isset($main_battery[4]->DMBTR) === true ? number_format((int)round($main_battery[4]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1236'] = isset($main_battery[5]->DMBTR) === true ? number_format((int)round($main_battery[5]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1237'] = isset($main_battery[6]->DMBTR) === true ? number_format((int)round($main_battery[6]->DMBTR) / 1000) : 0;
                            $maintenance[2]['company_1901'] = isset($main_battery[7]->DMBTR) === true ? number_format((int)round($main_battery[7]->DMBTR) / 1000) : 0;

//                            $maintenance[2]['total'] = 0;
                        }
                    }
                }

                if ($main_battery_delivery !== []) {
                    for ($x = 0; $x < count($main_battery_delivery); $x++) {

                        if ($main_battery_delivery[$x]->DMBTR !== null) {
                            $maintenance[3]['company_1231'] = isset($main_battery_delivery[0]->DMBTR) === true ? number_format((int)round($main_battery_delivery[0]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1232'] = isset($main_battery_delivery[1]->DMBTR) === true ? number_format((int)round($main_battery_delivery[1]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1233'] = isset($main_battery_delivery[2]->DMBTR) === true ? number_format((int)round($main_battery_delivery[2]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1234'] = isset($main_battery_delivery[3]->DMBTR) === true ? number_format((int)round($main_battery_delivery[3]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1235'] = isset($main_battery_delivery[4]->DMBTR) === true ? number_format((int)round($main_battery_delivery[4]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1236'] = isset($main_battery_delivery[5]->DMBTR) === true ? number_format((int)round($main_battery_delivery[5]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1237'] = isset($main_battery_delivery[6]->DMBTR) === true ? number_format((int)round($main_battery_delivery[6]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1901'] = isset($main_battery_delivery[7]->DMBTR) === true ? number_format((int)round($main_battery_delivery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[3]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[3]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[3]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[3]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[3]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[3]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[3]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[3]['company_1901'] = 0;
                            }
                            $maintenance[3]['total'] = number_format((int)str_replace(',', '', $maintenance[3]['company_1231']) + (int)str_replace(',', '', $maintenance[3]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[3]['company_1233']) + (int)str_replace(',', '', $maintenance[3]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[3]['company_1235']) + (int)str_replace(',', '', $maintenance[3]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[3]['company_1237']) + (int)str_replace(',', '', $maintenance[3]['company_1901']));

                        }
                        else {
                            $maintenance[3]['company_1231'] = isset($main_battery_delivery[0]->DMBTR) === true ? number_format((int)round($main_battery_delivery[0]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1232'] = isset($main_battery_delivery[1]->DMBTR) === true ? number_format((int)round($main_battery_delivery[1]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1233'] = isset($main_battery_delivery[2]->DMBTR) === true ? number_format((int)round($main_battery_delivery[2]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1234'] = isset($main_battery_delivery[3]->DMBTR) === true ? number_format((int)round($main_battery_delivery[3]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1235'] = isset($main_battery_delivery[4]->DMBTR) === true ? number_format((int)round($main_battery_delivery[4]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1236'] = isset($main_battery_delivery[5]->DMBTR) === true ? number_format((int)round($main_battery_delivery[5]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1237'] = isset($main_battery_delivery[6]->DMBTR) === true ? number_format((int)round($main_battery_delivery[6]->DMBTR) / 1000) : 0;
                            $maintenance[3]['company_1901'] = isset($main_battery_delivery[7]->DMBTR) === true ? number_format((int)round($main_battery_delivery[7]->DMBTR) / 1000) : 0;

                        }

                    }
                }
                if ($main_machinery !== []) {
                    for ($x = 0; $x < count($main_machinery); $x++) {

                        if ($main_machinery[$x]->DMBTR !== null) {
                            $maintenance[4]['company_1231'] = isset($main_machinery[0]->DMBTR) === true ? number_format((int)round($main_machinery[0]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1232'] = isset($main_machinery[1]->DMBTR) === true ? number_format((int)round($main_machinery[1]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1233'] = isset($main_machinery[2]->DMBTR) === true ? number_format((int)round($main_machinery[2]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1234'] = isset($main_machinery[3]->DMBTR) === true ? number_format((int)round($main_machinery[3]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1235'] = isset($main_machinery[4]->DMBTR) === true ? number_format((int)round($main_machinery[4]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1236'] = isset($main_machinery[5]->DMBTR) === true ? number_format((int)round($main_machinery[5]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1237'] = isset($main_machinery[6]->DMBTR) === true ? number_format((int)round($main_machinery[6]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1901'] = isset($main_machinery[7]->DMBTR) === true ? number_format((int)round($main_machinery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[4]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[4]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[4]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[4]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[4]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[4]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[4]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[4]['company_1901'] = 0;
                            }
                            $maintenance[4]['total'] = number_format((int)str_replace(',', '', $maintenance[4]['company_1231']) + (int)str_replace(',', '', $maintenance[4]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[4]['company_1233']) + (int)str_replace(',', '', $maintenance[4]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[4]['company_1235']) + (int)str_replace(',', '', $maintenance[4]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[4]['company_1237']) + (int)str_replace(',', '', $maintenance[4]['company_1901']));

                        }
                        else {
                            $maintenance[4]['company_1231'] = isset($main_machinery[0]->DMBTR) === true ? number_format((int)round($main_machinery[0]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1232'] = isset($main_machinery[1]->DMBTR) === true ? number_format((int)round($main_machinery[1]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1233'] = isset($main_machinery[2]->DMBTR) === true ? number_format((int)round($main_machinery[2]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1234'] = isset($main_machinery[3]->DMBTR) === true ? number_format((int)round($main_machinery[3]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1235'] = isset($main_machinery[4]->DMBTR) === true ? number_format((int)round($main_machinery[4]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1236'] = isset($main_machinery[5]->DMBTR) === true ? number_format((int)round($main_machinery[5]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1237'] = isset($main_machinery[6]->DMBTR) === true ? number_format((int)round($main_machinery[6]->DMBTR) / 1000) : 0;
                            $maintenance[4]['company_1901'] = isset($main_machinery[7]->DMBTR) === true ? number_format((int)round($main_machinery[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($main_oil_vehicles !== []) {
                    for ($x = 0; $x < count($main_oil_vehicles); $x++) {

                        if ($main_oil_vehicles[$x]->DMBTR !== null) {
                            $maintenance[5]['company_1231'] = isset($main_oil_vehicles[0]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[0]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1232'] = isset($main_oil_vehicles[1]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[1]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1233'] = isset($main_oil_vehicles[2]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[2]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1234'] = isset($main_oil_vehicles[3]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[3]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1235'] = isset($main_oil_vehicles[4]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[4]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1236'] = isset($main_oil_vehicles[5]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[5]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1237'] = isset($main_oil_vehicles[6]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[6]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1901'] = isset($main_oil_vehicles[7]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[5]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[5]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[5]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[5]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[5]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[5]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[5]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[5]['company_1901'] = 0;
                            }
                            $maintenance[5]['total'] = number_format((int)str_replace(',', '', $maintenance[5]['company_1231']) + (int)str_replace(',', '', $maintenance[5]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[5]['company_1233']) + (int)str_replace(',', '', $maintenance[5]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[5]['company_1235']) + (int)str_replace(',', '', $maintenance[5]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[5]['company_1237']) + (int)str_replace(',', '', $maintenance[5]['company_1901']));

                        }
                        else {
                            $maintenance[5]['company_1231'] = isset($main_oil_vehicles[0]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[0]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1232'] = isset($main_oil_vehicles[1]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[1]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1233'] = isset($main_oil_vehicles[2]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[2]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1234'] = isset($main_oil_vehicles[3]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[3]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1235'] = isset($main_oil_vehicles[4]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[4]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1236'] = isset($main_oil_vehicles[5]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[5]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1237'] = isset($main_oil_vehicles[6]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[6]->DMBTR) / 1000) : 0;
                            $maintenance[5]['company_1901'] = isset($main_oil_vehicles[7]->DMBTR) === true ? number_format((int)round($main_oil_vehicles[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($main_oil_vehicles_delivery !== []) {
                    for ($x = 0; $x < count($main_oil_vehicles_delivery); $x++) {

                        if ($main_oil_vehicles_delivery[$x]->DMBTR !== null) {
                            $maintenance[6]['company_1231'] = isset($main_oil_vehicles_delivery[0]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[0]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1232'] = isset($main_oil_vehicles_delivery[1]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[1]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1233'] = isset($main_oil_vehicles_delivery[2]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[2]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1234'] = isset($main_oil_vehicles_delivery[3]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[3]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1235'] = isset($main_oil_vehicles_delivery[4]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[4]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1236'] = isset($main_oil_vehicles_delivery[5]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[5]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1237'] = isset($main_oil_vehicles_delivery[6]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[6]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1901'] = isset($main_oil_vehicles_delivery[7]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $maintenance[6]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $maintenance[6]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $maintenance[6]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $maintenance[6]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $maintenance[6]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $maintenance[6]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $maintenance[6]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $maintenance[6]['company_1901'] = 0;
                            }
                            $maintenance[6]['total'] = number_format((int)str_replace(',', '', $maintenance[6]['company_1231']) + (int)str_replace(',', '', $maintenance[6]['company_1232'])
                                + (int)str_replace(',', '', $maintenance[6]['company_1233']) + (int)str_replace(',', '', $maintenance[6]['company_1234'])
                                + (int)str_replace(',', '', $maintenance[6]['company_1235']) + (int)str_replace(',', '', $maintenance[6]['company_1236'])
                                + (int)str_replace(',', '', $maintenance[6]['company_1237']) + (int)str_replace(',', '', $maintenance[6]['company_1901']));

                        }
                        else {
                            $maintenance[6]['company_1231'] = isset($main_oil_vehicles_delivery[0]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[0]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1232'] = isset($main_oil_vehicles_delivery[1]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[1]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1233'] = isset($main_oil_vehicles_delivery[2]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[2]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1234'] = isset($main_oil_vehicles_delivery[3]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[3]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1235'] = isset($main_oil_vehicles_delivery[4]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[4]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1236'] = isset($main_oil_vehicles_delivery[5]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[5]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1237'] = isset($main_oil_vehicles_delivery[6]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[6]->DMBTR) / 1000) : 0;
                            $maintenance[6]['company_1901'] = isset($main_oil_vehicles_delivery[7]->DMBTR) === true ? number_format((int)round($main_oil_vehicles_delivery[7]->DMBTR) / 1000) : 0;
                        }
                    }
                }

                if ($main_tire === []) {
                    $maintenance[0]['company_1231'] = 0;
                    $maintenance[0]['company_1232'] = 0;
                    $maintenance[0]['company_1233'] = 0;
                    $maintenance[0]['company_1234'] = 0;
                    $maintenance[0]['company_1235'] = 0;
                    $maintenance[0]['company_1236'] = 0;
                    $maintenance[0]['company_1237'] = 0;
                    $maintenance[0]['company_1901'] = 0;
                    $maintenance[0]['total'] = 0;
                }
                if ($main_tire_delivery === []) {
                    $maintenance[1]['company_1231'] = 0;
                    $maintenance[1]['company_1232'] = 0;
                    $maintenance[1]['company_1233'] = 0;
                    $maintenance[1]['company_1234'] = 0;
                    $maintenance[1]['company_1235'] = 0;
                    $maintenance[1]['company_1236'] = 0;
                    $maintenance[1]['company_1237'] = 0;
                    $maintenance[1]['company_1901'] = 0;
                    $maintenance[1]['total'] = 0;
                }
                if ($main_battery === []) {
                    $maintenance[2]['company_1231'] = 0;
                    $maintenance[2]['company_1232'] = 0;
                    $maintenance[2]['company_1233'] = 0;
                    $maintenance[2]['company_1234'] = 0;
                    $maintenance[2]['company_1235'] = 0;
                    $maintenance[2]['company_1236'] = 0;
                    $maintenance[2]['company_1237'] = 0;
                    $maintenance[2]['company_1901'] = 0;
                    $maintenance[2]['total'] = 0;
                }
                if ($main_battery_delivery === []) {
                    $maintenance[3]['company_1231'] = 0;
                    $maintenance[3]['company_1232'] = 0;
                    $maintenance[3]['company_1233'] = 0;
                    $maintenance[3]['company_1234'] = 0;
                    $maintenance[3]['company_1235'] = 0;
                    $maintenance[3]['company_1236'] = 0;
                    $maintenance[3]['company_1237'] = 0;
                    $maintenance[3]['company_1901'] = 0;
                    $maintenance[3]['total'] = 0;
                }
                if ($main_machinery === []) {
                    $maintenance[4]['company_1231'] = 0;
                    $maintenance[4]['company_1232'] = 0;
                    $maintenance[4]['company_1233'] = 0;
                    $maintenance[4]['company_1234'] = 0;
                    $maintenance[4]['company_1235'] = 0;
                    $maintenance[4]['company_1236'] = 0;
                    $maintenance[4]['company_1237'] = 0;
                    $maintenance[4]['company_1901'] = 0;
                    $maintenance[4]['total'] = 0;
                }
                if ($main_oil_vehicles === []) {
                    $maintenance[5]['company_1231'] = 0;
                    $maintenance[5]['company_1232'] = 0;
                    $maintenance[5]['company_1233'] = 0;
                    $maintenance[5]['company_1234'] = 0;
                    $maintenance[5]['company_1235'] = 0;
                    $maintenance[5]['company_1236'] = 0;
                    $maintenance[5]['company_1237'] = 0;
                    $maintenance[5]['company_1901'] = 0;
                    $maintenance[5]['total'] = 0;
                }
                if ($main_oil_vehicles_delivery === []) {
                    $maintenance[6]['company_1231'] = 0;
                    $maintenance[6]['company_1232'] = 0;
                    $maintenance[6]['company_1233'] = 0;
                    $maintenance[6]['company_1234'] = 0;
                    $maintenance[6]['company_1235'] = 0;
                    $maintenance[6]['company_1236'] = 0;
                    $maintenance[6]['company_1237'] = 0;
                    $maintenance[6]['company_1901'] = 0;
                    $maintenance[6]['total'] = 0;
                }


                //maintenance
                $company_1231_main = (int)str_replace(',', '', $maintenance[0]['company_1231']) + (int)str_replace(',', '', $maintenance[1]['company_1231'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1231']) + (int)str_replace(',', '', $maintenance[3]['company_1231'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1231']) + (int)str_replace(',', '', $maintenance[5]['company_1231'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1231']);
                $company_1232_main = (int)str_replace(',', '', $maintenance[0]['company_1232']) + (int)str_replace(',', '', $maintenance[1]['company_1232'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1232']) + (int)str_replace(',', '', $maintenance[3]['company_1232'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1232']) + (int)str_replace(',', '', $maintenance[5]['company_1232'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1232']);
                $company_1233_main = (int)str_replace(',', '', $maintenance[0]['company_1233']) + (int)str_replace(',', '', $maintenance[1]['company_1233'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1233']) + (int)str_replace(',', '', $maintenance[3]['company_1233'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1233']) + (int)str_replace(',', '', $maintenance[5]['company_1233'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1233']);
                $company_1234_main = (int)str_replace(',', '', $maintenance[0]['company_1234']) + (int)str_replace(',', '', $maintenance[1]['company_1234'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1234']) + (int)str_replace(',', '', $maintenance[3]['company_1234'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1234']) + (int)str_replace(',', '', $maintenance[5]['company_1234'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1234']);
                $company_1235_main = (int)str_replace(',', '', $maintenance[0]['company_1235']) + (int)str_replace(',', '', $maintenance[1]['company_1235'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1235']) + (int)str_replace(',', '', $maintenance[3]['company_1235'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1235']) + (int)str_replace(',', '', $maintenance[5]['company_1235'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1235']);
                $company_1236_main = (int)str_replace(',', '', $maintenance[0]['company_1236']) + (int)str_replace(',', '', $maintenance[1]['company_1236'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1236']) + (int)str_replace(',', '', $maintenance[3]['company_1236'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1236']) + (int)str_replace(',', '', $maintenance[5]['company_1236'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1236']);
                $company_1237_main = (int)str_replace(',', '', $maintenance[0]['company_1237']) + (int)str_replace(',', '', $maintenance[1]['company_1237'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1237']) + (int)str_replace(',', '', $maintenance[3]['company_1237'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1237']) + (int)str_replace(',', '', $maintenance[5]['company_1237'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1237']);
                $company_1901_main = (int)str_replace(',', '', $maintenance[0]['company_1901']) + (int)str_replace(',', '', $maintenance[1]['company_1901'])
                    + (int)str_replace(',', '', $maintenance[2]['company_1901']) + (int)str_replace(',', '', $maintenance[3]['company_1901'])
                    + (int)str_replace(',', '', $maintenance[4]['company_1901']) + (int)str_replace(',', '', $maintenance[5]['company_1901'])
                    + (int)str_replace(',', '', $maintenance[6]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_main = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_main = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_main = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_main = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_main = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_main = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_main = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_main = 0;
                }

                $total_all_main = $company_1231_main + $company_1232_main + $company_1233_main + $company_1234_main + $company_1235_main + $company_1236_main + $company_1237_main + $company_1901_main;
            }
//            return response()->json($maintenance);
            for ($i = 0; $i < count($repairs); $i++) {
                $repairs[0]['name'] = 'REPAIR - VEHICLES';
                $repairs[1]['name'] = 'REPAIRS - DELIVERY VEHICLES';
                if ($repair_vehicles !== []) {
                    for ($x = 0; $x < count($repair_vehicles); $x++) {
                        if ($repair_vehicles [$x]->DMBTR !== null) {
                            $repairs[0]['company_1231'] = isset($repair_vehicles [0]->DMBTR) === true ? number_format((int)round($repair_vehicles [0]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1232'] = isset($repair_vehicles [1]->DMBTR) === true ? number_format((int)round($repair_vehicles [1]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1233'] = isset($repair_vehicles [2]->DMBTR) === true ? number_format((int)round($repair_vehicles [2]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1234'] = isset($repair_vehicles [3]->DMBTR) === true ? number_format((int)round($repair_vehicles [3]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1235'] = isset($repair_vehicles [4]->DMBTR) === true ? number_format((int)round($repair_vehicles [4]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1236'] = isset($repair_vehicles [5]->DMBTR) === true ? number_format((int)round($repair_vehicles [5]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1237'] = isset($repair_vehicles [6]->DMBTR) === true ? number_format((int)round($repair_vehicles [6]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1901'] = isset($repair_vehicles [7]->DMBTR) === true ? number_format((int)round($repair_vehicles [7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $repairs[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $repairs[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $repairs[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $repairs[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $repairs[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $repairs[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $repairs[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $repairs[0]['company_1901'] = 0;
                            }
                            $repairs[0]['total'] = number_format((int)str_replace(',', '', $repairs[0]['company_1231']) + (int)str_replace(',', '', $repairs[0]['company_1232'])
                                + (int)str_replace(',', '', $repairs[0]['company_1233']) + (int)str_replace(',', '', $repairs[0]['company_1234'])
                                + (int)str_replace(',', '', $repairs[0]['company_1235']) + (int)str_replace(',', '', $repairs[0]['company_1236'])
                                + (int)str_replace(',', '', $repairs[0]['company_1237']) + (int)str_replace(',', '', $repairs[0]['company_1901']));
                        }
                        else {
                            $repairs[0]['company_1231'] = isset($repair_vehicles [0]->DMBTR) === true ? number_format((int)round($repair_vehicles [0]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1232'] = isset($repair_vehicles [1]->DMBTR) === true ? number_format((int)round($repair_vehicles [1]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1233'] = isset($repair_vehicles [2]->DMBTR) === true ? number_format((int)round($repair_vehicles [2]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1234'] = isset($repair_vehicles [3]->DMBTR) === true ? number_format((int)round($repair_vehicles [3]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1235'] = isset($repair_vehicles [4]->DMBTR) === true ? number_format((int)round($repair_vehicles [4]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1236'] = isset($repair_vehicles [5]->DMBTR) === true ? number_format((int)round($repair_vehicles [5]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1237'] = isset($repair_vehicles [6]->DMBTR) === true ? number_format((int)round($repair_vehicles [6]->DMBTR) / 1000) : 0;
                            $repairs[0]['company_1901'] = isset($repair_vehicles [7]->DMBTR) === true ? number_format((int)round($repair_vehicles [7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($repair_vehicles_delivery !== []) {
                    for ($x = 0; $x < count($repair_vehicles_delivery); $x++) {
                        if ($repair_vehicles_delivery[$x]->DMBTR !== null) {
                            $repairs[1]['company_1231'] = isset($repair_vehicles_delivery[0]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[0]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1232'] = isset($repair_vehicles_delivery[1]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[1]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1233'] = isset($repair_vehicles_delivery[2]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[2]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1234'] = isset($repair_vehicles_delivery[3]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[3]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1235'] = isset($repair_vehicles_delivery[4]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[4]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1236'] = isset($repair_vehicles_delivery[5]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[5]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1237'] = isset($repair_vehicles_delivery[6]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[6]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1901'] = isset($repair_vehicles_delivery[7]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $repairs[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $repairs[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $repairs[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $repairs[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $repairs[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $repairs[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $repairs[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $repairs[1]['company_1901'] = 0;
                            }
                            $repairs[1]['total'] = number_format((int)str_replace(',', '', $repairs[1]['company_1231']) + (int)str_replace(',', '', $repairs[1]['company_1232'])
                                + (int)str_replace(',', '', $repairs[1]['company_1233']) + (int)str_replace(',', '', $repairs[1]['company_1234'])
                                + (int)str_replace(',', '', $repairs[1]['company_1235']) + (int)str_replace(',', '', $repairs[1]['company_1236'])
                                + (int)str_replace(',', '', $repairs[1]['company_1237']) + (int)str_replace(',', '', $repairs[1]['company_1901']));
                        }
                        else {
                            $repairs[1]['company_1231'] = isset($repair_vehicles_delivery[0]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[0]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1232'] = isset($repair_vehicles_delivery[1]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[1]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1233'] = isset($repair_vehicles_delivery[2]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[2]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1234'] = isset($repair_vehicles_delivery[3]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[3]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1235'] = isset($repair_vehicles_delivery[4]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[4]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1236'] = isset($repair_vehicles_delivery[5]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[5]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1237'] = isset($repair_vehicles_delivery[6]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[6]->DMBTR) / 1000) : 0;
                            $repairs[1]['company_1901'] = isset($repair_vehicles_delivery[7]->DMBTR) === true ? number_format((int)round($repair_vehicles_delivery[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }

                if ($repair_vehicles === []) {
                    $repairs[0]['company_1231'] = 0;
                    $repairs[0]['company_1232'] = 0;
                    $repairs[0]['company_1233'] = 0;
                    $repairs[0]['company_1234'] = 0;
                    $repairs[0]['company_1235'] = 0;
                    $repairs[0]['company_1236'] = 0;
                    $repairs[0]['company_1237'] = 0;
                    $repairs[0]['company_1901'] = 0;
                    $repairs[0]['total'] = 0;
                }
                if ($repair_vehicles_delivery === []) {
                    $repairs[1]['company_1231'] = 0;
                    $repairs[1]['company_1232'] = 0;
                    $repairs[1]['company_1233'] = 0;
                    $repairs[1]['company_1234'] = 0;
                    $repairs[1]['company_1235'] = 0;
                    $repairs[1]['company_1236'] = 0;
                    $repairs[1]['company_1237'] = 0;
                    $repairs[1]['company_1901'] = 0;
                    $repairs[1]['total'] = 0;
                }
                //repairs
                $company_1231_repairs = (int)str_replace(',', '', $repairs[0]['company_1231']) + (int)str_replace(',', '', $repairs[1]['company_1231']);
                $company_1232_repairs = (int)str_replace(',', '', $repairs[0]['company_1232']) + (int)str_replace(',', '', $repairs[1]['company_1232']);
                $company_1233_repairs = (int)str_replace(',', '', $repairs[0]['company_1233']) + (int)str_replace(',', '', $repairs[1]['company_1233']);
                $company_1234_repairs = (int)str_replace(',', '', $repairs[0]['company_1234']) + (int)str_replace(',', '', $repairs[1]['company_1234']);
                $company_1235_repairs = (int)str_replace(',', '', $repairs[0]['company_1235']) + (int)str_replace(',', '', $repairs[1]['company_1235']);
                $company_1236_repairs = (int)str_replace(',', '', $repairs[0]['company_1236']) + (int)str_replace(',', '', $repairs[1]['company_1236']);
                $company_1237_repairs = (int)str_replace(',', '', $repairs[0]['company_1237']) + (int)str_replace(',', '', $repairs[1]['company_1237']);
                $company_1901_repairs = (int)str_replace(',', '', $repairs[0]['company_1901']) + (int)str_replace(',', '', $repairs[1]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_repairs = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_repairs = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_repairs = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_repairs = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_repairs = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_repairs = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_repairs = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_repairs = 0;
                }
                $total_all_repairs = $company_1231_repairs + $company_1232_repairs + $company_1233_repairs + $company_1234_repairs
                    + $company_1235_repairs + $company_1236_repairs + $company_1237_repairs + $company_1901_repairs;
            }

            for ($i = 0; $i < count($parking); $i++) {
                $parking[0]['name'] = 'PARKING - OFFICE VEHICLES';
//                $parking[1]['name'] = 'PARKING - FSP VEHICLES';
                $parking[1]['name'] = 'PARKING - DELIVERY VEHICLES'; // [2]
                if ($parking_office !== []) {
                    for ($x = 0; $x < count($parking_office); $x++) {
                        if ($parking_office[$x]->DMBTR !== null) {
                            $parking[0]['company_1231'] = isset($parking_office[0]->DMBTR) === true ? number_format((int)round($parking_office[0]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1232'] = isset($parking_office[1]->DMBTR) === true ? number_format((int)round($parking_office[1]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1233'] = isset($parking_office[2]->DMBTR) === true ? number_format((int)round($parking_office[2]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1234'] = isset($parking_office[3]->DMBTR) === true ? number_format((int)round($parking_office[3]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1235'] = isset($parking_office[4]->DMBTR) === true ? number_format((int)round($parking_office[4]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1236'] = isset($parking_office[5]->DMBTR) === true ? number_format((int)round($parking_office[5]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1237'] = isset($parking_office[6]->DMBTR) === true ? number_format((int)round($parking_office[6]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1901'] = isset($parking_office[7]->DMBTR) === true ? number_format((int)round($parking_office[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $parking[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $parking[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $parking[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $parking[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $parking[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $parking[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $parking[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $parking[0]['company_1901'] = 0;
                            }
                            $parking[0]['total'] = number_format((int)str_replace(',', '', $parking[0]['company_1231']) + (int)str_replace(',', '', $parking[0]['company_1232'])
                                + (int)str_replace(',', '', $parking[0]['company_1233']) + (int)str_replace(',', '', $parking[0]['company_1234'])
                                + (int)str_replace(',', '', $parking[0]['company_1235']) + (int)str_replace(',', '', $parking[0]['company_1236'])
                                + (int)str_replace(',', '', $parking[0]['company_1237']) + (int)str_replace(',', '', $parking[0]['company_1901']));
                        }
                        else {
                            $parking[0]['company_1231'] = isset($parking_office[0]->DMBTR) === true ? number_format((int)round($parking_office[0]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1232'] = isset($parking_office[1]->DMBTR) === true ? number_format((int)round($parking_office[1]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1233'] = isset($parking_office[2]->DMBTR) === true ? number_format((int)round($parking_office[2]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1234'] = isset($parking_office[3]->DMBTR) === true ? number_format((int)round($parking_office[3]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1235'] = isset($parking_office[4]->DMBTR) === true ? number_format((int)round($parking_office[4]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1236'] = isset($parking_office[5]->DMBTR) === true ? number_format((int)round($parking_office[5]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1237'] = isset($parking_office[6]->DMBTR) === true ? number_format((int)round($parking_office[6]->DMBTR) / 1000) : 0;
                            $parking[0]['company_1901'] = isset($parking_office[7]->DMBTR) === true ? number_format((int)round($parking_office[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
//                if ($parking_fsp_vehicles !== []) {
//                    for ($x = 0; $x < count($parking_fsp_vehicles); $x++) {
//                        if ($parking_fsp_vehicles[$x]->DMBTR !== null) {
//                            $parking[1]['company_1231'] = isset($parking_fsp_vehicles[0]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[0]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1232'] = isset($parking_fsp_vehicles[1]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[1]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1233'] = isset($parking_fsp_vehicles[2]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[2]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1234'] = isset($parking_fsp_vehicles[3]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[3]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1235'] = isset($parking_fsp_vehicles[4]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[4]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1236'] = isset($parking_fsp_vehicles[5]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[5]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1237'] = isset($parking_fsp_vehicles[6]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[6]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1901'] = isset($parking_fsp_vehicles[7]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[7]->DMBTR) / 1000) : 0;
//
//                            if ($isUser_HIL === null) {
//                                $parking[1]['company_1231'] = 0;
//                            }
//                            if ($isUser_SIB === null) {
//                                $parking[1]['company_1232'] = 0;
//                            }
//                            if ($isUser_ROX === null) {
//                                $parking[1]['company_1233'] = 0;
//                            }
//                            if ($isUser_SUM === null) {
//                                $parking[1]['company_1234'] = 0;
//                            }
//                            if ($isUser_DIV === null) {
//                                $parking[1]['company_1235'] = 0;
//                            }
//                            if ($isUser_JARO === null) {
//                                $parking[1]['company_1236'] = 0;
//                            }
//                            if ($isUser_KAB === null) {
//                                $parking[1]['company_1237'] = 0;
//                            }
//                            if ($isUser_CS === null) {
//                                $parking[1]['company_1901'] = 0;
//                            }
//                            $parking[1]['total'] = number_format((int)str_replace(',', '', $parking[1]['company_1231']) + (int)str_replace(',', '', $parking[1]['company_1232'])
//                                + (int)str_replace(',', '', $parking[1]['company_1233']) + (int)str_replace(',', '', $parking[1]['company_1234'])
//                                + (int)str_replace(',', '', $parking[1]['company_1235']) + (int)str_replace(',', '', $parking[1]['company_1236'])
//                                + (int)str_replace(',', '', $parking[1]['company_1237']) + (int)str_replace(',', '', $parking[1]['company_1901']));
//                        }
//                        else {
//                            $parking[1]['company_1231'] = isset($parking_fsp_vehicles[0]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[0]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1232'] = isset($parking_fsp_vehicles[1]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[1]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1233'] = isset($parking_fsp_vehicles[2]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[2]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1234'] = isset($parking_fsp_vehicles[3]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[3]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1235'] = isset($parking_fsp_vehicles[4]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[4]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1236'] = isset($parking_fsp_vehicles[5]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[5]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1237'] = isset($parking_fsp_vehicles[6]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[6]->DMBTR) / 1000) : 0;
//                            $parking[1]['company_1901'] = isset($parking_fsp_vehicles[7]->DMBTR) === true ? number_format((int)round($parking_fsp_vehicles[7]->DMBTR) / 1000) : 0;
//
//                        }
//                    }
//                }
                if ($parking_delivery_vehicles !== []) {
                    for ($x = 0; $x < count($parking_delivery_vehicles); $x++) {
                        if ($parking_delivery_vehicles[$x]->DMBTR !== null) {

                            $parking[1]['company_1231'] = isset($parking_delivery_vehicles[0]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[0]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1232'] = isset($parking_delivery_vehicles[1]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[1]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1233'] = isset($parking_delivery_vehicles[2]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[2]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1234'] = isset($parking_delivery_vehicles[3]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[3]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1235'] = isset($parking_delivery_vehicles[4]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[4]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1236'] = isset($parking_delivery_vehicles[5]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[5]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1237'] = isset($parking_delivery_vehicles[6]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[6]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1901'] = isset($parking_delivery_vehicles[7]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[7]->DMBTR) / 1000) : 0;

//                            dd($parking[1]['company_1901']);
                            if ($isUser_HIL === null) {
                                $parking[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $parking[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $parking[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $parking[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $parking[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $parking[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $parking[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $parking[1]['company_1901'] = 0;
                            }
                            $parking[1]['total'] = number_format((int)str_replace(',', '', $parking[1]['company_1231']) + (int)str_replace(',', '', $parking[1]['company_1232'])
                                + (int)str_replace(',', '', $parking[1]['company_1233']) + (int)str_replace(',', '', $parking[1]['company_1234'])
                                + (int)str_replace(',', '', $parking[1]['company_1235']) + (int)str_replace(',', '', $parking[1]['company_1236'])
                                + (int)str_replace(',', '', $parking[1]['company_1237']) + (int)str_replace(',', '', $parking[1]['company_1901']));
                        }
                        else {
                            $parking[1]['company_1231'] = isset($parking_delivery_vehicles[0]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[0]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1232'] = isset($parking_delivery_vehicles[1]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[1]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1233'] = isset($parking_delivery_vehicles[2]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[2]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1234'] = isset($parking_delivery_vehicles[3]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[3]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1235'] = isset($parking_delivery_vehicles[4]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[4]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1236'] = isset($parking_delivery_vehicles[5]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[5]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1237'] = isset($parking_delivery_vehicles[6]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[6]->DMBTR) / 1000) : 0;
                            $parking[1]['company_1901'] = isset($parking_delivery_vehicles[7]->DMBTR) === true ? number_format((int)round($parking_delivery_vehicles[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }

                if ($parking_office === []) {
                    $parking[0]['company_1231'] = 0;
                    $parking[0]['company_1232'] = 0;
                    $parking[0]['company_1233'] = 0;
                    $parking[0]['company_1234'] = 0;
                    $parking[0]['company_1235'] = 0;
                    $parking[0]['company_1236'] = 0;
                    $parking[0]['company_1237'] = 0;
                    $parking[0]['company_1901'] = 0;
                    $parking[0]['total'] = 0;
                }
//                if ($parking_fsp_vehicles === []) {
//                    $parking[1]['company_1231'] = 0;
//                    $parking[1]['company_1232'] = 0;
//                    $parking[1]['company_1233'] = 0;
//                    $parking[1]['company_1234'] = 0;
//                    $parking[1]['company_1235'] = 0;
//                    $parking[1]['company_1236'] = 0;
//                    $parking[1]['company_1237'] = 0;
//                    $parking[1]['company_1901'] = 0;
//                    $parking[1]['total'] = 0;
//                }
                if ($parking_delivery_vehicles === []) {
                    $parking[1]['company_1231'] = 0;
                    $parking[1]['company_1232'] = 0;
                    $parking[1]['company_1233'] = 0;
                    $parking[1]['company_1234'] = 0;
                    $parking[1]['company_1235'] = 0;
                    $parking[1]['company_1236'] = 0;
                    $parking[1]['company_1237'] = 0;
                    $parking[1]['company_1901'] = 0;
                    $parking[1]['total'] = 0;
                }

                //parking
                $company_1231_parking = (int)str_replace(',', '', $parking[0]['company_1231'])
                    + (int)str_replace(',', '', $parking[1]['company_1231']);
                $company_1232_parking = (int)str_replace(',', '', $parking[0]['company_1232'])
                    + (int)str_replace(',', '', $parking[1]['company_1232']);
                $company_1233_parking = (int)str_replace(',', '', $parking[0]['company_1233'])
                    + (int)str_replace(',', '', $parking[1]['company_1233']);
                $company_1234_parking = (int)str_replace(',', '', $parking[0]['company_1234'])
                    + (int)str_replace(',', '', $parking[1]['company_1234']);
                $company_1235_parking = (int)str_replace(',', '', $parking[0]['company_1235'])
                    + (int)str_replace(',', '', $parking[1]['company_1235']);
                $company_1236_parking = (int)str_replace(',', '', $parking[0]['company_1236'])
                    + (int)str_replace(',', '', $parking[1]['company_1236']);
                $company_1237_parking = (int)str_replace(',', '', $parking[0]['company_1237'])
                    + (int)str_replace(',', '', $parking[1]['company_1237']);
                $company_1901_parking = (int)str_replace(',', '', $parking[0]['company_1901'])
                    + (int)str_replace(',', '', $parking[1]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_parking = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_parking = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_parking = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_parking = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_parking = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_parking = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_parking = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_parking = 0;
                }
                $total_all_parking = $company_1231_parking + $company_1232_parking + $company_1233_parking
                    + $company_1234_parking + $company_1235_parking + $company_1236_parking + $company_1237_parking + $company_1901_parking;
            }

            for ($i = 0; $i < count($tolling); $i++) {
                $tolling[0]['name'] = 'TOLL - OFFICE VEHICLES';
                $tolling[1]['name'] = 'TOLL - FSP VEHICLES';
                $tolling[2]['name'] = 'TOLL - DELIVERY VEHICLES';
                if ($toll_office !== []) {
                    for ($x = 0; $x < count($toll_office); $x++) {
                        if ($toll_office[$x]->DMBTR !== null) {
                            $tolling[0]['company_1231'] = isset($toll_office[0]->DMBTR) === true ? number_format((int)round($toll_office[0]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1232'] = isset($toll_office[1]->DMBTR) === true ? number_format((int)round($toll_office[1]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1233'] = isset($toll_office[2]->DMBTR) === true ? number_format((int)round($toll_office[2]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1234'] = isset($toll_office[3]->DMBTR) === true ? number_format((int)round($toll_office[3]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1235'] = isset($toll_office[4]->DMBTR) === true ? number_format((int)round($toll_office[4]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1236'] = isset($toll_office[5]->DMBTR) === true ? number_format((int)round($toll_office[5]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1237'] = isset($toll_office[6]->DMBTR) === true ? number_format((int)round($toll_office[6]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1901'] = isset($toll_office[7]->DMBTR) === true ? number_format((int)round($toll_office[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $tolling[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $tolling[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $tolling[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $tolling[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $tolling[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $tolling[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $tolling[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $tolling[0]['company_1901'] = 0;
                            }
                            $tolling[0]['total'] = number_format((int)str_replace(',', '', $tolling[0]['company_1231']) + (int)str_replace(',', '', $tolling[0]['company_1232'])
                                + (int)str_replace(',', '', $tolling[0]['company_1233']) + (int)str_replace(',', '', $tolling[0]['company_1234'])
                                + (int)str_replace(',', '', $tolling[0]['company_1235']) + (int)str_replace(',', '', $tolling[0]['company_1236'])
                                + (int)str_replace(',', '', $tolling[0]['company_1237']) + (int)str_replace(',', '', $tolling[0]['company_1901']));
                        }
                        else {
                            $tolling[0]['company_1231'] = isset($toll_office[0]->DMBTR) === true ? number_format((int)round($toll_office[0]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1232'] = isset($toll_office[1]->DMBTR) === true ? number_format((int)round($toll_office[1]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1233'] = isset($toll_office[2]->DMBTR) === true ? number_format((int)round($toll_office[2]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1234'] = isset($toll_office[3]->DMBTR) === true ? number_format((int)round($toll_office[3]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1235'] = isset($toll_office[4]->DMBTR) === true ? number_format((int)round($toll_office[4]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1236'] = isset($toll_office[5]->DMBTR) === true ? number_format((int)round($toll_office[5]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1237'] = isset($toll_office[6]->DMBTR) === true ? number_format((int)round($toll_office[6]->DMBTR) / 1000) : 0;
                            $tolling[0]['company_1901'] = isset($toll_office[7]->DMBTR) === true ? number_format((int)round($toll_office[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($toll_fsp_vehicles !== []) {
                    for ($x = 0; $x < count($toll_fsp_vehicles); $x++) {
                        if ($toll_fsp_vehicles[$x]->DMBTR !== null) {
                            $tolling[1]['company_1231'] = isset($toll_fsp_vehicles[0]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[0]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1232'] = isset($toll_fsp_vehicles[1]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[1]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1233'] = isset($toll_fsp_vehicles[2]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[2]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1234'] = isset($toll_fsp_vehicles[3]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[3]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1235'] = isset($toll_fsp_vehicles[4]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[4]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1236'] = isset($toll_fsp_vehicles[5]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[5]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1237'] = isset($toll_fsp_vehicles[6]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[6]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1901'] = isset($toll_fsp_vehicles[7]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $tolling[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $tolling[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $tolling[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $tolling[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $tolling[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $tolling[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $tolling[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $tolling[1]['company_1901'] = 0;
                            }
                            $tolling[1]['total'] = number_format((int)str_replace(',', '', $tolling[1]['company_1231']) + (int)str_replace(',', '', $tolling[1]['company_1232'])
                                + (int)str_replace(',', '', $tolling[1]['company_1233']) + (int)str_replace(',', '', $tolling[1]['company_1234'])
                                + (int)str_replace(',', '', $tolling[1]['company_1235']) + (int)str_replace(',', '', $tolling[1]['company_1236'])
                                + (int)str_replace(',', '', $tolling[1]['company_1237']) + (int)str_replace(',', '', $tolling[1]['company_1901']));
                        }
                        else {
                            $tolling[1]['company_1231'] = isset($toll_fsp_vehicles[0]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[0]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1232'] = isset($toll_fsp_vehicles[1]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[1]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1233'] = isset($toll_fsp_vehicles[2]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[2]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1234'] = isset($toll_fsp_vehicles[3]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[3]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1235'] = isset($toll_fsp_vehicles[4]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[4]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1236'] = isset($toll_fsp_vehicles[5]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[5]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1237'] = isset($toll_fsp_vehicles[6]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[6]->DMBTR) / 1000) : 0;
                            $tolling[1]['company_1901'] = isset($toll_fsp_vehicles[7]->DMBTR) === true ? number_format((int)round($toll_fsp_vehicles[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($toll_delivery_vehicles !== []) {
                    for ($x = 0; $x < count($toll_delivery_vehicles); $x++) {
                        if ($toll_delivery_vehicles[$x]->DMBTR !== null) {
                            $tolling[2]['company_1231'] = isset($toll_delivery_vehicles[0]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[0]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1232'] = isset($toll_delivery_vehicles[1]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[1]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1233'] = isset($toll_delivery_vehicles[2]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[2]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1234'] = isset($toll_delivery_vehicles[3]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[3]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1235'] = isset($toll_delivery_vehicles[4]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[4]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1236'] = isset($toll_delivery_vehicles[5]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[5]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1237'] = isset($toll_delivery_vehicles[6]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[6]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1901'] = isset($toll_delivery_vehicles[7]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $tolling[2]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $tolling[2]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $tolling[2]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $tolling[2]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $tolling[2]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $tolling[2]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $tolling[2]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $tolling[2]['company_1901'] = 0;
                            }
                            $tolling[2]['total'] = number_format((int)str_replace(',', '', $tolling[2]['company_1231']) + (int)str_replace(',', '', $tolling[2]['company_1232'])
                                + (int)str_replace(',', '', $tolling[2]['company_1233']) + (int)str_replace(',', '', $tolling[2]['company_1234'])
                                + (int)str_replace(',', '', $tolling[2]['company_1235']) + (int)str_replace(',', '', $tolling[2]['company_1236'])
                                + (int)str_replace(',', '', $tolling[2]['company_1237']) + (int)str_replace(',', '', $tolling[2]['company_1901']));
                        }
                        else {
                            $tolling[2]['company_1231'] = isset($toll_delivery_vehicles[0]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[0]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1232'] = isset($toll_delivery_vehicles[1]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[1]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1233'] = isset($toll_delivery_vehicles[2]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[2]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1234'] = isset($toll_delivery_vehicles[3]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[3]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1235'] = isset($toll_delivery_vehicles[4]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[4]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1236'] = isset($toll_delivery_vehicles[5]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[5]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1237'] = isset($toll_delivery_vehicles[6]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[6]->DMBTR) / 1000) : 0;
                            $tolling[2]['company_1901'] = isset($toll_delivery_vehicles[7]->DMBTR) === true ? number_format((int)round($toll_delivery_vehicles[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }

                if ($toll_office === []) {
                    $tolling[0]['company_1231'] = 0;
                    $tolling[0]['company_1232'] = 0;
                    $tolling[0]['company_1233'] = 0;
                    $tolling[0]['company_1234'] = 0;
                    $tolling[0]['company_1235'] = 0;
                    $tolling[0]['company_1236'] = 0;
                    $tolling[0]['company_1237'] = 0;
                    $tolling[0]['company_1901'] = 0;
                    $tolling[0]['total'] = 0;
                }
                if ($toll_fsp_vehicles === []) {
                    $tolling[1]['company_1231'] = 0;
                    $tolling[1]['company_1232'] = 0;
                    $tolling[1]['company_1233'] = 0;
                    $tolling[1]['company_1234'] = 0;
                    $tolling[1]['company_1235'] = 0;
                    $tolling[1]['company_1236'] = 0;
                    $tolling[1]['company_1237'] = 0;
                    $tolling[1]['company_1901'] = 0;
                    $tolling[1]['total'] = 0;
                }
                if ($toll_delivery_vehicles === []) {
                    $tolling[2]['company_1231'] = 0;
                    $tolling[2]['company_1232'] = 0;
                    $tolling[2]['company_1233'] = 0;
                    $tolling[2]['company_1234'] = 0;
                    $tolling[2]['company_1235'] = 0;
                    $tolling[2]['company_1236'] = 0;
                    $tolling[2]['company_1237'] = 0;
                    $tolling[2]['company_1901'] = 0;
                    $tolling[2]['total'] = 0;
                }

                //tolling
                $company_1231_tolling = (int)str_replace(',', '', $tolling[0]['company_1231']) + (int)str_replace(',', '', $tolling[1]['company_1231'])
                    + (int)str_replace(',', '', $tolling[2]['company_1231']);
                $company_1232_tolling = (int)str_replace(',', '', $tolling[0]['company_1232']) + (int)str_replace(',', '', $tolling[1]['company_1232'])
                    + (int)str_replace(',', '', $tolling[2]['company_1232']);
                $company_1233_tolling = (int)str_replace(',', '', $tolling[0]['company_1233']) + (int)str_replace(',', '', $tolling[1]['company_1233'])
                    + (int)str_replace(',', '', $tolling[2]['company_1233']);
                $company_1234_tolling = (int)str_replace(',', '', $tolling[0]['company_1234']) + (int)str_replace(',', '', $tolling[1]['company_1234'])
                    + (int)str_replace(',', '', $tolling[2]['company_1234']);
                $company_1235_tolling = (int)str_replace(',', '', $tolling[0]['company_1235']) + (int)str_replace(',', '', $tolling[1]['company_1235'])
                    + (int)str_replace(',', '', $tolling[2]['company_1235']);
                $company_1236_tolling = (int)str_replace(',', '', $tolling[0]['company_1236']) + (int)str_replace(',', '', $tolling[1]['company_1236'])
                    + (int)str_replace(',', '', $tolling[2]['company_1236']);
                $company_1237_tolling = (int)str_replace(',', '', $tolling[0]['company_1237']) + (int)str_replace(',', '', $tolling[1]['company_1237'])
                    + (int)str_replace(',', '', $tolling[2]['company_1237']);
                $company_1901_tolling = (int)str_replace(',', '', $tolling[0]['company_1901']) + (int)str_replace(',', '', $tolling[1]['company_1901'])
                    + (int)str_replace(',', '', $tolling[2]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_tolling = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_tolling = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_tolling = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_tolling = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_tolling = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_tolling = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_tolling = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_tolling = 0;
                }
                $total_all_tolling = $company_1231_tolling + $company_1232_tolling + $company_1233_tolling
                    + $company_1234_tolling + $company_1235_tolling + $company_1236_tolling + $company_1237_tolling + $company_1901_tolling;
            }

            for ($i = 0; $i < count($fuel_and_selling_exp); $i++) {
                $fuel_and_selling_exp[0]['name'] = 'Advertising and Promotions';
                $fuel_and_selling_exp[1]['name'] = 'Transportation and Travel';
                if ($ad_and_promotions !== []) {
                    for ($x = 0; $x < count($ad_and_promotions); $x++) {
                        if ($ad_and_promotions[$x]->DMBTR !== null) {
                            $fuel_and_selling_exp[0]['company_1231'] = isset($ad_and_promotions[0]->DMBTR) === true ? number_format((int)round($ad_and_promotions[0]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1232'] = isset($ad_and_promotions[1]->DMBTR) === true ? number_format((int)round($ad_and_promotions[1]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1233'] = isset($ad_and_promotions[2]->DMBTR) === true ? number_format((int)round($ad_and_promotions[2]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1234'] = isset($ad_and_promotions[3]->DMBTR) === true ? number_format((int)round($ad_and_promotions[3]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1235'] = isset($ad_and_promotions[4]->DMBTR) === true ? number_format((int)round($ad_and_promotions[4]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1236'] = isset($ad_and_promotions[5]->DMBTR) === true ? number_format((int)round($ad_and_promotions[5]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1237'] = isset($ad_and_promotions[6]->DMBTR) === true ? number_format((int)round($ad_and_promotions[6]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1901'] = isset($ad_and_promotions[7]->DMBTR) === true ? number_format((int)round($ad_and_promotions[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $fuel_and_selling_exp[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $fuel_and_selling_exp[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $fuel_and_selling_exp[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $fuel_and_selling_exp[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $fuel_and_selling_exp[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $fuel_and_selling_exp[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $fuel_and_selling_exp[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $fuel_and_selling_exp[0]['company_1901'] = 0;
                            }
                            $fuel_and_selling_exp[0]['total'] = number_format((int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1231']) + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1232'])
                                + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1233']) + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1234'])
                                + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1235']) + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1236'])
                                + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1237']) + (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1901']));
                        }
                        else {
                            $fuel_and_selling_exp[0]['company_1231'] = isset($ad_and_promotions[0]->DMBTR) === true ? number_format((int)round($ad_and_promotions[0]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1232'] = isset($ad_and_promotions[1]->DMBTR) === true ? number_format((int)round($ad_and_promotions[1]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1233'] = isset($ad_and_promotions[2]->DMBTR) === true ? number_format((int)round($ad_and_promotions[2]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1234'] = isset($ad_and_promotions[3]->DMBTR) === true ? number_format((int)round($ad_and_promotions[3]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1235'] = isset($ad_and_promotions[4]->DMBTR) === true ? number_format((int)round($ad_and_promotions[4]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1236'] = isset($ad_and_promotions[5]->DMBTR) === true ? number_format((int)round($ad_and_promotions[5]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1237'] = isset($ad_and_promotions[6]->DMBTR) === true ? number_format((int)round($ad_and_promotions[6]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[0]['company_1901'] = isset($ad_and_promotions[7]->DMBTR) === true ? number_format((int)round($ad_and_promotions[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($trans_and_travel !== []) {
                    for ($x = 0; $x < count($trans_and_travel); $x++) {
                        if ($trans_and_travel[$x]->DMBTR !== null) {
                            $fuel_and_selling_exp[1]['company_1231'] = isset($trans_and_travel[0]->DMBTR) === true ? number_format((int)round($trans_and_travel[0]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1232'] = isset($trans_and_travel[1]->DMBTR) === true ? number_format((int)round($trans_and_travel[1]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1233'] = isset($trans_and_travel[2]->DMBTR) === true ? number_format((int)round($trans_and_travel[2]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1234'] = isset($trans_and_travel[3]->DMBTR) === true ? number_format((int)round($trans_and_travel[3]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1235'] = isset($trans_and_travel[4]->DMBTR) === true ? number_format((int)round($trans_and_travel[4]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1236'] = isset($trans_and_travel[5]->DMBTR) === true ? number_format((int)round($trans_and_travel[5]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1237'] = isset($trans_and_travel[6]->DMBTR) === true ? number_format((int)round($trans_and_travel[6]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1901'] = isset($trans_and_travel[7]->DMBTR) === true ? number_format((int)round($trans_and_travel[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $fuel_and_selling_exp[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $fuel_and_selling_exp[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $fuel_and_selling_exp[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $fuel_and_selling_exp[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $fuel_and_selling_exp[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $fuel_and_selling_exp[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $fuel_and_selling_exp[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $fuel_and_selling_exp[1]['company_1901'] = 0;
                            }
                            $fuel_and_selling_exp[1]['total'] = number_format((int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1231']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1232'])
                                + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1233']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1234'])
                                + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1235']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1236'])
                                + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1237']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1901']));
                        }
                        else {
                            $fuel_and_selling_exp[1]['company_1231'] = isset($trans_and_travel[0]->DMBTR) === true ? number_format((int)round($trans_and_travel[0]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1232'] = isset($trans_and_travel[1]->DMBTR) === true ? number_format((int)round($trans_and_travel[1]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1233'] = isset($trans_and_travel[2]->DMBTR) === true ? number_format((int)round($trans_and_travel[2]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1234'] = isset($trans_and_travel[3]->DMBTR) === true ? number_format((int)round($trans_and_travel[3]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1235'] = isset($trans_and_travel[4]->DMBTR) === true ? number_format((int)round($trans_and_travel[4]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1236'] = isset($trans_and_travel[5]->DMBTR) === true ? number_format((int)round($trans_and_travel[5]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1237'] = isset($trans_and_travel[6]->DMBTR) === true ? number_format((int)round($trans_and_travel[6]->DMBTR) / 1000) : 0;
                            $fuel_and_selling_exp[1]['company_1901'] = isset($trans_and_travel[7]->DMBTR) === true ? number_format((int)round($trans_and_travel[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }

                if ($ad_and_promotions === []) {
                    $fuel_and_selling_exp[0]['company_1231'] = 0;
                    $fuel_and_selling_exp[0]['company_1232'] = 0;
                    $fuel_and_selling_exp[0]['company_1233'] = 0;
                    $fuel_and_selling_exp[0]['company_1234'] = 0;
                    $fuel_and_selling_exp[0]['company_1235'] = 0;
                    $fuel_and_selling_exp[0]['company_1236'] = 0;
                    $fuel_and_selling_exp[0]['company_1237'] = 0;
                    $fuel_and_selling_exp[0]['company_1901'] = 0;
                    $fuel_and_selling_exp[0]['total'] = 0;
                }
                if ($trans_and_travel === []) {
                    $fuel_and_selling_exp[1]['company_1231'] = 0;
                    $fuel_and_selling_exp[1]['company_1232'] = 0;
                    $fuel_and_selling_exp[1]['company_1233'] = 0;
                    $fuel_and_selling_exp[1]['company_1234'] = 0;
                    $fuel_and_selling_exp[1]['company_1235'] = 0;
                    $fuel_and_selling_exp[1]['company_1236'] = 0;
                    $fuel_and_selling_exp[1]['company_1237'] = 0;
                    $fuel_and_selling_exp[1]['company_1901'] = 0;
                    $fuel_and_selling_exp[1]['total'] = 0;
                }

                //fuel_and_selling_exp
                $company_1231_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1231']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1231']);
                $company_1232_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1232']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1232']);
                $company_1233_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1233']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1233']);
                $company_1234_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1234']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1234']);
                $company_1235_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1235']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1235']);
                $company_1236_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1236']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1236']);
                $company_1237_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1237']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1237']);
                $company_1901_fuel_and_selling_exp = (int)str_replace(',', '', $fuel_and_selling_exp[0]['company_1901']) + (int)str_replace(',', '', $fuel_and_selling_exp[1]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_fuel_and_selling_exp = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_fuel_and_selling_exp = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_fuel_and_selling_exp = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_fuel_and_selling_exp = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_fuel_and_selling_exp = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_fuel_and_selling_exp = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_fuel_and_selling_exp = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_fuel_and_selling_exp = 0;
                }
                $total_details_fuel_and_selling_exp = $company_1231_fuel_and_selling_exp + $company_1232_fuel_and_selling_exp + $company_1233_fuel_and_selling_exp + $company_1234_fuel_and_selling_exp
                    + $company_1235_fuel_and_selling_exp + $company_1236_fuel_and_selling_exp + $company_1237_fuel_and_selling_exp + $company_1901_fuel_and_selling_exp;
            }

            for ($i = 0; $i < count($utilities); $i++) {
                $utilities[0]['name'] = 'UTILITIES - ELECTRICITY';
                $utilities[1]['name'] = 'UTILITIES - WATER';
                if ($utilities_electricity !== []) {
                    for ($x = 0; $x < count($utilities_electricity); $x++) {
                        if ($utilities_electricity[$x]->DMBTR !== null) {
                            $utilities[0]['company_1231'] = isset($utilities_electricity[0]->DMBTR) === true ? number_format((int)round($utilities_electricity[0]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1232'] = isset($utilities_electricity[1]->DMBTR) === true ? number_format((int)round($utilities_electricity[1]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1233'] = isset($utilities_electricity[2]->DMBTR) === true ? number_format((int)round($utilities_electricity[2]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1234'] = isset($utilities_electricity[3]->DMBTR) === true ? number_format((int)round($utilities_electricity[3]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1235'] = isset($utilities_electricity[4]->DMBTR) === true ? number_format((int)round($utilities_electricity[4]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1236'] = isset($utilities_electricity[5]->DMBTR) === true ? number_format((int)round($utilities_electricity[5]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1237'] = isset($utilities_electricity[6]->DMBTR) === true ? number_format((int)round($utilities_electricity[6]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1901'] = isset($utilities_electricity[7]->DMBTR) === true ? number_format((int)round($utilities_electricity[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $utilities[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $utilities[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $utilities[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $utilities[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $utilities[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $utilities[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $utilities[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $utilities[0]['company_1901'] = 0;
                            }
                            $utilities[0]['total'] = number_format((int)str_replace(',', '', $utilities[0]['company_1231']) + (int)str_replace(',', '', $utilities[0]['company_1232'])
                                + (int)str_replace(',', '', $utilities[0]['company_1233']) + (int)str_replace(',', '', $utilities[0]['company_1234'])
                                + (int)str_replace(',', '', $utilities[0]['company_1235']) + (int)str_replace(',', '', $utilities[0]['company_1236'])
                                + (int)str_replace(',', '', $utilities[0]['company_1237']) + (int)str_replace(',', '', $utilities[0]['company_1901']));
                        }
                        else {
                            $utilities[0]['company_1231'] = isset($utilities_electricity[0]->DMBTR) === true ? number_format((int)round($utilities_electricity[0]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1232'] = isset($utilities_electricity[1]->DMBTR) === true ? number_format((int)round($utilities_electricity[1]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1233'] = isset($utilities_electricity[2]->DMBTR) === true ? number_format((int)round($utilities_electricity[2]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1234'] = isset($utilities_electricity[3]->DMBTR) === true ? number_format((int)round($utilities_electricity[3]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1235'] = isset($utilities_electricity[4]->DMBTR) === true ? number_format((int)round($utilities_electricity[4]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1236'] = isset($utilities_electricity[5]->DMBTR) === true ? number_format((int)round($utilities_electricity[5]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1237'] = isset($utilities_electricity[6]->DMBTR) === true ? number_format((int)round($utilities_electricity[6]->DMBTR) / 1000) : 0;
                            $utilities[0]['company_1901'] = isset($utilities_electricity[7]->DMBTR) === true ? number_format((int)round($utilities_electricity[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($utilities_water !== []) {
                    for ($x = 0; $x < count($utilities_water); $x++) {
                        if ($utilities_water[$x]->DMBTR !== null) {
                            $utilities[1]['company_1231'] = isset($utilities_water[0]->DMBTR) === true ? number_format((int)round($utilities_water[0]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1232'] = isset($utilities_water[1]->DMBTR) === true ? number_format((int)round($utilities_water[1]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1233'] = isset($utilities_water[2]->DMBTR) === true ? number_format((int)round($utilities_water[2]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1234'] = isset($utilities_water[3]->DMBTR) === true ? number_format((int)round($utilities_water[3]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1235'] = isset($utilities_water[4]->DMBTR) === true ? number_format((int)round($utilities_water[4]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1236'] = isset($utilities_water[5]->DMBTR) === true ? number_format((int)round($utilities_water[5]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1237'] = isset($utilities_water[6]->DMBTR) === true ? number_format((int)round($utilities_water[6]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1901'] = isset($utilities_water[7]->DMBTR) === true ? number_format((int)round($utilities_water[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $utilities[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $utilities[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $utilities[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $utilities[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $utilities[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $utilities[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $utilities[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $utilities[1]['company_1901'] = 0;
                            }
                            $utilities[1]['total'] = number_format((int)str_replace(',', '', $utilities[1]['company_1231']) + (int)str_replace(',', '', $utilities[1]['company_1232'])
                                + (int)str_replace(',', '', $utilities[1]['company_1233']) + (int)str_replace(',', '', $utilities[1]['company_1234'])
                                + (int)str_replace(',', '', $utilities[1]['company_1235']) + (int)str_replace(',', '', $utilities[1]['company_1236'])
                                + (int)str_replace(',', '', $utilities[1]['company_1237']) + (int)str_replace(',', '', $utilities[1]['company_1901']));
                        }
                        else {
                            $utilities[1]['company_1231'] = isset($utilities_water[0]->DMBTR) === true ? number_format((int)round($utilities_water[0]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1232'] = isset($utilities_water[1]->DMBTR) === true ? number_format((int)round($utilities_water[1]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1233'] = isset($utilities_water[2]->DMBTR) === true ? number_format((int)round($utilities_water[2]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1234'] = isset($utilities_water[3]->DMBTR) === true ? number_format((int)round($utilities_water[3]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1235'] = isset($utilities_water[4]->DMBTR) === true ? number_format((int)round($utilities_water[4]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1236'] = isset($utilities_water[5]->DMBTR) === true ? number_format((int)round($utilities_water[5]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1237'] = isset($utilities_water[6]->DMBTR) === true ? number_format((int)round($utilities_water[6]->DMBTR) / 1000) : 0;
                            $utilities[1]['company_1901'] = isset($utilities_water[7]->DMBTR) === true ? number_format((int)round($utilities_water[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }

                if ($utilities_electricity === []) {
                    $utilities[0]['company_1231'] = 0;
                    $utilities[0]['company_1232'] = 0;
                    $utilities[0]['company_1233'] = 0;
                    $utilities[0]['company_1234'] = 0;
                    $utilities[0]['company_1235'] = 0;
                    $utilities[0]['company_1236'] = 0;
                    $utilities[0]['company_1237'] = 0;
                    $utilities[0]['company_1901'] = 0;
                    $utilities[0]['total'] = 0;
                }
                if ($utilities_water === []) {
                    $utilities[1]['company_1231'] = 0;
                    $utilities[1]['company_1232'] = 0;
                    $utilities[1]['company_1233'] = 0;
                    $utilities[1]['company_1234'] = 0;
                    $utilities[1]['company_1235'] = 0;
                    $utilities[1]['company_1236'] = 0;
                    $utilities[1]['company_1237'] = 0;
                    $utilities[1]['company_1901'] = 0;
                    $utilities[1]['total'] = 0;
                }

                //utilities
                $company_1231_utilities = (int)str_replace(',', '', $utilities[0]['company_1231']) + (int)str_replace(',', '', $utilities[1]['company_1231']);
                $company_1232_utilities = (int)str_replace(',', '', $utilities[0]['company_1232']) + (int)str_replace(',', '', $utilities[1]['company_1232']);
                $company_1233_utilities = (int)str_replace(',', '', $utilities[0]['company_1233']) + (int)str_replace(',', '', $utilities[1]['company_1233']);
                $company_1234_utilities = (int)str_replace(',', '', $utilities[0]['company_1234']) + (int)str_replace(',', '', $utilities[1]['company_1234']);
                $company_1235_utilities = (int)str_replace(',', '', $utilities[0]['company_1235']) + (int)str_replace(',', '', $utilities[1]['company_1235']);
                $company_1236_utilities = (int)str_replace(',', '', $utilities[0]['company_1236']) + (int)str_replace(',', '', $utilities[1]['company_1236']);
                $company_1237_utilities = (int)str_replace(',', '', $utilities[0]['company_1237']) + (int)str_replace(',', '', $utilities[1]['company_1237']);
                $company_1901_utilities = (int)str_replace(',', '', $utilities[0]['company_1901']) + (int)str_replace(',', '', $utilities[1]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_utilities = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_utilities = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_utilities = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_utilities = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_utilities = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_utilities = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_utilities = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_utilities = 0;
                }
                $total_all_utilities = $company_1231_utilities + $company_1232_utilities + $company_1233_utilities + $company_1234_utilities
                    + $company_1235_utilities + $company_1236_utilities + $company_1237_utilities + $company_1901_utilities;
            }

            for ($i = 0; $i < count($admin_exp); $i++) {
                $admin_exp[0]['name'] = 'COMMUNICATION';
                $admin_exp[1]['name'] = 'POSTAGE';
                $admin_exp[2]['name'] = 'REPAIRS';
                $admin_exp[3]['name'] = 'REPRESENTATION AND ENTERTAINMENT';
                $admin_exp[4]['name'] = 'PLOG FEES  - OTHERS';
                $admin_exp[5]['name'] = 'SUPPLIES';
                $admin_exp[6]['name'] = 'SUBSCRIPTIONS';
//                $admin_exp[7]['name'] = 'SOLICITATION AND DONATION';

                if ($communication !== []) {
                    for ($x = 0; $x < count($communication); $x++) {
                        if ($communication[$x]->DMBTR !== null) {
                            $admin_exp[0]['company_1231'] = isset($communication[0]->DMBTR) === true ? number_format((int)round($communication[0]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1232'] = isset($communication[1]->DMBTR) === true ? number_format((int)round($communication[1]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1233'] = isset($communication[2]->DMBTR) === true ? number_format((int)round($communication[2]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1234'] = isset($communication[3]->DMBTR) === true ? number_format((int)round($communication[3]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1235'] = isset($communication[4]->DMBTR) === true ? number_format((int)round($communication[4]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1236'] = isset($communication[5]->DMBTR) === true ? number_format((int)round($communication[5]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1237'] = isset($communication[6]->DMBTR) === true ? number_format((int)round($communication[6]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1901'] = isset($communication[7]->DMBTR) === true ? number_format((int)round($communication[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[0]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[0]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[0]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[0]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[0]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[0]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[0]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[0]['company_1901'] = 0;
                            }
                            $admin_exp[0]['total'] = number_format((int)str_replace(',', '', $admin_exp[0]['company_1231']) + (int)str_replace(',', '', $admin_exp[0]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[0]['company_1233']) + (int)str_replace(',', '', $admin_exp[0]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[0]['company_1235']) + (int)str_replace(',', '', $admin_exp[0]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[0]['company_1237']) + (int)str_replace(',', '', $admin_exp[0]['company_1901']));
                        }
                        else {
                            $admin_exp[0]['company_1231'] = isset($communication[0]->DMBTR) === true ? number_format((int)round($communication[0]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1232'] = isset($communication[1]->DMBTR) === true ? number_format((int)round($communication[1]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1233'] = isset($communication[2]->DMBTR) === true ? number_format((int)round($communication[2]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1234'] = isset($communication[3]->DMBTR) === true ? number_format((int)round($communication[3]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1235'] = isset($communication[4]->DMBTR) === true ? number_format((int)round($communication[4]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1236'] = isset($communication[5]->DMBTR) === true ? number_format((int)round($communication[5]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1237'] = isset($communication[6]->DMBTR) === true ? number_format((int)round($communication[6]->DMBTR) / 1000) : 0;
                            $admin_exp[0]['company_1901'] = isset($communication[7]->DMBTR) === true ? number_format((int)round($communication[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($postage !== []) {
                    for ($x = 0; $x < count($postage); $x++) {
                        if ($postage[$x]->DMBTR !== null) {
                            $admin_exp[1]['company_1231'] = isset($postage[0]->DMBTR) === true ? number_format((int)round($postage[0]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1232'] = isset($postage[1]->DMBTR) === true ? number_format((int)round($postage[1]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1233'] = isset($postage[2]->DMBTR) === true ? number_format((int)round($postage[2]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1234'] = isset($postage[3]->DMBTR) === true ? number_format((int)round($postage[3]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1235'] = isset($postage[4]->DMBTR) === true ? number_format((int)round($postage[4]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1236'] = isset($postage[5]->DMBTR) === true ? number_format((int)round($postage[5]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1237'] = isset($postage[6]->DMBTR) === true ? number_format((int)round($postage[6]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1901'] = isset($postage[7]->DMBTR) === true ? number_format((int)round($postage[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[1]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[1]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[1]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[1]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[1]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[1]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[1]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[1]['company_1901'] = 0;
                            }
                            $admin_exp[1]['total'] = number_format((int)str_replace(',', '', $admin_exp[1]['company_1231']) + (int)str_replace(',', '', $admin_exp[1]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[1]['company_1233']) + (int)str_replace(',', '', $admin_exp[1]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[1]['company_1235']) + (int)str_replace(',', '', $admin_exp[1]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[1]['company_1237']) + (int)str_replace(',', '', $admin_exp[1]['company_1901']));
                        }
                        else {
                            $admin_exp[1]['company_1231'] = isset($postage[0]->DMBTR) === true ? number_format((int)round($postage[0]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1232'] = isset($postage[1]->DMBTR) === true ? number_format((int)round($postage[1]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1233'] = isset($postage[2]->DMBTR) === true ? number_format((int)round($postage[2]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1234'] = isset($postage[3]->DMBTR) === true ? number_format((int)round($postage[3]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1235'] = isset($postage[4]->DMBTR) === true ? number_format((int)round($postage[4]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1236'] = isset($postage[5]->DMBTR) === true ? number_format((int)round($postage[5]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1237'] = isset($postage[6]->DMBTR) === true ? number_format((int)round($postage[6]->DMBTR) / 1000) : 0;
                            $admin_exp[1]['company_1901'] = isset($postage[7]->DMBTR) === true ? number_format((int)round($postage[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($repairs_total !== []) {
                    for ($x = 0; $x < count($repairs_total); $x++) {
                        if ($repairs_total[$x]->DMBTR !== null) {
                            $admin_exp[2]['company_1231'] = isset($repairs_total[0]->DMBTR) === true ? number_format((int)round($repairs_total[0]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1232'] = isset($repairs_total[1]->DMBTR) === true ? number_format((int)round($repairs_total[1]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1233'] = isset($repairs_total[2]->DMBTR) === true ? number_format((int)round($repairs_total[2]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1234'] = isset($repairs_total[3]->DMBTR) === true ? number_format((int)round($repairs_total[3]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1235'] = isset($repairs_total[4]->DMBTR) === true ? number_format((int)round($repairs_total[4]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1236'] = isset($repairs_total[5]->DMBTR) === true ? number_format((int)round($repairs_total[5]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1237'] = isset($repairs_total[6]->DMBTR) === true ? number_format((int)round($repairs_total[6]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1901'] = isset($repairs_total[7]->DMBTR) === true ? number_format((int)round($repairs_total[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[2]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[2]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[2]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[2]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[2]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[2]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[2]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[2]['company_1901'] = 0;
                            }
                            $admin_exp[2]['total'] = number_format((int)str_replace(',', '', $admin_exp[2]['company_1231']) + (int)str_replace(',', '', $admin_exp[2]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[2]['company_1233']) + (int)str_replace(',', '', $admin_exp[2]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[2]['company_1235']) + (int)str_replace(',', '', $admin_exp[2]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[2]['company_1237']) + (int)str_replace(',', '', $admin_exp[2]['company_1901']));
                        }
                        else {
                            $admin_exp[2]['company_1231'] = isset($repairs_total[0]->DMBTR) === true ? number_format((int)round($repairs_total[0]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1232'] = isset($repairs_total[1]->DMBTR) === true ? number_format((int)round($repairs_total[1]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1233'] = isset($repairs_total[2]->DMBTR) === true ? number_format((int)round($repairs_total[2]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1234'] = isset($repairs_total[3]->DMBTR) === true ? number_format((int)round($repairs_total[3]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1235'] = isset($repairs_total[4]->DMBTR) === true ? number_format((int)round($repairs_total[4]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1236'] = isset($repairs_total[5]->DMBTR) === true ? number_format((int)round($repairs_total[5]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1237'] = isset($repairs_total[6]->DMBTR) === true ? number_format((int)round($repairs_total[6]->DMBTR) / 1000) : 0;
                            $admin_exp[2]['company_1901'] = isset($repairs_total[7]->DMBTR) === true ? number_format((int)round($repairs_total[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($represent_and_entertainment !== []) {
                    for ($x = 0; $x < count($represent_and_entertainment); $x++) {
                        if ($represent_and_entertainment[$x]->DMBTR !== null) {
                            $admin_exp[3]['company_1231'] = isset($represent_and_entertainment[0]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[0]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1232'] = isset($represent_and_entertainment[1]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[1]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1233'] = isset($represent_and_entertainment[2]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[2]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1234'] = isset($represent_and_entertainment[3]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[3]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1235'] = isset($represent_and_entertainment[4]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[4]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1236'] = isset($represent_and_entertainment[5]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[5]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1237'] = isset($represent_and_entertainment[6]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[6]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1901'] = isset($represent_and_entertainment[7]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[3]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[3]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[3]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[3]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[3]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[3]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[3]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[3]['company_1901'] = 0;
                            }
                            $admin_exp[3]['total'] = number_format((int)str_replace(',', '', $admin_exp[3]['company_1231']) + (int)str_replace(',', '', $admin_exp[3]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[3]['company_1233']) + (int)str_replace(',', '', $admin_exp[3]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[3]['company_1235']) + (int)str_replace(',', '', $admin_exp[3]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[3]['company_1237']) + (int)str_replace(',', '', $admin_exp[3]['company_1901']));

                        }
                        else {
                            $admin_exp[3]['company_1231'] = isset($represent_and_entertainment[0]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[0]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1232'] = isset($represent_and_entertainment[1]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[1]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1233'] = isset($represent_and_entertainment[2]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[2]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1234'] = isset($represent_and_entertainment[3]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[3]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1235'] = isset($represent_and_entertainment[4]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[4]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1236'] = isset($represent_and_entertainment[5]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[5]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1237'] = isset($represent_and_entertainment[6]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[6]->DMBTR) / 1000) : 0;
                            $admin_exp[3]['company_1901'] = isset($represent_and_entertainment[7]->DMBTR) === true ? number_format((int)round($represent_and_entertainment[7]->DMBTR) / 1000) : 0;

                        }

                    }
                }
                if ($plog_fees_others !== []) {
                    for ($x = 0; $x < count($plog_fees_others); $x++) {
                        if ($plog_fees_others[$x]->DMBTR !== null) {
                            $admin_exp[4]['company_1231'] = isset($plog_fees_others[0]->DMBTR) === true ? number_format((int)round($plog_fees_others[0]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1232'] = isset($plog_fees_others[1]->DMBTR) === true ? number_format((int)round($plog_fees_others[1]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1233'] = isset($plog_fees_others[2]->DMBTR) === true ? number_format((int)round($plog_fees_others[2]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1234'] = isset($plog_fees_others[3]->DMBTR) === true ? number_format((int)round($plog_fees_others[3]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1235'] = isset($plog_fees_others[4]->DMBTR) === true ? number_format((int)round($plog_fees_others[4]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1236'] = isset($plog_fees_others[5]->DMBTR) === true ? number_format((int)round($plog_fees_others[5]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1237'] = isset($plog_fees_others[6]->DMBTR) === true ? number_format((int)round($plog_fees_others[6]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1901'] = isset($plog_fees_others[7]->DMBTR) === true ? number_format((int)round($plog_fees_others[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[4]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[4]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[4]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[4]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[4]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[4]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[4]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[4]['company_1901'] = 0;
                            }
                            $admin_exp[4]['total'] = number_format((int)str_replace(',', '', $admin_exp[4]['company_1231']) + (int)str_replace(',', '', $admin_exp[4]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[4]['company_1233']) + (int)str_replace(',', '', $admin_exp[4]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[4]['company_1235']) + (int)str_replace(',', '', $admin_exp[4]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[4]['company_1237']) + (int)str_replace(',', '', $admin_exp[4]['company_1901']));

                        }
                        else {
                            $admin_exp[4]['company_1231'] = isset($plog_fees_others[0]->DMBTR) === true ? number_format((int)round($plog_fees_others[0]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1232'] = isset($plog_fees_others[1]->DMBTR) === true ? number_format((int)round($plog_fees_others[1]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1233'] = isset($plog_fees_others[2]->DMBTR) === true ? number_format((int)round($plog_fees_others[2]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1234'] = isset($plog_fees_others[3]->DMBTR) === true ? number_format((int)round($plog_fees_others[3]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1235'] = isset($plog_fees_others[4]->DMBTR) === true ? number_format((int)round($plog_fees_others[4]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1236'] = isset($plog_fees_others[5]->DMBTR) === true ? number_format((int)round($plog_fees_others[5]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1237'] = isset($plog_fees_others[6]->DMBTR) === true ? number_format((int)round($plog_fees_others[6]->DMBTR) / 1000) : 0;
                            $admin_exp[4]['company_1901'] = isset($plog_fees_others[7]->DMBTR) === true ? number_format((int)round($plog_fees_others[7]->DMBTR) / 1000) : 0;

                        }

                    }
                }
                if ($supplies !== []) {
                    for ($x = 0; $x < count($supplies); $x++) {
                        if ($supplies[$x]->DMBTR !== null) {
                            $admin_exp[5]['company_1231'] = isset($supplies[0]->DMBTR) === true ? number_format((int)round($supplies[0]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1232'] = isset($supplies[1]->DMBTR) === true ? number_format((int)round($supplies[1]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1233'] = isset($supplies[2]->DMBTR) === true ? number_format((int)round($supplies[2]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1234'] = isset($supplies[3]->DMBTR) === true ? number_format((int)round($supplies[3]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1235'] = isset($supplies[4]->DMBTR) === true ? number_format((int)round($supplies[4]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1236'] = isset($supplies[5]->DMBTR) === true ? number_format((int)round($supplies[5]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1237'] = isset($supplies[6]->DMBTR) === true ? number_format((int)round($supplies[6]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1901'] = isset($supplies[7]->DMBTR) === true ? number_format((int)round($supplies[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[5]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[5]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[5]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[5]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[5]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[5]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[5]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[5]['company_1901'] = 0;
                            }
                            $admin_exp[5]['total'] = number_format((int)str_replace(',', '', $admin_exp[5]['company_1231']) + (int)str_replace(',', '', $admin_exp[5]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[5]['company_1233']) + (int)str_replace(',', '', $admin_exp[5]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[5]['company_1235']) + (int)str_replace(',', '', $admin_exp[5]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[5]['company_1237']) + (int)str_replace(',', '', $admin_exp[5]['company_1901']));

                        }
                        else {
                            $admin_exp[5]['company_1231'] = isset($supplies[0]->DMBTR) === true ? number_format((int)round($supplies[0]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1232'] = isset($supplies[1]->DMBTR) === true ? number_format((int)round($supplies[1]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1233'] = isset($supplies[2]->DMBTR) === true ? number_format((int)round($supplies[2]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1234'] = isset($supplies[3]->DMBTR) === true ? number_format((int)round($supplies[3]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1235'] = isset($supplies[4]->DMBTR) === true ? number_format((int)round($supplies[4]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1236'] = isset($supplies[5]->DMBTR) === true ? number_format((int)round($supplies[5]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1237'] = isset($supplies[6]->DMBTR) === true ? number_format((int)round($supplies[6]->DMBTR) / 1000) : 0;
                            $admin_exp[5]['company_1901'] = isset($supplies[7]->DMBTR) === true ? number_format((int)round($supplies[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
                if ($subscriptions !== []) {
                    for ($x = 0; $x < count($subscriptions); $x++) {
                        if ($subscriptions[$x]->DMBTR !== null) {
                            $admin_exp[6]['company_1231'] = isset($subscriptions[0]->DMBTR) === true ? number_format((int)round($subscriptions[0]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1232'] = isset($subscriptions[1]->DMBTR) === true ? number_format((int)round($subscriptions[1]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1233'] = isset($subscriptions[2]->DMBTR) === true ? number_format((int)round($subscriptions[2]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1234'] = isset($subscriptions[3]->DMBTR) === true ? number_format((int)round($subscriptions[3]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1235'] = isset($subscriptions[4]->DMBTR) === true ? number_format((int)round($subscriptions[4]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1236'] = isset($subscriptions[5]->DMBTR) === true ? number_format((int)round($subscriptions[5]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1237'] = isset($subscriptions[6]->DMBTR) === true ? number_format((int)round($subscriptions[6]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1901'] = isset($subscriptions[7]->DMBTR) === true ? number_format((int)round($subscriptions[7]->DMBTR) / 1000) : 0;

                            if ($isUser_HIL === null) {
                                $admin_exp[6]['company_1231'] = 0;
                            }
                            if ($isUser_SIB === null) {
                                $admin_exp[6]['company_1232'] = 0;
                            }
                            if ($isUser_ROX === null) {
                                $admin_exp[6]['company_1233'] = 0;
                            }
                            if ($isUser_SUM === null) {
                                $admin_exp[6]['company_1234'] = 0;
                            }
                            if ($isUser_DIV === null) {
                                $admin_exp[6]['company_1235'] = 0;
                            }
                            if ($isUser_JARO === null) {
                                $admin_exp[6]['company_1236'] = 0;
                            }
                            if ($isUser_KAB === null) {
                                $admin_exp[6]['company_1237'] = 0;
                            }
                            if ($isUser_CS === null) {
                                $admin_exp[6]['company_1901'] = 0;
                            }
                            $admin_exp[6]['total'] = number_format((int)str_replace(',', '', $admin_exp[6]['company_1231']) + (int)str_replace(',', '', $admin_exp[6]['company_1232'])
                                + (int)str_replace(',', '', $admin_exp[6]['company_1233']) + (int)str_replace(',', '', $admin_exp[6]['company_1234'])
                                + (int)str_replace(',', '', $admin_exp[6]['company_1235']) + (int)str_replace(',', '', $admin_exp[6]['company_1236'])
                                + (int)str_replace(',', '', $admin_exp[6]['company_1237']) + (int)str_replace(',', '', $admin_exp[6]['company_1901']));

                        }
                        else {
                            $admin_exp[6]['company_1231'] = isset($subscriptions[0]->DMBTR) === true ? number_format((int)round($subscriptions[0]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1232'] = isset($subscriptions[1]->DMBTR) === true ? number_format((int)round($subscriptions[1]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1233'] = isset($subscriptions[2]->DMBTR) === true ? number_format((int)round($subscriptions[2]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1234'] = isset($subscriptions[3]->DMBTR) === true ? number_format((int)round($subscriptions[3]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1235'] = isset($subscriptions[4]->DMBTR) === true ? number_format((int)round($subscriptions[4]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1236'] = isset($subscriptions[5]->DMBTR) === true ? number_format((int)round($subscriptions[5]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1237'] = isset($subscriptions[6]->DMBTR) === true ? number_format((int)round($subscriptions[6]->DMBTR) / 1000) : 0;
                            $admin_exp[6]['company_1901'] = isset($subscriptions[7]->DMBTR) === true ? number_format((int)round($subscriptions[7]->DMBTR) / 1000) : 0;

                        }
                    }
                }
//                if ($solicit_and_donations !== []) {
//                    for ($x = 0; $x < count($solicit_and_donations); $x++) {
//                        if ($solicit_and_donations[$x]->DMBTR !== null) {
//                            $admin_exp[7]['company_1231'] = isset($solicit_and_donations[0]->DMBTR) === true ? number_format((int)round($solicit_and_donations[0]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1232'] = isset($solicit_and_donations[1]->DMBTR) === true ? number_format((int)round($solicit_and_donations[1]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1233'] = isset($solicit_and_donations[2]->DMBTR) === true ? number_format((int)round($solicit_and_donations[2]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1234'] = isset($solicit_and_donations[3]->DMBTR) === true ? number_format((int)round($solicit_and_donations[3]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1235'] = isset($solicit_and_donations[4]->DMBTR) === true ? number_format((int)round($solicit_and_donations[4]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1236'] = isset($solicit_and_donations[5]->DMBTR) === true ? number_format((int)round($solicit_and_donations[5]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1237'] = isset($solicit_and_donations[6]->DMBTR) === true ? number_format((int)round($solicit_and_donations[6]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['company_1901'] = isset($solicit_and_donations[7]->DMBTR) === true ? number_format((int)round($solicit_and_donations[7]->DMBTR) / 1000) : 0;
//                            $admin_exp[7]['total'] = number_format((int)str_replace(',', '', $admin_exp[7]['company_1231']) + (int)str_replace(',', '', $admin_exp[7]['company_1232'])
//                                + (int)str_replace(',', '', $admin_exp[7]['company_1233']) + (int)str_replace(',', '', $admin_exp[7]['company_1234'])
//                                + (int)str_replace(',', '', $admin_exp[7]['company_1235']) + (int)str_replace(',', '', $admin_exp[7]['company_1236'])
//                                + (int)str_replace(',', '', $admin_exp[7]['company_1237']) + (int)str_replace(',', '', $admin_exp[7]['company_1901']));
//
//                        } else {
//                            $admin_exp[7]['company_1231'] = 0;
//                            $admin_exp[7]['company_1232'] = 0;
//                            $admin_exp[7]['company_1233'] = 0;
//                            $admin_exp[7]['company_1234'] = 0;
//                            $admin_exp[7]['company_1235'] = 0;
//                            $admin_exp[7]['company_1236'] = 0;
//                            $admin_exp[7]['company_1237'] = 0;
//                            $admin_exp[7]['company_1901'] = 0;
//                            $admin_exp[7]['total'] = 0;
//                        }
//                    }
//                }

                if ($communication === []) {
                    $admin_exp[0]['company_1231'] = 0;
                    $admin_exp[0]['company_1232'] = 0;
                    $admin_exp[0]['company_1233'] = 0;
                    $admin_exp[0]['company_1234'] = 0;
                    $admin_exp[0]['company_1235'] = 0;
                    $admin_exp[0]['company_1236'] = 0;
                    $admin_exp[0]['company_1237'] = 0;
                    $admin_exp[0]['company_1901'] = 0;
                    $admin_exp[0]['total'] = 0;
                }
                if ($postage === []) {
                    $admin_exp[1]['company_1231'] = 0;
                    $admin_exp[1]['company_1232'] = 0;
                    $admin_exp[1]['company_1233'] = 0;
                    $admin_exp[1]['company_1234'] = 0;
                    $admin_exp[1]['company_1235'] = 0;
                    $admin_exp[1]['company_1236'] = 0;
                    $admin_exp[1]['company_1237'] = 0;
                    $admin_exp[1]['company_1901'] = 0;
                    $admin_exp[1]['total'] = 0;
                }

                if ($repairs_total === []) {
                    $admin_exp[2]['company_1231'] = 0;
                    $admin_exp[2]['company_1232'] = 0;
                    $admin_exp[2]['company_1233'] = 0;
                    $admin_exp[2]['company_1234'] = 0;
                    $admin_exp[2]['company_1235'] = 0;
                    $admin_exp[2]['company_1236'] = 0;
                    $admin_exp[2]['company_1237'] = 0;
                    $admin_exp[2]['company_1901'] = 0;
                    $admin_exp[2]['total'] = 0;
                }
                if ($represent_and_entertainment === []) {
                    $admin_exp[3]['company_1231'] = 0;
                    $admin_exp[3]['company_1232'] = 0;
                    $admin_exp[3]['company_1233'] = 0;
                    $admin_exp[3]['company_1234'] = 0;
                    $admin_exp[3]['company_1235'] = 0;
                    $admin_exp[3]['company_1236'] = 0;
                    $admin_exp[3]['company_1237'] = 0;
                    $admin_exp[3]['company_1901'] = 0;
                    $admin_exp[3]['total'] = 0;
                }
                if ($plog_fees_others === []) {
                    $admin_exp[4]['company_1231'] = 0;
                    $admin_exp[4]['company_1232'] = 0;
                    $admin_exp[4]['company_1233'] = 0;
                    $admin_exp[4]['company_1234'] = 0;
                    $admin_exp[4]['company_1235'] = 0;
                    $admin_exp[4]['company_1236'] = 0;
                    $admin_exp[4]['company_1237'] = 0;
                    $admin_exp[4]['company_1901'] = 0;
                    $admin_exp[4]['total'] = 0;
                }
                if ($supplies === []) {
                    $admin_exp[5]['company_1231'] = 0;
                    $admin_exp[5]['company_1232'] = 0;
                    $admin_exp[5]['company_1233'] = 0;
                    $admin_exp[5]['company_1234'] = 0;
                    $admin_exp[5]['company_1235'] = 0;
                    $admin_exp[5]['company_1236'] = 0;
                    $admin_exp[5]['company_1237'] = 0;
                    $admin_exp[5]['company_1901'] = 0;
                    $admin_exp[5]['total'] = 0;
                }
                if ($subscriptions === []) {
                    $admin_exp[6]['company_1231'] = 0;
                    $admin_exp[6]['company_1232'] = 0;
                    $admin_exp[6]['company_1233'] = 0;
                    $admin_exp[6]['company_1234'] = 0;
                    $admin_exp[6]['company_1235'] = 0;
                    $admin_exp[6]['company_1236'] = 0;
                    $admin_exp[6]['company_1237'] = 0;
                    $admin_exp[6]['company_1901'] = 0;
                    $admin_exp[6]['total'] = 0;
                }
//                if ($solicit_and_donations === []) {
//                    $admin_exp[7]['company_1231'] = 0;
//                    $admin_exp[7]['company_1232'] = 0;
//                    $admin_exp[7]['company_1233'] = 0;
//                    $admin_exp[7]['company_1234'] = 0;
//                    $admin_exp[7]['company_1235'] = 0;
//                    $admin_exp[7]['company_1236'] = 0;
//                    $admin_exp[7]['company_1237'] = 0;
//                    $admin_exp[7]['company_1901'] = 0;
//                    $admin_exp[7]['total'] = 0;
//                }


                //admin_exp
                $company_1231_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1231']) + (int)str_replace(',', '', $admin_exp[1]['company_1231'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1231']) + (int)str_replace(',', '', $admin_exp[3]['company_1231'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1231']) + (int)str_replace(',', '', $admin_exp[5]['company_1231'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1231']);
                $company_1232_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1232']) + (int)str_replace(',', '', $admin_exp[1]['company_1232'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1232']) + (int)str_replace(',', '', $admin_exp[3]['company_1232'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1232']) + (int)str_replace(',', '', $admin_exp[5]['company_1232'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1232']);
                $company_1233_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1233']) + (int)str_replace(',', '', $admin_exp[1]['company_1233'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1233']) + (int)str_replace(',', '', $admin_exp[3]['company_1233'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1233']) + (int)str_replace(',', '', $admin_exp[5]['company_1233'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1233']);
                $company_1234_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1234']) + (int)str_replace(',', '', $admin_exp[1]['company_1234'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1234']) + (int)str_replace(',', '', $admin_exp[3]['company_1234'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1234']) + (int)str_replace(',', '', $admin_exp[5]['company_1234'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1234']);
                $company_1235_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1235']) + (int)str_replace(',', '', $admin_exp[1]['company_1235'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1235']) + (int)str_replace(',', '', $admin_exp[3]['company_1235'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1235']) + (int)str_replace(',', '', $admin_exp[5]['company_1235'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1235']);
                $company_1236_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1236']) + (int)str_replace(',', '', $admin_exp[1]['company_1236'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1236']) + (int)str_replace(',', '', $admin_exp[3]['company_1236'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1236']) + (int)str_replace(',', '', $admin_exp[5]['company_1236'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1236']);
                $company_1237_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1237']) + (int)str_replace(',', '', $admin_exp[1]['company_1237'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1237']) + (int)str_replace(',', '', $admin_exp[3]['company_1237'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1237']) + (int)str_replace(',', '', $admin_exp[5]['company_1237'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1237']);
                $company_1901_admin_exp = (int)str_replace(',', '', $admin_exp[0]['company_1901']) + (int)str_replace(',', '', $admin_exp[1]['company_1901'])
                    + (int)str_replace(',', '', $admin_exp[2]['company_1901']) + (int)str_replace(',', '', $admin_exp[3]['company_1901'])
                    + (int)str_replace(',', '', $admin_exp[4]['company_1901']) + (int)str_replace(',', '', $admin_exp[5]['company_1901'])
                    + (int)str_replace(',', '', $admin_exp[6]['company_1901']);

                if ($isUser_HIL === null) {
                    $company_1231_admin_exp = 0;
                }
                if ($isUser_SIB === null) {
                    $company_1232_admin_exp = 0;
                }
                if ($isUser_ROX === null) {
                    $company_1233_admin_exp = 0;
                }
                if ($isUser_SUM === null) {
                    $company_1234_admin_exp = 0;
                }
                if ($isUser_DIV === null) {
                    $company_1235_admin_exp = 0;
                }
                if ($isUser_JARO === null) {
                    $company_1236_admin_exp = 0;
                }
                if ($isUser_KAB === null) {
                    $company_1237_admin_exp = 0;
                }
                if ($isUser_CS === null) {
                    $company_1901_admin_exp = 0;
                }

                $total_all_admin_exp = $company_1231_admin_exp + $company_1232_admin_exp + $company_1233_admin_exp + $company_1234_admin_exp
                    + $company_1235_admin_exp + $company_1236_admin_exp + $company_1237_admin_exp + $company_1901_admin_exp;
            }

            $accArr = [
                'SALARIES' => $salaries,
                'FUELS' => $fuels,
                'MAINTENANCE' => $maintenance,
                'REPAIRS' => $repairs,
                'PARKING' => $parking,
                'TOLLING' => $tolling,
                'FUEL_AND_SELLING_EXP' => $fuel_and_selling_exp,
                'UTILITIES' => $utilities,
                'ADMIN_EXPENSE' => $admin_exp,
            ];

            $total_admin_exp_1231 = $company_1231_utilities + $company_1231_admin_exp;
            $total_admin_exp_1232 = $company_1232_utilities + $company_1232_admin_exp;
            $total_admin_exp_1233 = $company_1233_utilities + $company_1233_admin_exp;
            $total_admin_exp_1234 = $company_1234_utilities + $company_1234_admin_exp;
            $total_admin_exp_1235 = $company_1235_utilities + $company_1235_admin_exp;
            $total_admin_exp_1236 = $company_1236_utilities + $company_1236_admin_exp;
            $total_admin_exp_1237 = $company_1237_utilities + $company_1237_admin_exp;
            $total_admin_exp_1901 = $company_1901_utilities + $company_1901_admin_exp;
            $total_admin_exp_all = $total_all_utilities + $total_all_admin_exp;

            //add here below under of fuel and selling exp
            $total_all_fuel_and_selling_exp_1231 = $company_1231_fuel + $company_1231_main + $company_1231_repairs + $company_1231_parking + $company_1231_tolling + $company_1231_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1232 = $company_1232_fuel + $company_1232_main + $company_1232_repairs + $company_1232_parking + $company_1232_tolling + $company_1232_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1233 = $company_1233_fuel + $company_1233_main + $company_1233_repairs + $company_1233_parking + $company_1233_tolling + $company_1233_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1234 = $company_1234_fuel + $company_1234_main + $company_1234_repairs + $company_1234_parking + $company_1234_tolling + $company_1234_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1235 = $company_1235_fuel + $company_1235_main + $company_1235_repairs + $company_1235_parking + $company_1235_tolling + $company_1235_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1236 = $company_1236_fuel + $company_1236_main + $company_1236_repairs + $company_1236_parking + $company_1236_tolling + $company_1236_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1237 = $company_1237_fuel + $company_1237_main + $company_1237_repairs + $company_1237_parking + $company_1237_tolling + $company_1237_fuel_and_selling_exp;
            $total_all_fuel_and_selling_exp_1901 = $company_1901_fuel + $company_1901_main + $company_1901_repairs + $company_1901_parking + $company_1901_tolling + $company_1901_fuel_and_selling_exp;

            // add salaries , fuel and sell exp, admin exp below
            //$company_1231 is understood to be salaries as default
//            if ($isUserSal === null) {
//                $company_1231 = 0;
//            }
//
//            if ($isUser_HIL === null && $isUserSal === null) {
//                $company_1231 = 0;
//            }
//            if ($isUser_SIB === null && $isUserSal === null) {
//                $company_1232 = 0;
//            }
//            if ($isUser_ROX === null && $isUserSal === null) {
//                $company_1233 = 0;
//            }
//            if ($isUser_SUM === null && $isUserSal === null) {
//                $company_1234 = 0;
//            }
//            if ($isUser_DIV === null && $isUserSal === null) {
//                $company_1235 = 0;
//            }
//            if ($isUser_JARO === null && $isUserSal === null) {
//                $company_1236 = 0;
//            }
//            if ($isUser_KAB === null && $isUserSal === null) {
//                $company_1237 = 0;
//            }
//            if ($isUser_CS === null && $isUserSal === null) {
//                $company_1901 = 0;
//            }

            $opex_1231 = $company_1231 + $total_all_fuel_and_selling_exp_1231 + $total_admin_exp_1231;
            $opex_1232 = $company_1232 + $total_all_fuel_and_selling_exp_1232 + $total_admin_exp_1232;
            $opex_1233 = $company_1233 + $total_all_fuel_and_selling_exp_1233 + $total_admin_exp_1233;
            $opex_1234 = $company_1234 + $total_all_fuel_and_selling_exp_1234 + $total_admin_exp_1234;
            $opex_1235 = $company_1235 + $total_all_fuel_and_selling_exp_1235 + $total_admin_exp_1235;
            $opex_1236 = $company_1236 + $total_all_fuel_and_selling_exp_1236 + $total_admin_exp_1236;
            $opex_1237 = $company_1237 + $total_all_fuel_and_selling_exp_1237 + $total_admin_exp_1237;
            $opex_1901 = $company_1901 + $total_all_fuel_and_selling_exp_1901 + $total_admin_exp_1901;
//            return response()->json($opex_1901);
            if ($isUser_HIL === null) {
                $opex_1231 = 0;
            }
            if ($isUser_SIB === null) {
                $opex_1232 = 0;
            }
            if ($isUser_ROX === null) {
                $opex_1233 = 0;
            }
            if ($isUser_SUM === null) {
                $opex_1234 = 0;
            }
            if ($isUser_DIV === null) {
                $opex_1235 = 0;
            }
            if ($isUser_JARO === null) {
                $opex_1236 = 0;
            }
            if ($isUser_KAB === null) {
                $opex_1237 = 0;
            }
            if ($isUser_CS === null) {
                $opex_1901 = 0;
            }

            $opex_total = $opex_1231 + $opex_1232 + $opex_1233 + $opex_1234 + $opex_1235 + $opex_1236 + $opex_1237 + $opex_1901;

//            Carbon::now()->format('Y-m-d');
            $from = strtotime($isFrom);
            $to = strtotime($isTo);

            $format_From_Month = date('m', $from);
            $format_To_Month = date('m', $to);

            $format_From_Year = date('Y', $from);
            $format_To_Year = date('Y', $to);

            $checkDiffMonth = abs($format_From_Month - $format_To_Month);
            $totalMonthDiff = 1;
            $finalTotalMonthDiff = null;
            if ($format_From_Year === $format_To_Year) {
                if ($checkDiffMonth >= 1) {
                    $totalMonthDiff = $checkDiffMonth + 1;
                }
            } else {
                $diffYear = abs($format_From_Year - $format_To_Year);
                $addYear = 0;
                if ($diffYear > 1) {
                    $addYear = 12;
                }
                $diffMonthInAYear = abs($format_From_Month - 12) + 1; // e.g 12 - 8 = 4 + 1
                $totalMonthDiff = $diffMonthInAYear + (int)$format_To_Month + $addYear;
            }

            $budget_1231 = $getPerCompany['1231']['total_operating'] ?? 0;
            $budget_1232 = $getPerCompany['1232']['total_operating'] ?? 0;
            $budget_1233 = $getPerCompany['1233']['total_operating'] ?? 0;
            $budget_1234 = $getPerCompany['1234']['total_operating'] ?? 0;
            $budget_1235 = $getPerCompany['1235']['total_operating'] ?? 0;
            $budget_1236 = $getPerCompany['1236']['total_operating'] ?? 0;
            $budget_1237 = $getPerCompany['1237']['total_operating'] ?? 0;
            $budget_1901 = $getPerCompany['1901']['total_operating'] ?? 0;

            if ($isUser_HIL === null) {
                $budget_1231 = 0;
            }
            if ($isUser_SIB === null) {
                $budget_1232 = 0;
            }
            if ($isUser_ROX === null) {
                $budget_1233 = 0;
            }
            if ($isUser_SUM === null) {
                $budget_1234 = 0;
            }
            if ($isUser_DIV === null) {
                $budget_1235 = 0;
            }
            if ($isUser_JARO === null) {
                $budget_1236 = 0;
            }
            if ($isUser_KAB === null) {
                $budget_1237 = 0;
            }
            if ($isUser_CS === null) {
                $budget_1901 = 0;
            }


            $budget_total = ($budget_1231 * $totalMonthDiff) + ($budget_1232 * $totalMonthDiff)+ ($budget_1233 * $totalMonthDiff)
                + ($budget_1234 * $totalMonthDiff) + ($budget_1235 * $totalMonthDiff) + ($budget_1236 * $totalMonthDiff)
                + ($budget_1237 * $totalMonthDiff) + ($budget_1901 * $totalMonthDiff);

            $variance_1231 = ($budget_1231 * $totalMonthDiff) - $opex_1231;
            $variance_1232 = ($budget_1232 * $totalMonthDiff) - $opex_1232;
            $variance_1233 = ($budget_1233 * $totalMonthDiff) - $opex_1233;
            $variance_1234 = ($budget_1234 * $totalMonthDiff) - $opex_1234;
            $variance_1235 = ($budget_1235 * $totalMonthDiff) - $opex_1235;
            $variance_1236 = ($budget_1236 * $totalMonthDiff) - $opex_1236;
            $variance_1237 = ($budget_1237 * $totalMonthDiff) - $opex_1237;
            $variance_1901 = ($budget_1901 * $totalMonthDiff) - $opex_1901;
//            return response()->json($variance_1231);
            $variance_total = $variance_1231 + $variance_1232 + $variance_1233 + $variance_1234 + $variance_1235 + $variance_1236 + $variance_1237 + $variance_1901;


            $total_all_fuel_and_selling_exp = $total_all_fuel_and_selling_exp_1231 + $total_all_fuel_and_selling_exp_1232
                + $total_all_fuel_and_selling_exp_1233 + $total_all_fuel_and_selling_exp_1234 + $total_all_fuel_and_selling_exp_1235
                + $total_all_fuel_and_selling_exp_1236 + $total_all_fuel_and_selling_exp_1237 + $total_all_fuel_and_selling_exp_1901;

            $accounts = [
                'ACCOUNTS' => [
                    'DETAILS' => $accArr,
                    //salaries
                    'TOTAL_1231' => number_format($company_1231),
                    'TOTAL_1232' => number_format($company_1232),
                    'TOTAL_1233' => number_format($company_1233),
                    'TOTAL_1234' => number_format($company_1234),
                    'TOTAL_1235' => number_format($company_1235),
                    'TOTAL_1236' => number_format($company_1236),
                    'TOTAL_1237' => number_format($company_1237),
                    'TOTAL_1901' => number_format($company_1901),
                    'TOTAL_ALL' => number_format($total_all),

                    //fuels
                    'TOTAL_1231_FUELS' => number_format($company_1231_fuel),
                    'TOTAL_1232_FUELS' => number_format($company_1232_fuel),
                    'TOTAL_1233_FUELS' => number_format($company_1233_fuel),
                    'TOTAL_1234_FUELS' => number_format($company_1234_fuel),
                    'TOTAL_1235_FUELS' => number_format($company_1235_fuel),
                    'TOTAL_1236_FUELS' => number_format($company_1236_fuel),
                    'TOTAL_1237_FUELS' => number_format($company_1237_fuel),
                    'TOTAL_1901_FUELS' => number_format($company_1901_fuel),
                    'TOTAL_ALL_FUELS' => number_format($total_all_fuel),

                    //fuels and selling exp
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1231' => number_format($total_all_fuel_and_selling_exp_1231),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1232' => number_format($total_all_fuel_and_selling_exp_1232),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1233' => number_format($total_all_fuel_and_selling_exp_1233),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1234' => number_format($total_all_fuel_and_selling_exp_1234),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1235' => number_format($total_all_fuel_and_selling_exp_1235),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1236' => number_format($total_all_fuel_and_selling_exp_1236),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1237' => number_format($total_all_fuel_and_selling_exp_1237),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP_1901' => number_format($total_all_fuel_and_selling_exp_1901),
                    'TOTAL_ALL_FUELS_AND_SELLING_EXP' => number_format($total_all_fuel_and_selling_exp),

                    //maintenance
                    'TOTAL_1231_MAIN' => number_format($company_1231_main),
                    'TOTAL_1232_MAIN' => number_format($company_1232_main),
                    'TOTAL_1233_MAIN' => number_format($company_1233_main),
                    'TOTAL_1234_MAIN' => number_format($company_1234_main),
                    'TOTAL_1235_MAIN' => number_format($company_1235_main),
                    'TOTAL_1236_MAIN' => number_format($company_1236_main),
                    'TOTAL_1237_MAIN' => number_format($company_1237_main),
                    'TOTAL_1901_MAIN' => number_format($company_1901_main),
                    'TOTAL_ALL_MAIN' => number_format($total_all_main),

                    //repairs
                    'TOTAL_1231_REPAIRS' => number_format($company_1231_repairs),
                    'TOTAL_1232_REPAIRS' => number_format($company_1232_repairs),
                    'TOTAL_1233_REPAIRS' => number_format($company_1233_repairs),
                    'TOTAL_1234_REPAIRS' => number_format($company_1234_repairs),
                    'TOTAL_1235_REPAIRS' => number_format($company_1235_repairs),
                    'TOTAL_1236_REPAIRS' => number_format($company_1236_repairs),
                    'TOTAL_1237_REPAIRS' => number_format($company_1237_repairs),
                    'TOTAL_1901_REPAIRS' => number_format($company_1901_repairs),
                    'TOTAL_ALL_REPAIRS' => number_format($total_all_repairs),

                    //parking
                    'TOTAL_1231_PARKING' => number_format($company_1231_parking),
                    'TOTAL_1232_PARKING' => number_format($company_1232_parking),
                    'TOTAL_1233_PARKING' => number_format($company_1233_parking),
                    'TOTAL_1234_PARKING' => number_format($company_1234_parking),
                    'TOTAL_1235_PARKING' => number_format($company_1235_parking),
                    'TOTAL_1236_PARKING' => number_format($company_1236_parking),
                    'TOTAL_1237_PARKING' => number_format($company_1237_parking),
                    'TOTAL_1901_PARKING' => number_format($company_1901_parking),
                    'TOTAL_ALL_PARKING' => number_format($total_all_parking),

                    //tolling
                    'TOTAL_1231_TOLLING' => number_format($company_1231_tolling),
                    'TOTAL_1232_TOLLING' => number_format($company_1232_tolling),
                    'TOTAL_1233_TOLLING' => number_format($company_1233_tolling),
                    'TOTAL_1234_TOLLING' => number_format($company_1234_tolling),
                    'TOTAL_1235_TOLLING' => number_format($company_1235_tolling),
                    'TOTAL_1236_TOLLING' => number_format($company_1236_tolling),
                    'TOTAL_1237_TOLLING' => number_format($company_1237_tolling),
                    'TOTAL_1901_TOLLING' => number_format($company_1901_tolling),
                    'TOTAL_ALL_TOLLING' => number_format($total_all_tolling),

                    // fuel_and_selling_exp // details
                    'TOTAL_1231_FUEL_AND_SELLING_EXP' => number_format($company_1231_fuel_and_selling_exp),
                    'TOTAL_1232_FUEL_AND_SELLING_EXP' => number_format($company_1232_fuel_and_selling_exp),
                    'TOTAL_1233_FUEL_AND_SELLING_EXP' => number_format($company_1233_fuel_and_selling_exp),
                    'TOTAL_1234_FUEL_AND_SELLING_EXP' => number_format($company_1234_fuel_and_selling_exp),
                    'TOTAL_1235_FUEL_AND_SELLING_EXP' => number_format($company_1235_fuel_and_selling_exp),
                    'TOTAL_1236_FUEL_AND_SELLING_EXP' => number_format($company_1236_fuel_and_selling_exp),
                    'TOTAL_1237_FUEL_AND_SELLING_EXP' => number_format($company_1237_fuel_and_selling_exp),
                    'TOTAL_1901_FUEL_AND_SELLING_EXP' => number_format($company_1901_fuel_and_selling_exp),
                    'TOTAL_ALL_FUEL_AND_SELLING_EXP' => number_format($total_details_fuel_and_selling_exp),

                    // utilities
                    'TOTAL_1231_UTILITIES' => number_format($company_1231_utilities),
                    'TOTAL_1232_UTILITIES' => number_format($company_1232_utilities),
                    'TOTAL_1233_UTILITIES' => number_format($company_1233_utilities),
                    'TOTAL_1234_UTILITIES' => number_format($company_1234_utilities),
                    'TOTAL_1235_UTILITIES' => number_format($company_1235_utilities),
                    'TOTAL_1236_UTILITIES' => number_format($company_1236_utilities),
                    'TOTAL_1237_UTILITIES' => number_format($company_1237_utilities),
                    'TOTAL_1901_UTILITIES' => number_format($company_1901_utilities),
                    'TOTAL_ALL_UTILITIES' => number_format($total_all_utilities),

                    //admin_exp
                    'TOTAL_1231_admin_exp' => number_format($total_admin_exp_1231),
                    'TOTAL_1232_admin_exp' => number_format($total_admin_exp_1232),
                    'TOTAL_1233_admin_exp' => number_format($total_admin_exp_1233),
                    'TOTAL_1234_admin_exp' => number_format($total_admin_exp_1234),
                    'TOTAL_1235_admin_exp' => number_format($total_admin_exp_1235),
                    'TOTAL_1236_admin_exp' => number_format($total_admin_exp_1236),
                    'TOTAL_1237_admin_exp' => number_format($total_admin_exp_1237),
                    'TOTAL_1901_admin_exp' => number_format($total_admin_exp_1901),
                    'TOTAL_ALL_admin_exp' => number_format($total_admin_exp_all),

                    'OPEX_1231' => number_format($opex_1231),
                    'OPEX_1232' => number_format($opex_1232),
                    'OPEX_1233' => number_format($opex_1233),
                    'OPEX_1234' => number_format($opex_1234),
                    'OPEX_1235' => number_format($opex_1235),
                    'OPEX_1236' => number_format($opex_1236),
                    'OPEX_1237' => number_format($opex_1237),
                    'OPEX_1901' => number_format($opex_1901),
                    'OPEX_TOTAL' => number_format($opex_total),

                    'BUDGET_1231' => number_format($budget_1231 * $totalMonthDiff),
                    'BUDGET_1232' => number_format($budget_1232 * $totalMonthDiff),
                    'BUDGET_1233' => number_format($budget_1233 * $totalMonthDiff),
                    'BUDGET_1234' => number_format($budget_1234 * $totalMonthDiff),
                    'BUDGET_1235' => number_format($budget_1235 * $totalMonthDiff),
                    'BUDGET_1236' => number_format($budget_1236 * $totalMonthDiff),
                    'BUDGET_1237' => number_format($budget_1237 * $totalMonthDiff),
                    'BUDGET_1901' => number_format($budget_1901 * $totalMonthDiff),
                    'BUDGET_TOTAL' => number_format($budget_total),

                    'VARIANCE_1231' => $variance_1231 > 0 ? "(" . number_format(abs($variance_1231)) . ")" : $variance_1231 == 0 ? number_format(abs($variance_1231)) : "(" . number_format(abs($variance_1231)) . ")",
                    'VARIANCE_1232' => $variance_1232 > 0 ? "(" . number_format(abs($variance_1232)) . ")" : $variance_1232 == 0 ? number_format(abs($variance_1232)) : "(" . number_format(abs($variance_1232)) . ")",
                    'VARIANCE_1233' => $variance_1233 > 0 ? "(" . number_format(abs($variance_1233)) . ")" : $variance_1233 == 0 ? number_format(abs($variance_1233)) : "(" . number_format(abs($variance_1233)) . ")",
                    'VARIANCE_1234' => $variance_1234 > 0 ? "(" . number_format(abs($variance_1234)) . ")" : $variance_1234 == 0 ? number_format(abs($variance_1234)) : "(" . number_format(abs($variance_1234)) . ")",
                    'VARIANCE_1235' => $variance_1235 > 0 ? "(" . number_format(abs($variance_1235)) . ")" : $variance_1235 == 0 ? number_format(abs($variance_1235)) : "(" . number_format(abs($variance_1235)) . ")",
                    'VARIANCE_1236' => $variance_1236 > 0 ? "(" . number_format(abs($variance_1236)) . ")" : $variance_1236 == 0 ? number_format(abs($variance_1236)) : "(" . number_format(abs($variance_1236)) . ")",
                    'VARIANCE_1237' => $variance_1237 > 0 ? "(" . number_format(abs($variance_1237)) . ")" : $variance_1237 == 0 ? number_format(abs($variance_1237)) : "(" . number_format(abs($variance_1237)) . ")",
                    'VARIANCE_1901' => $variance_1901 > 0 ? "(" . number_format(abs($variance_1901)) . ")" : $variance_1901 == 0 ? number_format(abs($variance_1901)) : "(" . number_format(abs($variance_1901)) . ")",
                    'VARIANCE_TOTAL' => $variance_total > 0 ? "(" . number_format(abs($variance_total)) . ")" : $variance_total == 0 ? number_format(abs($variance_total)) : "(" . number_format(abs($variance_total)) . ")",
                ],
            ];
            return response()->json($accounts);
        }
    }
}
