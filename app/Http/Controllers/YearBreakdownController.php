<?php

namespace App\Http\Controllers;

use App\Models\AdminExpense;
use App\Models\BSIS;
use App\Models\FuelAndSellingExp;
use App\Models\Fuels;
use App\Models\Maintenance;
use App\Models\Parking;
use App\Models\Repairs;
use App\Models\Salary;
use App\Models\StaticBudget;
use App\Models\Tolling;
use App\Models\Utilities;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class YearBreakdownController extends Controller
{

    public function yearBreakDown(Request $request){

        $company = $request->company;
        if($request->filter !== 'null'){
            $company = $request->filter;
        }
        $currentDate = Carbon::now()->format('Y-m-d');
        $currentMonth = Carbon::now()->format('m');
        $currentYear = Carbon::now()->format('Y');
        $currentDay = Carbon::now()->format('d');

        $currentPastMonth = Carbon::now()->format('m') - 1;
        $currentPastDate = $currentYear . '-' . $currentPastMonth . '-' . $currentDay;

        $databaseName = Config::get('database.connections')['sqlsrv2']['database'];

        $isCurrentMonth = (int)$currentMonth;

        $static_budgets = StaticBudget::where('company', $company)->first();
        $admin_expenses = AdminExpense::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $fuels = Fuels::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $fuel_and_selling_exp = FuelAndSellingExp::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $maintenance = Maintenance::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $parking = Parking::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $repairs = Repairs::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $salaries = Salary::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $tolling = Tolling::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();
        $utilities = Utilities::where('static_budget_id', $static_budgets->id)->select('name', 'budget')->orderBy('id', 'asc')->get();



        $months = 12; // in a year
        $storeByMonth = [];


        $total_gl_account = DB::connection('sqlsrv2')
            ->select("SELECT ROUND(SUM(DMBTR) / 1000,0) AS DMBTR,MONAT FROM (

            SELECT ROUND(SUM(CASE WHEN HKONT IN (600001, 600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (600001, 600006) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (600004, 600005, 600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (600004, 600005, 600008) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (600300, 600301, 600302, 600303, 600400, 600410, 603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (600300, 600301, 600302, 600303, 600400, 600410, 603901) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (600500,600501,600502,600503) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (6025008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602500) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602501) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602502) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602400) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602401) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602403) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602404) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602405) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602406) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602407) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602302) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602303) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602700) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602702) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602800) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602801) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602802) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602600, 602601, 602602, 602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602600, 602601, 602602, 602603) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (601700) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (601701) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (601800, 601801, 601802, 601803, 601804, 601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (601800, 601801, 601802, 601803, 601804, 601805) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602100, 602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602100, 602101) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (602300, 602301, 602304, 602305, 602306, 602307, 602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (602300, 602301, 602304, 602305, 602306, 602307, 602308) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (603500, 603501, 603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (603500, 603501, 603502) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (603002) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (603700, 603701, 603702, 603703, 603704, 603705, 603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (603700, 603701, 603702, 603703, 603704, 603705, 603706) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            UNION ALL
            SELECT ROUND(SUM(CASE WHEN HKONT IN (603800, 603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END),0) AS DMBTR,MONAT FROM $databaseName.dbo.BSIS
            WHERE HKONT IN (603800, 603801) AND GJAHR = 2022 AND BUKRS = $company
            GROUP BY MONAT

            ) AS x

            GROUP BY MONAT
            ORDER BY MONAT ASC
            ");


        $arrDes = ['Budget','Actual','Variance'];
        $des = [];
        for($x = 0; $x < 3;$x++){
            $des[$x] = [
                'name' => $arrDes[$x],
            ];
        }

        $newStoreByMonth = array_slice($storeByMonth,-12,$isCurrentMonth,true);
        $budgetPush = [];
        $actualPush = [];
        $variancePush = [];
        $actual_exp = '';
        $running_bal = '';
        for ($i = 0; $i < count($des); $i++) {
            for($x = 0; $x < count($total_gl_account); $x++){
                $total_gl_account[$x]->DMBTR = (int)$total_gl_account[$x]->DMBTR;
                $total_gl_account[$x]->budget = (int)$static_budgets->total_operating;
                $total_gl_account[$x]->variance = $total_gl_account[$x]->budget - $total_gl_account[$x]->DMBTR;

                if((int)$total_gl_account[$x]->MONAT === $isCurrentMonth){
                    $actual_exp = $total_gl_account[$x]->DMBTR;
                    $running_bal = $total_gl_account[$x]->variance;
                }
                $budgetPush[$total_gl_account[$x]->MONAT] = [
                   'val' => $total_gl_account[$x]->budget,
                ];
                $actualPush[$total_gl_account[$x]->MONAT] = [
                   'val' => $total_gl_account[$x]->DMBTR,
                ];
                $variancePush[$total_gl_account[$x]->MONAT] = [
                   'val' => $total_gl_account[$x]->variance,
                ];
            }
            $des[0]['data'] = $budgetPush;
            $des[1]['data'] = $actualPush;
            $des[2]['data'] = $variancePush;
        }

        //BUDGET DETAILS PAST MONTH
        $regular_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [600001, 600006]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $agency_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [600004, 600005, 600008]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $other_benefits_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600401, 600402, 600403, 600404, 600405, 600406, 600407, 600408, 600409, 600410, 603901]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $non_mandatory_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [600500,600501,600502,600503]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $fuel_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602500]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $fuel_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602501]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $fuel_machinery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602502]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_tire_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602400]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_tire_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602401]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_battery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602403]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_battery_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602404]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_machinery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602405]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_oil_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602406]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_oil_vehicles_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602407]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $repair_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602302]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $repair_vehicles_delivery_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602303]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $parking_office_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602700]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

//            $parking_fsp_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
//                $query->where('BUKRS', $company)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth - 1)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

        $parking_delivery_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602702]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $toll_office_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602800]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $toll_fsp_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602801]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $toll_delivery_vehicles_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602802]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $ad_and_promotions_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $trans_and_travel_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $utilities_electricity_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [601700]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $utilities_water_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [601701]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $communication_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $postage_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602100, 602101]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $repairs_total_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $represent_and_entertainment_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [603500, 603501, 603502]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $plog_fees_others_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [603002]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $supplies_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $subscriptions_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentMonth === '01' ? $currentYear - 1 : $currentYear)
                ->where('MONAT', $currentMonth - 1 === 0 ? 12 : $currentMonth - 1)
                ->whereIn('HKONT', [603800, 603801]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

//            $solicit_and_donations_past_month = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $currentPastDate) {
//                $query->where('BUKRS', $company)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth - 1)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();
        $balance_last_month = 0;
        if($isCurrentMonth !== 1){
            $balance_last_month = $static_budgets->total_operating - (number_format(round((float)$regular_past_month->DMBTR / 1000)) + number_format(round((float)$agency_past_month->DMBTR / 1000))
                    + number_format(round((float)$other_benefits_past_month->DMBTR / 1000))
                    + number_format(round((float)$non_mandatory_past_month->DMBTR / 1000))
                    + number_format(round((float)$fuel_vehicles_past_month->DMBTR / 1000)) + number_format(round((float)$fuel_delivery_past_month->DMBTR / 1000))
                    + number_format(round((float)$fuel_machinery_past_month->DMBTR/ 1000)) + number_format(round((float)$main_tire_past_month->DMBTR/ 1000))
                    + number_format(round((float)$main_tire_delivery_past_month->DMBTR/ 1000)) + number_format(round((float)$main_battery_past_month->DMBTR/ 1000))
                    + number_format(round((float)$main_battery_delivery_past_month->DMBTR / 1000)) + number_format(round((float)$main_machinery_past_month->DMBTR / 1000))
                    + number_format(round((float)$main_oil_vehicles_past_month->DMBTR / 1000)) + number_format(round((float)$main_oil_vehicles_delivery_past_month->DMBTR / 1000))
                    + number_format(round((float)$repair_vehicles_past_month->DMBTR/ 1000)) + number_format(round((float)$repair_vehicles_delivery_past_month->DMBTR/ 1000))
                    + number_format(round((float)$parking_office_past_month->DMBTR/ 1000)) + number_format(round((float)$parking_delivery_vehicles_past_month->DMBTR/ 1000))
                    + number_format(round((float)$toll_office_past_month->DMBTR/ 1000)) + number_format(round((float)$toll_fsp_vehicles_past_month->DMBTR/ 1000))
                    + number_format(round((float)$toll_delivery_vehicles_past_month->DMBTR/ 1000)) + number_format(round((float)$ad_and_promotions_past_month->DMBTR/ 1000))
                    + number_format(round((float)$trans_and_travel_past_month->DMBTR/ 1000)) + number_format(round((float)$utilities_electricity_past_month->DMBTR/ 1000))
                    + number_format(round((float)$utilities_water_past_month->DMBTR/ 1000)) + number_format(round((float)$communication_past_month->DMBTR/ 1000))
                    + number_format(round((float)$postage_past_month->DMBTR/ 1000)) + number_format(round((float)$repairs_total_past_month->DMBTR/ 1000))
                    + number_format(round((float)$represent_and_entertainment_past_month->DMBTR/ 1000))
                    + number_format(round((float)$plog_fees_others_past_month->DMBTR/ 1000)) + number_format(round((float)$supplies_past_month->DMBTR/ 1000))
                    + number_format(round((float)$subscriptions_past_month->DMBTR/ 1000)));
        }

        $electricity_exp = [];
        $months_des = ['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        for($x = 1; $x <= $months;$x++){
            $utilities_electricity_2021 = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate,$x) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', 2021)
                    ->where('MONAT', $x)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $utilities_electricity_2020 = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate,$x) {
                $query->where('BUKRS', $company)
                    ->where('GJAHR', 2020)
                    ->where('MONAT', $x)
                    ->whereIn('HKONT', [601700]);
            })
                ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                ->first();

            $electricity_exp[]= [
               $months_des[$x],
                (int)$utilities_electricity_2020->DMBTR,
                (int)$utilities_electricity_2021->DMBTR,
            ];
        }

        $regular_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [600001, 600006]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $agency_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [600004, 600005, 600008]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $other_benefits_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600410, 603901]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $non_mandatory_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [600500,600501,600502,600503]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500,600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $fuel_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602500]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $fuel_delivery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602501]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $fuel_machinery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602502]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_tire_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602400]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_tire_delivery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602401]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_battery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602403]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_battery_delivery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602404]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_machinery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602405]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_oil_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602406]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $main_oil_vehicles_delivery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602407]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $repair_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602302]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $repair_vehicles_delivery_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602303]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $parking_office_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602700]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

//            $parking_fsp_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth)
//                    ->whereIn('HKONT', [602701]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

        $parking_delivery_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602702]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $toll_office_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602800]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $toll_fsp_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602801]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $toll_delivery_vehicles_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602802]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $ad_and_promotions_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $trans_and_travel_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $utilities_electricity_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [601700]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $utilities_water_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [601701]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $communication_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $postage_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602100, 602101]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $repairs_total_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $represent_and_entertainment_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [603500, 603501, 603502]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $plog_fees_others_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [603002]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $supplies_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

        $subscriptions_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
            $query->where('BUKRS', $company)
                ->where('GJAHR', $currentYear)
                ->where('MONAT', $currentMonth)
                ->whereIn('HKONT', [603800, 603801]);
        })
            ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
            ->first();

//            $solicit_and_donations_top_exp = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                $query->where('BUKRS', $company)
//                    ->where('BUDAT', '<=', $currentDate)
//                    ->where('GJAHR', $currentYear)
//                    ->where('MONAT', $currentMonth)
//                    ->whereIn('HKONT', [604001]);
//            })
//                ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                ->first();

        for ($i = 0; $i < count($salaries); $i++) {
            //CURRENT MONTH
            $salaries[0]['actual_top_exp'] = number_format(round((float)$regular_top_exp->DMBTR / 1000));
            $salaries[1]['actual_top_exp'] = number_format(round((float)$agency_top_exp->DMBTR / 1000));
            $salaries[2]['actual_top_exp'] = number_format(round((float)$other_benefits_top_exp->DMBTR / 1000));
            $salaries[3]['actual_top_exp'] = number_format(round((float)$non_mandatory_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $salaries[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $salaries[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $salaries[$i]['variance_top_exp'] = $var_total_top_exp;
            $salaries[$i]['budget'] = (int)$salaries[$i]['budget'];
        }
        for ($i = 0; $i < count($fuels); $i++) {
            //CURRENT MONTH
            $fuels[0]['actual_top_exp'] = number_format(round((float)$fuel_vehicles_top_exp->DMBTR / 1000));
            $fuels[1]['actual_top_exp'] = number_format(round((float)$fuel_delivery_top_exp->DMBTR / 1000));
            $fuels[2]['actual_top_exp'] = number_format(round((float)$fuel_machinery_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $fuels[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $fuels[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $fuels[$i]['variance_top_exp'] = $var_total_top_exp;
            $fuels[$i]['budget'] = (int)$fuels[$i]['budget'];
        }
        for ($i = 0; $i < count($maintenance); $i++) {
            $maintenance[0]['actual_top_exp'] = number_format(round((float)$main_tire_top_exp->DMBTR / 1000));
            $maintenance[1]['actual_top_exp'] = number_format(round((float)$main_tire_delivery_top_exp->DMBTR / 1000));
            $maintenance[2]['actual_top_exp'] = number_format(round((float)$main_battery_top_exp->DMBTR / 1000));
            $maintenance[3]['actual_top_exp'] = number_format(round((float)$main_battery_delivery_top_exp->DMBTR / 1000));
            $maintenance[4]['actual_top_exp'] = number_format(round((float)$main_machinery_top_exp->DMBTR / 1000));
            $maintenance[5]['actual_top_exp'] = number_format(round((float)$main_oil_vehicles_top_exp->DMBTR / 1000));
            $maintenance[6]['actual_top_exp'] = number_format(round((float)$main_oil_vehicles_delivery_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $maintenance[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $maintenance[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $maintenance[$i]['variance_top_exp'] = $var_total_top_exp;
            $maintenance[$i]['budget'] = (int)$maintenance[$i]['budget'];
        }
        for ($i = 0; $i < count($repairs); $i++) {
            $repairs[0]['actual_top_exp'] = number_format(round((float)$repair_vehicles_top_exp->DMBTR / 1000));
            $repairs[1]['actual_top_exp'] = number_format(round((float)$repair_vehicles_delivery_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $repairs[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $repairs[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $repairs[$i]['variance_top_exp'] = $var_total_top_exp;
            $repairs[$i]['budget'] = (int)$repairs[$i]['budget'];
        }
        for ($i = 0; $i < count($parking); $i++) {
            $parking[0]['actual_top_exp'] = number_format(round((float)$parking_office_top_exp->DMBTR / 1000));
//                $parking[1]['actual_top_exp'] = number_format(round((float)$parking_fsp_vehicles_top_exp->DMBTR / 1000));
            $parking[1]['actual_top_exp'] = number_format(round((float)$parking_delivery_vehicles_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $parking[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $parking[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $parking[$i]['variance_top_exp'] = $var_total_top_exp;
            $parking[$i]['budget'] = (int)$parking[$i]['budget'];
        }
        for ($i = 0; $i < count($tolling); $i++) {
            $tolling[0]['actual_top_exp'] = number_format(round((float)$toll_office_top_exp->DMBTR / 1000));
            $tolling[1]['actual_top_exp'] = number_format(round((float)$toll_fsp_vehicles_top_exp->DMBTR / 1000));
            $tolling[2]['actual_top_exp'] = number_format(round((float)$toll_delivery_vehicles_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $tolling[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $tolling[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $tolling[$i]['variance_top_exp'] = $var_total_top_exp;
            $tolling[$i]['budget'] = (int)$tolling[$i]['budget'];
        }
        for ($i = 0; $i < count($fuel_and_selling_exp); $i++) {
            $fuel_and_selling_exp[0]['actual_top_exp'] = number_format(round((float)$ad_and_promotions_top_exp->DMBTR / 1000));
            $fuel_and_selling_exp[1]['actual_top_exp'] = number_format(round((float)$trans_and_travel_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $fuel_and_selling_exp[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $fuel_and_selling_exp[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $fuel_and_selling_exp[$i]['variance_top_exp'] = $var_total_top_exp;
            $fuel_and_selling_exp[$i]['budget'] = (int)$fuel_and_selling_exp[$i]['budget'];
        }
        for ($i = 0; $i < count($utilities); $i++) {
            $utilities[0]['actual_top_exp'] = number_format(round((float)$utilities_electricity_top_exp->DMBTR / 1000));
            $utilities[1]['actual_top_exp'] = number_format(round((float)$utilities_water_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $utilities[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $utilities[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $utilities[$i]['variance_top_exp'] = $var_total_top_exp;
            $utilities[$i]['budget'] = (int)$utilities[$i]['budget'];
        }
        for ($i = 0; $i < count($admin_expenses); $i++) {
            $admin_expenses[0]['actual_top_exp'] = number_format(round((float)$communication_top_exp->DMBTR / 1000));
            $admin_expenses[1]['actual_top_exp'] = number_format(round((float)$postage_top_exp->DMBTR / 1000));
            $admin_expenses[2]['actual_top_exp'] = number_format(round((float)$repairs_total_top_exp->DMBTR / 1000));
            $admin_expenses[3]['actual_top_exp'] = number_format(round((float)$represent_and_entertainment_top_exp->DMBTR / 1000));
            $admin_expenses[4]['actual_top_exp'] = number_format(round((float)$plog_fees_others_top_exp->DMBTR / 1000));
            $admin_expenses[5]['actual_top_exp'] = number_format(round((float)$supplies_top_exp->DMBTR / 1000));
            $admin_expenses[6]['actual_top_exp'] = number_format(round((float)$subscriptions_top_exp->DMBTR / 1000));
//            $admin_expenses[7]['actual_top_exp'] = number_format(round((float)$solicit_and_donations_top_exp->DMBTR / 1000));

            //VARIANCE [BUDGET - ACTUAL]
            $convert_budget = str_replace(',', '', $admin_expenses[$i]['budget']);
            $convert_num_budget = (int)$convert_budget;

            $convert_actual = str_replace(',', '', $admin_expenses[$i]['actual_top_exp']);
            $convert_num_actual = (int)$convert_actual;

            $var_total_top_exp = $convert_num_budget - $convert_num_actual;
            $admin_expenses[$i]['variance_top_exp'] = $var_total_top_exp;
            $admin_expenses[$i]['budget'] = (int)$admin_expenses[$i]['budget'];
        }

        $arrMerge = array_merge($salaries->toArray(),$fuels->toArray(),$maintenance->toArray(),
            $repairs->toArray(),$parking->toArray(),$tolling->toArray(),$fuel_and_selling_exp->toArray(),
            $utilities->toArray(), $admin_expenses->toArray());

        $arrMergeTopTen = array_merge($fuels->toArray(),$maintenance->toArray(),
            $repairs->toArray(),$parking->toArray(),$tolling->toArray(),$fuel_and_selling_exp->toArray(),
            $utilities->toArray(), $admin_expenses->toArray());

        $arrGetVar = [];
        for($x = 0;$x < count($arrMerge);$x++){
            $arrGetVar[] = $arrMerge[$x]['variance_top_exp'];
        }

        $arrTopTen = [];
        for($x = 0;$x < count($arrMergeTopTen);$x++){
           $arrTopTen[] = (int)$arrMergeTopTen[$x]['actual_top_exp'];
        }

        arsort($arrTopTen);
        asort($arrGetVar);

        $removeLeadingZeroArr = [];
        foreach ($arrGetVar as $key => $value){
            if($value !== 0){
                $removeLeadingZeroArr[$key] = $value;
            }
        }

        $newArrTopTen = array_slice($arrTopTen,0,10,true);
        $newArrVar = array_slice($removeLeadingZeroArr,0,10,true);

        $newFormArrTopOverBudget = [];
        foreach($newArrVar as $key => $value){
            foreach($arrMerge as $keyMerge => $data){
                if($key === $keyMerge){
                    $newFormArrTopOverBudget[] = $data;
                }
            }
        }

        $newFormArrTopTen = [];
        foreach($newArrTopTen as $key => $value){
            foreach($arrMergeTopTen as $keyMerge => $data){
                if($key === $keyMerge && $value !== 0){
                    $newFormArrTopTen[] = $data;
                }
            }
        }

        return response()->json([
            'balance_last_month' => $balance_last_month,
            'budget_this_month' => $static_budgets->total_operating,
            'total_budget' => $static_budgets->total_operating + $balance_last_month,
            'actual_exp' => $actual_exp,
            'running_bal' => $running_bal,
            'details' => $des,
            'electricity_exp' => $electricity_exp,
            'top_expenses' => $newFormArrTopTen,
            'top_over_budget' => $newFormArrTopOverBudget,
        ]);
    }
}
