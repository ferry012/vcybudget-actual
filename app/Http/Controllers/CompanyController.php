<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $GLAccountController;
    public function __construct(GLAccountController $GLAccountController)
    {
        $this->GLAccountController = $GLAccountController;
    }

    public function allCompany(Request $request){
        $query = Company::orderBy('EKORG','asc')
            ->where('EKORG','!=','1351')
            ->where('EKORG','!=','1601')
            ->where('EKORG','!=','1902')
            ->get();
//       $test = $this->GLAccountController->index($query->toArray());
        return response()->json($query);
    }
}
