<?php

namespace App\Http\Controllers;

use App\Models\AdminExpense;
use App\Models\BSIS;
use App\Models\Company;
use App\Models\FuelAndSellingExp;
use App\Models\Fuels;
use App\Models\Maintenance;
use App\Models\Parking;
use App\Models\Repairs;
use App\Models\Salary;
use App\Models\StaticBudget;
use App\Models\Tolling;
use App\Models\User;
use App\Models\Utilities;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SummaryController extends Controller
{
    protected $BudgetDetailsController;

    public function __construct(BudgetDetailsController $BudgetDetailsController)
    {
        $this->BudgetDetailsController = $BudgetDetailsController;
    }


    public function summaryOperatingExp(Request $request)
    {
        $currentDate = Carbon::now()->format('Y-m-d');
        $currentMonth = Carbon::now()->format('m');
        $currentYear = Carbon::now()->format('Y');
        $currentDay = Carbon::now()->format('d');

        $get_PY_DATE = explode("-", $currentDate);
        foreach ($get_PY_DATE as $key => $value) {
            if ($key === 0) {
                $get_PY_DATE[0] -= 1;
                break;
            }
        }

        $PY_DATE = implode("-", $get_PY_DATE);

        $user = auth()->user()->name;
        $admin = User::permission('summary_admin_view_all_company')->get();
        $isAdmin = false;

//        $isAdmin = true;
//        $user = 'Super admin';

        foreach ($admin as $key => $value) {
            if ($value['name'] === $user) {
                $isAdmin = true;
                break;
            }
        }

        if ($isAdmin) {
            $User_V = User::role('operations_thd_V')->get();
            $isUser_V = null;
            $isCurrentUser_V = false;

            foreach($User_V as $key => $value){
                if($user === $value['name']){
                    $isUser_V = $value['name'];
                    $isCurrentUser_V = true;
                    break;
                }
            }

            $User_II = User::role('operations_thd_II')->get();
            $isUser_II = null;
            $isCurrentUser_II = false;

            foreach($User_II as $key => $value){
                if($user === $value['name']){
                    $isUser_II = $value['name'];
                    $isCurrentUser_II = true;
                    break;
                }
            }

            $User_HIL = User::permission('hilado_company_dashboard')->get();
            $isUser_HIL = null;

            foreach($User_HIL as $key => $value){
                if($user === $value['name']){
                    $isUser_HIL = $value['name'];
                    break;
                }
            }

            $User_SIB = User::permission('sibulan_company_dashboard')->get();
            $isUser_SIB = null;

            foreach($User_SIB as $key => $value){
                if($user === $value['name']){
                    $isUser_SIB = $value['name'];
                    break;
                }
            }

            $User_ROX = User::permission('roxas_company_dashboard')->get();
            $isUser_ROX = null;

            foreach($User_ROX as $key => $value){
                if($user === $value['name']){
                    $isUser_ROX = $value['name'];
                    break;
                }
            }

            $User_SUM = User::permission('sumag_company_dashboard')->get();
            $isUser_SUM = null;

            foreach($User_SUM as $key => $value){
                if($user === $value['name']){
                    $isUser_SUM = $value['name'];
                    break;
                }
            }

            $User_DIV = User::permission('diversion_company_dashboard')->get();
            $isUser_DIV = null;

            foreach($User_DIV as $key => $value){
                if($user === $value['name']){
                    $isUser_DIV = $value['name'];
                    break;
                }
            }

            $User_JARO = User::permission('jaro_company_dashboard')->get();
            $isUser_JARO = null;

            foreach($User_JARO as $key => $value){
                if($user === $value['name']){
                    $isUser_JARO = $value['name'];
                    break;
                }
            }

            $User_KAB = User::permission('kabankalan_company_dashboard')->get();
            $isUser_KAB = null;

            foreach($User_KAB as $key => $value){
                if($user === $value['name']){
                    $isUser_KAB = $value['name'];
                    break;
                }
            }

            $User_CS = User::permission('corporate_services_company_dashboard')->get();
            $isUser_CS = null;

            foreach($User_CS as $key => $value){
                if($user === $value['name']){
                    $isUser_CS = $value['name'];
                    break;
                }
            }


            $getCompany = Company::where('EKORG', '!=', '1351')
                ->where('EKORG', '!=', '1601')
                ->where('EKORG', '!=', '1902')
                ->where('EKORG', '!=', '1238')
                ->when($isUser_V === $user, function ($query) { //1231, 1232, 1233, 1234, 1237

                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_II === $user, function ($query) { //1235, 1236

                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_HIL === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_SIB === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_ROX === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_SUM === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_DIV === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_JARO === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1237')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_KAB === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1901');
                    });
                })
                ->when($isUser_CS === $user && !$isCurrentUser_V && !$isCurrentUser_II, function ($query) {
                    $query->where(function($q) {
                        $q->where('EKORG', '!=', '1231')
                            ->where('EKORG', '!=', '1232')
                            ->where('EKORG', '!=', '1233')
                            ->where('EKORG', '!=', '1234')
                            ->where('EKORG', '!=', '1235')
                            ->where('EKORG', '!=', '1236')
                            ->where('EKORG', '!=', '1237');
                    });
                })
                ->orderBy('EKORG', 'asc')
                ->get();
//            dd($getCompany);
            $arrOperating = [];
            $getCompanyCodes = [];

            foreach ($getCompany as $key => $value) {
                $static_budgets = StaticBudget::where('company', $value['EKORG'])->first();
                $company = $value['EKORG'];
                $getCompanyCodes[$key] = $value['EKORG'];
                if ($static_budgets !== null) {
                    //CY_ACTUAL
                    $regular = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[600001,600006]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $agency = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[600004,600005,600008]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $other_benefits = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $non_mandatory = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[600500, 600501,600502,600503]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500, 600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $fuel_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602500]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $fuel_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602501]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $fuel_machinery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602502]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_tire = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602400]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_tire_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602401]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_battery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602403]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_battery_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602404]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_machinery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602405]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_oil_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602406]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_oil_vehicles_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602407]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $repair_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602302]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $repair_vehicles_delivery = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602303]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $parking_office = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602700]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

//                    $parking_fsp_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                        $query->where('BUKRS', $company)
//                            ->where('BUDAT', '<=', $currentDate)
//                            ->where('GJAHR', $currentYear)
//                            ->whereIn('HKONT',[602701]);
//                    })
//                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                        ->first();

                    $parking_delivery_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602702]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $toll_office = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602800]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $toll_fsp_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602801]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $toll_delivery_vehicles = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602802]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $ad_and_promotions = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[600900,601000,601001,601002,601003,601004,601005,601006,601007]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $trans_and_travel = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602600,602601,602602,602603]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $utilities_electricity = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[601700]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $utilities_water = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[601701]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $communication = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[601800,601801,601802,601803,601804,601805]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $postage = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602100,602101]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $repairs_total = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[602300,602301,602304,602305,602306,602307,602308]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $represent_and_entertainment = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[603500,603501,603502]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $plog_fees_others = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[603002]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $supplies = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[603700,603701,603702,603703,603704,603705,603706]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $subscriptions = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $currentDate)
                            ->where('GJAHR', $currentYear)
                            ->whereIn('HKONT',[603800,603801]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

//                    $solicit_and_donations = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate) {
//                        $query->where('BUKRS', $company)
//                            ->where('BUDAT', '<=', $currentDate)
//                            ->where('GJAHR', $currentYear)
//                            ->whereIn('HKONT',[604001]);
//                    })
//                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                        ->first();

                    //PY_ACTUAL
                    $regular_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [600001, 600006]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600001,600006) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $agency_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [600004, 600005, 600008]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600004,600005,600008) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $other_benefits_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [600300, 600301, 600302, 600303, 600400, 600410, 603901]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600300,600301,600302,600303,600400,600401,600402,600403,600404,600405,600406,600407,600408,600409,600410,603901) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $non_mandatory_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [600500, 600503]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600500, 600501,600502,600503) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $fuel_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602500]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602500) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $fuel_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602501]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602501) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $fuel_machinery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602502]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_tire_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602400]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602400) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_tire_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602401]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602401) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_battery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602403]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602403) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_battery_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602404]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602404) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_machinery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602405]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602405) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_oil_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602406]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602406) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $main_oil_vehicles_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602407]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602407) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $repair_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602302]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602302) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $repair_vehicles_delivery_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602303]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602303) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $parking_office_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602700]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

//                    $parking_fsp_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
//                        $query->where('BUKRS', $company)
//                            ->where('BUDAT', '<=', $PY_DATE)
//                            ->where('GJAHR', $currentYear - 1)
//                            ->whereIn('HKONT', [602701]);
//                    })
//                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                        ->first();

                    $parking_delivery_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602702]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602702) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $toll_office_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602800]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602800) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $toll_fsp_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602801]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $toll_delivery_vehicles_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602802]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602802) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $ad_and_promotions_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [600900, 601000, 601001, 601002, 601003, 601004, 601005, 601006, 601007]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (600900,601000,601001,601002,601003,601004,601005,601006,601007) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $trans_and_travel_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602600, 602601, 602602, 602603]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602600,602601,602602,602603) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $utilities_electricity_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [601700]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (601700) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $utilities_water_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [601701]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (601701) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $communication_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [601800, 601801, 601802, 601803, 601804, 601805]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (601800,601801,601802,601803,601804,601805) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $postage_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602100, 602101]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602100,602101) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $repairs_total_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [602300, 602301, 602304, 602305, 602306, 602307, 602308]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (602300,602301,602304,602305,602306,602307,602308) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $represent_and_entertainment_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [603500, 603501, 603502]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603500,603501,603502) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $plog_fees_others_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [603002]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603002) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $supplies_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [603700, 603701, 603702, 603703, 603704, 603705, 603706]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603700,603701,603702,603703,603704,603705,603706) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

                    $subscriptions_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
                        $query->where('BUKRS', $company)
                            ->where('BUDAT', '<=', $PY_DATE)
                            ->where('GJAHR', $currentYear - 1)
                            ->whereIn('HKONT', [603800, 603801]);
                    })
                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (603800,603801) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
                        ->first();

//                    $solicit_and_donations_PY = BSIS::where(function ($query) use ($company, $currentMonth, $currentYear, $currentDate, $PY_DATE) {
//                        $query->where('BUKRS', $company)
//                            ->where('BUDAT', '<=', $PY_DATE)
//                            ->where('GJAHR', $currentYear - 1)
//                            ->whereIn('HKONT', [604001]);
//                    })
//                        ->select(DB::raw("SUM(CASE WHEN HKONT IN (604001) AND SHKZG = 'H' THEN DMBTR * - 1 ELSE DMBTR END) AS DMBTR"))
//                        ->first();

                    $BUDGET = $static_budgets->total_operating * (int)$currentMonth;

                    $CY_ACTUAL = round($regular->DMBTR / 1000 ) + round($agency->DMBTR / 1000 ) + round($other_benefits->DMBTR / 1000)+ round($non_mandatory->DMBTR / 1000)
                        + round($fuel_vehicles->DMBTR / 1000) + round($fuel_delivery->DMBTR /1000) + round($fuel_machinery->DMBTR /1000)
                        + round($main_tire->DMBTR /1000 ) + round($main_tire_delivery->DMBTR / 1000) + round($main_battery->DMBTR / 1000)+ round($main_battery_delivery->DMBTR / 1000)+ round($main_machinery->DMBTR / 1000 )+ round($main_oil_vehicles->DMBTR / 1000) + round($main_oil_vehicles_delivery->DMBTR /1000)
                        + round($repair_vehicles->DMBTR / 1000) + round($repair_vehicles_delivery->DMBTR / 1000)
                        + round($parking_office->DMBTR /1000)  + round($parking_delivery_vehicles->DMBTR / 1000)
                        + round($toll_office->DMBTR /1000) + round($toll_fsp_vehicles->DMBTR / 1000) + round($toll_delivery_vehicles->DMBTR / 1000)
                        + round($ad_and_promotions->DMBTR / 1000) + round($trans_and_travel->DMBTR / 1000)
                        + round($utilities_electricity->DMBTR / 1000) + round($utilities_water->DMBTR / 1000)
                        + round($communication->DMBTR / 1000) + round( $postage->DMBTR / 1000) + round($repairs_total->DMBTR /1000) + round($represent_and_entertainment->DMBTR / 1000) + round($plog_fees_others->DMBTR / 1000) + round( $supplies->DMBTR /1000) + round($subscriptions->DMBTR / 1000);

                    $PY_ACTUAL = round($regular_PY->DMBTR / 1000 ) + round($agency_PY->DMBTR / 1000 ) + round($other_benefits_PY->DMBTR / 1000)+ round($non_mandatory_PY->DMBTR / 1000)
                        + round($fuel_vehicles_PY->DMBTR / 1000) + round($fuel_delivery_PY->DMBTR /1000) + round($fuel_machinery_PY->DMBTR /1000)
                        + round($main_tire_PY->DMBTR /1000 ) + round($main_tire_delivery_PY->DMBTR / 1000) + round($main_battery_PY->DMBTR / 1000)+ round($main_battery_delivery_PY->DMBTR / 1000)+ round($main_machinery_PY->DMBTR / 1000 )+ round($main_oil_vehicles_PY->DMBTR / 1000) + round($main_oil_vehicles_delivery_PY->DMBTR /1000)
                        + round($repair_vehicles_PY->DMBTR / 1000) + round($repair_vehicles_delivery_PY->DMBTR / 1000)
                        + round($parking_office_PY->DMBTR /1000) + round($parking_delivery_vehicles_PY->DMBTR / 1000)
                        + round($toll_office_PY->DMBTR /1000) + round($toll_fsp_vehicles_PY->DMBTR / 1000) + round($toll_delivery_vehicles_PY->DMBTR / 1000)
                        + round($ad_and_promotions_PY->DMBTR / 1000) + round($trans_and_travel_PY->DMBTR / 1000)
                        + round($utilities_electricity_PY->DMBTR / 1000) + round($utilities_water_PY->DMBTR / 1000)
                        + round($communication_PY->DMBTR / 1000) + round( $postage_PY->DMBTR / 1000) + round($repairs_total_PY->DMBTR /1000) + round($represent_and_entertainment_PY->DMBTR / 1000) + round($plog_fees_others_PY->DMBTR / 1000) + round( $supplies_PY->DMBTR /1000) + round($subscriptions_PY->DMBTR / 1000);

                    $VARIANCE = $CY_ACTUAL - $BUDGET;
                    $catch_it = null;
                    $check_var = null;
                    try {
                        $check_var = (int)round($VARIANCE / (int)$BUDGET * 100);
                    } catch (\Exception $e) {
                        $catch_it = $e->getMessage();
                    }
                    $var_total_percent = 0;
                    if ($catch_it !== null && $check_var === null) {
                        // not null which means it has an error, so the default value will be 100%
                        //the $check_var will be null in return because it has an error. if the $check_var is not null which means theirs no error and the $catch_it will be null;
                        $var_total_percent = 100;
                    } else {
                        $var_total_percent = (int)round($VARIANCE / (int)$BUDGET * 100);
                    }
                    $VARIANCE_PERCENT = $var_total_percent. "%";

                    $CHANGE = $CY_ACTUAL - $PY_ACTUAL;
                    //CHANGE PERCENT
                    $catch_it_PY = null;
                    $check_var_PY = null;
                    try {
                        $check_var_PY = (int)round($CHANGE / $PY_ACTUAL * 100);
                    } catch (\Exception $e) {
                        $catch_it_PY = $e->getMessage();
                    }
                    $var_total_percent_PY = 0;
                    if ($catch_it_PY !== null && $check_var_PY === null) {
                        // not null which means it has an error, so the default value will be 100%
                        $var_total_percent_PY = 100;
                    } else {
                        $var_total_percent_PY = (int)round($VARIANCE / (int)$BUDGET * 100);
                    }
                    $CHANGE_PERCENT = $var_total_percent_PY."%";

                    $arrOperating[] =
                        [
                            'COMPANY' => $static_budgets->company,
                            'BUDGET' => number_format($static_budgets->total_operating * (int)$currentMonth, 2),
                            'CY_ACTUAL' => number_format($CY_ACTUAL, 2),
                            'PY_ACTUAL' => number_format($PY_ACTUAL, 2),
                            'VARIANCE' => $VARIANCE > 0 ? "(" . number_format(abs($VARIANCE)) . ")" : $VARIANCE == 0 ? number_format(abs($VARIANCE)) : "(" . number_format(abs($VARIANCE)) . ")",
                            'PERCENT_OF_VARIANCE' => $VARIANCE_PERCENT,
                            'CHANGE' => $CHANGE > 0 ? "(" . number_format(abs($CHANGE)) . ")" : $CHANGE == 0 ? number_format(abs($CHANGE)) : "(" . number_format(abs($CHANGE)) . ")",
                            'PERCENT_CHANGE' => $CHANGE_PERCENT,
                        ];
                }
            }
            return response()->json([
                'OPERATING_EXP' => $arrOperating,
                'isAdmin' => true,
            ]);
        } else {
            $company = Company::where('EKORG', '=', $request->company)
                ->orderBy('EKORG', 'asc')
                ->get();
            return response()->json([
                'company' => $company,
                'isAdmin' => false,
            ]);
        }


    }
}
